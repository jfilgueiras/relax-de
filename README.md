# Relax-DE
## _Relax based on Differential Evolution_

Relax-DE is a platform for protein structure refinement. It combines the local search provided by Rosetta Relax main classes and the global search from Differential Evolution, allowing for protein structure optimization. Aditionally, a exploitation app is served, which allows for user, project, and refinement task management, as well as visualization of results.

## Architecture
The main structure of Relax-DE is divided into three cores. The first one is the standalone API that allows protein structure refinement as-is, and helps into the result management. The second and third serve the same purpose, but they are seggregated for architectural scalability. The backend core contains all the bussiness logic, while the frontend one is meant to be the main interface of the exploitation web app. The C4 architecture diagrams can be found in ``docs``.

![Component Diagram for Relax-DE](./docs/contenedores_Relax-DE_2.png)

## Refinement API
The core of Relax-DE relies in the protein structure refinement module. This module contains all the main clases for Differential Evolution (evolutionary computation algorithm) and the main interface classes that adapt the PyRosetta Relax behaviour (divided into Repack and Minimize). The folder ``api`` contains all the relevant files for this purpose, as well as a configuration template that allows for usage (``template-config.ini``). It is suggested not to modify this template as-is, as it serves for the API config generation. Some example configurations and input files are provided in ``configs`` and ``input_files`` folders, respectively.

### Requirements
This module is built with Python3 and its construction requires both Docker and Docker-Compose. The library requirements for this module are:
```
matplotlib==3.3.3
pandas==1.1.4
numpy==1.19.4
seaborn==0.11.0
imageio==2.9.0
pyrosetta==./pyrosetta/<pyrosetta-2022.5+release.b3365a12916-cp39-cp39-linux_x86_64.whl>(local_wheel)
flask
celery
redis
gunicorn
```

### Module deployment

```
cd api
docker-compose build
docker-compose up -D
```

## Exploitation system
As said before, this module has two main components: backend and frontend, built in different ecosystems. They both need each other in order to work together, and they are strongly dependant on the refinement module API.

### Backend
The backend allows for the API communication and acts as a intermediate between the interface and the refinement module. It also allows for project and user management, refinement executions, visualization of results, and download of files.  

#### Requirements
This system is a Maven project, written mainly with the Spring-Boot framework and Java 8. 

- Maven 3.0 or superior
- Java Runtime Environment 8 or superior


#### Module deployment

```
cd backend
mvn install
mvn spring-boot:run -P mysql
```

### Frontend
This module contains all the interface componentes that allow to use the bussiness logic in the backend in a fancy way, as well as clear use cases and visualization tools. 
#### Requirements
This module is built with Yarn and ReactJS, written mainly in JavaScript (under ECMAScript specification).

- Yarn 1.22.19 or superior
- React 16.12.0 or superior

#### Module deployment

```
cd frontend
yarn install --production
yarn run start
```


> Any doubts or inquiries can be sent to juan.filgueiras.rilo@udc.es.