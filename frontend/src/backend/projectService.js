import { config, appFetch } from './appFetch';

export const findProjectById = (projectId, onSuccess, onErrors) =>
    appFetch(`/projects/${projectId}`, config('GET'), onSuccess, onErrors);

export const findUserProjects = ({ page }, onSuccess) =>
    appFetch(`/projects?page=${page}`, config('GET'), onSuccess);

export const addProject = (name, description, accesibility, onSuccess, onErrors) =>
    appFetch(`/projects`, config('POST', {name, description, accesibility}), onSuccess, onErrors);

export const updateProject = (projectId, name, description, accesibility, onSuccess, onErrors) => {
    appFetch(`/projects/${projectId}`, config('PUT', {name, description, accesibility}), onSuccess, onErrors);
}

export const deleteProject = (projectId, onSuccess) => {
    appFetch(`/projects/${projectId}`, config('POST'), onSuccess);}
