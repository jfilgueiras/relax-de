import { config, appFetch } from './appFetch';

export const addProteinToDirectory = (proteinId, protein, onSuccess,
    onErrors) => {
    let formData = new FormData();
    console.log(protein.meta.name);
    formData.append('protein', protein.file);
    console.log(formData);
    appFetch(`/directory/upload-protein?projectId=${proteinId}`,
        config('POST', formData), onSuccess, onErrors);
}
