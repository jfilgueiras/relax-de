import { config, appFetch } from './appFetch';

export const executeRefinement = (projectId, refinementCall, 
    onSuccess, 
    onErrors) => {
    appFetch(`/refinements?projectId=${projectId}`,
        config('POST', refinementCall),
        onSuccess,
        onErrors
    );
}

export const getUserPendingTasks = (onSuccess) =>
    appFetch(`/refinements/userPending`, config('GET'), onSuccess);

export const getProjectPendingTasks = (projectId, onSuccess, onErrors) =>
    appFetch(`/refinements/projectPending?projectId=${projectId}`, config('GET'), onSuccess, onErrors);

export const getUserRefinements = ({projectId, page}, onSuccess, onErrors) =>
    appFetch(`/refinements?projectId=${projectId}&page=${page}`, config('GET'), onSuccess, onErrors);

export const findRefinementById = (callId, onSuccess, onErrors) => {
    appFetch(`/refinements/${callId}`, config('GET'), onSuccess, onErrors);
}

export const downloadRefinementById = (callId, onSuccess, onErrors) => {
    appFetch(`/refinements/download/${callId}`, config('GET'), onSuccess, onErrors);
}

export const getConfigNamesByUser = (onSuccess) => {
    appFetch(`/refinements/configNames`, config('GET'), onSuccess);
}

export const getRefinementConfiguration = (configName, onSuccess, onErrors) => {
    console.log(configName)
    appFetch(`/refinements/refinementConfig?configName=${configName}`, config('GET'), onSuccess, onErrors);
}