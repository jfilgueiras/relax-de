import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import AppBar from '@material-ui/core/AppBar';
import PersonIcon from '@material-ui/icons/Person';

import { Errors } from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'block',
    },
    appBar: {
        position: 'relative',
        backgroundColor: theme.palette.primary.dark
        // background: 'linear-gradient(135deg, rgba(65,0,31,1) 0%, rgba(0,0,0,1) 40%, rgba(0,0,0,1) 60%, rgba(65,0,31,1) 100%)'

    },
    flexList: {
        display: 'inline-flex',
        padding: theme.spacing(1),
        float: 'center',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: 'transparent'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
}));

const UpdateProfile = () => {

    const classes = useStyles();
    const user = useSelector(selectors.getUser);
    const dispatch = useDispatch();
    const history = useHistory();
    const [firstName, setFirstName] = useState(user.firstName);
    const [lastName, setLastName] = useState(user.lastName);
    const [email, setEmail] = useState(user.email);
    const [backendErrors, setBackendErrors] = useState(null);
    let form;

    const handleSubmit = event => {

        event.preventDefault();

        if (form.checkValidity()) {

            dispatch(actions.updateProfile(
                {
                    id: user.id,
                    firstName: firstName.trim(),
                    lastName: lastName.trim(),
                    email: email.trim()
                },
                () => history.push('/'),
                errors => setBackendErrors(errors)));

        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }

    }

    return (
        <Card className={classes.root} variant="outlined">
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <AppBar className={classes.appBar}>

                <Typography variant='h6' className="card-header" color='secondary'>
                    <FormattedMessage id="project.users.UpdateProfile.title" />
                </Typography>
            </AppBar>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <div className="card bg-light border-dark">
                <div className="card-body">
                    <form ref={node => form = node}
                        className="needs-validation" noValidate onSubmit={e => handleSubmit(e)}>
                        <div className="form-group row">                            
                            <label htmlFor="firstName" className="col-md-3 col-form-label">
                            <Typography variant='body1' color='primary'>
                                <FormattedMessage id="project.global.fields.firstName" />
                            </Typography>
                            </label>
                            <div className="col-md-4">
                                <input type="text" id="firstName" className="form-control"
                                    value={firstName}
                                    onChange={e => setFirstName(e.target.value)}
                                    autoFocus
                                    required />
                                <div className="invalid-feedback">
                                    <FormattedMessage id='project.global.validator.required' />
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="lastName" className="col-md-3 col-form-label">
                            <Typography variant='body1' color='primary'>
                                <FormattedMessage id="project.global.fields.lastName" />
                            </Typography>
                            </label>
                            <div className="col-md-4">
                                <input type="text" id="lastName" className="form-control"
                                    value={lastName}
                                    onChange={e => setLastName(e.target.value)}
                                    required />
                                <div className="invalid-feedback">
                                    <FormattedMessage id='project.global.validator.required' />
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="email" className="col-md-3 col-form-label">
                            <Typography variant='body1' color='primary'>
                                <FormattedMessage id="project.global.fields.email" />
                            </Typography>
                            </label>
                            <div className="col-md-4">
                                <input type="email" id="email" className="form-control"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                    required />
                                <div className="invalid-feedback">
                                    <FormattedMessage id='project.global.validator.email' />
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="offset-md-3 col-md-1">
                                <Button type="submit" variant='contained' color='primary'>
                                    <FormattedMessage id="project.global.buttons.save" />
                                </Button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Card>
    );

}

export default UpdateProfile;