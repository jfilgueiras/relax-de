import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import AppBar from '@material-ui/core/AppBar';
import PersonIcon from '@material-ui/icons/Person';

import { Errors } from '../../common';
import * as actions from '../actions';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'block',
    },
    appBar: {
        position: 'relative',
        backgroundColor: theme.palette.primary.dark
        // background: 'linear-gradient(135deg, rgba(65,0,31,1) 0%, rgba(0,0,0,1) 40%, rgba(0,0,0,1) 60%, rgba(65,0,31,1) 100%)'

    },
    flexList: {
        display: 'inline-flex',
        padding: theme.spacing(1),
        float: 'center',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: 'transparent'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
}));

const SignUp = () => {

    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [backendErrors, setBackendErrors] = useState(null);
    const [passwordsDoNotMatch, setPasswordsDoNotMatch] = useState(false);
    let form;
    let confirmPasswordInput;

    const handleSubmit = event => {

        event.preventDefault();

        if (form.checkValidity() && checkConfirmPassword()) {

            dispatch(actions.signUp(
                {
                    userName: userName.trim(),
                    password: password,
                    firstName: firstName.trim(),
                    lastName: lastName.trim(),
                    email: email.trim()
                },
                () => history.push('/'),
                errors => setBackendErrors(errors),
                () => {
                    history.push('/users/login');
                    dispatch(actions.logout());
                }
            ));


        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }

    }

    const checkConfirmPassword = () => {

        if (password !== confirmPassword) {

            confirmPasswordInput.setCustomValidity('error');
            setPasswordsDoNotMatch(true);

            return false;

        } else {
            return true;
        }

    }

    const handleConfirmPasswordChange = value => {

        confirmPasswordInput.setCustomValidity('');
        setConfirmPassword(value);
        setPasswordsDoNotMatch(false);

    }

    return (
        <Card className={classes.root} variant="outlined">
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <AppBar className={classes.appBar}>

                <Typography variant='h6' className="card-header" color='secondary'>
                    <FormattedMessage id="project.users.SignUp.title" />
                </Typography>
            </AppBar>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <div className="card bg-light border-dark">
                <div className="card-body">
                    <form ref={node => form = node}
                        className="needs-validation" noValidate
                        onSubmit={e => handleSubmit(e)}>
                        <div className="form-group row">
                            <label htmlFor="userName" className="col-md-3 col-form-label">
                                <Typography variant='body1' color='primary'>
                                    <FormattedMessage id="project.global.fields.userName" />
                                </Typography>
                            </label>
                            <div className="col-md-4">
                                <input type="text" id="userName" className="form-control"
                                    value={userName}
                                    onChange={e => setUserName(e.target.value)}
                                    autoFocus
                                    required />
                                <div className="invalid-feedback">
                                    <FormattedMessage id='project.global.validator.required' />
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="password" className="col-md-3 col-form-label">
                                <Typography variant='body1' color='primary'>
                                    <FormattedMessage id="project.global.fields.password" />
                                </Typography>
                            </label>
                            <div className="col-md-4">
                                <input type="password" id="password" className="form-control"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                    required />
                                <div className="invalid-feedback">
                                    <FormattedMessage id='project.global.validator.required' />
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="confirmPassword" className="col-md-3 col-form-label">
                                <Typography variant='body1' color='primary'>
                                    <FormattedMessage id="project.users.SignUp.fields.confirmPassword" />
                                </Typography>
                            </label>
                            <div className="col-md-4">
                                <input ref={node => confirmPasswordInput = node}
                                    type="password" id="confirmPassword" className="form-control"
                                    value={confirmPassword}
                                    onChange={e => handleConfirmPasswordChange(e.target.value)}
                                    required />
                                <div className="invalid-feedback">
                                    {passwordsDoNotMatch ?
                                        <FormattedMessage id='project.global.validator.passwordsDoNotMatch' /> :
                                        <FormattedMessage id='project.global.validator.required' />}
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="firstName" className="col-md-3 col-form-label">
                                <Typography variant='body1' color='primary'>
                                    <FormattedMessage id="project.global.fields.firstName" />
                                </Typography>
                            </label>
                            <div className="col-md-4">
                                <input type="text" id="firstName" className="form-control"
                                    value={firstName}
                                    onChange={e => setFirstName(e.target.value)}
                                    required />
                                <div className="invalid-feedback">
                                    <FormattedMessage id='project.global.validator.required' />
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="lastName" className="col-md-3 col-form-label">
                                <Typography variant='body1' color='primary'>
                                    <FormattedMessage id="project.global.fields.lastName" />
                                </Typography>
                            </label>
                            <div className="col-md-4">
                                <input type="text" id="lastName" className="form-control"
                                    value={lastName}
                                    onChange={e => setLastName(e.target.value)}
                                    required />
                                <div className="invalid-feedback">
                                    <FormattedMessage id='project.global.validator.required' />
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="email" className="col-md-3 col-form-label">
                                <Typography variant='body1' color='primary'>
                                    <FormattedMessage id="project.global.fields.email" />
                                </Typography>
                            </label>
                            <div className="col-md-4">
                                <input type="email" id="email" className="form-control"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                    required />
                                <div className="invalid-feedback">
                                    <FormattedMessage id='project.global.validator.email' />
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="offset-md-3 col-md-2">
                                <Button type="submit" variant='contained' color='primary'>
                                    <FormattedMessage id="project.users.SignUp.title" />
                                </Button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Card>
    );

}

export default SignUp;