import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import AppBar from '@material-ui/core/AppBar';
import PersonIcon from '@material-ui/icons/Person';

import { Errors } from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'block',
    },
    appBar: {
        position: 'relative',
        backgroundColor: theme.palette.primary.dark
        // background: 'linear-gradient(135deg, rgba(65,0,31,1) 0%, rgba(0,0,0,1) 40%, rgba(0,0,0,1) 60%, rgba(65,0,31,1) 100%)'

    },
    flexList: {
        display: 'inline-flex',
        padding: theme.spacing(1),
        float: 'center',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: 'transparent'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
}));

const ChangePassword = () => {

    const classes = useStyles();

    const user = useSelector(selectors.getUser);
    const dispatch = useDispatch();
    const history = useHistory();
    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmNewPassword, setConfirmNewPassword] = useState('');
    const [backendErrors, setBackendErrors] = useState(null);
    const [passwordsDoNotMatch, setPasswordsDoNotMatch] = useState(false);
    let form;
    let confirmNewPasswordInput;

    const handleSubmit = event => {

        event.preventDefault();

        if (form.checkValidity() && checkConfirmNewPassword()) {

            dispatch(actions.changePassword(user.id, oldPassword, newPassword,
                () => history.push('/'),
                errors => setBackendErrors(errors)));

        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }

    }

    const checkConfirmNewPassword = () => {

        if (newPassword !== confirmNewPassword) {

            confirmNewPasswordInput.setCustomValidity('error');
            setPasswordsDoNotMatch(true);

            return false;

        } else {
            return true;
        }

    }

    const handleConfirmNewPasswordChange = event => {

        confirmNewPasswordInput.setCustomValidity('');
        setConfirmNewPassword(event.target.value);
        setPasswordsDoNotMatch(false);

    }

    return (
        <Card className={classes.root} variant="outlined">
            <AppBar className={classes.appBar}>
                <Typography variant='h6' className="card-header" color='secondary'>
                <FormattedMessage id="project.users.ChangePassword.title" />
                </Typography>
            </AppBar>
            <div className="card bg-light border-dark">
                <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
                <div className="card-body">
                    <form ref={node => form = node}
                        className="needs-validation" noValidate onSubmit={e => handleSubmit(e)}>
                        <div className="form-group row">
                            <label htmlFor="oldPassword" className="col-md-3 col-form-label">
                                <Typography variant='body1' color='primary'>
                                    <FormattedMessage id="project.users.ChangePassword.fields.oldPassword" />
                                </Typography>
                            </label>
                            <div className="col-md-4">
                                <input type="password" id="oldPassword" className="form-control"
                                    value={oldPassword}
                                    onChange={e => setOldPassword(e.target.value)}
                                    autoFocus
                                    required />
                                <div className="invalid-feedback">
                                    <FormattedMessage id='project.global.validator.required' />
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="newPassword" className="col-md-3 col-form-label">
                                <Typography variant='body1' color='primary'>
                                    <FormattedMessage id="project.users.ChangePassword.fields.newPassword" />
                                </Typography>
                            </label>
                            <div className="col-md-4">
                                <input type="password" id="newPassword" className="form-control"
                                    value={newPassword}
                                    onChange={e => setNewPassword(e.target.value)}
                                    required />
                                <div className="invalid-feedback">
                                    <FormattedMessage id='project.global.validator.required' />
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="confirmNewPassword" className="col-md-3 col-form-label">
                                <Typography variant='body1' color='primary'>
                                    <FormattedMessage id="project.users.ChangePassword.fields.confirmNewPassword" />
                                </Typography>
                            </label>
                            <div className="col-md-4">
                                <input ref={node => confirmNewPasswordInput = node}
                                    type="password" id="confirmNewPassword" className="form-control"
                                    value={confirmNewPassword}
                                    onChange={e => handleConfirmNewPasswordChange(e)}
                                    required />
                                <div className="invalid-feedback">
                                    {passwordsDoNotMatch ?
                                        <FormattedMessage id='project.global.validator.passwordsDoNotMatch' /> :
                                        <FormattedMessage id='project.global.validator.required' />}

                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="offset-md-3 col-md-1">

                                <Button type="submit" variant='contained' color='primary'>
                                    <FormattedMessage id="project.global.buttons.save" />
                                </Button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Card>
    );

}

export default ChangePassword;
