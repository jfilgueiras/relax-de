import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import AppBar from '@material-ui/core/AppBar';
import PersonIcon from '@material-ui/icons/Person';

import { Errors, ListItemLink } from '../../common';
import * as actions from '../actions';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'block',
    },
    appBar: {
        position: 'relative',
        backgroundColor: theme.palette.primary.dark
        // background: 'linear-gradient(135deg, rgba(65,0,31,1) 0%, rgba(0,0,0,1) 40%, rgba(0,0,0,1) 60%, rgba(65,0,31,1) 100%)'

    },
    flexList: {
        display: 'inline-flex',
        padding: theme.spacing(1),
        float: 'center',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: 'transparent'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
}));

const Login = () => {

    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [backendErrors, setBackendErrors] = useState(null);
    let form;

    const handleSubmit = event => {

        event.preventDefault();

        if (form.checkValidity()) {

            dispatch(actions.login(
                userName.trim(),
                password,
                () => history.push('/'),
                errors => setBackendErrors(errors),
                () => {
                    history.push('/users/login');
                    dispatch(actions.logout());
                }
            ));

        } else {
            setBackendErrors(null);
            form.classList.add('was-validated');
        }

    }

    return (
        <Card className={classes.root} variant="outlined">
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <AppBar className={classes.appBar}>

                <Typography variant='h6' className="card-header" color='secondary'>
                    <FormattedMessage id="project.users.Login.title" />
                </Typography>
            </AppBar>

            <div className="card-body">
                <form ref={node => form = node}
                    className="needs-validation" noValidate
                    onSubmit={e => handleSubmit(e)}>
                    <div className="form-group row">
                        <label htmlFor="userName" className="col-md-3 col-form-label">
                            <Typography variant='body1' color='primary'>
                                <FormattedMessage id="project.global.fields.userName" />
                            </Typography>
                        </label>
                        <div className="col-md-4">
                            <input type="text" id="userName" className="form-control"
                                value={userName}
                                onChange={e => setUserName(e.target.value)}
                                autoFocus
                                required />
                            <div className="invalid-feedback">
                                <FormattedMessage id='project.global.validator.required' />
                            </div>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="password" className="col-md-3 col-form-label">
                            <Typography variant='body1' color='primary'>

                                <FormattedMessage id="project.global.fields.password" />
                            </Typography>
                        </label>
                        <div className="col-md-4">
                            <input type="password" id="password" className="form-control"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                                required />
                            <div className="invalid-feedback">
                                <FormattedMessage id='project.global.validator.required' />
                            </div>
                        </div>
                    </div>
                    <div className="form-group row">
                        <div className="offset-md-3 col-md-1">
                            <Button type="submit" variant='contained' color='primary'>
                                <FormattedMessage id="project.users.Login.title" />
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
            <List className={classes.flexList}>
                <ListItem>
                    <FormattedMessage id="project.users.SignUp.fields.notRegisteredYet" />
                </ListItem>
                <ListItemLink to={'/users/signup'} primary="project.users.SignUp.title" icon={<PersonIcon color={'primary'} />} />
            </List>
        </Card>
    );

}

export default Login;