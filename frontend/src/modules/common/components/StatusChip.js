import React from "react";
import PropTypes from 'prop-types';
import Chip from "@material-ui/core/Chip";
import DoneIcon from "@material-ui/icons/Done";
import ErrorIcon from "@material-ui/icons/Error";
import { green, red, orange, indigo, grey } from "@material-ui/core/colors";
import CircularProgress from '@material-ui/core/CircularProgress';

function colorForStatus(status) {
  switch (status) {
    case "SUCCESS":
      return ['#8db94a','white'];
    case "FAILURE":
      return ["#8b0000", 'white'];
    case "EXECUTING":
      return ['#FFBA49', 'white'];
    case "PENDING":
      return ['#6E7DAB', 'white'];
    default:
        return "#CCCCCC"
  }
}

function StatusChip({ status, taskId, handleDeleteFunc, marginLeft }) {
    return (
        <Chip
            label={status}
            avatar={status === "SUCCESS" ? 
                    <DoneIcon style={{ color: "white" }}/> :
                    status === "FAILURE" ? 
                    <ErrorIcon style={{ color: "white" }}/> :  
                    <CircularProgress size={20} variant="indeterminate" style={{ color: colorForStatus(status)[1] }}/>}
            style={{ marginLeft: marginLeft, float:"inherit", backgroundColor: colorForStatus(status)[0], color: colorForStatus(status)[1], width: "125px" }}
            onDelete={(status === "SUCCESS" || status === "FAILURE") && handleDeleteFunc ? () => handleDeleteFunc(taskId) : undefined}
        />
    );
}

StatusChip.propTypes = {
    status: PropTypes.string.isRequired,
    taskId: PropTypes.string.isRequired,
};

export default StatusChip;
