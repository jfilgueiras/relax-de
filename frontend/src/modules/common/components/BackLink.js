import React from 'react';
import {useHistory} from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    button: {
      marginBottom: theme.spacing(1),
    },
  }));

const BackLink = ({times}) => {
    const classes = useStyles();

    const history = useHistory();
    if (history.length < times) {
        return null;
    } 
    return (
        <Button className={classes.button} 
            size='large'
            variant="contained"
            color="secondary" 
            onClick={times ? () => history.go(-times) : () => history.goBack()} 
            startIcon={<ChevronLeftIcon fontSize="large" color={"primary"}/> }>
            <Typography color='primary' variant='button'><FormattedMessage id="project.global.buttons.back" /></Typography>
        </Button>

    );

};
// { <FormattedMessage id='project.global.buttons.back'/> }

export default BackLink;
