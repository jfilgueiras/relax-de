import React from 'react';
import PropTypes from 'prop-types';

const Button = ({args, onClick, icon}) => {

    return (
        <button type="button" className="delete" data-dismiss="alert" aria-label="Delete" 
            onClick={() => onClick(args)}>
            {icon}
        </button>
    );

}

Button.propTypes = {
    args: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    onClick: PropTypes.func.isRequired
};

export default Button;
