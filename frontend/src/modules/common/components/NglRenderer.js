import React, { useMemo, useState, useCallback } from 'react';
import { FormattedMessage } from 'react-intl';
import { Stage, Component } from 'react-ngl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
    flexOption: {
        flexGrow: 1,
        marginLeft: '10%',
        height: '5%',
        width: 'auto',
    },
}));

const NglRenderer = ({ native, result }) => {
    const reprLists = useMemo(() => ({
        'ball+stick': [{
            type: 'ball+stick'
        }],
        cartoon: [{
            type: 'cartoon',
            param: {
                color: 'atomindex'
            }
        }],
        'ribbon and line': [{
            type: 'ribbon',
            param: {
                color: 'atomindex'
            }
        }, {
            type: 'line',
            param: {
                color: 'element'
            }
        }],
        spacefill: [{
            type: 'spacefill',
            param: {
                color: 'element'
            }
        }],
        surface: [{
            type: 'surface',
            param: {
                color: 'element'
            }
        }]
    }), []);

    const nativeblob = new Blob([native], { type: 'text/plain' });
    const resultblob = new Blob([result], { type: 'text/plain' });
    const nativefile = new File([nativeblob], "native.pdb", { type: 'text/plain' });
    const resultfile = new File([resultblob], "result.pdb", { type: 'text/plain' });
    const proteins = useMemo(() => ({
        native: nativefile,
        result: resultfile
    }));
    const [reprName, setReprName] = useState('cartoon');
    const [protein, setProtein] = useState('native');
    const handleReprChange = useCallback(event => setReprName(event.target.value), []);
    const classes = useStyles();

    return (<>
        <Stage width="100%" height="85%">
            <Component path={proteins[protein]} reprList={reprLists[reprName]} />
        </Stage>
        <Box style={{ marginTop: '1%', textAlign: 'center' }}>

            <Box style={{ display: 'inline-block' }}>
                <Typography variant='button'><FormattedMessage id='project.display' /></Typography>
                <Select
                    className={classes.flexOption}
                    style={{ marginRight: '5%' }}
                    // label={
                    //     <Typography variant='subtitle2'>
                    //       <FormattedMessage id="project.global.fields.cr"/>
                    //     </Typography>
                    // }
                    value={reprName}
                    onChange={
                        (event) => {
                            handleReprChange(event);
                        }
                    }
                >
                    {Object.keys(reprLists).map(name =>
                        <MenuItem key={name} value={name}>
                            {name}
                        </MenuItem>)
                    }
                </Select>
            </Box>
            <Box style={{ display: 'inline-block', marginLeft: '20%' }}>

                <Typography variant='button'><FormattedMessage id='project.structure' /></Typography>

                <Select
                    className={classes.flexOption}
                    // label={
                    //     <Typography variant='subtitle2'>
                    //       <FormattedMessage id="project.global.fields.cr"/>
                    //     </Typography>
                    // }
                    value={protein}
                    onChange={
                        (event) => {
                            setProtein(event.target.value);
                        }
                    }
                >
                    <MenuItem value={"native"}>
                        <FormattedMessage id="project.refinements.GetRefinementDetail.native" />
                    </MenuItem>
                    <MenuItem value={"result"}>
                        <FormattedMessage id="project.refinements.GetRefinementDetail.result" />
                    </MenuItem>
                </Select>
            </Box>
        </Box>
    </>
    )
};

export default NglRenderer;