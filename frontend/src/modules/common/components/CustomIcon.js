import React from 'react'
import { Icon } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    iconWhite: {
        // Filter SVG: https://stackoverflow.com/questions/22252472/how-to-change-the-color-of-an-svg-element
        // https://codepen.io/sosuke/pen/Pjoqqp
        filter: "invert(98%) sepia(14%) saturate(184%) hue-rotate(49deg) brightness(116%) contrast(100%)"
    },
}));


export const CustomIcon = ({icon}) => {
    const classes = useStyles();

    return <Icon>
        <img style={{"verticalAlign": "super"}} src={icon} className={classes.iconWhite} height={"100%"} width={"100%"}/>
    </Icon>
}