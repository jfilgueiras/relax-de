import React, { useEffect } from "react";
import './deps/dependencies';

import * as icn3d from 'icn3d/icn3d.module'
import "icn3d/css/icn3d.css"

function Icn3dViewer({native, result, width}) {
    const a = `ATOM      1  N   ARG A   1      -6.266  -5.597  -3.441  1.00  0.00           N  
ATOM      2  CA  ARG A   1      -6.000  -5.651  -2.273  1.00  0.00           C  
ATOM      3  C   ARG A   1      -4.960  -6.501  -2.322  1.00  0.00           C  
ATOM      4  O   ARG A   1      -4.936  -7.577  -2.753  1.00  0.00           O  
ATOM      5  CB  ARG A   1      -7.161  -6.163  -1.432  1.00  0.00           C  
ATOM      6  CG  ARG A   1      -8.402  -5.285  -1.454  1.00  0.00           C  
ATOM      7  CD  ARG A   1      -9.483  -5.842  -0.602  1.00  0.00           C  
ATOM      8  NE  ARG A   1     -10.665  -4.995  -0.603  1.00  0.00           N  
ATOM      9  CZ  ARG A   1     -11.802  -5.267   0.068  1.00  0.00           C  
ATOM     10  NH1 ARG A   1     -11.894  -6.363   0.787  1.00  0.00           N  
ATOM     11  NH2 ARG A   1     -12.823  -4.431   0.003  1.00  0.00           N  
ATOM     12 1H   ARG A   1      -7.045  -4.985  -3.578  1.00  0.00           H  
ATOM     13 2H   ARG A   1      -5.477  -5.250  -3.947  1.00  0.00           H  
ATOM     14 3H   ARG A   1      -6.499  -6.511  -3.773  1.00  0.00           H  
ATOM     15  HA  ARG A   1      -5.530  -4.707  -2.091  1.00  0.00           H  
ATOM     16 1HB  ARG A   1      -7.451  -7.153  -1.779  1.00  0.00           H  
ATOM     17 2HB  ARG A   1      -6.843  -6.260  -0.394  1.00  0.00           H  
ATOM     18 1HG  ARG A   1      -8.151  -4.291  -1.083  1.00  0.00           H  
ATOM     19 2HG  ARG A   1      -8.775  -5.208  -2.476  1.00  0.00           H  
ATOM     20 1HD  ARG A   1      -9.769  -6.826  -0.973  1.00  0.00           H  
ATOM     21 2HD  ARG A   1      -9.129  -5.931   0.424  1.00  0.00           H  
ATOM     22  HE  ARG A   1     -10.632  -4.142  -1.146  1.00  0.00           H  
ATOM     23 1HH1 ARG A   1     -11.113  -7.002   0.837  1.00  0.00           H  
ATOM     24 2HH1 ARG A   1     -12.746  -6.567   1.290  1.00  0.00           H  
ATOM     25 1HH2 ARG A   1     -12.752  -3.588  -0.551  1.00  0.00           H  
ATOM     26 2HH2 ARG A   1     -13.675  -4.635   0.505  1.00  0.00           H  
ATOM     27  N   GLY A   2      -4.835  -5.943  -1.289  1.00  0.00           N  
ATOM     28  CA  GLY A   2      -3.892  -6.331  -0.627  1.00  0.00           C  
ATOM     29  C   GLY A   2      -3.055  -5.458  -0.348  1.00  0.00           C  
ATOM     30  O   GLY A   2      -3.013  -5.749   0.792  1.00  0.00           O  
ATOM     31  H   GLY A   2      -5.457  -5.209  -0.983  1.00  0.00           H  
ATOM     32 1HA  GLY A   2      -4.273  -6.732   0.292  1.00  0.00           H  
ATOM     33 2HA  GLY A   2      -3.395  -7.107  -1.177  1.00  0.00           H  
ATOM     34  N   LYS A   3      -3.041  -4.412  -1.084  1.00  0.00           N  
ATOM     35  CA  LYS A   3      -2.084  -3.489  -0.591  1.00  0.00           C  
ATOM     36  C   LYS A   3      -2.721  -2.141  -0.602  1.00  0.00           C  
ATOM     37  O   LYS A   3      -3.545  -1.834  -1.454  1.00  0.00           O  
ATOM     38  CB  LYS A   3      -0.806  -3.505  -1.431  1.00  0.00           C  
ATOM     39  CG  LYS A   3      -0.092  -4.850  -1.465  1.00  0.00           C  
ATOM     40  CD  LYS A   3       1.236  -4.752  -2.199  1.00  0.00           C  
ATOM     41  CE  LYS A   3       1.944  -6.099  -2.247  1.00  0.00           C  
ATOM     42  NZ  LYS A   3       3.317  -5.986  -2.810  1.00  0.00           N  
ATOM     43  H   LYS A   3      -3.624  -4.265  -1.895  1.00  0.00           H  
ATOM     44  HA  LYS A   3      -1.767  -3.756   0.396  1.00  0.00           H  
ATOM     45 1HB  LYS A   3      -1.042  -3.226  -2.458  1.00  0.00           H  
ATOM     46 2HB  LYS A   3      -0.106  -2.765  -1.043  1.00  0.00           H  
ATOM     47 1HG  LYS A   3       0.090  -5.191  -0.445  1.00  0.00           H  
ATOM     48 2HG  LYS A   3      -0.722  -5.584  -1.967  1.00  0.00           H  
ATOM     49 1HD  LYS A   3       1.064  -4.405  -3.219  1.00  0.00           H  
ATOM     50 2HD  LYS A   3       1.880  -4.033  -1.693  1.00  0.00           H  
ATOM     51 1HE  LYS A   3       2.009  -6.511  -1.242  1.00  0.00           H  
ATOM     52 2HE  LYS A   3       1.369  -6.790  -2.863  1.00  0.00           H  
ATOM     53 1HZ  LYS A   3       3.751  -6.898  -2.825  1.00  0.00           H  
ATOM     54 2HZ  LYS A   3       3.267  -5.621  -3.751  1.00  0.00           H  
ATOM     55 3HZ  LYS A   3       3.866  -5.362  -2.236  1.00  0.00           H  
ATOM     56  N   TRP A   4      -2.353  -1.387   0.399  1.00  0.00           N  
ATOM     57  CA  TRP A   4      -2.842  -0.014   0.506  1.00  0.00           C  
ATOM     58  C   TRP A   4      -1.685   0.897   0.769  1.00  0.00           C  
ATOM     59  O   TRP A   4      -0.742   0.557   1.449  1.00  0.00           O  
ATOM     60  CB  TRP A   4      -3.874   0.126   1.627  1.00  0.00           C  
ATOM     61  CG  TRP A   4      -5.101  -0.710   1.418  1.00  0.00           C  
ATOM     62  CD1 TRP A   4      -6.279  -0.304   0.867  1.00  0.00           C  
ATOM     63  CD2 TRP A   4      -5.277  -2.106   1.759  1.00  0.00           C  
ATOM     64  NE1 TRP A   4      -7.173  -1.346   0.842  1.00  0.00           N  
ATOM     65  CE2 TRP A   4      -6.576  -2.457   1.382  1.00  0.00           C  
ATOM     66  CE3 TRP A   4      -4.448  -3.072   2.343  1.00  0.00           C  
ATOM     67  CZ2 TRP A   4      -7.074  -3.736   1.571  1.00  0.00           C  
ATOM     68  CZ3 TRP A   4      -4.948  -4.355   2.532  1.00  0.00           C  
ATOM     69  CH2 TRP A   4      -6.227  -4.678   2.155  1.00  0.00           C  
ATOM     70  H   TRP A   4      -1.727  -1.754   1.102  1.00  0.00           H  
ATOM     71  HA  TRP A   4      -3.262   0.258  -0.440  1.00  0.00           H  
ATOM     72 1HB  TRP A   4      -3.421  -0.161   2.576  1.00  0.00           H  
ATOM     73 2HB  TRP A   4      -4.180   1.168   1.712  1.00  0.00           H  
ATOM     74  HD1 TRP A   4      -6.482   0.700   0.500  1.00  0.00           H  
ATOM     75  HE1 TRP A   4      -8.116  -1.303   0.484  1.00  0.00           H  
ATOM     76  HE3 TRP A   4      -3.431  -2.821   2.642  1.00  0.00           H  
ATOM     77  HZ2 TRP A   4      -8.088  -4.012   1.279  1.00  0.00           H  
ATOM     78  HZ3 TRP A   4      -4.296  -5.101   2.989  1.00  0.00           H  
ATOM     79  HH2 TRP A   4      -6.586  -5.694   2.317  1.00  0.00           H  
ATOM     80  N   THR A   5      -1.818   2.091   0.275  1.00  0.00           N  
ATOM     81  CA  THR A   5      -0.765   3.097   0.460  1.00  0.00           C  
ATOM     82  C   THR A   5      -1.393   4.307   1.097  1.00  0.00           C  
ATOM     83  O   THR A   5      -2.443   4.797   0.685  1.00  0.00           O  
ATOM     84  CB  THR A   5      -0.086   3.489  -0.866  1.00  0.00           C  
ATOM     85  OG1 THR A   5       0.516   2.331  -1.458  1.00  0.00           O  
ATOM     86  CG2 THR A   5       0.982   4.545  -0.626  1.00  0.00           C  
ATOM     87  H   THR A   5      -2.654   2.330  -0.240  1.00  0.00           H  
ATOM     88  HA  THR A   5       0.002   2.707   1.097  1.00  0.00           H  
ATOM     89  HB  THR A   5      -0.833   3.885  -1.554  1.00  0.00           H  
ATOM     90  HG1 THR A   5       1.465   2.355  -1.313  1.00  0.00           H  
ATOM     91 1HG2 THR A   5       1.450   4.810  -1.574  1.00  0.00           H  
ATOM     92 2HG2 THR A   5       0.525   5.431  -0.187  1.00  0.00           H  
ATOM     93 3HG2 THR A   5       1.737   4.151   0.053  1.00  0.00           H  
ATOM     94  N   TYR A   6      -0.725   4.741   2.140  1.00  0.00           N  
ATOM     95  CA  TYR A   6      -1.154   5.960   2.785  1.00  0.00           C  
ATOM     96  C   TYR A   6       0.004   6.905   2.959  1.00  0.00           C  
ATOM     97  O   TYR A   6       0.965   6.590   3.661  1.00  0.00           O  
ATOM     98  CB  TYR A   6      -1.802   5.657   4.138  1.00  0.00           C  
ATOM     99  CG  TYR A   6      -2.992   4.728   4.051  1.00  0.00           C  
ATOM    100  CD1 TYR A   6      -2.816   3.359   4.197  1.00  0.00           C  
ATOM    101  CD2 TYR A   6      -4.259   5.243   3.824  1.00  0.00           C  
ATOM    102  CE1 TYR A   6      -3.903   2.510   4.117  1.00  0.00           C  
ATOM    103  CE2 TYR A   6      -5.346   4.394   3.744  1.00  0.00           C  
ATOM    104  CZ  TYR A   6      -5.171   3.033   3.890  1.00  0.00           C  
ATOM    105  OH  TYR A   6      -6.253   2.188   3.810  1.00  0.00           O  
ATOM    106  H   TYR A   6       0.076   4.232   2.486  1.00  0.00           H  
ATOM    107  HA  TYR A   6      -1.934   6.427   2.219  1.00  0.00           H  
ATOM    108 1HB  TYR A   6      -1.064   5.204   4.802  1.00  0.00           H  
ATOM    109 2HB  TYR A   6      -2.130   6.587   4.601  1.00  0.00           H  
ATOM    110  HD1 TYR A   6      -1.819   2.954   4.374  1.00  0.00           H  
ATOM    111  HD2 TYR A   6      -4.398   6.318   3.709  1.00  0.00           H  
ATOM    112  HE1 TYR A   6      -3.764   1.436   4.231  1.00  0.00           H  
ATOM    113  HE2 TYR A   6      -6.343   4.799   3.566  1.00  0.00           H  
ATOM    114  HH  TYR A   6      -5.992   1.307   4.091  1.00  0.00           H  
ATOM    115  N   ASN A   7      -0.204   8.073   2.375  1.00  0.00           N  
ATOM    116  CA  ASN A   7       0.784   9.137   2.470  1.00  0.00           C  
ATOM    117  C   ASN A   7       2.143   8.642   1.996  1.00  0.00           C  
ATOM    118  O   ASN A   7       3.175   8.992   2.555  1.00  0.00           O  
ATOM    119  CB  ASN A   7       0.868   9.669   3.889  1.00  0.00           C  
ATOM    120  CG  ASN A   7      -0.422  10.286   4.352  1.00  0.00           C  
ATOM    121  OD1 ASN A   7      -1.111  10.966   3.583  1.00  0.00           O  
ATOM    122  ND2 ASN A   7      -0.763  10.062   5.596  1.00  0.00           N  
ATOM    123  H   ASN A   7      -1.057   8.228   1.857  1.00  0.00           H  
ATOM    124  HA  ASN A   7       0.475   9.985   1.896  1.00  0.00           H  
ATOM    125 1HB  ASN A   7       1.134   8.857   4.567  1.00  0.00           H  
ATOM    126 2HB  ASN A   7       1.658  10.418   3.951  1.00  0.00           H  
ATOM    127 1HD2 ASN A   7      -1.612  10.447   5.959  1.00  0.00           H  
ATOM    128 2HD2 ASN A   7      -0.176   9.506   6.183  1.00  0.00           H  
ATOM    129  N   GLY A   8       2.083   7.796   0.969  1.00  0.00           N  
ATOM    130  CA  GLY A   8       3.297   7.349   0.285  1.00  0.00           C  
ATOM    131  C   GLY A   8       3.820   6.064   0.912  1.00  0.00           C  
ATOM    132  O   GLY A   8       4.676   5.399   0.344  1.00  0.00           O  
ATOM    133  H   GLY A   8       1.185   7.458   0.656  1.00  0.00           H  
ATOM    134 1HA  GLY A   8       3.075   7.173  -0.749  1.00  0.00           H  
ATOM    135 2HA  GLY A   8       4.048   8.112   0.365  1.00  0.00           H  
ATOM    136  N   ILE A   9       3.262   5.709   2.054  1.00  0.00           N  
ATOM    137  CA  ILE A   9       3.733   4.506   2.748  1.00  0.00           C  
ATOM    138  C   ILE A   9       2.816   3.356   2.391  1.00  0.00           C  
ATOM    139  O   ILE A   9       1.616   3.367   2.668  1.00  0.00           O  
ATOM    140  CB  ILE A   9       3.763   4.699   4.275  1.00  0.00           C  
ATOM    141  CG1 ILE A   9       4.699   5.851   4.649  1.00  0.00           C  
ATOM    142  CG2 ILE A   9       4.193   3.414   4.965  1.00  0.00           C  
ATOM    143  CD1 ILE A   9       4.608   6.265   6.100  1.00  0.00           C  
ATOM    144  H   ILE A   9       2.516   6.263   2.448  1.00  0.00           H  
ATOM    145  HA  ILE A   9       4.745   4.283   2.483  1.00  0.00           H  
ATOM    146  HB  ILE A   9       2.769   4.974   4.626  1.00  0.00           H  
ATOM    147 1HG1 ILE A   9       5.729   5.565   4.440  1.00  0.00           H  
ATOM    148 2HG1 ILE A   9       4.470   6.721   4.033  1.00  0.00           H  
ATOM    149 1HG2 ILE A   9       4.209   3.568   6.044  1.00  0.00           H  
ATOM    150 2HG2 ILE A   9       3.489   2.618   4.723  1.00  0.00           H  
ATOM    151 3HG2 ILE A   9       5.189   3.133   4.623  1.00  0.00           H  
ATOM    152 1HD1 ILE A   9       5.300   7.086   6.289  1.00  0.00           H  
ATOM    153 2HD1 ILE A   9       3.591   6.589   6.323  1.00  0.00           H  
ATOM    154 3HD1 ILE A   9       4.867   5.419   6.736  1.00  0.00           H  
ATOM    155  N   THR A  10       3.447   2.371   1.797  1.00  0.00           N  
ATOM    156  CA  THR A  10       2.730   1.177   1.351  1.00  0.00           C  
ATOM    157  C   THR A  10       2.716   0.149   2.463  1.00  0.00           C  
ATOM    158  O   THR A  10       3.743  -0.202   3.030  1.00  0.00           O  
ATOM    159  CB  THR A  10       3.363   0.570   0.086  1.00  0.00           C  
ATOM    160  OG1 THR A  10       3.271   1.508  -0.995  1.00  0.00           O  
ATOM    161  CG2 THR A  10       2.650  -0.716  -0.303  1.00  0.00           C  
ATOM    162  H   THR A  10       4.444   2.438   1.645  1.00  0.00           H  
ATOM    163  HA  THR A  10       1.720   1.450   1.127  1.00  0.00           H  
ATOM    164  HB  THR A  10       4.415   0.354   0.273  1.00  0.00           H  
ATOM    165  HG1 THR A  10       2.354   1.595  -1.264  1.00  0.00           H  
ATOM    166 1HG2 THR A  10       3.111  -1.131  -1.199  1.00  0.00           H  
ATOM    167 2HG2 THR A  10       2.728  -1.436   0.512  1.00  0.00           H  
ATOM    168 3HG2 THR A  10       1.600  -0.505  -0.500  1.00  0.00           H  
ATOM    169  N   TYR A  11       1.530  -0.333   2.732  1.00  0.00           N  
ATOM    170  CA  TYR A  11       1.341  -1.369   3.732  1.00  0.00           C  
ATOM    171  C   TYR A  11       0.945  -2.667   3.095  1.00  0.00           C  
ATOM    172  O   TYR A  11      -0.149  -2.845   2.573  1.00  0.00           O  
ATOM    173  CB  TYR A  11       0.289  -0.947   4.760  1.00  0.00           C  
ATOM    174  CG  TYR A  11       0.682   0.267   5.573  1.00  0.00           C  
ATOM    175  CD1 TYR A  11       0.450   1.540   5.074  1.00  0.00           C  
ATOM    176  CD2 TYR A  11       1.274   0.106   6.817  1.00  0.00           C  
ATOM    177  CE1 TYR A  11       0.809   2.648   5.817  1.00  0.00           C  
ATOM    178  CE2 TYR A  11       1.633   1.214   7.559  1.00  0.00           C  
ATOM    179  CZ  TYR A  11       1.403   2.481   7.063  1.00  0.00           C  
ATOM    180  OH  TYR A  11       1.760   3.585   7.803  1.00  0.00           O  
ATOM    181  H   TYR A  11       0.728   0.024   2.233  1.00  0.00           H  
ATOM    182  HA  TYR A  11       2.217  -1.487   4.337  1.00  0.00           H  
ATOM    183 1HB  TYR A  11      -0.650  -0.725   4.250  1.00  0.00           H  
ATOM    184 2HB  TYR A  11       0.101  -1.771   5.448  1.00  0.00           H  
ATOM    185  HD1 TYR A  11      -0.016   1.667   4.097  1.00  0.00           H  
ATOM    186  HD2 TYR A  11       1.457  -0.895   7.208  1.00  0.00           H  
ATOM    187  HE1 TYR A  11       0.627   3.649   5.425  1.00  0.00           H  
ATOM    188  HE2 TYR A  11       2.100   1.087   8.536  1.00  0.00           H  
ATOM    189  HH  TYR A  11       1.575   4.380   7.298  1.00  0.00           H  
ATOM    190  N   GLU A  12       1.855  -3.593   3.161  1.00  0.00           N  
ATOM    191  CA  GLU A  12       1.602  -4.826   2.437  1.00  0.00           C  
ATOM    192  C   GLU A  12       0.894  -5.847   3.278  1.00  0.00           C  
ATOM    193  O   GLU A  12       1.433  -6.807   3.841  1.00  0.00           O  
ATOM    194  CB  GLU A  12       2.915  -5.417   1.920  1.00  0.00           C  
ATOM    195  CG  GLU A  12       3.665  -4.524   0.942  1.00  0.00           C  
ATOM    196  CD  GLU A  12       4.793  -5.235   0.248  1.00  0.00           C  
ATOM    197  OE1 GLU A  12       5.376  -6.109   0.844  1.00  0.00           O  
ATOM    198  OE2 GLU A  12       5.074  -4.904  -0.880  1.00  0.00           O  
ATOM    199  H   GLU A  12       2.703  -3.461   3.694  1.00  0.00           H  
ATOM    200  HA  GLU A  12       1.131  -4.665   1.490  1.00  0.00           H  
ATOM    201 1HB  GLU A  12       3.578  -5.622   2.761  1.00  0.00           H  
ATOM    202 2HB  GLU A  12       2.715  -6.366   1.422  1.00  0.00           H  
ATOM    203 1HG  GLU A  12       2.965  -4.158   0.191  1.00  0.00           H  
ATOM    204 2HG  GLU A  12       4.060  -3.664   1.480  1.00  0.00           H  
ATOM    205  N   GLY A  13      -0.388  -5.556   3.323  1.00  0.00           N  
ATOM    206  CA  GLY A  13      -1.321  -6.366   4.099  1.00  0.00           C  
ATOM    207  C   GLY A  13      -1.282  -7.785   3.608  1.00  0.00           C  
ATOM    208  O   GLY A  13      -1.110  -8.768   4.318  1.00  0.00           O  
ATOM    209  H   GLY A  13      -0.733  -4.757   2.811  1.00  0.00           H  
ATOM    210 1HA  GLY A  13      -1.041  -6.339   5.135  1.00  0.00           H  
ATOM    211 2HA  GLY A  13      -2.314  -5.976   3.987  1.00  0.00           H  
ATOM    212  N   ARG A  14      -1.573  -7.847   2.364  1.00  0.00           N  
ATOM    213  CA  ARG A  14      -1.506  -9.157   1.846  1.00  0.00           C  
ATOM    214  C   ARG A  14      -0.390  -9.252   0.896  1.00  0.00           C  
ATOM    215  O   ARG A  14      -0.036 -10.360   0.519  1.00  0.00           O  
ATOM    216  OXT ARG A  14      -0.089  -8.172   0.427  1.00  0.00           O  
ATOM    217  CB  ARG A  14      -2.804  -9.540   1.151  1.00  0.00           C  
ATOM    218  CG  ARG A  14      -4.026  -9.577   2.056  1.00  0.00           C  
ATOM    219  CD  ARG A  14      -5.253  -9.949   1.306  1.00  0.00           C  
ATOM    220  NE  ARG A  14      -6.422  -9.999   2.169  1.00  0.00           N  
ATOM    221  CZ  ARG A  14      -7.654 -10.369   1.769  1.00  0.00           C  
ATOM    222  NH1 ARG A  14      -7.861 -10.717   0.518  1.00  0.00           N  
ATOM    223  NH2 ARG A  14      -8.654 -10.382   2.633  1.00  0.00           N  
ATOM    224  H   ARG A  14      -1.825  -7.041   1.810  1.00  0.00           H  
ATOM    225  HA  ARG A  14      -1.533  -9.932   2.583  1.00  0.00           H  
ATOM    226 1HB  ARG A  14      -3.009  -8.834   0.349  1.00  0.00           H  
ATOM    227 2HB  ARG A  14      -2.698 -10.527   0.700  1.00  0.00           H  
ATOM    228 1HG  ARG A  14      -3.872 -10.311   2.847  1.00  0.00           H  
ATOM    229 2HG  ARG A  14      -4.180  -8.592   2.499  1.00  0.00           H  
ATOM    230 1HD  ARG A  14      -5.439  -9.213   0.524  1.00  0.00           H  
ATOM    231 2HD  ARG A  14      -5.120 -10.931   0.855  1.00  0.00           H  
ATOM    232  HE  ARG A  14      -6.302  -9.738   3.139  1.00  0.00           H  
ATOM    233 1HH1 ARG A  14      -7.097 -10.708  -0.142  1.00  0.00           H  
ATOM    234 2HH1 ARG A  14      -8.785 -10.995   0.218  1.00  0.00           H  
ATOM    235 1HH2 ARG A  14      -8.494 -10.114   3.594  1.00  0.00           H  
ATOM    236 2HH2 ARG A  14      -9.577 -10.659   2.333  1.00  0.00           H  
TER                                                                             
`
    useEffect(() => {
        const cfg = {
            divid: 'viewer',
            mobilemenu: true,
            width: width/1.56,
            showcommand: false,
            resize: true,
            showtitle: false,
            command: "style sidec lines"
        };
        const icn3dui = new icn3d.iCn3DUI(cfg);
        const ic = new icn3d.iCn3D(icn3dui);
        // ic.InputfileData = result
        icn3dui.show3DStructure(a)
        console.log(native);
        // ic.setOptionCls.setStyle('protein', 'lines')
        // parser.loadPdbData(native);
        // icn3dui.pdbParserCls.downloadUrl('https://files.rcsb.org/view/1gpk.pdb');
        
    }, []);
    return <div id="viewer"></div>;
}

export default Icn3dViewer;