import React from 'react';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  drawerLink: {
      "&:hover, &:focus":{
          color: theme.palette.secondary.dark            
      },
  },
}));


function ListItemLink(props) {
  const { icon, primary, to } = props;
  const classes = useStyles();

  const renderLink = React.useMemo(
    () => React.forwardRef((itemProps, ref) => <RouterLink to={to} ref={ref} {...itemProps} />),
    [to],
  );

  return (
    <li>
      <ListItem className={classes.drawerLink} button component={renderLink}>
        {icon ? <ListItemIcon style={{minWidth: 35 +"px"}}>{icon}</ListItemIcon> : null}
        <Typography variant='body2'><FormattedMessage id={primary} /></Typography>
      </ListItem>
    </li>
  );
}

ListItemLink.propTypes = {
  icon: PropTypes.element,
  primary: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
};

export default ListItemLink