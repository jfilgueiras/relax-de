import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import AppGlobalComponents from './AppGlobalComponents';
import Home from './Home';
import { AddProjectForm, ProjectDetails, FindUserProjects, FindUserProjectsResult, ProjectAdded } from '../../projects';
import { Login, SignUp, UpdateProfile, ChangePassword, Logout } from '../../users';
import users from '../../users';
import Container from '@material-ui/core/Container';
import Toolbar from '@material-ui/core/Toolbar';
import { RefinementCreated, FindRefinementCalls, FindRefinementCallsResult, ExecuteRefinementForm, RefinementDetails } from '../../refinements';

const useStyles = makeStyles((theme) => ({
    root: {
      height:'100%'
    },
  }));

const Body = () => {
    const classes = useStyles();
    const loggedIn = useSelector(users.selectors.isLoggedIn);
    return (
        <React.Fragment>
            <Toolbar/>
            <Container className={classes.root}>
                <br />
                <AppGlobalComponents />
                <Switch>
                    <Route exact path="/"><Home /></Route>
                    {loggedIn && <Route exact path="/users/update-profile"><UpdateProfile /></Route>}
                    {loggedIn && <Route exact path="/users/change-password"><ChangePassword /></Route>}
                    {loggedIn && <Route exact path="/users/logout"><Logout /></Route>}
                    {!loggedIn && <Route exact path="/users/login"><Login /></Route>}
                    {!loggedIn && <Route exact path="/users/signup"><SignUp /></Route>}
                    {loggedIn && <Route exact path="/projects/add-project"><AddProjectForm /></Route>}
                    {loggedIn && <Route exact path="/projects/project-added"><ProjectAdded /></Route>}
                    {loggedIn && <Route exact path="/projects/find-user-projects"><FindUserProjects/></Route>}
                    {loggedIn && <Route exact path="/projects/find-user-projects-result"><FindUserProjectsResult /></Route>}
                    {loggedIn && <Route exact path="/projects/:id"><ProjectDetails /></Route>}
                    {loggedIn && <Route exact path="/refinements/execute-refinement"><ExecuteRefinementForm /></Route>}
                    {loggedIn && <Route exact path="/refinements/refinement-created"><RefinementCreated /></Route>}
                    {loggedIn && <Route exact path="/refinements/find-refinement-calls"><FindRefinementCalls /></Route>}
                    {loggedIn && <Route exact path="/refinements/find-refinement-calls-result"><FindRefinementCallsResult /></Route>}
                    {loggedIn && <Route exact path="/refinements/:id"><RefinementDetails /></Route>}
                    <Route><Home /></Route>
                </Switch>
            </Container>
            <Toolbar/>
        </React.Fragment>
    );

};

export default Body;
