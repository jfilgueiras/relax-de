import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import clsx from 'clsx';
import { Link as RouterLink } from 'react-router-dom';
import Link from '@material-ui/core/Link';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import users from '../../users';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SvgIcon from '@material-ui/core/SvgIcon';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import FolderSharedIcon from '@material-ui/icons/FolderShared';
import IconButton from '@material-ui/core/IconButton';
import LockIcon from '@material-ui/icons/Lock';
import PersonIcon from '@material-ui/icons/Person';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { ListItemLink } from '../../common';
import { PendingTasksDropdown } from '../../refinements';
import {AddEditProjectForm} from '../../projects';


const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        //   backgroundColor: 'rgb(10,10,10)',
        background: 'rgb(255,255,255);',

        // background: 'linear-gradient(135deg, rgba(101,45,84,1) 0%, rgba(0,0,0,1) 10%, rgba(0,0,0,1) 90%, rgba(101,45,84,1) 100%)'
        // background: 'linear-gradient(135deg, rgba(70,15,42,1) 0%, rgba(0,0,0,1) 40%, rgba(0,0,0,1) 60%, rgba(70,15,42,1) 100%)'
        background: 'linear-gradient(135deg, rgba(65,0,31,1) 0%, rgba(0,0,0,1) 40%, rgba(0,0,0,1) 60%, rgba(65,0,31,1) 100%)'
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginRight: drawerWidth,
    },
    title: {
        flexGrow: 1,
        marginLeft: '20px',
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-start',
    },
    home: {
        filter: "",
        "&:hover, &:focus": {
            filter: 'invert(48%) sepia(46%) saturate(322%) hue-rotate(187deg) brightness(94%) contrast(88%)'
        },
    },
    drawerLink: {
        "&:hover, &:focus": {
            color: theme.palette.secondary.dark
        },
    },
}));

function HomeIcon(props) {
    return (
        <SvgIcon {...props}>
            <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
        </SvgIcon>
    );
}

const Header = () => {
    const classes = useStyles();
    const theme = useTheme();
    const userName = useSelector(users.selectors.getUserName);
    const [open, setOpen] = React.useState(false);

    const [addProjectDialogOpen, setAddProjectDialogOpen] = useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const toggleDrawer = (open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setOpen(open);
    };

    const handleDialogClose = () => {
        setAddProjectDialogOpen(false);
    };

    const OptionList = () => (
        <div
            role="presentation"
            onClick={toggleDrawer(false)}
            onKeyDown={toggleDrawer(false)}>
            <div className={classes.drawerHeader}>
                <IconButton onClick={toggleDrawer(false)}>
                    {theme.direction === 'rtl' ? <ChevronLeftIcon color={"primary"} /> : <ChevronRightIcon color={"primary"} />}
                </IconButton>
            </div>
            <Divider />
            <List>
                <ListItem button className={classes.drawerLink} onClick={() => {
                    setAddProjectDialogOpen(true);
                }}>
                    <ListItemIcon style={{ minWidth: '35px' }}> <CreateNewFolderIcon color={'primary'} /> </ListItemIcon>
                    <Typography variant='body2'><FormattedMessage id="project.app.Header.AddProject" /></Typography>
                </ListItem>
                <AddEditProjectForm open={addProjectDialogOpen} handleDialogClose={handleDialogClose} />
                {/* <ListItemLink to="/projects/add-project" primary="project.app.Header.AddProject" icon={<CreateNewFolderIcon color={'primary'}/>} /> */}
                <ListItemLink button to="/projects/find-user-projects" primary="project.app.Header.FindProject" icon={<FolderSharedIcon color={'primary'} />} />
            </List>
            <Divider />
            <List>
                <ListItemLink to="/users/update-profile" primary="project.users.UpdateProfile.title" icon={<BorderColorIcon color={'primary'} />} />
                <ListItemLink to="/users/change-password" primary="project.users.ChangePassword.title" icon={<LockIcon color={'primary'} />} />
            </List>
            <Divider />
            <List>
                <ListItemLink to="/users/logout" primary="project.app.Header.logout" icon={<ExitToAppIcon color={'primary'} />} />
            </List>
        </div>
    );

    return (
        <div className={classes.root}>
            <CssBaseline />

            <AppBar
                position="fixed"
                className={clsx(classes.appBar, { [classes.appBarShift]: open, })}>
                <Toolbar>
                    <Link component={RouterLink} to="/">
                        <HomeIcon className={classes.home} style={{ color: 'white' }} fontSize="large" />
                    </Link>
                    <Typography variant="h6" noWrap className={classes.title}>RELAX-DE</Typography>
                    {userName ?
                        <React.Fragment>
                            <PendingTasksDropdown />
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                edge="end"
                                onClick={handleDrawerOpen}
                                className={clsx(open && classes.hide)}
                            >
                                <MenuIcon />
                            </IconButton>
                        </React.Fragment>
                        :
                        <List>
                            <ListItemLink to="/users/login" primary="project.users.Login.title" icon={<PersonIcon style={{ color: 'white' }} />} />
                        </List>
                    }

                </Toolbar>
            </AppBar>
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="right"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}
                onClose={toggleDrawer(false)}
            >
                {OptionList("right")}
            </Drawer>
        </div>

    );

};

export default Header;
