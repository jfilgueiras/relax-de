import React from 'react';
import { FormattedMessage } from 'react-intl';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import CardMedia from '@material-ui/core/CardMedia';
import Card from '@material-ui/core/Card';

const useStyles = makeStyles((theme) => ({
    root: {
        justifyContent: 'left',
        flexWrap: 'wrap',
        listStyle: 'none',
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(4),
        paddingLeft: theme.spacing(5),
        paddingRight: theme.spacing(5),
        width: '100%',
        marginTop: theme.spacing(10),
        backgroundColor: 'rgb(255,255,255,0.97)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    typography: {
        // backgroundColor: theme.palette.background.paper,
        paddingBottom: theme.spacing(5),
        backgroundColor: 'transparent',
        textAlign:'center'
    },
    media: {
        height: 0,
        paddingTop: '50.25%', // 16:9
        backgroundSize: '100%'
    },
    mediab: {
        height: 0,
        paddingTop: '30.25%', // 16:9
        backgroundSize: '100%'
    },
    mediac: {
        height: 0,
        paddingTop: '75.25%', // 16:9
        backgroundSize: '100%'
    },
    centered: {
        textAlign:'center'
    }
}));

const Home = () => {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Card className={classes.root} variant="outlined">
                <Typography className={classes.typography} variant='h2'>
                    <FormattedMessage id="project.app.Home.welcome" />
                </Typography>
                <Typography paragraph >
                    <FormattedMessage id='project.app.Home.welcomeText'/>
                </Typography>
                <CardMedia
                    className={classes.media}
                    image={require('../../../images/collage_N.png')}
                    title="Proteína"
                />
            </Card>
            <Card className={classes.root} variant="outlined">
                <Typography className={classes.typography} variant='h3'>
                    <FormattedMessage id="project.app.Home.Relax" />
                </Typography>
                <Typography paragraph>
                    <FormattedMessage id='project.app.Home.RelaxText'/>
                </Typography>
                <CardMedia
                    className={classes.mediab}
                    image={require('../../../images/coarse_grained_representation.png')}
                    title="Proteína"
                />
            </Card>
            <Card className={classes.root} variant="outlined">
                <Typography className={classes.typography} variant='h3'>
                    <FormattedMessage id="project.app.Home.DifferentialEvolution" />
                </Typography>
                <Typography paragraph>
                    <FormattedMessage id='project.app.Home.DEText'/>
                </Typography>
                <CardMedia
                    className={classes.mediac}
                    image={require('../../../images/de.png')}
                    title="Proteína"
                />
            </Card>
        </React.Fragment>
    )
};

export default Home;
