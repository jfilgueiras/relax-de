import React from 'react';
import {FormattedMessage} from 'react-intl';
import {useSelector, useDispatch} from 'react-redux';

import {ErrorDialog, Loader} from '../../common';
import { makeStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import * as actions from '../actions';
import * as selectors from '../selectors';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    }
}));

const ConnectedErrorDialog = () => {

    const error = useSelector(selectors.getError);
    const dispatch = useDispatch();

    return <ErrorDialog error={error} 
                onClose={() => dispatch(actions.error(null))}/>

};

const ConnectedStatusBar = () => {
    const toast = useSelector(selectors.getToast);
    const dispatch = useDispatch();
    
    const handleToastClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        dispatch(actions.toastClosed());
    };

    return (
        <Snackbar open={toast.open} autoHideDuration={6000} onClose={handleToastClose}>
            <Alert style={{color:'black'}} onClose={handleToastClose} severity={toast.severity}>
                {toast.id && <FormattedMessage id={toast.id}/>}
            </Alert>
        </Snackbar>
    );
}
const ConnectedLoader = () => {

    const loading = useSelector(selectors.isLoading);
    const classes = useStyles();
    return (
        <Backdrop
            className= {classes.backdrop}
            open={loading}>
            <CircularProgress color="inherit"/>
        </Backdrop>
    );
    // return <Loader loading={loading}/>

};

const AppGlobalComponents = () => (

    <div>
        <ConnectedErrorDialog/>
        <ConnectedLoader/>
        <ConnectedStatusBar/>
    </div>

);

export default AppGlobalComponents;