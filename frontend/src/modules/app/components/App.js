import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';

import Header from './Header';
import Body from './Body';
import Footer from './Footer';
import users from '../../users';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

const theme = createMuiTheme({
    typography: {
        // Use the system font.
        fontFamily:
            '-apple-system,system-ui,BlinkMacSystemFont,' +
            '"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
        button: {
            fontWeight: 700,
        }
    },
    palette: {
        primary: {
            main: '#652d54',
            dark: '#3f1934',
            light: '#c36e6a',
            button: '#c36e6a',
            
        },
        secondary: {
            // Mostaza
            // main: '#FFBA49',
            // Lila clarito
            main: '#E3D8F1',
            // Azulón liláceo
            // dark: '#6E7DAB',
            // Oliva clarito
            // main: '#D4E4bc',
            // dark: '#6E7DAB'
        },
        success: {
            main: '#D4E4bc',
        },
        warning: {
            main: '#FFBA49',
        }
    },
})

const App = () => {

    const dispatch = useDispatch();

    useEffect(() => {

        dispatch(users.actions.tryLoginFromServiceToken(
            () => dispatch(users.actions.logout())));

    });

    return (
        <ThemeProvider theme={theme}>
            <div className="body">
                <Router>
                    <div>
                        <Header/>
                        <Body/>
                    </div>
                </Router>
                <Footer/>
            </div>
        </ThemeProvider>
    );

}
    
export default App;