export const CREATE_REFINEMENT_COMPLETED = "project/refinements/createRefinementCompleted";
// export const CLEAR_PENDING_TASKS = "project/refinements/clearPendingTasks";
export const GET_PENDING_TASKS_COMPLETED = "project/refinements/getPendingTasksCompleted";
export const DELETE_COMPLETED_TASK = "project/refinements/deleteCompletedTask";
export const FIND_REFINEMENT_CALLS_COMPLETED = "project/refinements/findRefinementCallsCompleted";
export const CLEAR_REFINEMENT_CALL_SEARCH = "project/refinements/clearRefinementCallSearch";
export const FIND_REFINEMENT_BY_ID_COMPLETED = "project/refinements/findRefinementByIdCompleted";
export const CLEAR_REFINEMENT = "project/refinements/clearRefinement";