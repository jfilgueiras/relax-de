import {combineReducers} from 'redux';

import * as actionTypes from './actionTypes';

const initialState = {
    pendingTasks: [],
    refinementCallSearch: null,
    refinement: null,
};

const pendingTasks = (state = initialState.pendingTasks, action) => {

    switch (action.type) {

        case actionTypes.GET_PENDING_TASKS_COMPLETED:
            let actionTaskIds = new Set(action.pendingTasks.map(d => d.taskId));
            return [...action.pendingTasks, ...state.filter(task => !(actionTaskIds.has(task.taskId)))]

        case actionTypes.DELETE_COMPLETED_TASK:
            return state.filter((task) => task.taskId !== action.taskId);

        // case actionTypes.CLEAR_PENDING_TASKS:
        //     return initialState.pendingTasks;

        default:
            return state;

    }

}

const refinementCallSearch = (state = initialState.refinementCallSearch, action) => {

    switch (action.type) {

        case actionTypes.FIND_REFINEMENT_CALLS_COMPLETED:
            return action.refinementCallSearch;

        case actionTypes.CLEAR_REFINEMENT_CALL_SEARCH:
            return initialState.refinementCallSearch;

        default:
            return state;

    }

}

const refinement = (state = initialState.refinement, action) => {

    switch (action.type) {

        case actionTypes.FIND_REFINEMENT_BY_ID_COMPLETED:
            return action.refinement;

        case actionTypes.CLEAR_REFINEMENT:
            return initialState.refinement;

        default:
            return state;

    }

}
const reducer = combineReducers({
    pendingTasks: pendingTasks,
    refinementCallSearch: refinementCallSearch,
    refinement: refinement,
});

export default reducer;


