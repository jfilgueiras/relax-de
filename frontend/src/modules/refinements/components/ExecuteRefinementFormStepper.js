import React, { useState, useEffect, useLocation } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { FormattedMessage } from 'react-intl';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import SettingsIcon from '@material-ui/icons/Settings';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import VideoLabelIcon from '@material-ui/icons/VideoLabel';
import StepConnector from '@material-ui/core/StepConnector';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { CustomIcon } from '../../common/components/CustomIcon';
import SaveIcon from '@material-ui/icons/Save';
import Protein from '../../../images/icons8-protein-50.svg';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Divider from '@material-ui/core/Divider';
import Slider from '@material-ui/core/Slider';
import Switch from '@material-ui/core/Switch';
import { Errors } from '../../common';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import AssistantIcon from '@material-ui/icons/Assistant';

import * as actions from '../actions';
import * as global_actions from '../../app/actions';

const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(65,0,31,1) 50%,rgb(138,35,135) 100%)',
    },
  },
  completed: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(65,0,31,1) 50%,rgb(138,35,135) 100%)',
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: '#eaeaf0',
    borderRadius: 1,
  },
})(StepConnector);

const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: '#ccc',
    zIndex: 1,
    color: '#fff',
    width: 50,
    height: 50,
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    //background: 'linear-gradient(135deg, rgba(65,0,31,1) 0%, rgba(0,0,0,1) 40%, rgba(0,0,0,1) 60%, rgba(65,0,31,1) 100%)'

    backgroundImage:
      'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(65,0,31,1) 50%, rgb(138,35,135) 100%)',
    boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
  },
  completed: {
    backgroundImage:
      'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(65,0,31,1) 50%, rgb(138,35,135) 100%)',
  },
});

function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles();
  const { active, completed } = props;

  const icons = {
    1: <AssistantIcon />,
    2: <CustomIcon icon={Protein} />,
    3: <SettingsIcon />,
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {icons[String(props.icon)]}
    </div>
  );
}

ColorlibStepIcon.propTypes = {
  /**
   * Whether this step is active.
   */
  active: PropTypes.bool,
  /**
   * Mark the step as completed. Is passed to child components.
   */
  completed: PropTypes.bool,
  /**
   * The label displayed in the step icon.
   */
  icon: PropTypes.node,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    marginRight: theme.spacing(1),
  },
  form: {
    minHeight: '28rem',
    height: 'auto',
    width: '60%',
    textAlign: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(20),
    padding: theme.spacing(2)
  },
  paper: {
    margin: theme.spacing(0),
    padding: theme.spacing(0),
    textAlign: 'center',
    // background: 'linear-gradient(135deg, rgba(65,0,31,1) 0%, rgba(0,0,0,1) 40%, rgba(0,0,0,1) 60%, rgba(65,0,31,1) 100%)'
    //color: theme.palette.text.primary,
  },
  titlePaper: {
    padding: theme.spacing(0.5),
    textAlign: 'center',
    backgroundColor: 'lightgrey',
    color: theme.palette.text.primary,
  },
  value: {
    color: "#28a745"
  },
  option: {
    fontSize: 15,
    '& > span': {
      marginRight: 10,
      fontSize: 18,
    },
  },
}));

function getSteps() {
  return ['project.refinements.ExecuteRefinement.DifferentialEvolutionParamsShort', 'project.refinements.ExecuteRefinement.RelaxParamsShort', 'project.refinements.ExecuteRefinement.ExecConfigParamsShort'];
}

export default function ExecuteRefinementFormStepper({ handleCloseFun, setToast }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [activeStep, setActiveStep] = useState(0);
  const steps = getSteps();

  const [preserveConfig, setPreserveConfig] = useState(false);
  const [configName, setConfigName] = useState("");
  const [configNameList, setConfigNameList] = useState([]);
  const [presetConfig, setPresetConfig] = useState(null);

  const [disturbDegrees, setDisturbDegrees] = useState(0);
  // DE
  const [scheme, setScheme] = useState('BEST');
  const [popsize, setPopsize] = useState(100);
  const [f, setF] = useState(0.01);
  const [cr, setCr] = useState(0.95);
  const [gens, setGens] = useState(100);
  const [tag, setTag] = useState('');
  const [logStats, setLogStats] = useState('False');
  // Relax
  const [populExecutorChain, setPopulExecutorChain] = useState('');
  const [trialExecutorChain, setTrialExecutorChain] = useState('');
  const [executeEach, setExecuteEach] = useState(1);
  const [executionsPerGen, setExecutionsPerGen] = useState(4);
  const [popToRepackmin, setPopToRepackmin] = useState(100);
  const [repackFaRepScale, setRepackFaRepScale] = useState('');
  const [minFaRepScale, setMinFaRepScale] = useState('');
  const [checkExecutionImprovement, setCheckExecutionImprovement] = useState('True');
  const [aaProportionRepmin, setAaProportionRepmin] = useState(1);
  const [sortWorstEterms, setSortWorstEterms] = useState('False');
  // Exec
  const [timeLimit, setTimeLimit] = useState("");

  const [backendErrors, setBackendErrors] = useState(null);
  const { id } = useParams();
  const projectId = Number(id);
  let form;

  useEffect(() => {
    actions.getConfigNamesByUser((configList) => { console.log(configList); setConfigNameList(configList) });
  }, []);

  const handleSubmit = event => {
    event.preventDefault();
    if (form.checkValidity()) {
      dispatch(global_actions.loading());
      const refinementCall =
      {
        configName,
        disturbDegrees,
        de: {
          scheme,
          popsize,
          f,
          cr,
          gens,
          tag,
          logStats
        },
        relax: {
          populExecutorChain,
          trialExecutorChain,
          executeEach,
          executionsPerGen,
          popToRepackmin,
          repackFaRepScale,
          minFaRepScale,
          checkExecutionImprovement,
          aaProportionRepmin,
          sortWorstEterms
        },
        exec: {
          timeLimit
        }
      }
      dispatch(actions.executeRefinement(projectId, refinementCall,
        () => {
          handleCloseFun();
          dispatch(global_actions.toastOpen('project.projects.ProjectDetails.pendingCall', 'success'));
          dispatch(global_actions.loaded());
        }, //onSuccess
        errors => {
          dispatch(global_actions.toastOpen('project.projects.ProjectDetails.pendingCallError', 'error'));
          dispatch(global_actions.loaded());
          setBackendErrors(errors)
        }
      ));

    } else {
      setBackendErrors(null);
      form.classList.add('was-validated');
    }

  }

  const handleNext = (event) => {
    event.preventDefault();
    if (form.checkValidity()) {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    } else {
      form.classList.add('was-validated');
    }

  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleBlur = (value, setValue) => {
    if (value < 0) {
      setValue(0);
    } else if (value > 100) {
      setValue(100);
    }
  };

  const fillPresetConfig = (presetRefinenement) => {
    setDisturbDegrees(presetRefinenement.disturbDegrees);
    setScheme(presetRefinenement.scheme);
    setPopsize(presetRefinenement.popsize);
    setF(presetRefinenement.f);
    setCr(presetRefinenement.cr);
    setGens(presetRefinenement.gens);
    setTag(presetRefinenement.tag);

    setLogStats(presetRefinenement.logStats);
    setPopulExecutorChain(presetRefinenement.populExecutorChain);
    setTrialExecutorChain(presetRefinenement.trialExecutorChain);
    setExecuteEach(presetRefinenement.executeEach);
    setExecutionsPerGen(presetRefinenement.executionsPerGen);
    setPopToRepackmin(presetRefinenement.popToRepackmin);
    setRepackFaRepScale(presetRefinenement.repackFaRepScale);
    setMinFaRepScale(presetRefinenement.minFaRepScale);
    setCheckExecutionImprovement(presetRefinenement.checkExecutionImprovement);
    setAaProportionRepmin(presetRefinenement.aaProportionRepmin);
    setSortWorstEterms(presetRefinenement.sortWorstEterms);
    // setTimeLimit(presetRefinenement.timeLimit);
    dispatch(global_actions.toastOpen('project.projects.ExecuteRefinementForm.getrefinementConfig', 'success'));
  }

  function DeConfigForm() {
    return (
      <>
        <Paper variant='outlined' className={classes.titlePaper}>
          <Typography variant="h6" gutterBottom>
            <FormattedMessage id="project.refinements.ExecuteRefinement.DifferentialEvolutionParams" />
          </Typography>
        </Paper>
        <br />
        <Grid style={{ marginTop: '50' }} container spacing={3}>
          <Grid item xs={12}>
            <FormControlLabel
              value={
                <Typography variant='subtitle2'>
                  <FormattedMessage id="project.global.fields.scheme" />
                </Typography>}
              control={
                <Select style={{ marginInline: '2%' }}
                  value={scheme}
                  className="form-control"
                  onChange={(event) => {
                    setScheme(event.target.value);
                  }
                  }
                >
                  <MenuItem value={"BEST"}>BEST</MenuItem>
                  <MenuItem value={"RANDOM"}>RANDOM</MenuItem>
                </Select>
              }
              label={
                <Typography variant='subtitle2'>
                  <FormattedMessage id="project.global.fields.scheme"
                    values={{
                      '1': <sub>1</sub>
                    }}
                  />
                </Typography>
              }
              labelPlacement="start"
            />
          </Grid>
          <Grid item xs={12} sm={12}>
            <TextField
              required
              value={disturbDegrees}
              onChange={e => setDisturbDegrees(Number(e.target.value))}
              type="number"
              InputProps={{ inputProps: { min: 0, max: 180, step: 0.01 } }}
              variant='outlined'
              id="disturbDegrees"
              name="disturbDegrees"
              helperText="[0-180]"
              label={
                <Typography variant='subtitle2'>
                  <FormattedMessage id="project.global.fields.disturbDegrees" />
                </Typography>
              }
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              value={popsize}
              onChange={e => setPopsize(Number(e.target.value))}
              InputProps={{ inputProps: { min: 0 } }}
              type="number"
              variant='outlined'
              id="popsize"
              name="popsize"
              label={
                <Typography variant='subtitle2'>
                  <FormattedMessage id="project.global.fields.popsize" />
                </Typography>}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              value={gens}
              onChange={e => setGens(Number(e.target.value))}
              type="number"
              InputProps={{ inputProps: { min: 0, step: 1 } }}
              variant='outlined'
              id="gens"
              name="gens"
              label={
                <Typography variant='subtitle2'>
                  <FormattedMessage id="project.global.fields.gens" />
                </Typography>
              }
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              value={f}
              onChange={e => setF(Number(e.target.value))}
              type="number"
              InputProps={{ inputProps: { min: 0, max: 1, step: 0.01 } }}
              variant='outlined'
              id="f"
              name="f"
              helperText="[0-1]"
              label={
                <Typography variant='subtitle2'>
                  <FormattedMessage id="project.global.fields.cr" />
                </Typography>
              }
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              value={cr}
              onChange={e => setCr(Number(e.target.value))}
              type="number"
              InputProps={{ inputProps: { min: 0, max: 1, step: 0.01 } }}
              variant='outlined'
              id="f"
              name="f"
              helperText="[0-1]"
              label={
                <Typography variant='subtitle2'>
                  <FormattedMessage id="project.global.fields.f" />
                </Typography>
              }
              fullWidth
            />
          </Grid>
        </Grid>
      </>
    );
  }

  function RelaxConfigForm() {
    return (
      <>
        <Paper variant='outlined' className={classes.titlePaper}>
          <Typography variant="h6" gutterBottom>
            <FormattedMessage id="project.refinements.ExecuteRefinement.RelaxParams" />
          </Typography>
        </Paper>
        <br />
        <Grid style={{ marginTop: '50' }} container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              value={executeEach}
              onChange={e => setExecuteEach(Number(e.target.value))}
              InputProps={{ inputProps: { min: 0 } }}
              type="number"
              variant='outlined'
              id="executeEach"
              name="executeEach"
              label={
                <Typography variant='subtitle2'>
                  <FormattedMessage id="project.global.fields.executeEach" />
                </Typography>
              }
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              value={executionsPerGen}
              onChange={e => setExecutionsPerGen(Number(e.target.value))}
              InputProps={{ inputProps: { min: 0 } }}
              type="number"
              variant='outlined'
              id="executionsPerGen"
              name="executionsPerGen"
              label={
                <Typography variant='subtitle2'>
                  <FormattedMessage id="project.global.fields.executionsPerGen" />
                </Typography>
              }
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={10}>
            <Typography variant='subtitle2'>
              <FormattedMessage id="project.global.fields.popToRepackmin" />
            </Typography>
            <Slider
              value={typeof popToRepackmin === 'number' ? popToRepackmin : 0}
              onChange={(e, value) => setPopToRepackmin(value)}
              aria-labelledby="input-slider"
            />
          </Grid>
          <Grid item xs={12} sm={2}>
            <Input
              value={popToRepackmin}
              onChange={e => setPopToRepackmin(e.target.value === '' ? '' : Number(e.target.value))}
              margin="dense"
              onBlur={handleBlur(popToRepackmin, setPopToRepackmin)}
              inputProps={{
                step: 1,
                min: 0,
                max: 100,
                type: 'number',
                'aria-labelledby': 'input-slider',
              }}
            />
          </Grid>
        </Grid>
      </>
    );

  }

  function ExecConfigForm() {

    return (
      <>
        <Paper variant='outlined' className={classes.titlePaper}>
          <Typography variant="h6" gutterBottom>
            <FormattedMessage id="project.refinements.ExecuteRefinement.ExecConfigParams" />
          </Typography>
        </Paper>
        <br />
        <Grid style={{ marginTop: '50' }} container spacing={3}>
          <Grid item xs={12} sm={6}>
            <FormControlLabel
              value={<FormattedMessage id="project.global.fields.logStats" />}
              control={<Checkbox
                checked={logStats === 'True' ? true : false}
                onChange={e => setLogStats(e.target.checked ? 'True' : 'False')}
                inputProps={{ 'aria-label': 'primary checkbox' }}
              />}
              label={
                <Typography variant='subtitle2'>
                  <FormattedMessage id="project.global.fields.logStats" />
                </Typography>
              }
              labelPlacement="start"
            />
          </Grid>
          <Grid item xs={12} sm={6} style={{ 'alignSelf': 'center' }}>
            <FormControlLabel style={{ 'alignSelf': 'center' }}
              control={
                <Switch
                  checked={preserveConfig}
                  onChange={e => { setPreserveConfig(e.target.checked); if (e.target.checked) setConfigName("") }}
                  name="preserveConfig"
                  color="primary"
                />
              }
              label={
                <Typography variant='subtitle2'>
                  <FormattedMessage id="project.global.fields.preserveConfig" />
                </Typography>
              }
            />
          </Grid>
          <Grid item xs={12} sm={12}>
            <TextField
              hidden={!preserveConfig}
              value={configName}
              onChange={e => setConfigName(e.target.value)}
              type="text"
              variant='outlined'
              id="configName"
              name="configName"
              label={
                <Typography variant='subtitle2'>
                  <FormattedMessage id="project.global.fields.configName" />
                </Typography>
              }
              fullWidth
            />
          </Grid>
        </Grid>
      </>
    )
  }

  function ReviewParamsForm() {

    return (
      <>
        <Paper variant='outlined' className={classes.titlePaper}>
          <Typography variant="h6" gutterBottom>
            <FormattedMessage id="project.refinements.ExecuteRefinement.ReviewParams" />
          </Typography>
        </Paper>
        <br />
        <Grid style={{ marginTop: '50' }} container spacing={2}>
          <Grid item xs={12} sm={12}>
            <Paper variant='outlined' className={classes.paper}>
              <Typography variant='button'>
                <FormattedMessage id="project.global.fields.scheme"
                  values={{
                    '1': <sub>1</sub>
                  }}
                />
                :&nbsp;
                <Typography className={classes.value} variant='button'>{scheme}</Typography>
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper variant='outlined' className={classes.paper}>
              <Typography variant='button'>
                <FormattedMessage id="project.global.fields.popsize" />
                :&nbsp;
                <Typography className={classes.value} variant='button'>{popsize}</Typography>
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper variant='outlined' className={classes.paper}>
              <Typography variant='button'>
                <FormattedMessage id="project.global.fields.gens" />
                :&nbsp;
                <Typography className={classes.value} variant='button'>{gens}</Typography>
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper variant='outlined' className={classes.paper}>
              <Typography variant='button'>
                <FormattedMessage id="project.global.fields.f" />
                :&nbsp;
                <Typography className={classes.value} variant='button'>{f}</Typography>
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper variant='outlined' className={classes.paper}>
              <Typography variant='button'>
                <FormattedMessage id="project.global.fields.cr" />
                :&nbsp;
                <Typography className={classes.value} variant='button'>{cr}</Typography>
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12}>
            <Paper variant='outlined' className={classes.paper}>
              <Typography variant='button'>
                <FormattedMessage id="project.global.fields.executeEach" />
                :&nbsp;
                <Typography className={classes.value} variant='button'>{executeEach}</Typography>
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12}>
            <Paper variant='outlined' className={classes.paper}>
              <Typography variant='button'>
                <FormattedMessage id="project.global.fields.executionsPerGen" />
                :&nbsp;
                <Typography className={classes.value} variant='button'>{executionsPerGen}</Typography>
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12}>
            <Paper variant='outlined' className={classes.paper}>
              <Typography variant='button'>
                <FormattedMessage id="project.global.fields.popToRepackmin" />
                :&nbsp;
                <Typography className={classes.value} variant='button'>{popToRepackmin}</Typography>
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12}>
            <Paper variant='outlined' className={classes.paper}>
              <Typography variant='button'>
                <FormattedMessage id="project.global.fields.disturbDegrees" />
                :&nbsp;
                <Typography className={classes.value} variant='button'>{disturbDegrees}</Typography>
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12}>
            <Paper variant='outlined' className={classes.paper}>
              <Typography variant='button'>
                <FormattedMessage id="project.global.fields.preserveConfig" />
                {
                  <Checkbox
                    size='small'
                    disabled
                    checked={preserveConfig}
                  />
                }
                <Typography variant='button' hidden={!preserveConfig}>
                  <Typography className={classes.value} variant='button'>{configName}</Typography>
                </Typography>
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      </>
    )
  }

  function getStepFormPart(step) {
    switch (step) {
      case 0:
        return DeConfigForm();
      case 1:
        return RelaxConfigForm();
      case 2:
        return ExecConfigForm();
      case 3:
        return ReviewParamsForm();
      default:
        return 'Unknown step';
    }
  }

  return (
    <div className={classes.root}>
      <Stepper alternativeLabel activeStep={activeStep} connector={<ColorlibConnector />}>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel StepIconComponent={ColorlibStepIcon}>{
              <FormattedMessage id={label} />
            }</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        <Errors errors={backendErrors}
          onClose={() => setBackendErrors(null)} />
        <div style={{width: '60%', margin:'auto'}}>
          <Grid container variant="outlined" style={{ display: 'flex', width: '60%', marginInline: 'auto' }}>
            <Grid item xs={8} sm={8}>
              <Autocomplete
                id="use-prev-config"
                value={presetConfig}
                onChange={(_, val) => {
                  setPresetConfig(val);
                }}
                style={{ width: 300, marginInline: 'auto' }}
                options={configNameList}
                classes={{
                  option: classes.option,
                }}
                autoHighlight
                getOptionLabel={(option) => option}
                // renderOption={(option) => (
                //   <React.Fragment>
                //     <span>{countryToFlag(option.code)}</span>
                //     {option.label} ({option.code}) +{option.phone}
                //   </React.Fragment>
                // )}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label={
                      <FormattedMessage id="project.global.fields.usePrevConfig" />
                    }
                    variant="outlined"
                    inputProps={{
                      ...params.inputProps,
                      autoComplete: 'new-password', // disable autocomplete and autofill
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item style={{ margin: 'auto' }} xs={4} sm={4}>

              <ListItem style={{ justifyContent: 'center', justifyContent: 'flex-start' }} hidden={!presetConfig} button onClick={() => {
                presetConfig && actions.getRefinementConfiguration(presetConfig, fillPresetConfig);
              }}>
                <ListItemIcon>{<CheckCircleIcon color={'primary'} />}</ListItemIcon>
                <FormattedMessage id={'project.global.buttons.apply'} />
              </ListItem>
            </Grid>
          </Grid>
          <Card variant="outlined" className={classes.form}>
            <form ref={node => form = node} noValidate autoComplete="off">{getStepFormPart(activeStep)}</form>
          </Card>
          <div style={{ textAlign: 'center' }}>
            <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
              <FormattedMessage id='project.global.buttons.back'/>
            </Button>
            <Button
              type='submit'
              variant="contained"
              color="primary"
              onClick={activeStep === steps.length ? handleSubmit : handleNext}
              className={classes.button}
            >
              <FormattedMessage id={activeStep === steps.length ?
                'project.global.buttons.submit'
                : activeStep === steps.length - 1 ?
                  'project.global.buttons.review' :
                  'project.global.buttons.next'} />
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
