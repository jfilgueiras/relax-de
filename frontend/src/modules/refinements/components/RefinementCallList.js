import React from 'react';
import { FormattedMessage, FormattedDate, FormattedTime } from 'react-intl';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import RefinementCallLink from './RefinementCallLink';
import Checkbox from '@material-ui/core/Checkbox';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import Grid from '@material-ui/core/Grid';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import StatusChip from '../../common/components/StatusChip';

const StyledTableCell = withStyles((theme) => ({
    head: {
        width: 'auto',
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },

    },
}))(TableRow);

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 700,
    },
    date: {
        width: 220
    },
    little: {
        width: 120
    },
    typographyAccordion: {
        ...theme.typography.button,
        fontSize: 12,
        color: theme.palette.primary.dark,
        paddingLeft: theme.spacing(1),
    },
    root: {
        width: '100%',
      },
      button: {
        marginRight: theme.spacing(1),
      },
      form: {
        height: 'auto',
        width: '60%',
        textAlign: 'center',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        padding: theme.spacing(2)
      },
      paper: {
        margin: theme.spacing(0),
        padding: theme.spacing(0),
        textAlign: 'center',
        // background: 'linear-gradient(135deg, rgba(65,0,31,1) 0%, rgba(0,0,0,1) 40%, rgba(0,0,0,1) 60%, rgba(65,0,31,1) 100%)'
        //color: theme.palette.text.primary,
      },
      titlePaper: {
        padding: theme.spacing(0.5),
        textAlign: 'center',
        backgroundColor: 'lightgrey',
        color: theme.palette.text.primary,
      },
      value: {
        color: "#28a745"
      },
      option: {
        fontSize: 15,
        '& > span': {
          marginRight: 10,
          fontSize: 18,
        },
      },
      mainGrid: {
        background: theme.palette.secondary.main
      },
      itemGrid: {
        background: theme.palette.secondary.main
      }
}));

function ReviewParamsForm({refinementCall, classes}) {
    console.log(refinementCall)
    return (
        <>
            <Grid className={classes.mainGrid} style={{ marginTop: '50' }} container spacing={2}>
                <Grid item xs={12} sm={12}>
                    <Paper variant='elevation' className={classes.paper}>
                        <Typography variant='button'>
                            <FormattedMessage id="project.global.fields.scheme"
                                values={{
                                    '1': <sub>1</sub>
                                }}
                            />
                            :&nbsp;
                            <Typography className={classes.value} variant='button'>{refinementCall.scheme}</Typography>
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Paper variant='elevation' className={classes.paper}>
                        <Typography variant='button'>
                            <FormattedMessage id="project.global.fields.popsize" />
                            :&nbsp;
                            <Typography className={classes.value} variant='button'>{refinementCall.popsize}</Typography>
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Paper variant='elevation' className={classes.paper}>
                        <Typography variant='button'>
                            <FormattedMessage id="project.global.fields.gens" />
                            :&nbsp;
                            <Typography className={classes.value} variant='button'>{refinementCall.gens}</Typography>
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Paper variant='elevation' className={classes.paper}>
                        <Typography variant='button'>
                            <FormattedMessage id="project.global.fields.f" />
                            :&nbsp;
                            <Typography className={classes.value} variant='button'>{refinementCall.f}</Typography>
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Paper variant='elevation' className={classes.paper}>
                        <Typography variant='button'>
                            <FormattedMessage id="project.global.fields.cr" />
                            :&nbsp;
                            <Typography className={classes.value} variant='button'>{refinementCall.cr}</Typography>
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Paper variant='elevation' className={classes.paper}>
                        <Typography variant='button'>
                            <FormattedMessage id="project.global.fields.executeEach" />
                            :&nbsp;
                            <Typography className={classes.value} variant='button'>{refinementCall.executeEach}</Typography>
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Paper variant='elevation' className={classes.paper}>
                        <Typography variant='button'>
                            <FormattedMessage id="project.global.fields.executionsPerGen" />
                            :&nbsp;
                            <Typography className={classes.value} variant='button'>{refinementCall.executionsPerGen}</Typography>
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Paper variant='elevation' className={classes.paper}>
                        <Typography variant='button'>
                            <FormattedMessage id="project.global.fields.popToRepackmin" />
                            :&nbsp;
                            <Typography className={classes.value} variant='button'>{refinementCall.popToRepackmin}</Typography>
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Paper variant='elevation' className={classes.paper}>
                        <Typography variant='button'>
                            <FormattedMessage id="project.global.fields.disturbDegrees" />
                            :&nbsp;
                            <Typography className={classes.value} variant='button'>{refinementCall.disturbDegrees}</Typography>
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Paper variant='elevation' className={classes.paper}>
                        <Typography variant='button'>
                            <FormattedMessage id="project.global.fields.preserveConfig" />
                            {
                                <Checkbox
                                    size='small'
                                    disabled
                                    checked={refinementCall.preserveConfig}
                                />
                            }
                            <Typography variant='button' hidden={!refinementCall.preserveConfig}>
                                <Typography className={classes.value} variant='button'>{refinementCall.configName}</Typography>
                            </Typography>
                        </Typography>
                    </Paper>
                </Grid>
            </Grid>
        </>
    )
}


const RefinementCallList = ({ projectId, refinementCallList }) => {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const dispatch = useDispatch();

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
        if (isExpanded) {
            //dispatch(actions.getPendingTasks(projectId, onSuccess, onErrors));
        }
    };
    console.log(refinementCallList)
    return (
        <>
            <Card className={classes.contentProject} variant="outlined">
                {refinementCallList.map(refinementCall =>
                    <Accordion key={refinementCall.id} expanded={expanded === refinementCall.id} onChange={handleChange(refinementCall.id)}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1bh-content"
                            id="panel1bh-header"
                        >
                            {refinementCall.taskState == 'SUCCESS' && <RefinementCallLink callId={refinementCall.id} />}
                            <Typography className={classes.typographyAccordion}>
                                {refinementCall.configName || <FormattedMessage id='project.refinements.RefinementCallList.NoConfigName' />}&nbsp;-&nbsp;<FormattedDate value={new Date(refinementCall.date)} /> - <FormattedTime value={new Date(refinementCall.date)} />
                            </Typography>
                            <StatusChip style={{ marginLeft: 'auto !important' }} marginLeft='auto' taskId={String(refinementCall.id)} status={refinementCall.taskState}></StatusChip>
                        </AccordionSummary>
                        <Card className={classes.form} variant="outlined">
                            <ReviewParamsForm refinementCall={refinementCall} classes={classes}/>
                            {/* <ProjectPendingTasks pendingTasks={projectPendingTasks} handleDeleteFunc={(taskId) => handleDelete(taskId)} /> */}
                        </Card>
                    </Accordion>
                )}
            </Card>
        </>
    );
};

RefinementCallList.propTypes = {
    refinementCallList: PropTypes.array.isRequired,
    projectId: PropTypes.number.isRequired
};

export default RefinementCallList;
