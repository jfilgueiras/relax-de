import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';

import * as actions from '../actions';
import * as selectors from '../selectors';
import {Pager} from '../../common';
import RefinementCallList from './RefinementCallList';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import { BackLink } from '../../common';

const useStyles = makeStyles({
    pager: {
        backgroundColor: 'white'
    },
  });

const FindRefinementCallsResult = () => {
    const classes = useStyles();

    const refinementCallSearch = useSelector(selectors.getRefinementCallSearch);
    const dispatch = useDispatch();

    if (!refinementCallSearch) {
        return null;
    }

    if (refinementCallSearch.result.items.length === 0) {
        return (
            <React.Fragment>
                <BackLink times={2} />
                <div className="alert alert-danger" role="alert">
                    <FormattedMessage id='project.refinements.findRefinementCallsResult.noRefinementCallsFound'/>
                </div>
            </React.Fragment>
        );
    }
    
    return (

        <div>
            <BackLink times={2} />
            <RefinementCallList projectId={refinementCallSearch.criteria.projectId} refinementCallList={refinementCallSearch.result.items}/>
            <Pager 
                back={{
                    enabled: refinementCallSearch.criteria.page >= 1,
                    onClick: () => dispatch(actions.previousFindRefinementCallsResultPage(refinementCallSearch.criteria))}}
                next={{
                    enabled: refinementCallSearch.result.existMoreItems,
                    onClick: () => dispatch(actions.nextFindRefinementCallsResultPage(refinementCallSearch.criteria))}}/>
        </div>

    );

}

export default FindRefinementCallsResult;