import React from 'react';
import { FormattedMessage } from 'react-intl';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';

import ExecuteRefinementFormStepper from './ExecuteRefinementFormStepper';

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
    background: 'linear-gradient(135deg, rgba(65,0,31,1) 0%, rgba(0,0,0,1) 40%, rgba(0,0,0,1) 60%, rgba(65,0,31,1) 100%)'

  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function ExecuteRefinementModalForm({isOpen=false, handleCloseFun}) {
  const classes = useStyles();

  return (
    <div>
      <Dialog fullScreen open={isOpen} onClose={handleCloseFun} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleCloseFun} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              <FormattedMessage id="project.refinements.ExecuteRefinement.title"/>
            </Typography>
          </Toolbar>
        </AppBar>
        <ExecuteRefinementFormStepper handleCloseFun={handleCloseFun}/>
      </Dialog>
    </div>
  );
}
