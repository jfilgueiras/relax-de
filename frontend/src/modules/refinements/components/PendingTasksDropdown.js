import React from 'react';
import { FormattedMessage } from 'react-intl';
import GetPendingTasks from './GetPendingTasks';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Popover from '@material-ui/core/Popover';
import { usePopupState, bindTrigger, bindPopover } from 'material-ui-popup-state/hooks'
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const popoverWidth = 350;
const popoverMargin = 50;

const useStyles = makeStyles(() => ({

    popoverButton: {
        marginRight: popoverMargin,
        width: popoverWidth,
    },
    popover: {
        width: '100%',
    },
  }));

const PendingTasksDropdown = () => {
    const classes = useStyles();
    const popupState = usePopupState({ variant: 'popover', popupId: 'pendingTasksPopover' })

    return (
        <div className={classes.popoverButton}>
            <Button className={classes.popoverButton} variant="contained" color="secondary" {...bindTrigger(popupState)}>
                <Typography color='primary' variant='button'><FormattedMessage id="project.refinements.PendingTasksDropdown.getPendingTasks" /></Typography>
            </Button>
            <Popover className={classes.popover}
                {...bindPopover(popupState)}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
            >
                <Box p={2} width={popoverWidth}>
                    <GetPendingTasks isOpen={popupState.isOpen} />
                </Box>
            </Popover>
        </div>
    );
};

export default PendingTasksDropdown;