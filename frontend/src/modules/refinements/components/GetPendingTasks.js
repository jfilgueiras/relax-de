import React, { useEffect } from 'react';
import Schedule from 'react-schedule-job'
import 'react-schedule-job/dist/index.css'
import { useDispatch, useSelector } from 'react-redux';
import {FormattedMessage} from 'react-intl';

import * as actions from '../actions';
import * as selectors from '../selectors';
import PendingTasks from './PendingTasks';

const GetPendingTasks = ({ isOpen }) => {

    const dispatch = useDispatch();
    const pendingTasks = useSelector(selectors.getPendingTasks);
    const handleDelete = (taskId) => {
        dispatch(actions.deleteCompletedTask(taskId));
    };

    const retrieveTasks = () => {
        dispatch(actions.getPendingTasks());
    }

    const job = [
        {
          fn: retrieveTasks,
          id: '1',
          schedule: '* * * * *'
          // this runs every minutes
        }
    ]
    
    useEffect(() => {
        if(isOpen) {
            dispatch(actions.getPendingTasks());
        }
    }, [dispatch, isOpen]);

    let retVal = null;
    if (!pendingTasks) {
        retVal = null;
    }

    else if (pendingTasks.length === 0) {
        retVal = (
            <div className="alert" role="alert">
                <FormattedMessage id='project.refinements.getPendingTasksResult.noTasksFound'/>
            </div>
        );
    }
    
    else {
        retVal =  (
            <div>
                <PendingTasks pendingTasks={pendingTasks} handleDeleteFunc={(taskId) => handleDelete(taskId)}/>
            </div>
        );
    }
    return (
        <React.Fragment>
            {retVal}
            <Schedule
                jobs={job}
                timeZone='UTC'
                // "UTC", "local" or "YOUR PREFERRED TIMEZONE",
                dashboard={{
                    hidden: true
                    // if true, dashboard is hidden
                }}
            />
      </React.Fragment>
    )
};

export default GetPendingTasks;