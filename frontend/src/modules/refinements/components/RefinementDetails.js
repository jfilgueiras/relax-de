import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { FormattedMessage, FormattedDate, FormattedTime } from 'react-intl';
import { useParams } from 'react-router-dom';
import { BackLink, Errors, NglRenderer } from '../../common';
import users from '../../users';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ScatterChart, Label, Scatter } from 'recharts';
import GetAppIcon from '@material-ui/icons/GetApp';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import ListAltIcon from '@material-ui/icons/ListAlt';
import { ListItemLink } from '../../common';
import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Grid from '@material-ui/core/Grid';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as selectors from '../selectors';
import * as actions from '../actions';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import * as global_actions from '../../app/actions';
import { SvgIcon } from 'material-ui';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'block',
    },
    contentProject: {
        height: '60vh',
    },
    refinementInfo: {
        width: '100%',
        height: '5vh',
        display: 'inline-flex',
    },
    chartCard: {
        flexGrow: 1,
        height: '100%',
        width: '45%',
        display: 'inline-block',
        marginTop: 10
    },
    flexList: {
        padding: theme.spacing(7),
        float: 'right',
    },
    typography: {
        ...theme.typography.button,
        fontSize: 45,
        padding: theme.spacing(4),
        paddingLeft: theme.spacing(8),
    },
    typographyTags: {
        ...theme.typography.button,
        fontSize: 16,
        color: theme.palette.primary.dark,
        paddingLeft: theme.spacing(8),
        paddingBottom: theme.spacing(4),
    },
    typographyDescription: {
        ...theme.typography.caption,
        fontSize: 18,
        paddingLeft: theme.spacing(8),
        paddingBottom: theme.spacing(3),
        paddingTop: theme.spacing(3),
    },
    typographyApiDir: {
        ...theme.typography.subtitle1,
        color: theme.palette.primary.main,
        fontSize: 20,
        padding: theme.spacing(4),
    },
    button: {
        backgroundColor: 'transparent'
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        // height: 224,
        minHeight: '70vh'
    },
    tabs: {
        width: '15%',
        borderRight: `1px solid ${theme.palette.divider}`,
    },
}));

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            style={{ width: '100%' }}
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={2}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

const RefinementDetails = () => {

    const classes = useStyles();
    const [backdropOpen, setBackdropOpen] = useState(false);
    const [backendErrors, setBackendErrors] = useState(null);
    const [tab, setTab] = useState(0);

    const refinement = useSelector(selectors.getRefinement);
    const userName = useSelector(users.selectors.getUserName);
    const dispatch = useDispatch();
    const { id } = useParams();
    const refinementId = Number(id);

    const handleTabChange = (event, newValue) => {
        setTab(newValue);
    };

    const clearErrors = () => {
        setBackendErrors(null)
    }

    const onErrors = errors => {
        setBackendErrors(errors)
    }

    const onSuccess = file => {
        handleBackdropClose();
    }

    const onClickDownload = callId => {
        handleBackdropToggle();
        dispatch(actions.downloadRefinementById(callId, onSuccess, onErrors));
    }

    const handleBackdropClose = () => {
        setBackdropOpen(false);
    };

    const handleBackdropToggle = () => {
        setBackdropOpen(true);
    };

    useEffect(() => {

        if (!Number.isNaN(refinementId)) {
            dispatch(global_actions.loading());
            clearErrors();
            dispatch(actions.findRefinementById(refinementId, () => dispatch(global_actions.loaded()), onErrors));

        }

        return () => dispatch(actions.clearRefinement());

    }, [refinementId, dispatch]);

    if (backendErrors) {
        return <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />;
    }

    if (!refinement) {
        return null;
    }

    return (
        <>
            {console.log(refinement)}
            <BackLink times={2} />
            <Card style={{}} className={classes.refinementInfo} variant="outlined">
                <Box style={{ paddingLeft: '2.8%', alignSelf: 'center' }}>
                    {/* &nbsp;-&nbsp;<FormattedDate value={new Date(refinementCall.date)} /> - <FormattedTime value={new Date(refinementCall.date)} /> */}
                    <Typography variant='button'>{null || <FormattedMessage id='project.refinements.RefinementCallList.NoConfigName' />} - 9/9/2022 - 21:12</Typography>
                </Box>
                <Box style={{ marginLeft: 'auto', marginRight:'0.5%' }}>
                    <ListItem button onClick={() => onClickDownload(refinementId)} >
                        <ListItemIcon style={{ minWidth: 35 + "px" }}>{<GetAppIcon style={{ marginRight: "10" }} color={'primary'} />}</ListItemIcon>
                        <Typography variant='body1'><FormattedMessage id="project.projects.ProjectDetails.downloadResults" /></Typography>
                    </ListItem>
                </Box>
            </Card>
            <div className={classes.root}>
                <Tabs
                    orientation="vertical"
                    variant="scrollable"
                    value={tab}
                    onChange={handleTabChange}
                    aria-label="Vertical tabs example"
                    className={classes.tabs}
                >
                    <Tab label={
                        <FormattedMessage id='project.tabs.3DRender' />
                    } {...a11yProps(0)} />
                    <Tab label={
                        <FormattedMessage id='project.tabs.Charts' />
                    } {...a11yProps(1)} />
                    {/* <Tab label={
                        <FormattedMessage id='project.tabs.Params'/>
                    } {...a11yProps(2)} /> */}
                </Tabs>
                <TabPanel value={tab} index={0}>
                    <Card style={{ marginTop: '50', minHeight: '60vh', width: '100%' }} className={classes.contentProject} variant="outlined">
                        <NglRenderer native={refinement.nativePDB} result={refinement.resultPDB}></NglRenderer>
                    </Card>
                </TabPanel>
                <TabPanel value={tab} index={1}>
                    <Card style={{ 'width': '100%', display: 'inline-flex' }} className={classes.chartCard} variant="outlined">
                        <Box>
                            <Typography style={{ marginLeft: '10%' }} variant='button'><FormattedMessage id='project.graph.evolutionPlot' /></Typography>
                            <LineChart
                                width={500}
                                height={350}
                                data={refinement.gensInfo}
                                margin={{
                                    top: 20,
                                    right: 0,
                                    left: 30,
                                    bottom: 20,
                                }}
                            >
                                <CartesianGrid strokeDasharray="3 3" />
                                <Tooltip />
                                <Legend verticalAlign="top" />
                                <XAxis dataKey="gens" label={
                                    <Label value={"generations"} offset={-8} position="insideBottom" />
                                }>

                                </XAxis>
                                <YAxis label={{ value: 'ref2015', angle: -90, position: 'insideLeft' }}>
                                </YAxis>
                                <Line type="monotone" dataKey="avg" stroke="green" activeDot={{ r: 5 }} />
                                <Line type="monotone" dataKey="best" stroke="red" activeDot={{ r: 5 }} />
                            </LineChart>
                        </Box>
                        <Box>
                            <Typography style={{ marginLeft: '10%' }} variant='button'><FormattedMessage id='project.graph.scatterPlot' /></Typography>

                            <ScatterChart
                                width={500}
                                height={350}
                                margin={{
                                    top: 40,
                                    right: 0,
                                    left: 30,
                                    bottom: 20,
                                }}
                            >
                                <CartesianGrid />
                                <XAxis type="number" dataKey="rmsd" name="rmsd">
                                    <Label value="RMSD" offset={-10} position="insideBottom" />
                                </XAxis>
                                <YAxis type="number" dataKey="score" name="ref2015" label={{ value: 'ref2015', angle: -90, position: 'insideLeft' }} />
                                <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                                <Scatter name="A school" data={refinement.populInfo} fill="darkblue" />
                            </ScatterChart>
                        </Box>
                    </Card>
                </TabPanel>
            </div>
        </>

    );

}

export default RefinementDetails;
