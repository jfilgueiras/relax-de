import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Errors } from '../../common';
import * as actions from '../actions';
import { useHistory, useLocation } from 'react-router-dom';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import * as selectors from '../selectors';

const ExecuteRefinementForm = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const [preserveConfig, setPreserveConfig] = useState(false);

    const [configName, setConfigName] = useState(null);
    const [disturbDegrees, setDisturbDegrees] = useState(0);
    // DE
    const [scheme, setScheme] = useState('BEST');
    const [popsize, setPopsize] = useState(100);
    const [f, setF] = useState(0.01);
    const [cr, setCr] = useState(0.95);
    const [gens, setGens] = useState(100);
    const [tag, setTag] = useState('');
    const [logStats, setLogStats] = useState('False');
    // Relax
    const [populExecutorChain, setPopulExecutorChain] = useState('');
    const [trialExecutorChain, setTrialExecutorChain] = useState('');
    const [executeEach, setExecuteEach] = useState(1);
    const [executionsPerGen, setExecutionsPerGen] = useState(4);
    const [popToRepackmin, setPopToRepackmin] = useState(100);
    const [repackFaRepScale, setRepackFaRepScale] = useState('');
    const [minFaRepScale, setMinFaRepScale] = useState('');
    const [checkExecutionImprovement, setCheckExecutionImprovement] = useState('True');
    const [aaProportionRepmin, setAaProportionRepmin] = useState(1);
    const [sortWorstEterms, setSortWorstEterms] = useState('False');
    // Exec
    const [time_limit, setTime_limit] = useState(0.95);
    
    const [backendErrors, setBackendErrors] = useState(null);
    const params = new URLSearchParams(useLocation().search);
    const projectId = Number(params.get("projectId"));

    let form;
    
    const handleSubmit = event => {

        event.preventDefault();
        if (form.checkValidity()) {
            const refinementCall =
            {
                configName,
                disturbDegrees,
                de: {
                    scheme,
                    popsize,
                    f,
                    cr,
                    gens,
                    tag,
                    logStats
                },
                relax: {
                    populExecutorChain,
                    trialExecutorChain,
                    executeEach,
                    executionsPerGen,
                    popToRepackmin,
                    repackFaRepScale,
                    minFaRepScale,
                    checkExecutionImprovement,
                    aaProportionRepmin,
                    sortWorstEterms
                },
                exec: {
                    time_limit
                }
            }
            console.log(refinementCall);
            dispatch(actions.executeRefinement(projectId, refinementCall, 
                () => history.push('/refinements/refinement-created'),
                errors => setBackendErrors(errors)
            ));

        } else {
            setBackendErrors(null);
            form.classList.add('was-validated');
        }

    }

    useEffect(() => {
        // dispatch(actions.getTaxonPrefixes(projectId));
    }, [projectId, dispatch]);

    if (!projectId) {
        return null;
    }

    return (
        <div>
            <Errors errors={backendErrors}
                onClose={() => setBackendErrors(null)} />
            <div className="card bg-light border-dark">
                <h5 className="card-header">
                    <FormattedMessage id="project.refinements.ExecuteRefinement.title" />
                </h5>
                <div className="card-body">
                    <form ref={node => form = node}
                        className="needs-validation" noValidate
                        onSubmit={(e) => handleSubmit(e)}>
                        <div className="form-group row">
                            <FormControlLabel
                                    value={<FormattedMessage id="project.global.fields.preserveConfig"/>}
                                    control={<Checkbox
                                        checked={preserveConfig}
                                        onChange={e => {setPreserveConfig(e.target.checked); if (e.target.checked) setConfigName(null)}}
                                        inputProps={{ 'aria-label': 'primary checkbox' }}
                                    />}
                                    label={<FormattedMessage id="project.global.fields.preserveConfig"/>}
                                    labelPlacement="start"
                                />
                            
                            {preserveConfig && <FormControlLabel
                                    value={<FormattedMessage id="project.global.fields.configName"/>}
                                    control={
                                        <input type="string" id="configName" className="form-control"
                                            value={configName}
                                            onChange={e => setConfigName(e.target.value)}
                                            />
                                    }
                                    label={<FormattedMessage id="project.global.fields.configName"/>}
                                    labelPlacement="start"
                                />}
                        </div>
                        <div className="form-group row">
                            <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.disturbDegrees"/>}
                                control={
                                    <input type="float" id="disturbDegrees" className="form-control"
                                        value={disturbDegrees}
                                        onChange={e => setDisturbDegrees(parseFloat(e.target.value))}
                                        autoFocus
                                        min="0"
                                        max="180" />
                                }
                                label={<FormattedMessage id="project.global.fields.disturbDegrees"/>}
                                labelPlacement="start"
                            />  
                        </div>
                        <div className="form-group row">
                            <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.scheme"/>}
                                control={
                                    <Select
                                    value={scheme}
                                    className="form-control"
                                    onChange={(event) => {
                                        setScheme(event.target.value);
                                      }
                                    }
                                    >
                                        <MenuItem value={"BEST"}>BEST</MenuItem>
                                        <MenuItem value={"RANDOM"}>RANDOM</MenuItem>
                                    </Select>
                                }
                                label={<FormattedMessage id="project.global.fields.scheme"/>}
                                labelPlacement="start"
                            />
                            <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.popsize"/>}
                                control={
                                    <input type="number" id="popsize" className="form-control"
                                    value={popsize}
                                    onChange={e => setPopsize(Number(e.target.value))}
                                    min="10" />
                                }
                                label={<FormattedMessage id="project.global.fields.popsize"/>}
                                labelPlacement="start"
                            />
                            <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.f"/>}
                                control={
                                    <input type="number" id="f" className="form-control"
                                    value={f}
                                    onChange={e => setF(parseFloat(e.target.value))}
                                    min="0"
                                    max="1" 
                                    step="0.01"
                                    />
                                }
                                label={<FormattedMessage id="project.global.fields.f"/>}
                                labelPlacement="start"
                            />
                            <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.cr"/>}
                                control={
                                    <input type="number" id="cr" className="form-control"
                                    value={cr}
                                    onChange={e => setCr(parseFloat(e.target.value))}
                                    min="0"
                                    max="1"
                                    step="0.01"
                                    />
                                }
                                label={<FormattedMessage id="project.global.fields.cr"/>}
                                labelPlacement="start"
                            />
                            <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.gens"/>}
                                control={
                                    <input type="number" id="gens" className="form-control"
                                    value={gens}
                                    onChange={e => setGens(Number(e.target.value))}
                                    min="10" />
                                }
                                label={<FormattedMessage id="project.global.fields.gens"/>}
                                labelPlacement="start"
                            />
                        </div>
                        <div className="form-group row">
                            <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.tag"/>}
                                control={
                                    <input type="string" id="tag" className="form-control"
                                    value={tag}
                                    onChange={e => setTag(e.target.value)}
                                    />
                                }
                                label={<FormattedMessage id="project.global.fields.tag"/>}
                                labelPlacement="start"
                            />
                            <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.logStats"/>}
                                control={<Checkbox
                                    checked={logStats === 'True' ? true : false}
                                    onChange={e => setLogStats(e.target.checked ? 'True' : 'False')}
                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                                />}
                                label={<FormattedMessage id="project.global.fields.logStats"/>}
                                labelPlacement="start"
                            />
                        </div>
                        <div className="form-group row">
                            {/* <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.populExecutorChain"/>}
                                control={
                                    <input type="string" id="populExecutorChain" className="form-control"
                                    value={populExecutorChain}
                                    onChange={e => setPopulExecutorChain(e.target.value)}
                                    autoFocus />
                                }
                                label={<FormattedMessage id="project.global.fields.populExecutorChain"/>}
                                labelPlacement="start"
                            />
                            <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.trialExecutorChain"/>}
                                control={
                                    <input type="string" id="trialExecutorChain" className="form-control"
                                    value={trialExecutorChain}
                                    onChange={e => setTrialExecutorChain(e.target.value)}
                                    autoFocus />
                                }
                                label={<FormattedMessage id="project.global.fields.trialExecutorChain"/>}
                                labelPlacement="start"
                            /> */}
                            <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.executeEach"/>}
                                control={
                                    <input type="number" id="executeEach" className="form-control"
                                    value={executeEach}
                                    onChange={e => setExecuteEach(Number(e.target.value))}
                                    />
                                }
                                label={<FormattedMessage id="project.global.fields.executeEach"/>}
                                labelPlacement="start"
                            />
                            <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.executionsPerGen"/>}
                                control={
                                    <input type="number" id="executionsPerGen" className="form-control"
                                    value={executionsPerGen}
                                    onChange={e => setExecutionsPerGen(Number(e.target.value))}
                                    />
                                }
                                label={<FormattedMessage id="project.global.fields.executionsPerGen"/>}
                                labelPlacement="start"
                            />
                            <FormControlLabel
                                value={<FormattedMessage id="project.global.fields.popToRepackmin"/>}
                                control={
                                    <FormControl>
                                        <Input type="number" id="popToRepackmin" className="form-control"
                                        value={popToRepackmin}
                                        onChange={e => setPopToRepackmin(Number(e.target.value))}
                                        min="0"
                                        max="100"
                                        step="0.01"
                                        />
                                        <FormHelperText>
                                            <FormattedMessage id="project.global.fields.popToRepackminHelper"/>
                                        </FormHelperText>
                                    </FormControl>
                                }
                                label={<FormattedMessage id="project.global.fields.popToRepackmin"/>}
                                labelPlacement="start"
                            >
                            </FormControlLabel>
                        </div>
                        <div className="form-group row">
                            <div className="offset-md-3 col-md-1">
                                <button type="submit" className="btn btn-primary">
                                    <FormattedMessage id="project.global.buttons.create" />
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ExecuteRefinementForm;
