import {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {useHistory, useLocation} from 'react-router-dom';

import * as actions from '../actions';

const FindRefinementCalls = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();

    useEffect(() => {

        const search = new URLSearchParams(location.search);
        const projectId = Number(search.get('projectId'));

        dispatch(actions.findRefinementCalls({projectId, page: 0}));
        history.push('/refinements/find-refinement-calls-result');

    });

    return null;

}

export default FindRefinementCalls;
