import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import VisibilityIcon from '@material-ui/icons/Visibility';
import ListItemIcon from '@material-ui/core/ListItemIcon';

const RefinementCallLink = ({callId}) => {

    return (
        <Link to={`/refinements/${callId}`}>
            {/* Aqui va un formatted con un icono y boton para acceder */}
            <ListItemIcon style={{ minWidth: 35 + "px" }}>{<VisibilityIcon style={{ marginRight: "10" }} color={'primary'} />}</ListItemIcon>
        </Link>
    );

}

RefinementCallLink.propTypes = {
    projectId: PropTypes.number.isRequired,
    callId: PropTypes.number.isRequired
};

export default RefinementCallLink;