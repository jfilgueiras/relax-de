import React from 'react';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import ProgressBar from '../../common/components/ProgressBar';
import { FormattedDate, FormattedTime } from 'react-intl';
import { Typography, Divider } from '@material-ui/core';
import StatusChip from '../../common/components/StatusChip';

const PendingTasks = ({ pendingTasks, handleDeleteFunc }) => {
 
    return (
        <React.Fragment>
            {pendingTasks.map(pendingTask =>
            <React.Fragment key={pendingTask.taskId}>
                <Box p={1}>
                        <Typography display='inline' variant='overline' style={{"fontSize": "15pt", "fontWeight": "bold", "verticalAlign": "sub"}}>{pendingTask.projectName}&nbsp;&nbsp;&nbsp;&nbsp;</Typography>
                        <StatusChip display='inline' taskId={pendingTask.taskId} status={pendingTask.state} handleDeleteFunc={handleDeleteFunc}></StatusChip>
                </Box>
                <Box p={1}>
                        <Typography display='inline'>{<FormattedDate value={new Date(pendingTask.constructionDate)} />} - {<FormattedTime value={new Date(pendingTask.constructionDate)} />}</Typography>
                        <Typography display='block' variant="body2" color="textSecondary">{pendingTask.configName}&nbsp;</Typography>
                        <ProgressBar progress={pendingTask.current / pendingTask.total * 100} />
                        <br/>
                </Box>
                <Divider/>
            </React.Fragment>
            )}
        </React.Fragment>
    );
};

PendingTasks.propTypes = {
    pendingTasks: PropTypes.array.isRequired
};

export default PendingTasks;

