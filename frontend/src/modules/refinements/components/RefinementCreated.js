import React from 'react';
import {FormattedMessage} from 'react-intl';

const RefinementCreated = () => {

    return (
        <div className="alert alert-success" role="alert">
            <FormattedMessage id="project.dataproject.RefinementCreated.pendingRefinement"/>
        </div>
    );

}

export default RefinementCreated;