const getModuleState = state => state.refinements;

export const getPendingTasks = state =>
    getModuleState(state).pendingTasks;

export const getRefinementCallSearch = state =>
    getModuleState(state).refinementCallSearch;

export const getRefinement = state =>
    getModuleState(state).refinement;