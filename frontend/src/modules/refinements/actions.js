import backend from '../../backend';
import * as actionTypes from './actionTypes';

const executeRefinementCompleted = () => ({
    type: actionTypes.CREATE_REFINEMENT_COMPLETED
});

export const executeRefinement = (projectId, params,
        onSuccess, onErrors) => dispatch => {
    backend.refinementService.executeRefinement(
        projectId,
        params,
        () => {
            dispatch(executeRefinementCompleted());
            onSuccess();
        },
        onErrors
    );
}

// export const clearPendingTasks = () => ({
//     type: actionTypes.CLEAR_PENDING_TASKS
// });

export const deleteCompletedTask = (taskId) => ({
    type: actionTypes.DELETE_COMPLETED_TASK,
    taskId
});

const getPendingTasksCompleted = pendingTasks => ({
    type: actionTypes.GET_PENDING_TASKS_COMPLETED,
    pendingTasks
});

export const getPendingTasks = () => dispatch => {
    backend.refinementService.getUserPendingTasks(
        result => dispatch(getPendingTasksCompleted(result)));     
}

const findRefinementCallsCompleted = refinementCallSearch => ({
    type: actionTypes.FIND_REFINEMENT_CALLS_COMPLETED,
    refinementCallSearch
});

const clearRefinementCallSearch = () => ({
    type: actionTypes.CLEAR_REFINEMENT_CALL_SEARCH
});

export const findRefinementCalls = (criteria) => dispatch => {

    dispatch(clearRefinementCallSearch());
    backend.refinementService.getUserRefinements(criteria,
        result => dispatch(findRefinementCallsCompleted({ criteria, result })));

}

export const previousFindRefinementCallsResultPage = (criteria) =>
    findRefinementCalls({ projectId: criteria.projectId, page: criteria.page - 1 });

export const nextFindRefinementCallsResultPage = (criteria) =>
    findRefinementCalls({ projectId: criteria.projectId,  page: criteria.page + 1 });

export const clearRefinement = () => ({
    type: actionTypes.CLEAR_REFINEMENT
});

const findRefinementByIdCompleted = refinement => ({
    type: actionTypes.FIND_REFINEMENT_BY_ID_COMPLETED,
    refinement
});

export const findRefinementById = (id, onSuccess, onErrors) => dispatch => {
    backend.refinementService.findRefinementById(id,
        refinementResults => {dispatch(findRefinementByIdCompleted(refinementResults)); onSuccess()}, 
        onErrors);
}

export const downloadRefinementById = (id, onSuccess, onErrors) => dispatch => {
    backend.refinementService.downloadRefinementById(id,
        (file) => {
            const blobUrl = window.URL.createObjectURL(file);
            let tempLink = document.createElement('a');
            tempLink.href = blobUrl;
            tempLink.setAttribute('download', 'results.zip');
            tempLink.click();
            onSuccess(file);
        }, 
        onErrors);
}

export const getConfigNamesByUser = (onSuccess) => {
    backend.refinementService.getConfigNamesByUser(onSuccess);
}

export const getRefinementConfiguration = (configName, onSuccess) => {
    backend.refinementService.getRefinementConfiguration(configName, (refinement) => {console.log(refinement); onSuccess(refinement)}, () => console.log("a"));
}