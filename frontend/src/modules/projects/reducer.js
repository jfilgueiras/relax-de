import {combineReducers} from 'redux';

import * as actionTypes from './actionTypes';

const initialState = {
    addedProjectInfo: null,
    projectSearch: null,
    project: null
};

const projectSearch = (state = initialState.projectSearch, action) => {

    switch (action.type) {

        case actionTypes.FIND_USER_PROJECTS_COMPLETED:
            return action.projectSearch;

        case actionTypes.CLEAR_PROJECT_SEARCH:
            return initialState.projectSearch;

        default:
            return state;

    }

}

const project = (state = initialState.project, action) => {

    switch (action.type) {

        case actionTypes.FIND_PROJECT_BY_ID_COMPLETED:
            return action.project;

        case actionTypes.CLEAR_PROJECT:
            return initialState.project;

        case actionTypes.DIRECTORY_UPDATED:
            return {
                ...state,
                directory: action.directory
            };
        
        case actionTypes.GET_PENDING_TASKS_COMPLETED: 
            return {
                ...state,
                pendingTasks: action.pendingTasks
            };

        case actionTypes.DELETE_PROJECT_BY_ID_COMPLETED:
            return initialState.project;
        
        case actionTypes.UPDATE_PROJECT_COMPLETED: {
            return {
                ...state,
                name: action.projectInfo.name,
                description: action.projectInfo.description,
                accesibility: action.projectInfo.accesibility
            };
        }

        default:
            return state;

    }

}

const addedProjectInfo = (state = initialState.addedProjectInfo, action) => {

    switch (action.type) {
        case actionTypes.ADD_PROJECT_COMPLETED:
            return action.projectInfo;
        
        default:
            return state;
    }
}

const reducer = combineReducers({
    project: project,
    projectSearch: projectSearch,
    addedProjectInfo: addedProjectInfo
});

export default reducer;


