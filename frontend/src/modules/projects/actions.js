import backend from '../../backend';
import * as actionTypes from './actionTypes';

const directoryUpdated = directory => ({
    type: actionTypes.DIRECTORY_UPDATED,
    directory
});

export const addProteinToDirectory = (projectId, protein,
    onSuccess, onErrors) => dispatch =>
        backend.directoryService.addProteinToDirectory(projectId, protein, directory => {
            dispatch(directoryUpdated(directory));
            onSuccess();
        },
            onErrors
        );

const findProjectByIdCompleted = project => ({
    type: actionTypes.FIND_PROJECT_BY_ID_COMPLETED,
    project
});

export const findProjectById = (id, onErrors) => dispatch => {
    backend.projectService.findProjectById(id,
        project => dispatch(findProjectByIdCompleted(project)), 
        onErrors);
}

export const clearProject = () => ({
    type: actionTypes.CLEAR_PROJECT
});

const findUserProjectsCompleted = projectSearch => ({
    type: actionTypes.FIND_USER_PROJECTS_COMPLETED,
    projectSearch
});

const clearProjectSearch = () => ({
    type: actionTypes.CLEAR_PROJECT_SEARCH
});

export const findUserProjects = criteria => dispatch => {

    dispatch(clearProjectSearch());
    backend.projectService.findUserProjects(criteria,
        result => dispatch(findUserProjectsCompleted({ criteria, result })));

}

export const previousFindUserProjectsResultPage = criteria =>
    findUserProjects({ page: criteria.page - 1 });

export const nextFindUserProjectsResultPage = criteria =>
    findUserProjects({ page: criteria.page + 1 });

const addProjectCompleted = (projectInfo) => ({
    type: actionTypes.ADD_PROJECT_COMPLETED,
    projectInfo
});

export const addProject = (name, description, accesibility, onSuccess, 
    onErrors) => dispatch =>

    backend.projectService.addProject(
        name,
        description,
        accesibility,
        ({ id }) => {
            dispatch(addProjectCompleted({id, name}));
            onSuccess(id);
        },
        onErrors
    );

export const clearPendingTasks = (tasks) => ({
    type: actionTypes.CLEAR_PENDING_TASKS,
    tasks
});

const getPendingTasksCompleted = pendingTasks => ({
    type: actionTypes.GET_PENDING_TASKS_COMPLETED,
    pendingTasks
});

export const getPendingTasks = (projectId, onSuccess, onErrors) => dispatch => {
    backend.refinementService.getProjectPendingTasks(projectId,
        (pendingTasks) => {
            dispatch(getPendingTasksCompleted(pendingTasks));
            onSuccess(pendingTasks);
        },
        onErrors
    );
}

const deleteProjectByIdCompleted = () => ({
    type: actionTypes.DELETE_PROJECT_BY_ID_COMPLETED
});
    
export const deleteProject = (projectId, onSuccess) => dispatch => {
    backend.projectService.deleteProject(projectId, onSuccess);
    dispatch(deleteProjectByIdCompleted());
}

const updateProjectCompleted = (projectInfo) => ({
    type: actionTypes.UPDATE_PROJECT_COMPLETED,
    projectInfo
});
    
export const updateProject = ({projectId, name, description, accesibility}, onSuccess) => dispatch => {
    backend.projectService.updateProject(projectId, name, description, accesibility, (projectInfo) => {
        dispatch(updateProjectCompleted(projectInfo));
        onSuccess();
    });
}