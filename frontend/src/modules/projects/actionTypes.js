export const CLEAR_PROJECT = "project/projects/clearProject";
export const FIND_PROJECT_BY_ID_COMPLETED = "project/projects/findProjectByIdCompleted";
export const CLEAR_PROJECT_SEARCH = "project/projects/clearProjectSearch";
export const FIND_USER_PROJECTS_COMPLETED = "project/projects/findUserProjectsCompleted";
export const ADD_PROJECT_COMPLETED = "project/projects/addProjectCompleted";
export const DIRECTORY_UPDATED = "project/projects/directoryUpdated";
export const CLEAR_PENDING_TASKS = "project/projects/clearPendingTasks";
export const GET_PENDING_TASKS_COMPLETED = "project/projects/getPendingTasksCompleted";
export const DELETE_COMPLETED_TASK = "project/projects/deleteCompletedTask";
export const DELETE_PROJECT_BY_ID_COMPLETED = "project/projects/deleteProject";
export const UPDATE_PROJECT_COMPLETED = "project/projects/updateProject";