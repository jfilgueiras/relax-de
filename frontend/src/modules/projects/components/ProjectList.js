import React from 'react';
import { FormattedMessage, FormattedDate, FormattedTime } from 'react-intl';
import PropTypes from 'prop-types';
import ProjectLink from './ProjectLink';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';

const StyledTableCell = withStyles((theme) => ({
    head: {
        width: 'auto',
        background: 'linear-gradient(135deg, rgba(65,0,31,1) 0%, rgba(0,0,0,1) 40%, rgba(0,0,0,1) 60%, rgba(65,0,31,1) 100%) center fixed',
        // backgroundColor: theme.palette.primary.dark,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.secondary.light,
        },
        '&:nth-of-type(even)': {
            backgroundColor: theme.palette.secondary.main,
        },

    },
}))(TableRow);

const useStyles = makeStyles({
    table: {
        minWidth: 700,
    },
    date: {
        width: 220
    },
    little: {
        width: 120
    }
});


const ProjectList = ({ projectlist }) => {
    const classes = useStyles();

    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
                <TableHead>
                    <TableRow>
                        <StyledTableCell>
                        <Typography variant='button'>

                            <FormattedMessage id='project.global.fields.name' />
                            </Typography>
                        </StyledTableCell>
                        <StyledTableCell align="center">
                            <Typography variant='button'>
                            <FormattedMessage id='project.global.fields.accesibility' />
                            </Typography>
                        </StyledTableCell>
                        <StyledTableCell align="center">
                        <Typography variant='button'>
                            <FormattedMessage id='project.global.fields.creationDate' />
                            </Typography>
                        </StyledTableCell>
                    </TableRow>
                </TableHead>

                <TableBody>
                    {projectlist.map(project =>
                        <StyledTableRow key={project.id}>
                            <StyledTableCell component="th" scope="row">
                            <Typography variant='button'>
                                <ProjectLink id={project.id} name={project.name} />
                                </Typography>
                            </StyledTableCell>
                            <StyledTableCell align="center">
                                <Typography variant='button'>
                                <FormattedMessage id={`project.global.visibility.${project.accesibility}`} />
                                </Typography>
                            </StyledTableCell>
                            <StyledTableCell className={classes.date} align="center">
                                <Typography variant='button'>

                                <FormattedDate value={new Date(project.creationDate)} /> - <FormattedTime value={new Date(project.creationDate)} />
                                </Typography>
                            </StyledTableCell>
                        </StyledTableRow>
                    )}
                </TableBody>

            </Table>
        </TableContainer>
    );
};

ProjectList.propTypes = {
    projectlist: PropTypes.array.isRequired
};

export default ProjectList;
