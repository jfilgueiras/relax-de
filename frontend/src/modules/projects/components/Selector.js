import React from 'react';
import PropTypes from 'prop-types';

const Selector = (selectProps) => {

    return (

        <select {...selectProps}>
            {selectProps.itemlist && selectProps.itemlist.map(item => 
                <option key={item.id} value={item.name}>{item.name}</option>
            )}

        </select>

    );

}

Selector.propTypes = {
    selectProps: PropTypes.object,
};

export default Selector;
