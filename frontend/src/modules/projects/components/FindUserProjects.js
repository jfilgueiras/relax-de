import {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {useHistory} from 'react-router-dom';

import * as actions from '../actions';

const FindUserProjects = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {

        dispatch(actions.findUserProjects({page: 0}));
        history.push('/projects/find-user-projects-result');

    });

    return null;

}

export default FindUserProjects;
