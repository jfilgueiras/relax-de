import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import Uploader from './Uploader';
import { Errors } from '../../common';
import * as actions from '../actions';

const AddDirectoryForm = ({ projectId }) => {

    const dispatch = useDispatch();
    const [backendErrors, setBackendErrors] = useState(null);
    const [uploadStatus, setUploadStatus] = useState(null);

    const onSuccess = () => {
        setUploadStatus(true);
        setBackendErrors(null);
    }

    const onErrors = errors => {
        setUploadStatus(false);
        setBackendErrors(errors);
    }

    const addProteinAction = (protein) => {
        dispatch(actions.addProteinToDirectory(projectId, protein, onSuccess, onErrors));
    }
    
    return (
        <div>
            {uploadStatus &&
                <div className="alert alert-success" role="alert">
                    <FormattedMessage id="project.projects.AddDirectoryForm.uploadSuccessful" />
                </div>
            }
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />

            <Uploader className='no-overflow' projectId={projectId} maxFiles={1} handleSubmit={
                (files, allFiles) =>   
                {
                    addProteinAction(files[0]);
                    allFiles.forEach(f => f.remove());
                }} />
            <br />
        </div>
    );

}

AddDirectoryForm.propTypes = {
    projectId: PropTypes.number.isRequired,
};

export default AddDirectoryForm;
