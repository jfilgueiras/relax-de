import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import { FormattedMessage, FormattedDate, FormattedTime } from 'react-intl';
import { useHistory, useParams } from 'react-router-dom';
import { BackLink, Errors } from '../../common';
import users from '../../users';

import AddDirectoryForm from './AddDirectoryForm';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import ListAltIcon from '@material-ui/icons/ListAlt';
import { ListItemLink } from '../../common';
import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import * as selectors from '../selectors';
import * as actions from '../actions';
import * as global_actions from '../../app/actions';
import { values } from 'd3';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'block',
    },
    contentProject: {
        flexGrow: 1,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    flexList: {
        padding: theme.spacing(7),
        float: 'right',
    },
    typography: {
        ...theme.typography.button,
        fontSize: 45,
        padding: theme.spacing(4),
        paddingLeft: theme.spacing(8),
    },
    typographyTags: {
        ...theme.typography.button,
        fontSize: 16,
        color: theme.palette.primary.dark,
        paddingLeft: theme.spacing(8),
        paddingBottom: theme.spacing(4),
    },
    typographyDescription: {
        ...theme.typography.caption,
        fontSize: 18,
        paddingLeft: theme.spacing(8),
        paddingBottom: theme.spacing(3),
        paddingTop: theme.spacing(3),
    },
    typographyApiDir: {
        ...theme.typography.subtitle1,
        color: theme.palette.primary.main,
        fontSize: 20,
        padding: theme.spacing(4),
    },
    button: {
        backgroundColor: 'transparent'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
}));

const initialFormValues = {
    projectName: "",
    formSubmitted: false,
    success: false
}

export const useFormControls = (projectName = "") => {
    // We'll update "values" as the form updates
    const [values, setValues] = useState({ ...initialFormValues, projectName });
    // "errors" is used to check the form for errors
    const [errors, setErrors] = useState({});
    const validate = (fieldValues = values) => {
        let temp = { ...errors }

        if ("projectName" in fieldValues)
            temp.projectName = fieldValues.projectName ? "" : "This field is required."

        setErrors({
            ...temp
        });
    }
    const handleInputValue = (e) => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value
        });
        validate({ [name]: value });
    };
    const formIsValid = (fieldValues = values) => {
        const isValid =
            fieldValues.projectName &&
            Object.values(errors).every((x) => x === "");

        return isValid;
    };
    return {
        handleInputValue,
        formIsValid,
        values,
        errors
    };
}

const AddEditProjectForm = ({ open, handleDialogClose, projectId, name = '', description = '', accesibility = 'PUBLIC' }) => {

    const classes = useStyles();
    const history = useHistory();
    const accesibilities = ["PUBLIC", "READ_ONLY", "PRIVATE"]
    const [dialogProjectName, setDialogProjectName] = useState(name);
    const [dialogProjectDescription, setDialogProjectDescription] = useState(description);
    const [dialogProjectAccesibility, setDialogProjectAccesibility] = useState(accesibility);
    const dispatch = useDispatch();
    let form;
    const {
        handleInputValue,
        formIsValid,
        values,
        errors
    } = useFormControls(name);

    const handleEditProjectSubmit = (e) => {
        console.log(e)
        e.preventDefault();
        dispatch(global_actions.loading());
        if (form.checkValidity()) {
            projectId ?
                dispatch(
                    actions.updateProject({ projectId, name: values.projectName.trim(), description: dialogProjectDescription.trim(), accesibility: dialogProjectAccesibility },
                        () => {
                            handleDialogClose();
                            dispatch(global_actions.toastOpen('project.projects.EditProject.EditSuccessful', 'success'));
                            dispatch(global_actions.loaded());
                        }
                    )
                )
                :
                dispatch(
                    actions.addProject(values.projectName.trim(),
                        dialogProjectDescription.trim(), dialogProjectAccesibility,
                        (id) => {
                            handleDialogClose();
                            dispatch(global_actions.toastOpen('project.projects.ProjectAdded.ProjectAdded', 'success'));
                            dispatch(global_actions.loaded());
                            history.push(`/projects/${id}`);
                        },
                        errors => {
                            handleDialogClose();
                            dispatch(global_actions.toastOpen('project.projects.ProjectAdded.Couldnt', 'error'));
                            dispatch(global_actions.loaded());
                            console.log("bun")
                        } /*setBackendErrors(errors)*/
                    )
                )
        } else {
            dispatch(global_actions.loaded());
            // setBackendErrors(null);  
            form.classList.add('was-validated');
        }
    }

    return (
        <Dialog open={open} onClose={handleDialogClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">
                {projectId ?
                    <FormattedMessage id="project.projects.ProjectDetails.updateProject" />
                    :
                    <FormattedMessage id="project.projects.AddProjectForm.title" />
                }
            </DialogTitle>
            <form onSubmit={handleEditProjectSubmit} ref={node => form = node} noValidate autoComplete="off">
                <DialogContent className={classes.root}>
                    <TextField className={classes.formControl}
                        required
                        autoFocus
                        margin="dense"
                        id="projectName"
                        name="projectName"
                        label={
                            <FormattedMessage id="project.global.fields.name" />
                        }
                        type="text"
                        onBlur={handleInputValue}
                        onChange={handleInputValue}
                        value={values.projectName}
                        // onChange={(e) => setDialogProjectName(e.target.value)}
                        {...(errors["projectName"] && { error: true, helperText: errors["projectName"] })}
                    />
                    <TextField className={classes.formControl}
                        margin="dense"
                        id="desc"
                        label={
                            <FormattedMessage id="project.global.fields.description" />
                        }
                        type="text"
                        value={dialogProjectDescription}
                        onChange={(e) => setDialogProjectDescription(e.target.value)}
                    />
                    <FormControl className={classes.formControl}>
                        <InputLabel id="acc-select-label">
                            <FormattedMessage id="project.global.fields.accesibility" />
                        </InputLabel>
                        <Select
                            value={dialogProjectAccesibility}
                            // style={{height: '35px'}}
                            className="form-control"
                            labelId="acc-select-label"
                            label={
                                <FormattedMessage id="project.global.fields.accesibility" />
                            }
                            onChange={(event) => { setDialogProjectAccesibility(event.target.value); }}
                        >
                            {
                                accesibilities.map((acc) =>
                                    <MenuItem key={acc} value={acc}>
                                        <FormattedMessage id={`project.global.visibility.${acc}`} />
                                    </MenuItem>
                                )
                            }
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDialogClose} color="primary">
                        <FormattedMessage id="project.global.buttons.cancel" />
                    </Button>
                    <Button
                        type='submit'
                        disabled={!formIsValid()}
                        color="primary">
                        {projectId ?
                            <FormattedMessage id="project.global.buttons.edit" />
                            :
                            <FormattedMessage id="project.global.buttons.add" />
                        }
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    );
}

AddEditProjectForm.propTypes = {
    open: PropTypes.bool.isRequired,
    handleDialogClose: PropTypes.func.isRequired
};

export default AddEditProjectForm;