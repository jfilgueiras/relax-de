import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FormattedMessage, FormattedDate, FormattedTime } from 'react-intl';
import { useHistory, useParams } from 'react-router-dom';
import { BackLink, Errors } from '../../common';
import users from '../../users';

import AddDirectoryForm from './AddDirectoryForm';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import ListAltIcon from '@material-ui/icons/ListAlt';
import { ListItemLink } from '../../common';
import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import * as selectors from '../selectors';
import * as actions from '../actions';
import * as global_actions from '../../app/actions';

import GetProjectPendingTasks from './GetProjectPendingTasks';
import ExecuteRefinementModalForm from '../../refinements/components/ExecuteRefinementModalForm';
import AddEditProjectForm from './AddEditProjectForm';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'block',
    },
    contentProject: {
        flexGrow: 1,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    flexList: {
        padding: theme.spacing(7),
        float: 'right',
    },
    typography: {
        ...theme.typography.button,
        fontSize: 45,
        padding: theme.spacing(4),
        paddingLeft: theme.spacing(8),
    },
    typographyTags: {
        ...theme.typography.button,
        fontSize: 16,
        color: theme.palette.primary.dark,
        paddingLeft: theme.spacing(8),
        paddingBottom: theme.spacing(4),
    },
    typographyDescription: {
        ...theme.typography.caption,
        fontSize: 18,
        paddingLeft: theme.spacing(8),
        paddingBottom: theme.spacing(3),
        paddingTop: theme.spacing(3),
    },
    typographyApiDir: {
        ...theme.typography.subtitle1,
        color: theme.palette.primary.main,
        fontSize: 20,
        padding: theme.spacing(4),
    },
    button: {
        backgroundColor: 'transparent'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      },
}));

const ProjectDetails = () => {

    const classes = useStyles();
    const [modalOpen, setModalOpen] = React.useState(false);
    const [backendErrors, setBackendErrors] = useState(null);
    const project = useSelector(selectors.getProject);
    const userName = useSelector(users.selectors.getUserName);
    const dispatch = useDispatch();
    const history = useHistory();
    const { id } = useParams();
    const projectId = Number(id);

    const [editDialogOpen, setEditDialogOpen] = useState(false);

    const clearErrors = () => {
        setBackendErrors(null)
    }

    const onErrors = errors => {
        setBackendErrors(errors)
    }

    const handleModalOpen = () => {
        setModalOpen(true);
    };

    const handleClose = () => {
        setModalOpen(false);
    };

    const handleEditDialogClose = () => {
        setEditDialogOpen(false);
    };
    
    useEffect(() => {

        if (!Number.isNaN(projectId)) {
            clearErrors();
            dispatch(actions.findProjectById(projectId, onErrors));
        }

        return () => dispatch(actions.clearProject());

    }, [projectId, dispatch]);

    if (backendErrors) {
        return <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />;
    }

    if (!project) {
        return null;
    }

    return (

        <div>
            <BackLink times={2}/>
            <Card className={classes.contentProject} variant="outlined">
                <List className={classes.flexList}>
                    {project.directory &&
                        <React.Fragment>
                            {(project.userName === userName || project.accesibility === 'PUBLIC') &&
                                <>
                                    <ListItem button onClick={handleModalOpen}>
                                        <ListItemIcon style={{ minWidth: 35 + "px" }}>{<AddCircleIcon style={{ marginRight: "10" }} color={'primary'} />}</ListItemIcon>
                                        <FormattedMessage id="project.projects.ProjectDetails.executeRefinementLink" />
                                    </ListItem>
                                    <ExecuteRefinementModalForm isOpen={modalOpen} handleCloseFun={handleClose} />
                                    {/* <ListItem button onClick={handleModalOpen}>
                                        <ListItemIcon style={{ minWidth: 35 + "px" }}>{<ListAltIcon style={{ marginRight: "10" }} color={'primary'} />}</ListItemIcon>
                                        <FormattedMessage id="project.projects.ProjectDetails.findRefinementCallsLink" />
                                    </ListItem> */}
                                    <ListItemLink to={`/refinements/find-refinement-calls?projectId=${id}`} primary="project.projects.ProjectDetails.findRefinementCallsLink" icon={<ListAltIcon color={'primary'} />} />

                                </>
                            }
                        </React.Fragment>
                    }
                    {project.userName === userName && project.canDelete &&
                        <li>
                            <ListItem button onClick={() => {
                                // setDialogProjectName(project.name);
                                // setDialogProjectDescription(project.description);
                                // setDialogProjectAccesibility(project.accesibility);
                                setEditDialogOpen(true);
                            }}>
                            <ListItemIcon style={{ minWidth: 35 + "px" }}>{<BorderColorIcon color={'primary'}/>}</ListItemIcon>
                                <FormattedMessage id={'project.projects.ProjectDetails.updateProject'} />
                            </ListItem>
                            <AddEditProjectForm open={editDialogOpen} handleDialogClose={handleEditDialogClose} projectId={projectId} name={project.name} description={project.description} accesibility={project.accesibility}/>
                        </li>
                    }
                    {project.userName === userName && project.canDelete &&
                        <li>
                            <ListItem button onClick ={() => {
                                dispatch(actions.deleteProject(projectId, () => history.push('/')));
                            }}>
                                <ListItemIcon style={{minWidth: 35 +"px"}}>{<DeleteForeverIcon />}</ListItemIcon>
                                <FormattedMessage id={'project.projects.ProjectDetails.deleteProject'} />
                            </ListItem>
                        </li>
                    }
                </List>
                <Typography className={classes.typography} color="primary">
                    {project.name}
                </Typography>
                <Typography className={classes.typographyTags}>
                    <b><FormattedMessage id='project.global.fields.accesibility' /></b>:&nbsp;&nbsp;{
                        <FormattedMessage id={`project.global.visibility.${project.accesibility}`} />
                    }
                    <br />
                    <b><FormattedMessage id='project.global.fields.creationDate' /></b>:&nbsp;&nbsp;
                    <FormattedDate value={new Date(project.creationDate)} /> - <FormattedTime value={new Date(project.creationDate)} />&nbsp;&nbsp;
                    <b><FormattedMessage id='project.global.fields.userNameBy' /></b>&nbsp;&nbsp;{project.userName}
                    {project.directory &&
                        <React.Fragment>
                            <br />
                            <br />
                            <b><FormattedMessage id='project.global.fields.uploadedOn' /></b>:&nbsp;&nbsp;
                            <FormattedDate value={new Date(project.directory.date)} /> - <FormattedTime value={new Date(project.directory.date)} />&nbsp;&nbsp;
                            <b><FormattedMessage id='project.global.fields.userNameBy' /></b>&nbsp;&nbsp;{project.directory.uploader}
                        </React.Fragment>
                    }
                </Typography>
                <Divider />
                <Typography className={classes.typographyDescription} color="primary">
                    {project.description}
                </Typography>
            </Card>
            <br />
            {projectId && project.directory &&
                <GetProjectPendingTasks projectId={projectId} />
            }
            {project.userName === userName &&
                <Card className={classes.content} variant="outlined">
                    {project.directory ?

                        <h6 className="card-title">
                            <FormattedMessage id='project.projects.ProjectDetails.updateFiles' />
                        </h6>
                        :
                        <h6 className="card-title">
                            <FormattedMessage id='project.projects.ProjectDetails.addFiles' />
                        </h6>
                    }
                    <AddDirectoryForm projectId={projectId} />
                </Card>
            }
        </div>
    );

}

export default ProjectDetails;
