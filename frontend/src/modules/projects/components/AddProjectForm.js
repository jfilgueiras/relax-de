import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Errors } from '../../common';
import * as actions from '../actions';
import {useHistory} from 'react-router-dom';
import Selector from './Selector';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';

const AddProjectForm = () => {

    const accesibilityList = [
        {
            id: 1,
            name: 'PUBLIC'
        },
        {
            id: 2,
            name: 'READ_ONLY'
        },
        {
            id: 3,
            name: 'PRIVATE'
        }
    ];
    
    const dispatch = useDispatch();
    const accesibilities = ["PUBLIC", "READ_ONLY", "PRIVATE"];
    const history = useHistory();
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [accesibility, setAccesibility] = useState(accesibilityList[0].name);
    const [backendErrors, setBackendErrors] = useState(null);
    let form;

    const handleSubmit = event => {

        event.preventDefault();

        if (form.checkValidity()) {

            dispatch(actions.addProject(name.trim(),
                description.trim(), accesibility,
                () => history.push('/projects/project-added/'),
                errors => setBackendErrors(errors)));

        } else {
            setBackendErrors(null);
            form.classList.add('was-validated');
        }

    }

    return (

        <div>
            <Errors errors={backendErrors}
                onClose={() => setBackendErrors(null)}/>
            <div className="card bg-light border-dark">
                <h5 className="card-header">
                    <FormattedMessage id="project.projects.AddProjectForm.title"/>
                </h5>
                <div className="card-body">
                    <form ref={node => form = node}
                        className="needs-validation" noValidate 
                        onSubmit={(e) => handleSubmit(e)}>
                        <div className="form-group row">
                            <label htmlFor="name" className="col-md-3 col-form-label">
                                <FormattedMessage id="project.global.fields.name"/>
                            </label>
                            <div className="col-md-4">
                                <input type="text" id="name" className="form-control"
                                    value={name}
                                    onChange={e => setName(e.target.value)}
                                    autoFocus
                                    required/>
                                <div className="invalid-feedback">
                                    <FormattedMessage id='project.global.validator.required'/>
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="description" className="col-md-3 col-form-label">
                                <FormattedMessage id="project.global.fields.description"/>
                            </label>
                            <div className="col-md-4">
                                <input type="text" id="description" className="form-control"
                                    value={description}
                                    onChange={e => setDescription(e.target.value)}
                                    />
                            </div>
                        </div>
                        <FormControl /*className={classes.formControl}*/>
                            <InputLabel id="acc-select-label">
                                <FormattedMessage id="project.global.fields.accesibility"/>
                            </InputLabel>
                            <Select
                                value={accesibility}
                                className="form-control"
                                labelId="acc-select-label"
                                label= {
                                    <FormattedMessage id="project.global.fields.accesibility"/>
                                }
                                onChange={(event) => {setAccesibility(event.target.value);}}
                            > 
                                {
                                    accesibilities.map((acc) => 
                                        <MenuItem key={acc} value={acc}>
                                            <FormattedMessage id={`project.global.visibility.${acc}`} />
                                        </MenuItem>
                                    )
                                }
                            </Select>
                        </FormControl>
                        <div className="form-group row">
                            <div className="offset-md-3 col-md-1">
                                <button type="submit" className="btn btn-primary">
                                    <FormattedMessage id="project.global.buttons.add"/>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    );

}

export default AddProjectForm;
