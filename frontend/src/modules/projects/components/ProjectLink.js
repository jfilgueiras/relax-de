import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    link: {
        color: theme.palette.primary.main,
    },

}));

const ProjectLink = ({id, name}) => {

    const classes = useStyles();
    return (
        <Link className={classes.link} to={`/projects/${id}`}>
            {name}
        </Link>
    );

}

ProjectLink.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
};

export default ProjectLink;