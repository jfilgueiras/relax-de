import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';

import * as actions from '../actions';
import * as selectors from '../selectors';
import {Pager} from '../../common';
import ProjectList from './ProjectList';

const FindUserProjectsResult = () => {
    
    const projectSearch = useSelector(selectors.getProjectSearch);
    const dispatch = useDispatch();

    if (!projectSearch) {
        return null;
    }

    if (projectSearch.result.items.length === 0) {
        return (
            <div className="alert alert-danger" role="alert">
                <FormattedMessage id='project.projects.findUserProjectsResult.noProjectsFound'/>
            </div>
        );
    }
    
    return (

        <div>
            <ProjectList projectlist={projectSearch.result.items}/>
            <Pager 
                back={{
                    enabled: projectSearch.criteria.page >= 1,
                    onClick: () => dispatch(actions.previousFindUserProjectsResultPage(projectSearch.criteria))}}
                next={{
                    enabled: projectSearch.result.existMoreItems,
                    onClick: () => dispatch(actions.nextFindUserProjectsResultPage(projectSearch.criteria))}}/>
        </div>

    );

}

export default FindUserProjectsResult;