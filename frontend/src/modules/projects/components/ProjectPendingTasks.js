import React from 'react';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import ProgressBar from '../../common/components/ProgressBar';
import { FormattedDate, FormattedTime } from 'react-intl';
import { Typography, Divider, LinearProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import StatusChip from '../../common/components/StatusChip';

const useStyles = makeStyles((theme) => ({
    typographyTags: {
        ...theme.typography.button,
        fontSize: 20,
        color: theme.palette.primary.dark,
        // paddingLeft: theme.spacing(8),
        paddingBottom: theme.spacing(4),
    },
    chip: {
        float: 'right'
    },
}));


const ProjectPendingTasks = ({ pendingTasks, handleDeleteFunc }) => {
 
    const classes = useStyles();
    return (
        <React.Fragment>
            {pendingTasks.map(pendingTask => (pendingTask.state === 'PENDING' || pendingTask.state === 'EXECUTING') &&
                <React.Fragment key={pendingTask.taskId}>
                    <Box p={5}>
                            <Typography className={classes.typographyTags} display='inline'>{<FormattedDate value={new Date(pendingTask.constructionDate)} />} - {<FormattedTime value={new Date(pendingTask.constructionDate)} />}</Typography>
                            <div style={{float:"right"}}>
                                <StatusChip taskId={pendingTask.taskId} status={pendingTask.state} handleDeleteFunc={handleDeleteFunc}></StatusChip>
                            </div>
                            <Typography display='block' variant="body1" color="textSecondary" >{pendingTask.configName}&nbsp;</Typography>
                            <br/>
                            {(pendingTask.state === 'PENDING' || pendingTask.state === 'EXECUTING') && <LinearProgress />}
                    </Box>
                    <Divider/>
                </React.Fragment>
            )}
        </React.Fragment>
    );
};

ProjectPendingTasks.propTypes = {
    pendingTasks: PropTypes.array.isRequired
};

export default ProjectPendingTasks;

