import React from 'react';
import {useSelector} from 'react-redux';
import {FormattedMessage} from 'react-intl';

import * as selectors from '../selectors';
import ProjectLink from './ProjectLink';

const ProjectAdded = () => {

    const projectInfo = useSelector(selectors.getAddedProjectInfo);

    if (!projectInfo) {
        return null;
    }
    
    return (
        <div className="alert alert-success" role="alert">
            <FormattedMessage id="project.projects.ProjectAdded.projectGenerated"/>:
            &nbsp;
            <ProjectLink id={projectInfo.id} name={projectInfo.name}/>
        </div>
    );

}

export default ProjectAdded;