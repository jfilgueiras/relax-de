import React, { useEffect, useState } from 'react';
import Schedule from 'react-schedule-job'
import 'react-schedule-job/dist/index.css'
import { useDispatch, useSelector } from 'react-redux';
import {FormattedMessage} from 'react-intl';
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import * as actions from '../actions';
import * as selectors from '../selectors';
import ProjectPendingTasks from './ProjectPendingTasks';

const useStyles = makeStyles((theme) => ({
    typographyAccordion: {
        ...theme.typography.button,
        fontSize: 12,
        color: theme.palette.primary.dark,
        paddingLeft: theme.spacing(1),
    },
    schedule: {
        display: "none"
    }
}));

const GetProjectPendingTasks = ({projectId}) => {

    const classes = useStyles();

    const dispatch = useDispatch();
    const projectPendingTasks = useSelector(selectors.getProjectPendingTasks);
    const [backendErrors, setBackendErrors] = useState(null);
    const [expanded, setExpanded] = React.useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
        if (isExpanded){
            dispatch(actions.getPendingTasks(projectId, onSuccess, onErrors));
        }
    };

    const handleDelete = (taskId) => {
        // dispatch(actions.deleteCompletedTask(taskId));
    };

    const retrieveTasks = (projectId, onSuccess, onErrors) => {
        dispatch(actions.getPendingTasks(projectId, onSuccess, onErrors));
    }

    const onSuccess = result => {
        console.log(result)
        // setProjectPendingTasks(result);
    }

    const onErrors = errors => {
        setBackendErrors(errors)
    }

    const job = [
        {
          fn: () => retrieveTasks(projectId, onSuccess, onErrors),
          id: '2',
          schedule: '* * * * *'
          // this runs every minute
        }
    ]
    
    useEffect(() => {
        if (projectId && !Number.isNaN(projectId)) {
            dispatch(actions.getPendingTasks(projectId, onSuccess, onErrors));
        }

    }, [projectId, dispatch]);

    let retVal;
    if (!projectPendingTasks || projectPendingTasks.length === 0) {
        retVal = null;
    } else {
        retVal = <>
            <Card className={classes.contentProject} variant="outlined">
                <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1bh-content"
                        id="panel1bh-header"
                    >
                        <Typography className={classes.typographyAccordion}><FormattedMessage id="project.projects.ProjectDetails.getProjectPendingTasks" /></Typography>
                    </AccordionSummary>
                    <Card className={classes.contentProject} variant="outlined">
                        <ProjectPendingTasks pendingTasks={projectPendingTasks} handleDeleteFunc={(taskId) => handleDelete(taskId)} />
                    </Card>
                </Accordion>
            </Card>
            <br/>
        </>
    }

    return (
        <>
            {retVal}
            {<Schedule
                className={classes.schedule}
                jobs={job}
                timeZone='UTC'
                // "UTC", "local" or "YOUR PREFERRED TIMEZONE",
                dashboard={{
                    hidden: true
                    // if true, dashboard is hidden
                }}
            />}
        </>
    )
};

export default GetProjectPendingTasks;