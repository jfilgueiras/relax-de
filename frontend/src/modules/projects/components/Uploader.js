import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import 'react-dropzone-uploader/dist/styles.css';
import Dropzone from 'react-dropzone-uploader';
import { FormattedMessage } from 'react-intl';

const useStyles = makeStyles((theme) => ({
    dropzone: {
        color: theme.palette.primary.dark,
        background: theme.palette.secondary.light,
        textTransform: 'uppercase',
    },
    dropzoneDefault: {
        borderColor: theme.palette.primary.main,
    }
}));

const Uploader = ({ maxFiles, handleSubmit }) => {
    const classes = useStyles();
    const mime = require('mime-types');
    // called every time a file's `status` changes
    const handleChangeStatus = ({ meta, file }, status) => { console.log(mime.contentType(file)) }

    return (
        <Dropzone 
            // className='no-overflow'
            onChangeStatus={handleChangeStatus}
            maxFiles={maxFiles}
            addClassNames={{inputLabel: classes.dropzone, dropzoneActive: classes.dropzoneDefault}}
            inputWithFilesContent={files => `${maxFiles - files.length} more`}
            submitButtonDisabled={files => files.length < maxFiles}
            onSubmit={handleSubmit}
            accept=".pdb"
            inputContent={(files, extra) => (extra.reject ? '.pdb for Protein Data Bank files' : <FormattedMessage id='project.projects.AddDirectory.DragFiles'/>)}
            styles={{
                dropzoneReject: { borderColor: 'red', backgroundColor: '#DAA' },
                inputLabel: (files, extra) => (extra.reject ? { color: 'red' } : {}),
            }}
        />
    )
}

Uploader.propTypes = {
    maxFiles: PropTypes.number.isRequired,
    handleSubmit: PropTypes.func.isRequired
};

export default Uploader;