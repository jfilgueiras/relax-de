const getModuleState = state => state.projects;

export const getProject = state =>
    getModuleState(state).project;

export const getProjectSearch = state =>
    getModuleState(state).projectSearch;

export const getAddedProjectInfo = state =>
    getModuleState(state).addedProjectInfo;

export const getProjectPendingTasks = state =>
    getModuleState(state).project.pendingTasks;