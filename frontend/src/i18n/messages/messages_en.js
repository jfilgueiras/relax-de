export default {

    'project.app.Footer.text': 'Relax-DE - Information Retrieval Lab - Universidade da Coruña',
    'project.app.Header.home': 'Home',
    'project.app.Header.logout': 'Logout',
    'project.app.Header.FindProject': 'Project List',
    'project.app.Header.AddProject': 'Add Project',
    'project.app.Home.welcome': 'Welcome to Relax-DE!',
    'project.app.Home.welcomeText': 'Relax-DE is an application for the refinement (Relax) of protein structures through Differential Evolution (DE). It combines the Rosetta Relax operators Repack and Minimize with the global search offered by the ED memetic algorithm. \nIt allows to optimise protein structures from an input (native structure or computer prediction), in \'.pdb\' format.',
    'project.app.Home.Relax': 'What is Relax?',
    'project.app.Home.RelaxText': 'Rosetta Relax is a protocol of the Rosetta software suite (developed by the RosettaCommons group at the University of Washington), which refines protein structures through two main processes: Repack and Minimize.\nFirst, it performs a substitution of rotamers, or predefined configurations of chi angles of side chains. Then, it performs small perturbations on the main chain to minimise the energy obtained and avoid possible steric clashes. This process is repeated multiple times, until a number of iterations is reached (by default, 5).',
    'project.app.Home.DifferentialEvolution': 'What is DE?',
    'project.app.Home.DEText': 'Differential Evolution (DE) is an algorithm of the evolutionary family, which optimises individuals of a population based on a fitness function. In this case, the energy value (fitness) of a protein structure (individual). To do this, the individual is coded from a vector of values (in this case, the angles of the main chain and side chains), and a process of mutation and recombination of the values that make up the individual is carried out iteratively (i.e. over multiple generations), giving rise to multiple crossovers that will be translated into test vectors. These test vectors are candidates for the next generation, and undergo a selection process that allows them to replace the individual of the same position in the original population. In this way, an optimised population of individuals is obtained through a global search process.',
    'project.display': 'Display type',
    'project.structure': 'Structure',
    'project.common.ErrorDialog.title': 'Error',
    'project.common.corpus': 'Corpus',
    'project.common.entities': 'Entities',
    'project.tabs.3DRender': '3D Models',
    'project.tabs.Charts': 'Result Charts',
    'project.tabs.Params': 'Parameters',
    'project.graph.evolutionPlot': 'Evolution Plot',
    'project.graph.scatterPlot': 'Scatter Plot',
    'project.global.buttons.cancel': 'Cancel',
    'project.global.buttons.close': 'Close',
    'project.global.buttons.next': 'Next',
    'project.global.buttons.edit': 'Edit',
    'project.global.buttons.ok': 'OK',
    'project.global.buttons.back': 'Back',
    'project.global.buttons.save': 'Save',
    'project.global.buttons.add': 'Add',
    'project.global.button.create': 'Create',
    'project.global.button.rename': 'Rename',
    'project.global.buttons.changeName': 'Change name',
    'project.global.buttons.addNode': 'Add node',
    'project.global.buttons.deleteNode': 'Delete node',
    'project.global.buttons.search': 'Search',
    'project.global.buttons.review': 'Review',
    'project.global.buttons.submit': 'Submit',
    'project.global.buttons.apply': 'Apply',

    'project.global.visibility.PRIVATE': 'Private',
    'project.global.visibility.READ_ONLY': 'Read only',
    'project.global.visibility.PUBLIC': 'Public',

    'project.global.exceptions.NetworkError': 'Network error',
    'project.global.fields.date': 'Date',
    'project.global.fields.email': 'Email address',
    'project.global.fields.firstName': 'First name',
    'project.global.fields.lastName': 'Last name',
    'project.global.fields.name': 'Name',
    'project.global.fields.password': 'Password',
    'project.global.fields.postalAddress': 'Postal address',
    'project.global.fields.postalCode': 'Postal code',
    'project.global.fields.userName': 'User',
    'project.global.fields.userNameBy': 'by',
    'project.global.fields.accesibility': 'Accessibility',
    'project.global.fields.creationDate': 'Date of Creation',
    'project.global.fields.uploadedOn': 'Last files uploaded on',
    'project.global.fields.uploader': 'Uploaded by',
    'project.global.fields.description': 'Description',
    'project.global.fields.skipCorpusProcess': 'Skip corpus processing',
    'project.global.fields.skipExtractFeatures': 'Skip feature extraction',
    'project.global.fields.taxonPrefix': 'Prefix',
    'project.global.fields.maxIterations': 'Max number of iterations',
    'project.global.fields.useType': 'Use Type Features',
    'project.global.fields.useGlobalRefEdges': 'Use global ref edges',
    'project.global.fields.numInitialEdge': 'Number of nephew nodes for depth expansion',
    'project.global.fields.numInitialNode': 'Number of initial children for depth expansion',
    'project.global.fields.seed': 'Seed',
    'project.global.fields.querySearch': 'Query text',

    'project.global.fields.configName': 'Configuration name',
    'project.global.fields.preserveConfig': 'Save this configuration?',
    'project.global.fields.disturbDegrees': 'Distotion degrees of dihedric angles in initial population',
    'project.global.fields.scheme': 'Selection scheme of x{1} to generate each generation candidate crossover',
    'project.global.fields.popsize': 'Population size',
    'project.global.fields.f': 'Mutation factor',
    'project.global.fields.cr': 'Crossover probability',
    'project.global.fields.gens': 'Number of generations',
    'project.global.fields.tag': 'Optional tag',
    'project.global.fields.logStats': 'Save intermediate statistics of refinement process? (advanced)',
    'project.global.fields.executeEach': 'Each how many generations will the Repack&Minimize step apply?',
    'project.global.fields.executionsPerGen': 'How many times will the Repack&Minimize step apply each time?',
    'project.global.fields.popToRepackmin': 'What percentage of population will the Repack&Minimize apply to?',
    'project.global.fields.popToRepackminHelper': '(Percentage, between 0-100. Will apply to the individuals with the worst fitness)',
    'project.global.fields.usePrevConfig': 'Previous configurations',

    'project.global.validator.email': 'Provide a correct e-mail address',
    'project.global.validator.passwordsDoNotMatch': 'Passwords do not match',
    'project.global.validator.required': 'Required field',
    'project.global.validator.incorrectQuantity': 'Incorrect quantity',

    'project.users.ChangePassword.fields.confirmNewPassword': 'Confirm new password',
    'project.users.ChangePassword.fields.newPassword': 'New password',
    'project.users.ChangePassword.fields.oldPassword': 'Old password',
    'project.users.ChangePassword.title': 'Change password',
    'project.users.Login.title': 'Login',
    'project.users.SignUp.fields.confirmPassword': 'Confirm password',
    'project.users.SignUp.fields.notRegisteredYet': 'Not registered yet?',
    'project.users.SignUp.title': 'Sign up',
    'project.users.UpdateProfile.title': 'Update profile',
    'project.projects.AddDirectory.DragFiles': 'Drag your files here',
    'project.projects.ProjectDetails.addFiles': 'Add Protein Data Bank (.pdb) File ',
    'project.projects.ProjectDetails.getProjectPendingTasks': 'Refinements in progress',
    'project.projects.ProjectDetails.findRefinementCallsLink': 'List of completed refinements',
    'project.projects.findUserProjectsResult.noProjectsFound':'No project data was found.',
    'project.projects.AddProjectForm.title':'Add Project',
    'project.projects.ProjectAdded.projectGenerated':'Added project with name',
    'project.projects.ProjectAdded.ProjectAdded':'¡Project created successfully!',
    'project.projects.EditProject.EditSuccessful':'¡Project updated successfully!',

    'project.projects.AddDirectoryForm.uploadSuccessful':'File/s uploaded successfully',
    'project.projects.ProjectDetails.findConstructionCallsLink': 'List of calls',
    'project.projects.ProjectDetails.deleteProject': 'Delete project',
    'project.projects.ProjectDetails.updateProject': 'Update project',
    'project.projects.ProjectDetails.executeRefinementLink': 'Execute refinement',
    'project.projects.CallCreated.pendingCall': 'Call sent. Please, wait for the construction to finish. Check your progress in the pending tasks popup.',
    
    'project.refinements.ExecuteRefinement.DifferentialEvolutionParamsShort': 'DE',
    'project.refinements.ExecuteRefinement.DifferentialEvolutionParams': 'DE parameters',
    'project.refinements.ExecuteRefinement.RelaxParamsShort': 'Relax',
    'project.refinements.ExecuteRefinement.RelaxParams': 'Relax Parameters',
    'project.refinements.ExecuteRefinement.ExecConfigParamsShort': 'Config',
    'project.refinements.ExecuteRefinement.ExecConfigParams': 'General configuration',
    'project.refinements.PendingTasksDropdown.getPendingTasks': 'Pending tasks',
    'project.refinements.findRefinementsCallsResult.noRefinementCallsFound': 'No refinement calls were made yet.',
    'project.refinements.getPendingTasksResult.noTasksFound': 'No tasks found.',
    'project.refinements.ExecuteRefinement.title': 'Execute Refinement',
    'project.refinements.RefinementCallList.NoConfigName': 'No configuration name',
    'project.refinements.ExecuteRefinement.ReviewParams': 'Review parameters',
    'project.refinements.download': 'Download Results',
    'project.refinements.findRefinementCallsResult.noRefinementCallsFound': 'No completed refinements made by you',

    'project.refinements.GetRefinementDetail.native': 'Initial structure',
    'project.refinements.GetRefinementDetail.result': 'Best result',
    'project.projects.ProjectDetails.downloadResults': 'Download refinement results',
    
    'project.index.DoFacetedSearch': 'Do faceted search',
    'project.index.FacetedSearchResult.totalHits': 'Retrieved docs',
    'project.index.FacetedSearchResult.maxScore': 'Max. score',
    'project.index.FacetedSearchResult.docNo': 'Doc ID',
    'project.index.FacetedSearchResult.score': 'Score',
    'project.index.FacetedSearchResult.noTags': 'No tags',
    'project.index.FacetedSearchResult.tags': 'Tags',
    'project.index.FacetedSearchResult.tagsInThisList': 'All tags',
    'project.index.FacetedSearchResult.more': 'More...',
    'project.index.FacetedSearchResult.noSearchResults': 'No results found for this search.',

    'project.global.fields.useGlobalRefEdgesAbbr': 'Use global ref edges',
    'project.global.fields.maxIterationsAbbr': 'No. máx its.',
    'project.global.fields.numInitialEdgeAbbr': 'No. sibling for depth',
    'project.global.fields.numInitialNodeAbbr': 'Nº children for expansion',
}
