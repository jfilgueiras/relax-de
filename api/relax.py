#!/usr/bin/env python
# coding: utf-8

import configparser
import logging
import os
import sys
import random

import numpy as np
from mpi4py import MPI
from pyrosetta import init, Pose, create_score_function
from pyrosetta.rosetta.core.scoring import CA_rmsd, ScoreType, fa_rep, coordinate_constraint
from pyrosetta.rosetta.core.kinematics import MoveMap
from pyrosetta.rosetta.core.pack.task import TaskFactory
from pyrosetta.rosetta.core.pack.task.operation import TaskOperation, AppendRotamerSet, ExtraRotamersGeneric, InitializeFromCommandline, IncludeCurrent, RestrictToRepacking, NoRepackDisulfides
from pyrosetta.rosetta.core.pack.rotamer_set import UnboundRotamersOperation
from pyrosetta.rosetta.core.pack.dunbrack import load_unboundrot
from pyrosetta.rosetta.protocols.minimization_packing import PackRotamersMover
from pyrosetta.rosetta.protocols.relax import FastRelax
from pyrosetta.rosetta.protocols.simple_moves import SwitchResidueTypeSetMover
from pyrosetta.rosetta.protocols.minimization_packing import MinMover
from src.reader import StructureReader
from tqdm import tqdm

from src.differential_evolution import DifferentialEvolutionAlgorithm as DE
from src.init_random_positions import start_input_poses
from src.local_search import AbinitioBuilder
# from single_process import SingleMasterProcess as MasterProcess
from src.mpi_utils import MasterProcess, Worker
from src.population import ScorePopulation
from src.scfxn_psp import PSPFitnessFunction

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()


MAIN_PATH = os.getcwd()


def read_config(ini_file):
    config = configparser.ConfigParser()
    config.read(ini_file, encoding="utf-8-sig")
    return config

logging.basicConfig(level=logging.ERROR)
# logging.disable(logging.INFO)

def dump_popul(popul, tag):
    for i, ind_pose in enumerate(popul):
        ind_pose.dump_pdb("./resultados/{}_cart_fa_{}.pdb".format(tag,i))

def init_options(reference_input):
    opts = [
        # "-mute all",
        "-ignore_unrecognized_res",
        "-score::weights ref2015",
        # "-in:file:centroid_input",
        # "-nonideal true",
        # "-corrections:restore_talaris_behavior",
        # "-abinitio::rg_reweight 0.5",
        # "-abinitio::rsd_wt_helix 0.5",
        # "-abinitio::rsd_wt_loop 0.5",
        # "-abinitio::relax false",
        # "-output_secondary_structure true",
        # "-do_not_autoassign_SS true",
    ]
    return " ".join(opts)

def make_task_factory():
    local_tf = TaskFactory()
    local_tf.push_back(InitializeFromCommandline())
    local_tf.push_back(IncludeCurrent())
    local_tf.push_back(RestrictToRepacking())
    local_tf.push_back(NoRepackDisulfides())
    extrarot = ExtraRotamersGeneric()
    extrarot.ex1(True)
    extrarot.ex2aro(True)
    local_tf.push_back(extrarot)
    unboundrot = UnboundRotamersOperation()
    unboundrot.initialize_from_command_line()
    unboundrot_op = AppendRotamerSet(unboundrot)
    local_tf.push_back(unboundrot_op)
    return local_tf

def make_minimizer(move_map, sfxn, min_type, cartesian, max_iter, tolerance):
    min_mover = MinMover(move_map, sfxn, min_type, tolerance, cartesian)
    min_mover.max_iter(max_iter)
    return min_mover

def apply_repack(local_tf, pose, sfxn):
    load_unboundrot(pose) # adds scoring bonuses for the "unbound" rotamers, if any
    packer_task = local_tf.create_task_and_apply_taskoperations(pose)
    pack = PackRotamersMover( sfxn, packer_task )
    pack.apply(pose)

def apply_minimizer(min_mover, pose):
    min_mover.apply(pose)

def get_input_pose(pdb_filename):
    st_reader = StructureReader()
    print("pose input {} ".format(pdb_filename))
    pose = st_reader.get_fa_from_file(pdb_filename)
    return pose

def disturb_individual(pose, degrees):
    for res in range(2, pose.total_residue()):
        phi = pose.phi(res)
        psi = pose.psi(res)
        if np.random.uniform() > 0.5:
            pose.set_phi(res, phi + (np.random.uniform()*degrees))
        else:
            pose.set_phi(res, phi - (np.random.uniform()*degrees))
        if np.random.uniform() > 0.5:
            pose.set_psi(res, psi + (np.random.uniform()*degrees))
        else:
            pose.set_psi(res, psi - (np.random.uniform()*degrees))

def do_relax(native_pose, popul, scfxn):
    # min_type = "dfpmin_armijo_nonmonotone"
    min_type = "lbfgs_armijo_nonmonotone"
    move_map = MoveMap()
    move_map.set_bb(True)
    move_map.set_chi(True)
    move_map.set_jump(True)
    cartesian = False
    tolerance = 0.1
    max_iter = 2000
    minimizer = make_minimizer(move_map, scfxn, min_type, cartesian, max_iter, tolerance)
    repacker = make_task_factory()
    for idx, ind_pose in tqdm(enumerate(popul)):
        # repeat 5
        for i in range (1,5):
            # coord_cst_weight 1.0
            scfxn.set_weight(coordinate_constraint, 1.0)
            # scale:fa_rep 0.040
            scfxn.set_weight(fa_rep, .55 * .040)
            # repack
            apply_repack(repacker, ind_pose, scfxn)
            # scale:fa_rep 0.051
            scfxn.set_weight(fa_rep, .55 * .051)
            # min 0.01
            minimizer.tolerance(.01)
            apply_minimizer(minimizer, ind_pose)
            # coord_cst_weight 0.5
            scfxn.set_weight(coordinate_constraint, .5)
            # scale:fa_rep 0.265
            scfxn.set_weight(fa_rep, .55 * .265)
            # repack
            apply_repack(repacker, ind_pose, scfxn)
            # scale:fa_rep 0.280
            scfxn.set_weight(fa_rep, .55 * .280)
            # min 0.01
            minimizer.tolerance(.01)
            apply_minimizer(minimizer, ind_pose)
            # coord_cst_weight 0.0
            scfxn.set_weight(coordinate_constraint, 0)
            # scale:fa_rep 0.559
            scfxn.set_weight(fa_rep, .55 * .559)
            # repack
            apply_repack(repacker, ind_pose, scfxn)
            # scale:fa_rep 0.581
            scfxn.set_weight(fa_rep, .55 * .581)
            # min 0.01
            minimizer.tolerance(.01)
            apply_minimizer(minimizer, ind_pose)
            # coord_cst_weight 0.0
            scfxn.set_weight(coordinate_constraint, 0)
            # scale:fa_rep 1
            scfxn.set_weight(fa_rep, .55)
            # repack
            apply_repack(repacker, ind_pose, scfxn)
            # min 0.00001
            minimizer.tolerance(.00001)
            apply_minimizer(minimizer, ind_pose)
            # accept_to_best
            ind_pose.dump_pdb("./resultados/{}_{}_cart_fa_{}.pdb".format(ind_pose.pdb_info().name().split('/')[-1],"relax_lbfgs",idx))


    return popul

def get_initial_popul(native_pose, max_popul, scfxn):
    initial_popul = [Pose(native_pose) for i in range(max_popul)]
    # min_type = "dfpmin_armijo_nonmonotone"
    min_type = "lbfgs_armijo_nonmonotone"
    move_map = MoveMap()
    move_map.set_bb(True)
    move_map.set_chi(True)
    cartesian = False
    tolerance = 0.1
    max_iter = 5
    # minimizer = make_minimizer(move_map, scfxn, min_type, cartesian, max_iter, tolerance)
    # repacker = make_task_factory()
    #for i, ind_pose in enumerate(initial_popul):
        # disturb_individual(ind_pose)
        # apply_repack(repacker, ind_pose, scfxn)
        # apply_minimizer(minimizer, ind_pose)
    return initial_popul

def main():
    config = read_config(sys.argv[-1])
    max_popul = int(config["DE"].get("popsize"))
    disturb_degrees = int(config["initial"].get("disturb_degrees"))

    # max_popul = 1000
    if config.has_option("inputs", "reference_input"):
        # reference_input = MAIN_PATH + "/" + config["inputs"].get("reference_input")
        reference_input = config["inputs"].get("reference_input")
    else:
        reference_input = ""

    # pose_input = MAIN_PATH + "/" + config["inputs"].get("pose_input")
    pose_input = config["inputs"].get("pose_input")

    init(extra_options=init_options(reference_input))
    native_pose = get_input_pose(pose_input)
    scfxn_ref2015 = create_score_function("ref2015")
    init_popul = get_initial_popul(native_pose, max_popul, scfxn_ref2015)
    relax_popul = do_relax(pose_input, init_popul, scfxn_ref2015)
    #dump_popul(relax_popul, "relax")

if __name__ == "__main__":
    main()
