#!/usr/bin/env python
# coding: utf-8

import configparser
import logging
import os
import sys
import random
import time
from pathlib import Path

import numpy as np
from mpi4py import MPI
from pyrosetta import init, Pose, create_score_function, pose_from_file
from pyrosetta.rosetta.core.scoring import CA_rmsd, ScoreType, fa_rep, coordinate_constraint
from pyrosetta.rosetta.core.kinematics import MoveMap
from pyrosetta.rosetta.core.pack.task import TaskFactory
from pyrosetta.rosetta.core.pack.task.operation import TaskOperation, AppendRotamerSet, ExtraRotamersGeneric, InitializeFromCommandline, IncludeCurrent, RestrictToRepacking, NoRepackDisulfides
from pyrosetta.rosetta.core.pack.rotamer_set import UnboundRotamersOperation
from pyrosetta.rosetta.core.pack.dunbrack import load_unboundrot
from pyrosetta.rosetta.protocols.minimization_packing import PackRotamersMover
from pyrosetta.rosetta.protocols.relax import FastRelax
from pyrosetta.rosetta.protocols.simple_moves import SwitchResidueTypeSetMover
from pyrosetta.rosetta.protocols.minimization_packing import MinMover
from tqdm import tqdm

# from src.reader import StructureReader
from src.differential_evolution import DifferentialEvolutionAlgorithm as DE
from src.deoptions import DEOptions, DEOptionsBuilder
# from single_process import SingleMasterProcess as MasterProcess
# from src.mpi_utils import MasterProcess, Worker
# from src.population import ScorePopulation
from src.scfxn_psp import PSPFitnessFunction

# Timeout manager and main
import signal
from contextlib import contextmanager
from dateutil import parser as timeparser

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
MAIN_PATH = os.getcwd()

de:DE

def read_config(ini_file):
    config = configparser.ConfigParser()
    config.read(ini_file, encoding="utf-8-sig")
    return config

# logging.basicConfig(level=logging.ERROR)
# logging.disable(logging.INFO)

def dump_popul(popul, tag):
    for i, ind_pose in enumerate(popul):
        ind_pose.dump_pdb(tag + "/cart_fa_{}.pdb".format(i))

def init_options():
    opts = [
        "-mute all",
        "-ignore_unrecognized_res",
        "-score::weights ref2015",
        # "-in:file:centroid_input",
        # "-nonideal true",
        # "-corrections:restore_talaris_behavior",
        # "-abinitio::rg_reweight 0.5",
        # "-abinitio::rsd_wt_helix 0.5",
        # "-abinitio::rsd_wt_loop 0.5",
        # "-abinitio::relax false",
        # "-output_secondary_structure true",
        # "-do_not_autoassign_SS true",
    ]
    return " ".join(opts)

def make_task_factory():
    local_tf = TaskFactory()
    local_tf.push_back(InitializeFromCommandline())
    local_tf.push_back(IncludeCurrent())
    local_tf.push_back(RestrictToRepacking())
    # local_tf.push_back(NoRepackDisulfides())
    # extrarot = ExtraRotamersGeneric()
    # extrarot.ex1(True)
    # extrarot.ex2aro(True)
    # local_tf.push_back(extrarot)
    unboundrot = UnboundRotamersOperation()
    unboundrot.initialize_from_command_line()
    unboundrot_op = AppendRotamerSet(unboundrot)
    local_tf.push_back(unboundrot_op)
    return local_tf

def make_minimizer(move_map, sfxn, min_type, cartesian, max_iter, tolerance):
    min_mover = MinMover(move_map, sfxn, min_type, tolerance, cartesian)
    min_mover.max_iter(max_iter)
    return min_mover

def apply_repack(local_tf, pose, sfxn):
    load_unboundrot(pose) # adds scoring bonuses for the "unbound" rotamers, if any
    packer_task = local_tf.create_task_and_apply_taskoperations(pose)
    pack = PackRotamersMover( sfxn, packer_task )
    pack.apply(pose)

def apply_minimizer(min_mover, pose):
    min_mover.apply(pose)

# def get_input_pose(pdb_filename):
#     st_reader = StructureReader()
#     print("pose input {} ".format(pdb_filename))
#     pose = st_reader.get_fa_from_file(pdb_filename)
#     return pose

def get_initial_popul(native_pose, max_popul, scfxn):
    initial_popul = [Pose(native_pose) for i in range(max_popul)]
    # min_type = "dfpmin_armijo_nonmonotone"
    min_type = "lbfgs_armijo_nonmonotone"
    move_map = MoveMap()
    move_map.set_bb(True)
    move_map.set_chi(True)
    cartesian = False
    tolerance = 0.1
    max_iter = 5
    # minimizer = make_minimizer(move_map, scfxn, min_type, cartesian, max_iter, tolerance)
    # repacker = make_task_factory()
    #for i, ind_pose in enumerate(initial_popul):
        # disturb_individual(ind_pose)
        # apply_repack(repacker, ind_pose, scfxn)
        # apply_minimizer(minimizer, ind_pose)
    return initial_popul

def do_relax(native_pose, popul, scfxn):
    # min_type = "dfpmin_armijo_nonmonotone"
    min_type = "lbfgs_armijo_nonmonotone"
    move_map = MoveMap()
    move_map.set_bb(True)
    move_map.set_chi(True)
    move_map.set_jump(True)
    cartesian = False
    tolerance = 0.1
    max_iter = 2000
    minimizer = make_minimizer(move_map, scfxn, min_type, cartesian, max_iter, tolerance)
    repacker = make_task_factory()
    coord_start = 0
    score = create_score_function("ref2015")
    # repeat 5
    for i in tqdm(range(0,5)):
        for idx, ind_pose in enumerate(popul):
            # coord_cst_weight 1.0
            scfxn.set_weight(coordinate_constraint, 1.0 * coord_start)
            # scale:fa_rep 0.040 // REPACK
            scfxn.set_weight(fa_rep, .55 * .040)
            # repack
            apply_repack(repacker, ind_pose, scfxn)
            # scale:fa_rep 0.051 // MIN
            scfxn.set_weight(fa_rep, .55 * .051)
            # min 0.01
            minimizer.tolerance(.01)
            apply_minimizer(minimizer, ind_pose)
            # coord_cst_weight 0.5
            scfxn.set_weight(coordinate_constraint, .5 * coord_start)
            # scale:fa_rep 0.265 // REPACK
            scfxn.set_weight(fa_rep, .55 * .265)
            # repack
            apply_repack(repacker, ind_pose, scfxn)
            # scale:fa_rep 0.280 // MIN
            scfxn.set_weight(fa_rep, .55 * .280)
            # min 0.01
            minimizer.tolerance(.01)
            apply_minimizer(minimizer, ind_pose)
            # coord_cst_weight 0.0
            scfxn.set_weight(coordinate_constraint, 0)
            # scale:fa_rep 0.559 // REPACK
            scfxn.set_weight(fa_rep, .55 * .559)
            # repack
            apply_repack(repacker, ind_pose, scfxn)
            # scale:fa_rep 0.581 // MIN
            scfxn.set_weight(fa_rep, .55 * .581)
            # min 0.01
            minimizer.tolerance(.01)
            apply_minimizer(minimizer, ind_pose)
            # coord_cst_weight 0.0
            scfxn.set_weight(coordinate_constraint, 0)
            # scale:fa_rep 1
            scfxn.set_weight(fa_rep, .55)
            # repack
            apply_repack(repacker, ind_pose, scfxn)
            # min 0.00001
            minimizer.tolerance(.00001)
            apply_minimizer(minimizer, ind_pose)
            # accept_to_best
            # ind_pose.dump_pdb("./resultados/{}_{}_cart_fa_{}.pdb".format(ind_pose.pdb_info().name().split('/')[-1],"relax_lbfgs",idx))
        for p in popul:
            print(str(score.score(p)) + ", ")
    return popul

class TimeoutException(Exception): pass

@contextmanager
def time_limit(seconds):
    if not seconds: yield
    def signal_handler(signum, frame):
        raise TimeoutException("Timed out!")
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)

def get_time_limit(config):
    if not config.has_option("exec", "time_limit"): return 0
    tl_str = config["exec"].get("time_limit")
    if not tl_str: return 0
    time = timeparser.parse(tl_str)
    return time.hour*3600 + time.minute*60 + time.second

def get_de_completion_status():
    return float(de.current_gen/de.generations) * 100.0

def de_relax(asyncTask, config_filename='sample_psp.ini', tstamp=time.strftime("%Y%m%d-%H%M%S")):
    config = read_config(config_filename)
    print(config_filename)
    print({section: dict(config[section]) for section in config.sections()})
    disturb_degrees = config["initial"].getfloat("disturb_degrees")
    reference_input = config["inputs"].get("reference_input") if config.has_option("inputs", "reference_input") else ""
    init(extra_options=init_options())
    scheme = config["DE"].get("scheme")
    popsize = config["DE"].getint("popsize")
    mutate = config["DE"].getfloat("mutate")
    recombination = config["DE"].getfloat("recombination")
    generations = config["DE"].getint("generations")
    selection = config["DE"].get("selection")
    pose_input = config["inputs"].get("pose_input")
    native_input = config["inputs"].get("native_input") if config.has_option("inputs", "native_input") else pose_input
    extra_tag = config["DE"].get("extra_tag") if config.has_option("DE", "extra_tag") else ""
    execute_each = config["Relax"].getint("execute_each") if config.has_option("Relax", "execute_each") else 0
    repack_scale_fa_rep = [str(value) for value in config["Relax"].get("repack_fa_rep_scale").split(',')] \
        if config.has_option("Relax", "repack_fa_rep_scale") else []
    min_scale_fa_rep = [str(value) for value in config["Relax"].get("min_fa_rep_scale").split(',')] \
        if config.has_option("Relax", "min_fa_rep_scale") else []
    pop_to_repackmin = config["Relax"].getfloat("pop_to_repackmin") \
        if config.has_option("Relax", "pop_to_repackmin") else 1
    popul_executor_chain = [value for value in config["Relax"].get("popul_executor_chain").split(',')] \
        if config.has_option("Relax", "popul_executor_chain") else []
    trial_executor_chain = [value for value in config["Relax"].get("trial_executor_chain").split(',')] \
        if config.has_option("Relax", "trial_executor_chain") else []
    aa_proportion_repmin = config["Relax"].getfloat("aa_proportion_repmin") \
        if config.has_option("Relax", "aa_proportion_repmin") else 1
    log_stats = config["DE"].getboolean("log_stats")
    executions_per_gen = config["Relax"].getint("executions_per_gen")
    min_maxiter = config["Relax"].getint("min_maxiter")
    check_execution_improvement = config["Relax"].getboolean("check_execution_improvement")
    sort_worst_eterms = config["Relax"].getboolean("sort_worst_eterms") \
        if config.has_option("Relax", "sort_worst_eterms") else False
    probabilistic_rep_min = config["Relax"].getboolean("probabilistic_rep_min") \
        if config.has_option("Relax", "probabilistic_rep_min") else False
    log_path = config["outputs"].get("results_path") \
        if config.has_option("outputs", "results_path") else './resultados/'

    native_pose = pose_from_file(pose_input)
    init_state_pose = pose_from_file(native_input)
    stage = 'stage_relax'
    results_folder = log_path + tstamp + '/log/'
    pdbs_folder = log_path + tstamp + '/samples/'
    de_options:DEOptions = DEOptionsBuilder()\
        .scheme(scheme) \
        .popsize(popsize) \
        .mutate(mutate) \
        .recombination(recombination) \
        .generations(generations) \
        .selection_opt(selection) \
        .disturb_degrees(disturb_degrees) \
        .execute_each(execute_each) \
        .repack_scale_fa_rep(repack_scale_fa_rep) \
        .min_scale_fa_rep(min_scale_fa_rep) \
        .min_maxiter(min_maxiter) \
        .pop_to_repackmin(pop_to_repackmin) \
        .popul_executor_chain(popul_executor_chain) \
        .trial_executor_chain(trial_executor_chain) \
        .log_stats(log_stats) \
        .check_improvement(check_execution_improvement) \
        .executions_per_gen(executions_per_gen) \
        .aa_proportion_repmin(aa_proportion_repmin) \
        .sort_worst_eterms(sort_worst_eterms) \
        .probabilistic_rep_min(probabilistic_rep_min) \
        .evo_file(results_folder + 'evo_' + extra_tag + '.log') \
        .time_file(results_folder + 'time_' + extra_tag + '.log') \
        .debug_file(results_folder + 'debug_' + extra_tag + '.log') \
        .pdb_dump_path(pdbs_folder) \
        .extra_tag(extra_tag) \
        .tstamp(tstamp) \
        .build()

    Path(results_folder).mkdir(parents=True, exist_ok=True)
    Path(pdbs_folder).mkdir(parents=True, exist_ok=True)
    print(de_options.debug_file)
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(filename=de_options.debug_file, level=logging.INFO, format='%(message)s')
    logger = logging.getLogger("ProtRefDE")
    logger.setLevel(logging.INFO)
    print(logger.handlers)


    popul_calculator = PSPFitnessFunction(stage,
        native_pose,
        de_options.popul_executor_chain,
        de_options.trial_executor_chain,
        log_stats)
    logger.info("==============================")
    logger.info(" init the params ")
    logger.info(" starts stage {}".format(stage))
    logger.info(
        " init_state pose {:.2f}".format(
            popul_calculator.scfxn_rosetta.score(init_state_pose)
        )
    )
    de = DE(
        popul_calculator,
        de_options,
        asyncTask
    )
    de.init_population()
    # --- RUN -----------------------------------------+
    logger.info("==============================")
    logger.info(" start evolution algorithm stage ")
    logger.info(
        " native pose {:.2f}".format(popul_calculator.scfxn_rosetta.score(native_pose))
    )
    final_poses = []
    try:
        with time_limit(get_time_limit(config)):
            de.main()
    except TimeoutException as e:
        logger.info("Timed out!")
    finally:
        logger.info("Results")
        final_inds = de.population
        de.evaluate_population("final", True)
        final_poses = [ind.get_pose_object() for ind in final_inds]
        # dump_tag = './resultados/' + str(native_pose.pdb_info().name().split('/')[-1]) + \
        #            "_gen" + str(generations) + "_scheme" + scheme + "extra_" + extra_tag
        dump_tag = de_options.pdb_dump_path
        dump_popul(final_poses, dump_tag)
        with open(de_options.evo_file, "a") as file_evo:
            file_evo.write("[GEN] %d \n" % 99999)
            file_evo.write("[POP] ")
            for ind in de.population:
                file_evo.write(str(ind.score) + ', ')
            file_evo.write("\n[RMSD_NATIVE] ")
            for ind in de.population:
                file_evo.write(str(ind.rmsd) + ', ')
            file_evo.write('\n')

def main():
    de_relax(sys.argv[-1])

def main_relax():
    config = read_config(sys.argv[-1])
    max_popul = int(config["DE"].get("popsize"))
    disturb_degrees = int(config["initial"].get("disturb_degrees"))

    # max_popul = 1000
    if config.has_option("inputs", "reference_input"):
        # reference_input = MAIN_PATH + "/" + config["inputs"].get("reference_input")
        reference_input = config["inputs"].get("reference_input")
    else:
        reference_input = ""

    # pose_input = MAIN_PATH + "/" + config["inputs"].get("pose_input")
    pose_input = config["inputs"].get("pose_input")

    init(extra_options=init_options())
    # native_pose = get_input_pose(pose_input)
    scfxn_ref2015 = create_score_function("ref2015")
    init_popul = get_initial_popul(native_pose, max_popul, scfxn_ref2015)
    relax_popul = do_relax(pose_input, init_popul, scfxn_ref2015)
    scfxn_ref2015 = create_score_function("ref2015")
    for p in relax_popul:
        print(str(scfxn_ref2015.score(p)) + ", ", end=" ")
    #dump_popul(relax_popul, "relax")

if __name__ == "__main__":
    main()

