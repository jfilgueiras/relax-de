#!usr/bin/env python

import logging
from typing import Optional
import random

import numpy as np
from pyrosetta import Pose, create_score_function
from pyrosetta.rosetta.core.scoring import CA_rmsd, ScoreType

from src.new_individual import Individual
from src.rosetta.rosettaexecutor import RosettaExecutor, Repacker, Minimizer
from src.rosetta.statslogger import StatsLogger
from src.rosetta.executionoptions import ExecutionOptions
from pyrosetta.rosetta.core.scoring import fa_rep, fa_sol, fa_atr
from src.utils import minmax_norm
from itertools import compress

def create_executor(executor_chain, log_stats):
    first = None
    current = None
    _next = None
    for element in executor_chain:
        if element.strip() == 'rep':
            _next = Repacker()
        elif element.strip() == 'min':
            _next = Minimizer()
        # if log_stats and _next:
        #     _next = StatsLogger(_next)
        if not first and _next:
            first = _next
            first.declare_first()
        if current:
            # if log_stats: current.executor.next_executor = _next
            # else: current.next_executor = _next
            current.next_executor = _next
        current = _next
    return first



class PSPFitnessFunction:
    def __init__(self, stage, native_pose, popul_executor_chain, trial_executor_chain, log_stats):
        self.logger = logging.getLogger("evodock.scfxn")
        self.native_pose = native_pose
        self.chi_per_res = Individual.get_chis_per_res(native_pose)
        self.native_ind = Individual.from_pose(0, native_pose, chis_per_res=self.chi_per_res)
        self.logger.setLevel(logging.INFO)
        self.scfxn_rosetta = self.get_score_function(stage)
        self.scfxn_scoring = self.get_score_function(stage)
        self.popul_executor = create_executor(popul_executor_chain, log_stats)
        self.trials_executor = create_executor(trial_executor_chain, log_stats)

    def get_score_function(self, stage):
        scfxn = create_score_function("ref2015")
        if stage == "stage1":
            scfxn = create_score_function("score0")
        if stage == "stage2":
            scfxn = create_score_function("score1")
        if stage == "stage3":
            scfxn = create_score_function("score2")
        if stage == "stage4":
            scfxn = create_score_function("score3")
        if stage == "stage_relax":
            scfxn = create_score_function("ref2015")
        return scfxn

    def slice(self, popul, proportion):
        if proportion:
            popul.sort(key=lambda x: x.score, reverse=True)
            sorted_slice = popul[0:int(len(popul)*proportion)]
            popul.sort(key=lambda x: x._id, reverse=False)
        else:
            sorted_slice = popul
        return sorted_slice

    def get_aa_proportion_for_repmin(self, ind, options):
        aas = list(range(1, ind.pose.total_residue()+1))
        proportion = round(options.aa_proportion_repmin * ind.pose.total_residue())
        if options.sort_worst_eterms or options.probabilistic_rep_min:
            if options.sort_worst_eterms:
                fa_reps = []
                for aa in aas:
                    aa_energies = ind.pose.energies().residue_total_energies(aa)
                    fa_reps.append((aa, aa_energies.get(fa_rep)))
                fa_reps.sort(key=lambda x: x[1], reverse=True)
                sorted_aas = list(zip(*fa_reps))[0]
                aas_to_repmin = set(sorted_aas[:proportion])
                print("sorted_aas: " + str(fa_reps))
                print("AAs selected: " + str(aas_to_repmin))
            else: # options.probabilistic_rep_min:
                total_energies = []
                for aa in aas:
                    aa_total_energy = ind.pose.energies().residue_total_energy(aa)
                    total_energies.append(aa_total_energy)
                norm_energies = minmax_norm(total_energies)
                aa_probs = [random.random() < norm_e for norm_e in norm_energies]
                aas_to_repmin = set([i for i, x in enumerate(aa_probs, 1) if x])
                print("norm_energies: " + str(norm_energies))
                print("AAs selected: " + str(aas_to_repmin))
        else:
            aas_to_repmin = set(random.sample(aas, proportion))
        return aas_to_repmin

    def execute(self, popul, options:ExecutionOptions, is_popul=True):
        prev_scores = [ind.score for ind in popul]
        if is_popul and self.popul_executor:
            chosen_executor = self.popul_executor
        elif not is_popul and self.trials_executor:
            chosen_executor = self.trials_executor
        else: return
        for ind in popul:
            options.aas_to_repmin = self.get_aa_proportion_for_repmin(ind, options)
            chosen_executor.apply(ind, options)
        improved_inds = [ind.score < prev_score for prev_score,ind in zip(prev_scores,popul)]
        print("% of total improved inds of execute: " + str((float(improved_inds.count(True))/float(len(popul)))*100) + "\n")
        # print(improved_inds)
        # print(prev_scores)
        # print([ind.score for ind in popul])

    def get_native_ss(self):
        ss = [
            self.native_pose.secstruct(i) for i in range(1, len(self.native_pose) + 1)
        ]
        return ss

    def get_native_omegas(self):
        omegas = [
            self.native_pose.omega(i) for i in range(1, len(self.native_pose) + 1)
        ]
        return omegas

    def evaluate(self, pdb_id, dofs, is_best=None):
        pose = self.apply_dofs_to_pose(dofs)
        dst = self.scfxn_scoring.score(pose)
        if np.isnan(dst):
            dst = 10000
        prot_name = "popul" if is_best is None else is_best
        pose.pdb_info().name(prot_name + "_pose_" + str(pdb_id))
        # self.pymover.apply(pose)
        rmsd = self.get_rmsd(pose)
        return dst, rmsd

    def evaluate_and_update_popul(self, popul):
        for i, ind in enumerate(popul):
            ind.update_pose_angles(self.chi_per_res)
            pose = ind.pose
            dst = self.scfxn_scoring.score(pose)
            if np.isnan(dst):
                dst = 10000
            rmsd = self.get_rmsd(pose)
            # pose.pdb_info().name("popul_pose_" + str(i))
            ind.score = dst
            ind.rmsd = rmsd
            # popul[i] = ind
        # return popul

    def get_rmsd(self, pose):
        rmsd = CA_rmsd(self.native_pose, pose)
        return rmsd

    def score(self, genotype):
        pose = self.apply_dofs_to_pose(genotype)
        try:
            dst = self.scfxn_scoring.score(pose)
        except ValueError:
            dst = 10000
        if np.isnan(dst):
            dst = 10000
        return dst

    def apply_dofs_to_pose(self, genotype):
        ind = Individual(
            genotype, 0, 1000, self.get_native_omegas(), self.get_native_ss()
        )
        return ind.get_pose_object(self.native_pose, chis_per_res=self.chi_per_res)

    def dofs_size(self):
        return len(self.native_ind.genotype)

    def set_minimizer_tolerance(self, tolerance):
        self.minimizer.tolerance(tolerance)

    def get_minimizer_tolerance(self):
        return self.minimizer.tolerance()

    def scale_fa_rep(self, fa_rep_factor):
        self.scfxn_rosetta.set_weight(ScoreType.fa_rep, 0.55 * fa_rep_factor)

    def print_ind_dofs(self, ind, file, tag):
        # with open(file, "a") as file_object:
        #     file_object.write(tag + ": " + str(ind.genotype) + " \n")
        print(tag + ": " + str(ind.genotype) + " \n")
