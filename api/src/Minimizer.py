from pyrosetta.rosetta.protocols.minimization_packing import MinMover
from pyrosetta.rosetta.core.kinematics import MoveMap

def make_minimizer(move_map, scfxn, min_type, cartesian, max_iter, tolerance):
    min_mover = MinMover(move_map, scfxn, min_type, tolerance, cartesian)
    min_mover.max_iter(max_iter)
    return min_mover

def make_default_minimizer(scfxn):
    # min_type = "dfpmin_armijo_nonmonotone"
    min_type = "lbfgs_armijo_nonmonotone"
    # min_type = "linmin_iterated"
    move_map = MoveMap()
    move_map.set_bb(True)
    move_map.set_chi(True)
    move_map.set_jump(True)
    cartesian = False
    tolerance = 0.01
    max_iter = 2000
    minimizer = make_minimizer(move_map, scfxn, min_type, cartesian, max_iter, tolerance)
    return minimizer


def apply_minimizer(min_mover, pose, tolerance=0.01):
    min_mover.tolerance(tolerance)
    min_mover.apply(pose)