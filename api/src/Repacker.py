from pyrosetta.rosetta.core.pack.task import TaskFactory
from pyrosetta.rosetta.core.pack.task.operation import TaskOperation, AppendRotamerSet, ExtraRotamersGeneric, InitializeFromCommandline, IncludeCurrent, RestrictToRepacking, NoRepackDisulfides
from pyrosetta.rosetta.core.pack.rotamer_set import UnboundRotamersOperation
from pyrosetta.rosetta.core.pack.dunbrack import load_unboundrot
from pyrosetta.rosetta.protocols.minimization_packing import PackRotamersMover

def make_task_factory():
    local_tf = TaskFactory()
    local_tf.push_back(InitializeFromCommandline())
    local_tf.push_back(IncludeCurrent())
    local_tf.push_back(RestrictToRepacking())
    local_tf.push_back(NoRepackDisulfides())
    # extrarot = ExtraRotamersGeneric()
    # extrarot.ex1(True)
    # extrarot.ex2aro(True)
    # local_tf.push_back(extrarot)
    unboundrot = UnboundRotamersOperation()
    unboundrot.initialize_from_command_line()
    unboundrot_op = AppendRotamerSet(unboundrot)
    local_tf.push_back(unboundrot_op)
    return local_tf

def apply_repack(local_tf, pose, sfxn):
    load_unboundrot(pose) # adds scoring bonuses for the "unbound" rotamers, if any
    packer_task = local_tf.create_task_and_apply_taskoperations(pose)
    pack = PackRotamersMover( sfxn, packer_task )
    pack.apply(pose)