#!usr/bin/env python

import logging
from math import sqrt

import numpy as np
from pyrosetta import Pose, create_score_function
from pyrosetta.rosetta.core.scoring import CA_rmsd, ScoreType

from src.new_individual import Individual
from src.Repacker import make_task_factory, apply_repack
from src.Minimizer import make_minimizer, make_default_minimizer, apply_minimizer

# def l2_norm(zd_reference, zd_current):
#     L2_norm = 0
#     for i, v in enumerate(zd_current):
#         L2_norm += (zd_current[i] - zd_reference[i]) * (zd_current[i] - zd_reference[i])
#     sqrt_L2_norm = sqrt(L2_norm)
#     return sqrt_L2_norm

class PSPFitnessFunction:
    def __init__(self, stage, native_pose, input_pose):
        self.logger = logging.getLogger("evodock.scfxn")
        self.native_pose = native_pose
        self.chi_per_res = Individual.get_chis_per_res(native_pose)
        self.native_ind = Individual.from_pose(native_pose, chis_per_res=self.chi_per_res)
        self.logger.setLevel(logging.INFO)
        self.scfxn_rosetta = self.get_score_function(stage)
        self.scfxn_scoring = self.get_score_function(stage)
        self.dock_pose = Pose()
        self.dock_pose.assign(input_pose)
        self.dock_pose.pdb_info().name("INIT_STATE")
        self.task_factory = make_task_factory()
        self.minimizer = make_default_minimizer(self.scfxn_rosetta)

    def get_score_function(self, stage):
        scfxn = create_score_function("ref2015")
        if stage == "stage1":
            scfxn = create_score_function("score0")
        if stage == "stage2":
            scfxn = create_score_function("score1")
        if stage == "stage3":
            scfxn = create_score_function("score2")
        if stage == "stage4":
            scfxn = create_score_function("score3")
        if stage == "stage_relax":
            scfxn = create_score_function("ref2015")
        return scfxn

    def get_native_ss(self):
        ss = [
            self.native_pose.secstruct(i) for i in range(1, len(self.native_pose) + 1)
        ]
        return ss

    def get_native_omegas(self):
        omegas = [
            self.native_pose.omega(i) for i in range(1, len(self.native_pose) + 1)
        ]
        return omegas

    def evaluate(self, pdb_id, dofs, is_best=None):
        pose = self.apply_dofs_to_pose(dofs)
        dst = self.scfxn_scoring.score(pose)
        if np.isnan(dst):
            dst = 10000
        prot_name = "popul" if is_best is None else is_best
        pose.pdb_info().name(prot_name + "_pose_" + str(pdb_id))
        # self.pymover.apply(pose)
        rmsd = self.get_rmsd(pose)
        return dst, rmsd

    def evaluate_and_update_popul(self, popul):
        for i, ind in enumerate(popul):
            ind.update_pose_angles(self.chi_per_res)
            pose = ind.pose
            dst = self.scfxn_scoring.score(pose)
            if np.isnan(dst):
                dst = 10000
            rmsd = self.get_rmsd(pose)
            # pose.pdb_info().name("popul_pose_" + str(i))
            ind.score = dst
            ind.rmsd = rmsd
            # popul[i] = ind
        # return popul

    def get_rmsd(self, pose):
        rmsd = CA_rmsd(self.native_pose, pose)
        return rmsd

    def score(self, genotype):
        pose = self.apply_dofs_to_pose(genotype)
        try:
            dst = self.scfxn_scoring.score(pose)
        except ValueError:
            dst = 10000
        if np.isnan(dst):
            dst = 10000
        return dst

    def apply_dofs_to_pose(self, genotype):
        ind = Individual(
            genotype, 0, 1000, self.get_native_omegas(), self.get_native_ss()
        )
        return ind.get_pose_object(self.native_pose, chis_per_res=self.chi_per_res)

    def dofs_size(self):
        return len(self.native_ind.genotype)

    def repack_popul(self, popul):
        for i, ind in enumerate(popul):
            # fa_rep_scale_factor = np.random.uniform()
            # print("REPACK-FA_REP:" + str(fa_rep_scale_factor) + ". Result: " + str(fa_rep_scale_factor * .55))
            # self.scale_fa_rep(fa_rep_scale_factor)
            pose = ind.get_pose_object().clone()
            # Individual.print_dofs(pose, chis_per_res=self.chi_per_res, tag="Angles pre repacker")
            apply_repack(self.task_factory, pose, self.scfxn_rosetta)
            # Individual.print_dofs(pose, chis_per_res=self.chi_per_res, tag="Angles post repacker")
            post_score = self.scfxn_scoring.score(pose)
            post_rmsd = self.get_rmsd(pose)
            if np.isnan(post_score):
                post_score = 10000
            # print("REP-Ind: " + str(ind.score))
            # print("REP-Post: " + str(post_score))
            if post_score <= ind.score:
            # pose.pdb_info().name("popul_pose_" + str(i))
                ind.pose = pose
                ind.update_genotype(self.chi_per_res)
                # new_ind = Individual.from_pose(pose, chis_per_res=self.chi_per_res)
                ind.score = post_score
                ind.rmsd = post_rmsd
            # popul[i] = new_ind
        # return popul

    def minimize_popul(self, popul, tolerance=0.01):
        for i, ind in enumerate(popul):
            # fa_rep_scale_factor = np.random.uniform()
            # print("MIN-FA_REP:" + str(fa_rep_scale_factor) + ". Result: " + str(fa_rep_scale_factor * .55))
            # self.scale_fa_rep(fa_rep_scale_factor)
            pose = ind.get_pose_object().clone()
            # Individual.print_dofs(pose, chis_per_res=self.chi_per_res, tag="Angles pre minimizer")
            apply_minimizer(self.minimizer, pose, tolerance=tolerance)
            # Individual.print_dofs(pose, chis_per_res=self.chi_per_res, tag="Angles post minimizer")
            post_score = self.scfxn_scoring.score(pose)
            post_rmsd = self.get_rmsd(pose)
            if np.isnan(post_score):
                post_score = 10000
            # print("MIN-Ind: " + str(ind.score))
            # print("MIN-Post: " + str(post_score))
            if post_score <= ind.score:
                #pose.pdb_info().name("popul_pose_" + str(i))
                ind.pose = pose
                ind.update_genotype(self.chi_per_res)
                # new_ind = Individual.from_pose(pose, chis_per_res=self.chi_per_res)
                # prueba_pose = new_ind.get_pose_object(self.native_pose, chis_per_res=self.chi_per_res)
                # prueba_score = self.scfxn_scoring.score(prueba_pose)
                ind.score = post_score
                ind.rmsd = post_rmsd
            # popul[i] = new_ind
        # return popul

    def set_minimizer_tolerance(self, tolerance):
        self.minimizer.tolerance(tolerance)

    def get_minimizer_tolerance(self):
        return self.minimizer.tolerance()

    def scale_fa_rep(self, fa_rep_factor):
        self.scfxn_rosetta.set_weight(ScoreType.fa_rep, 0.55 * fa_rep_factor)

    def print_ind_dofs(self, ind, file, tag):
        # with open(file, "a") as file_object:
        #     file_object.write(tag + ": " + str(ind.genotype) + " \n")
        print(tag + ": " + str(ind.genotype) + " \n")
