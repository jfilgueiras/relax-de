import logging
import time
import random
from pyrosetta import Pose
from src.selection import (CrowdingSelection, EliteSelection, GreedySelection, VagueSelection, NoneSelection, nearest_neighbor)
from src.utils import ensure_bounds
from src.new_individual import Individual
from src.rosetta.executionoptions import OptionsBuilder, ExecutionOptions
from src.deoptions import DEOptions
import numpy as np
import pandas as pd
import seaborn as sns

# --- MAIN ---------------------------------------------------------------------+
class DifferentialEvolutionAlgorithm:
    def __init__(
        self,
        popul_calculator,
        de_options:DEOptions,
        asyncTask
    ):
        self.de_options = de_options
        self.scheme = de_options.scheme
        self.logger = logging.getLogger("ProtRefDE.de")
        self.logger.setLevel(logging.INFO)
        self.popul_calculator = popul_calculator
        self.popsize = de_options.popsize
        self.mutate = de_options.mutate
        self.recombination = de_options.recombination
        self.generations = de_options.generations
        self.ind_size = popul_calculator.dofs_size()
        self.bounds = [(-1, 1)] * self.ind_size
        # self.file_time_name = self.job_id.replace("evolution", "time")
        # self.file_time_name = "time.log"
        self.job_id = "output"
        # self.init_file()
        self.disturb_degrees = de_options.disturb_degrees
        self.population = []
        self.extra_tag = de_options.extra_tag
        self.execute_each = de_options.execute_each
        self.repack_scale_fa_rep = de_options.repack_scale_fa_rep
        self.min_scale_fa_rep = de_options.min_scale_fa_rep
        self.pop_to_repackmin = de_options.pop_to_repackmin
        self.executions_per_gen = de_options.executions_per_gen
        self.check_improvement = de_options.check_improvement
        self.log_stats = de_options.log_stats
        self.min_maxiter = de_options.min_maxiter
        self.aa_proportion_repmin = de_options.aa_proportion_repmin
        self.sort_worst_eterms = de_options.sort_worst_eterms
        self.probabilistic_rep_min = de_options.probabilistic_rep_min
        if de_options.selection_opt == "Greedy":
            self.selection = GreedySelection()
        if de_options.selection_opt == "Vague":
            self.selection = VagueSelection()
        if de_options.selection_opt == "None":
            self.selection = NoneSelection()
        self.evo_file = de_options.evo_file
        self.debug_file = de_options.debug_file
        self.time_file = de_options.time_file
        self.current_gen = 1
        self.asyncTask = asyncTask
        # if de_options.selection_opt == "Crowding":
        #     self.selection = CrowdingSelection(self.popul_calculator.cost_func)

    def update_progress(self):
        if not self.asyncTask: return
        self.asyncTask.update_state(state='COMPLETED'
            if self.current_gen == self.generations else 'EXECUTING',
            meta={'current': self.current_gen, 'total': self.generations}
        )

    def set_popul_calculator(self, popul_calculator):
        self.popul_calculator = popul_calculator

    def disturb_individual(self, pose, degrees):
        for res in range(2, pose.total_residue()):
            phi = pose.phi(res)
            psi = pose.psi(res)
            if np.random.uniform() > 0.5:
                pose.set_phi(res, phi + (np.random.uniform()*degrees))
            else:
                pose.set_phi(res, phi - (np.random.uniform()*degrees))
            if np.random.uniform() > 0.5:
                pose.set_psi(res, psi + (np.random.uniform()*degrees))
            else:
                pose.set_psi(res, psi - (np.random.uniform()*degrees))

    def init_population(self, popsize=None):
        # --- INITIALIZE A POPULATION (step #1) ----------------+
        if popsize is None:
            popsize = self.popsize
        self.logger.info(" init population")
        poses = [Pose(self.popul_calculator.native_pose) for _ in range(popsize)]
        population = []
        for (i,p) in enumerate(poses):
            self.disturb_individual(p, self.disturb_degrees)
            population.append(Individual.from_pose(i, p, chis_per_res=self.popul_calculator.chi_per_res))

        # for i in range(0, popsize):
        #     indv = []
        #     for j in range(len(self.bounds)):
        #         indv.append(np.random.uniform(self.bounds[j][0], self.bounds[j][1]))
        #     population.append(Individual(indv, 0, 1000, native_omega, native_ss))

        self.popul_calculator.evaluate_and_update_popul(population)

        scores = [p.score for p in population]
        rmsds = [p.rmsd for p in population]
        df = pd.DataFrame({"score": scores, "rms": rmsds})
        self.logger.info(df)
        # df.plot.scatter(x="rmsd", y="score")
        ax = sns.scatterplot(x="rms", y="score", data=df)
        fig = ax.get_figure()
        # fig.savefig("init_population.png")
        self.population = population
        self.population.sort(key=lambda x: id(x), reverse=False)
        # for ind in self.population: print(str(id(ind)))

    def evaluate_population(self, gen, output_scatter=False):
        self.popul_calculator.evaluate_and_update_popul(self.population)
        if output_scatter:
            scores = [p.score for p in self.population]
            rmsds = [p.rmsd for p in self.population]
            df = pd.DataFrame({"score": scores, "rms": rmsds})
            print(df)
            # df.plot.scatter(x="rmsd", y="score")
            ax = sns.scatterplot(x="rms", y="score", data=df)
            fig = ax.get_figure()
            # fig.savefig("{}_both_population.png".format(gen))
            fig.clf()
            ax = sns.scatterplot(x="rms", y="score", data=df)
            fig = ax.get_figure()
            # fig.savefig("{}_final_population.png".format(gen))

    def init_file(self):
        # with open(self.job_id, "w") as file_object:
        #     file_object.write(
        #         "CONF: generations : {}, np : {}, f {}, cr {} \n".format(
        #             self.generations, self.popsize, self.mutate, self.recombination
        #         )
        #     )
        #     file_object.write("INIT EVOLUTION\n")
        with open(self.evo_file, "w") as file_evo:
            file_evo.write(
                "CONF: generations : {}, np : {}, f {}, cr {} \n".format(
                    self.generations, self.popsize, self.mutate, self.recombination
                )
            )
            file_evo.write("INIT EVOLUTION\n")

    def crossover(self, x_t_i, x_1_i, x_2_i, x_3_i, i, r):
        #return x_1_i
        if i != r and np.random.uniform() >= self.recombination: return x_t_i
        else: return x_1_i + self.mutate * (x_2_i - x_3_i)

    def build_options(self, k):

        min_scale_range = self.min_scale_fa_rep[k%len(self.min_scale_fa_rep)].split('-') #if len(self.min_scale_fa_rep) > k else self.min_scale_fa_rep[len(self.min_scale_fa_rep)].split('-')
        repack_scale_range = self.repack_scale_fa_rep[k%len(self.min_scale_fa_rep)].split('-') #if len(self.repack_scale_fa_rep) > k else self.repack_scale_fa_rep[len(self.repack_scale_fa_rep)].split('-')
        opts = OptionsBuilder()\
            .min_fa_rep_scale(float(min_scale_range[0]) if len(min_scale_range)==1 else np.random.uniform(float(min_scale_range[0]), float(min_scale_range[1])))\
            .repack_fa_rep_scale(float(repack_scale_range[0]) if len(min_scale_range)==1 else np.random.uniform(float(repack_scale_range[0]), float(repack_scale_range[1])))\
            .min_maxiter(self.min_maxiter)\
            .check_improvement(self.check_improvement)\
            .log_stats(self.log_stats)\
            .min_tolerance(0.00001)\
            .chi_per_res(self.popul_calculator.chi_per_res)\
            .popsize(self.popsize)\
            .aa_proportion_repmin(self.aa_proportion_repmin)\
            .sort_worst_eterms(self.sort_worst_eterms)\
            .probabilistic_rep_min(self.probabilistic_rep_min)\
            .build()
        self.logger.info(str(opts))
        return opts

    def build_options_bis(self, k):
        min_scale_fa_rep = [0.051, 0.280, 0.581, 1]
        repack_scale_fa_rep = [0.040, 0.265, 0.559, 1]
        min_scale_range = min_scale_fa_rep[k] if len(min_scale_fa_rep) > k else min_scale_fa_rep[len(min_scale_fa_rep)]
        repack_scale_range = repack_scale_fa_rep[k] if len(repack_scale_fa_rep) > k else repack_scale_fa_rep[len(repack_scale_fa_rep)]
        opts = OptionsBuilder()\
            .min_fa_rep_scale(float(min_scale_range))\
            .repack_fa_rep_scale(float(repack_scale_range))\
            .min_maxiter(self.min_maxiter)\
            .check_improvement(self.check_improvement)\
            .log_stats(self.log_stats)\
            .min_tolerance(0.00001)\
            .chi_per_res(self.popul_calculator.chi_per_res)\
            .popsize(self.popsize)\
            .build()
        self.logger.info(str(opts))
        return opts
            # .min_tolerance(0.1 if k < self.executions_per_gen-1 else 0.00001)\

    def main(self):
        self.update_progress()
        self.logger.info(" DE")
        # --- SOLVE --------------------------------------------+
        # self.popul_calculator.cost_-.func.print_information(population)
        # _ = input("continue > ")
        # cycle through each generation (step #2)
        # file_object_name = './resultados/' + str(self.popul_calculator.native_pose.pdb_info().name().split('/')[-1]) + \
        #            "_gen" + str(self.generations) + "_scheme" + self.scheme + self.extra_tag + "_evolution_example.log"
        file_object_name = self.evo_file
        # file_time_name = './resultados/' + str(self.popul_calculator.native_pose.pdb_info().name().split('/')[-1]) + \
        #          "_gen" + str(self.generations) + "_scheme" + self.scheme + self.extra_tag + self.file_time_name
        file_time_name = self.time_file
        with open(file_object_name, "a") as file_object:
                file_object.write("PARAMS: \n" + "name: " + str(self.popul_calculator.native_pose.pdb_info().name().split('/')[-1]) + \
                                  "\nMaxIter: " + str(self.generations) + "\nF: " + str(self.mutate) + "\nCR: " + str(self.recombination) + \
                                  "\nScheme: " + self.scheme + "\nDisturbDeg: " + str(self.disturb_degrees) + "\nExecuteEach: " + str(self.execute_each) + \
                                  "\nPopsize: " + str(self.popsize) + "\nPop_to_repmin: " + str(self.pop_to_repackmin) + "\nExtraTag:" + self.extra_tag + "\n")

        for self.current_gen in range(1, self.generations + 1):
            # if not i % 200:
            #     self.popul_calculator.set_minimizer_tolerance(self.popul_calculator.get_minimizer_tolerance() / 10)
            self.logger.info(" GENERATION:" + str(self.current_gen))
            start = time.time()
            # file_object = open(self.job_id, "a")
            # file_object = open('./resultados/' + "evolution_example.txt", "a")
            repack_count=0
            min_count=0
            sorted_slice = self.popul_calculator.slice(self.population, self.pop_to_repackmin)
            # REPACK-MIN
            execution_iters = self.executions_per_gen #max(len(self.min_scale_fa_rep),len(self.repack_scale_fa_rep))
            if self.execute_each and not self.current_gen % self.execute_each:
                for k in range(0, self.executions_per_gen):
                    options = self.build_options(k)
                    # print(options)
                    # print(self.popul_calculator.popul_executor)
                    self.popul_calculator.execute(sorted_slice, options)
            self.popul_calculator.evaluate_and_update_popul(self.population)

            # with open(file_object_name, "a") as file_object, open(file_time_name, "a") as file_time:
            #     file_object.write("GENERATION: \t" + str(i) + "\t")
            #     file_time.write("GENERATION: \t" + str(i) + "\t")
            # before_sc = [ind.score for ind in population]
            gen_scores = [ind.score for ind in self.population]
            trials = []
            for j in range(0, self.popsize):
                # --- MUTATION (step #3.A) ---------------------+
                # --- RECOMBINATION (step #3.B) ----------------+
                # select three random vector index positions [0, self.popsize),
                # not including current vector (j)
                candidates = list(range(0, self.popsize))
                candidates.remove(j)
                random_index = random.sample(candidates, 3)
                # R
                r = np.random.randint(self.ind_size)
                rand = np.random.uniform()
                self.scheme = "RANDOM" if rand <= 0.95 else "BEST"
                if self.scheme == "RANDOM":
                    x_1 = self.population[random_index[0]].genotype
                    x_1_idx = random_index[0]
                elif self.scheme == "BEST":
                    best_index = gen_scores.index(min(gen_scores))
                    x_1 = self.population[best_index].genotype
                    x_1_idx = best_index
                else: # if self.scheme == "CURRENT":
                    x_1 = self.population[j].genotype
                    x_1_idx = j
                x_2 = self.population[random_index[1]].genotype
                x_3 = self.population[random_index[2]].genotype
                x_t = self.population[j].genotype  # target individual

                v_donor = [
                    self.crossover(x_t[i], x_1_i, x_2_i, x_3_i, i, r)
                    for i, (x_1_i, x_2_i, x_3_i) in enumerate(zip(x_1, x_2, x_3))
                ]
                # # subtract x3 from x2, and create a new vector (x_diff)
                # x_diff = [x_2_i - x_3_i for x_2_i, x_3_i in zip(x_2, x_3)]
                # # multiply x_diff by the mutation factor (F) and add to x_1
                # v_donor = [x_1_i + self.mutate * x_diff_i for x_1_i, x_diff_i in zip(x_1, x_diff)]
                v_donor = ensure_bounds(v_donor, self.bounds)
                # --- RECOMBINATION (step #3.B) ----------------+
                # v_trial = []
                # for k, obj in enumerate(x_t):
                #     crossover = random.random()
                #     if crossover <= self.recombination:
                #         v_trial.append(v_donor[k])
                #     else:
                #         v_trial.append(x_t[k])
                v_trial = v_donor
                trial_ind = Individual(
                    self.population[j]._id,
                    v_trial,
                    self.population[j].pose.clone()
                )
                trials.append(trial_ind)
            self.popul_calculator.evaluate_and_update_popul(trials)
            if self.log_stats:
                improved_inds = [trial.score < ind.score for trial,ind in zip(trials,self.population)]
                # print([(trial.score, ind.score) for trial,ind in zip(trials,self.population)])
                self.logger.info("% of total improved inds of DE: " + str((float(improved_inds.count(True))/float(len(self.population)))*100) + "\n")
            # --- REPACK --- +
            # self.popul_calculator.evaluate_and_update_popul(trials)
            # print("PRE everything:", trials[0].score)
            # gen_best_idx = gen_scores.index(min(gen_scores))
            sorted_slice = self.popul_calculator.slice(trials, self.pop_to_repackmin)
            if self.execute_each and not self.current_gen % self.execute_each:
                for k in range(0, self.executions_per_gen):
                    options = self.build_options(k)
                    self.popul_calculator.execute(sorted_slice, options, is_popul=False)
            # print("POST minimize:",trials[0].score)
            # self.popul_calculator.evaluate_and_update_popul(trials)
            # print("POST everything:",trials[0].score)
            # self.popul_calculator.print_ind_dofs(trials[gen_best_idx], file_object_name, "Post-min")
            # --- SELECTION (step #3.C) -------------+
            self.popul_calculator.evaluate_and_update_popul(trials)
            if self.log_stats:
                improved_inds = [trial.score < ind.score for trial,ind in zip(trials,self.population)]
                # print([(trial.score, ind.score) for trial,ind in zip(trials,self.population)])
                self.logger.info("% of total improved inds of DE + trials-executor: " + str((float(improved_inds.count(True))/float(len(self.population)))*100) + "\n")
            self.population, gen_scores, trial_scores = self.selection.apply(trials, self.population)
            self.popul_calculator.evaluate_and_update_popul(self.population)
            # if any([ind.score == 0 for ind in self.population]):
            #     print("hay score == 0")
            #     exit()
            # --- SCORE KEEPING --------------------------------+
            # self.logger.info("POPUL SCORES : ", gen_scores)
            rmsds = [target.rmsd for target in self.population]
            gen_avg = sum(gen_scores) / self.popsize  # current generation avg. fitness
            gen_best = min(gen_scores)  # fitness of best individual
            rsmd_best = min(rmsds)  # fitness of best individual
            rsmd_avg = sum(rmsds) / self.popsize  # fitness of best individual
            trial_avg = sum(trial_scores) / self.popsize
            trial_best = min(trial_scores)
            gen_best_idx = gen_scores.index(min(gen_scores))
            gen_sol = self.population[gen_best_idx]

            self.logger.info("   > GENERATION AVERAGE: %f " % gen_avg)
            self.logger.info("   > GENERATION BEST: {} popul nº {}".format(gen_best, gen_best_idx))
            self.logger.info(
                "   > TRIAL INFO: {:.4f} {:.4f} ".format(trial_best, trial_avg)
            )
            # problematic_terms = self.popul_calculator.cost_func.get_problematic_terms(trial_inds)
            # self.logger.info("   > TOP PROBLEM TERMS {}".format(str(problematic_terms)))

            # best_SixD_vector, best_rmsd = self.popul_calculator.cost_func.render_best(
            #     i, gen_sol, self.population
            # )
            # best_sol_str = self.popul_calculator.cost_func.scfxn.get_sol_string(
            #     best_SixD_vector
            # )
            # self.logger.info("   > BEST SOL: {} ".format(best_sol_str))
            # self.popul_calculator.cost_func.print_information(self.population)
            # self.popul_calculator.cost_func.pymol_visualization(self.population)
            # _ = input("continue > ")
            end = time.time()
            # self.population[gen_best_idx].get_pose_object(self.popul_calculator.native_pose, chis_per_res=self.popul_calculator.chi_per_res).dump_pdb("best_gen" + str(i) + "_1000gen_1min_20maxiter")
            with open(file_object_name, "a") as file_object, open(file_time_name, "a") as file_time:
                file_object.write("GENERATION: \t" + str(self.current_gen) + "\t")
                file_time.write("GENERATION: \t" + str(self.current_gen) + "\t")
                file_object.write("%f \t" % gen_best)
                file_object.write("%f \t" % gen_avg)
                file_object.write("%f \n" % rsmd_avg)
                # file_object.write("%f \n" % rsmd_best)
                file_time.write("%f \n" % (end - start))
                # if self.generations == self.current_gen:
                #     file_object.write("[GEN] %d \n" % 99999)
                #     file_object.write("[POP] " + str(gen_scores) + "\n")
                #     file_object.write("[RMSD_NATIVE] " + str(rmsds) + "\n")
            self.update_progress()

        # self.popul_calculator.cost_func.print_popul(self.population)

    # def main(self):
    #     self.logger.info(" DE")
    #     # --- SOLVE --------------------------------------------+
    #     # self.popul_calculator.cost_func.print_information(population)
    #     # _ = input("continue > ")
    #     # cycle through each generation (step #2)
    #     file_object_name = './resultados/' + str(self.popul_calculator.native_pose.pdb_info().name().split('/')[-1]) + \
    #                "_gen" + str(self.generations) + "_scheme" + self.scheme + self.extra_tag + "_evolution_example.log"
    #     # file_time = open(self.file_time_name, "a")
    #     file_time_name = './resultados/' + str(self.popul_calculator.native_pose.pdb_info().name().split('/')[-1]) + \
    #              "_gen" + str(self.generations) + "_scheme" + self.scheme + self.extra_tag + self.file_time_name
    #     with open(file_object_name, "a") as file_object:
    #             file_object.write("PARAMS: \n" + "name: " + str(self.popul_calculator.native_pose.pdb_info().name().split('/')[-1]) + \
    #                               "\nMaxIter: " + str(self.generations) + "\nF: " + str(self.mutate) + "\nCR: " + str(self.recombination) + \
    #                               "\nScheme: " + self.scheme + "\nDisturbDeg: " + str(self.disturb_degrees) + "\nRepackEach: " + str(self.repack_each) + \
    #                               "\nMinEach: " + str(self.min_each) + "\nPopsize: " + str(self.popsize) + "\nPop_to_repmin: " + str(self.pop_to_repackmin) + \
    #                               "\nExtraTag:" + self.extra_tag + "\n")
    #
    #     for i in range(0, self.generations):
    #         # if not i % 200:
    #         #     self.popul_calculator.set_minimizer_tolerance(self.popul_calculator.get_minimizer_tolerance() / 10)
    #         print(" GENERATION:" + str(i))
    #         start = time.time()
    #         # file_object = open(self.job_id, "a")
    #         # file_object = open('./resultados/' + "evolution_example.txt", "a")
    #         repack_count=0
    #         min_count=0
    #         if self.pop_to_repackmin:
    #             self.population.sort(key=lambda x: x.score, reverse=True)
    #             sorted_slice = self.population[0:int(len(self.population)*self.pop_to_repackmin)]
    #             self.population.sort(key=lambda x: id(x), reverse=False)
    #         else:
    #             sorted_slice = self.population
    #         # REPACK-MIN
    #         for k in range(0,4):
    #             if self.repack_each and not i % self.repack_each:
    #                 with open(file_object_name, "a") as file_object:
    #                     file_object.write("Repacking...\n")
    #                 if repack_count < len(self.repack_scale_fa_rep):
    #                     self.popul_calculator.scale_fa_rep(self.repack_scale_fa_rep[repack_count])
    #                     with open(file_object_name, "a") as file_object:
    #                         file_object.write("Changing score by index %d, value %f in repack_list\n" %(repack_count, self.repack_scale_fa_rep[repack_count]))
    #                     repack_count+=1
    #                 self.popul_calculator.repack_popul(sorted_slice)
    #                 # self.popul_calculator.print_ind_dofs(self.population[0], file_object_name, "Post-repack")
    #             if self.min_each and not i % self.min_each:
    #                 with open(file_object_name, "a") as file_object:
    #                     file_object.write("Minimizing...\n")
    #                 # self.popul_calculator.print_ind_dofs(self.population[0], file_object_name, "Pre-min")
    #                 if min_count < len(self.min_scale_fa_rep):
    #                     self.popul_calculator.scale_fa_rep(self.min_scale_fa_rep[min_count])
    #                     with open(file_object_name, "a") as file_object:
    #                         file_object.write("Changing score by index %d, value %f in min_list\n" % (min_count, self.min_scale_fa_rep[min_count]))
    #                     min_count+=1
    #                 self.popul_calculator.minimize_popul(sorted_slice, 0.00001 if k==3 else 0.1)
    #                 # self.popul_calculator.print_ind_dofs(self.population[0], file_object_name, "Post-min")
    #         self.popul_calculator.evaluate_and_update_popul(self.population)
    #
    #         # if self.pop_to_repackmin:
    #         #     self.population.sort(key=lambda x: id(x), reverse=False)
    #         # for ind in self.population: print(str(id(ind)))
    #
    #         with open(file_object_name, "a") as file_object, open(file_time_name, "a") as file_time:
    #             file_object.write("GENERATION: \t" + str(i) + "\t")
    #             file_time.write("GENERATION: \t" + str(i) + "\t")
    #         # before_sc = [ind.score for ind in population]
    #         # if (i % 10) == 0:
    #             # population = self.popul_calculator.run(population)
    #         gen_scores = [ind.score for ind in self.population]
    #         trials = []
    #         for j in range(0, self.popsize):
    #             # --- MUTATION (step #3.A) ---------------------+
    #             # --- RECOMBINATION (step #3.B) ----------------+
    #             # select three random vector index positions [0, self.popsize),
    #             # not including current vector (j)
    #             candidates = list(range(0, self.popsize))
    #             candidates.remove(j)
    #             random_index = random.sample(candidates, 3)
    #             # R
    #             r = np.random.randint(self.ind_size)
    #             if self.scheme == "RANDOM":
    #                 x_1 = self.population[random_index[0]].genotype
    #                 x_1_idx = random_index[0]
    #             elif self.scheme == "BEST":
    #                 best_index = gen_scores.index(min(gen_scores))
    #                 x_1 = self.population[best_index].genotype
    #                 x_1_idx = best_index
    #             else: # if self.scheme == "CURRENT":
    #                 x_1 = self.population[j].genotype
    #                 x_1_idx = j
    #             x_2 = self.population[random_index[1]].genotype
    #             x_3 = self.population[random_index[2]].genotype
    #             x_t = self.population[j].genotype  # target individual
    #
    #             v_donor = [
    #                 self.crossover(x_t[i], x_1_i, x_2_i, x_3_i, i, r)
    #                 for i, (x_1_i, x_2_i, x_3_i) in enumerate(zip(x_1, x_2, x_3))
    #             ]
    #             # # subtract x3 from x2, and create a new vector (x_diff)
    #             # x_diff = [x_2_i - x_3_i for x_2_i, x_3_i in zip(x_2, x_3)]
    #             # # multiply x_diff by the mutation factor (F) and add to x_1
    #             # v_donor = [x_1_i + self.mutate * x_diff_i for x_1_i, x_diff_i in zip(x_1, x_diff)]
    #             v_donor = ensure_bounds(v_donor, self.bounds)
    #             # --- RECOMBINATION (step #3.B) ----------------+
    #             # v_trial = []
    #             # for k, obj in enumerate(x_t):
    #             #     crossover = random.random()
    #             #     if crossover <= self.recombination:
    #             #         v_trial.append(v_donor[k])
    #             #     else:
    #             #         v_trial.append(x_t[k])
    #             v_trial = v_donor
    #             trial_ind = Individual(
    #                 v_trial,
    #                 10000,
    #                 1000,
    #                 self.population[x_1_idx].omegas,
    #                 self.population[x_1_idx].ss,
    #             )
    #             trials.append(trial_ind)
    #         # --- REPACK --- +
    #         # self.popul_calculator.evaluate_and_update_popul(trials)
    #         # print("PRE everything:", trials[0].score)
    #         # gen_best_idx = gen_scores.index(min(gen_scores))
    #         # for k in range(0,4):
    #         #     print("PRE repack:", trials[0].score)
    #         #     if self.repack_each and not i % self.repack_each:
    #         #         with open(file_object_name, "a") as file_object:
    #         #             file_object.write("\nRepacking trials...\n")
    #         #         if repack_count < len(self.repack_scale_fa_rep):
    #         #             self.popul_calculator.scale_fa_rep(self.repack_scale_fa_rep[repack_count])
    #         #             with open(file_object_name, "a") as file_object:
    #         #                 file_object.write("Changing score by index %d, value %f in repack_list\n" %(repack_count, self.repack_scale_fa_rep[repack_count]))
    #         #             repack_count+=1
    #         #         self.popul_calculator.repack_popul(trials)
    #         #     # else:
    #         #     #     self.popul_calculator.evaluate_and_update_popul(trials)
    #         #     # --- MINIMIZE --- +
    #         #     print("POST repack/PRE minimize:",trials[0].score)
    #         #     if self.min_each and not i % self.min_each:
    #         #         with open(file_object_name, "a") as file_object:
    #         #             file_object.write("Minimizing trials...\n")
    #         #         if min_count < len(self.min_scale_fa_rep):
    #         #             self.popul_calculator.scale_fa_rep(self.min_scale_fa_rep[min_count])
    #         #             with open(file_object_name, "a") as file_object:
    #         #                 file_object.write("Changing score by index %d, value %f in min_list\n" % (min_count, self.min_scale_fa_rep[min_count]))
    #         #             min_count+=1
    #         #         self.popul_calculator.minimize_popul(trials)
    #         #     print("POST minimize:",trials[0].score)
    #         # self.popul_calculator.evaluate_and_update_popul(trials)
    #         # print("POST everything:",trials[0].score)
    #         # self.popul_calculator.print_ind_dofs(trials[gen_best_idx], file_object_name, "Post-min")
    #         # --- SELECTION (step #3.C) -------------+
    #         # trial_inds = self.popul_calculator.run(trials)
    #         # self.popul_calculator.cost_func.print_information(trial_inds, True)
    #         self.population, gen_scores, trial_scores = self.selection.apply(trials, self.population)
    #         self.popul_calculator.evaluate_and_update_popul(self.population)
    #         # if any([ind.score == 0 for ind in self.population]):
    #         #     print("hay score == 0")
    #         #     exit()
    #         # --- SCORE KEEPING --------------------------------+
    #         # self.logger.info("POPUL SCORES : ", gen_scores)
    #         rmsds = [target.rmsd for target in self.population]
    #         gen_avg = sum(gen_scores) / self.popsize  # current generation avg. fitness
    #         gen_best = min(gen_scores)  # fitness of best individual
    #         rsmd_best = min(rmsds)  # fitness of best individual
    #         rsmd_avg = sum(rmsds) / self.popsize  # fitness of best individual
    #         trial_avg = sum(trial_scores) / self.popsize
    #         trial_best = min(trial_scores)
    #         gen_best_idx = gen_scores.index(min(gen_scores))
    #         gen_sol = self.population[gen_best_idx]
    #
    #         print("   > GENERATION AVERAGE: %f " % gen_avg)
    #         print("   > GENERATION BEST: {} popul nº {}".format(gen_best, gen_best_idx))
    #         print(
    #             "   > TRIAL INFO: {:.2f} {:.2f} ".format(trial_best, trial_avg)
    #         )
    #         # problematic_terms = self.popul_calculator.cost_func.get_problematic_terms(trial_inds)
    #         # self.logger.info("   > TOP PROBLEM TERMS {}".format(str(problematic_terms)))
    #
    #         # best_SixD_vector, best_rmsd = self.popul_calculator.cost_func.render_best(
    #         #     i, gen_sol, self.population
    #         # )
    #         # best_sol_str = self.popul_calculator.cost_func.scfxn.get_sol_string(
    #         #     best_SixD_vector
    #         # )
    #         # self.logger.info("   > BEST SOL: {} ".format(best_sol_str))
    #         # self.popul_calculator.cost_func.print_information(self.population)
    #         # self.popul_calculator.cost_func.pymol_visualization(self.population)
    #         # _ = input("continue > ")
    #         end = time.time()
    #         # self.population[gen_best_idx].get_pose_object(self.popul_calculator.native_pose, chis_per_res=self.popul_calculator.chi_per_res).dump_pdb("best_gen" + str(i) + "_1000gen_1min_20maxiter")
    #         with open(file_object_name, "a") as file_object, open(file_time_name, "a") as file_time:
    #             file_object.write("%f \t" % gen_avg)
    #             file_object.write("%f \t" % gen_best)
    #             file_object.write("%f \n" % rsmd_avg)
    #             # file_object.write("%f \n" % rsmd_best)
    #             file_time.write("%f \n" % (end - start))
    #             if self.generations == i:
    #                 file_object.write("[GEN] %d \n" % 99999)
    #                 file_object.write("[POP] " + str(gen_scores) + "\n")
    #                 file_object.write("[RMSD_NATIVE] " + str(rmsds) + "\n")
    #
    #     # self.popul_calculator.cost_func.print_popul(self.population)