from math import isclose
from .executionoptions import ExecutionOptions
from .rosettaexecutor import RosettaExecutor

class RosettaDecorator(RosettaExecutor):
    def __init__(self, executor: RosettaExecutor):
        super().__init__()
        self.executor = executor

    def apply(self, ind, options: ExecutionOptions):
        self.executor.apply(ind, options)

    def declare_first(self):
        super().declare_first()
        self.executor.declare_first()

class StatsLogger(RosettaDecorator):
    def __init__(self, executor: RosettaExecutor, popsize=100):
        super().__init__(executor)
        self.executor_name = type(executor).__name__
        self.popsize = popsize
        self.successful_applies = 0
        self.counted_applies = 0

    def apply(self, ind, options: ExecutionOptions):
        prev_score = self.scoring_sfcxn.score(ind.pose)
        super().apply(ind, options)
        post_score = self.scoring_sfcxn.score(ind.pose)
        if post_score < prev_score:
            self.successful_applies +=1
        self.counted_applies +=1
        if self.counted_applies == self.popsize:
            self._print_stats(options.out_stream)
            self._reset_stats()

    def _print_stats(self, output_stream):
        output_stream.write("% of improved inds of " + self.executor_name + ": " + \
                            str((float(self.successful_applies)/float(self.counted_applies))*100) + "\n")

    def _reset_stats(self):
        self.successful_applies = 0
        self.counted_applies = 0