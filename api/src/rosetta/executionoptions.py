from __future__ import annotations
import sys

class ExecutionOptions:
    def __init__(self):
        self.min_tolerance:float = 0.01
        self.min_maxiter:int = 2000
        self.min_fa_rep_scale:float = 1
        self.repack_fa_rep_scale:float = 1
        self.check_improvement:bool = True
        self.log_stats:bool = True
        self.out_stream = sys.stdout
        self.chi_per_res = []
        self.popsize = 100
        self.aa_proportion_repmin:float = 1
        self.aas_to_repmin:set = set()
        self.sort_worst_eterms = False
        self.probabilistic_rep_min = False

    def __str__(self):
        return "Min tolerance: " + str(self.min_tolerance) + "\n" + \
               "Min max_iter: " + str(self.min_maxiter) + "\n" + \
               "Min fa_rep scale factor: " + str(self.min_fa_rep_scale) + "\n" + \
               "Repack fa_rep scale factor: " + str(self.repack_fa_rep_scale) + "\n" + \
               "Check improvement: " + str(self.check_improvement) + "\n" + \
               "Output stream: " + str(self.out_stream) + "\n" + \
               "Popsize: " + str(self.popsize) + "\n" + \
               "aa_proportion_repmin: " + str(self.aa_proportion_repmin) + "\n" + \
               "sort_worst_eterms: " + str(self.sort_worst_eterms) + "\n" + \
               "probabilistic_rep_min: " + str(self.probabilistic_rep_min) + "\n"

class OptionsBuilder:
    def __init__(self) -> None:
        self._opts = None
        self.reset()

    def reset(self) -> None:
        self._opts = ExecutionOptions()

    def build(self) -> ExecutionOptions:
        opts = self._opts
        self.reset()
        return opts

    def min_tolerance(self, min_tolerance) -> OptionsBuilder:
        self._opts.min_tolerance = min_tolerance
        return self

    def min_maxiter(self, min_maxiter) -> OptionsBuilder:
        self._opts.min_maxiter = min_maxiter
        return self

    def min_fa_rep_scale(self, min_fa_rep_scale) -> OptionsBuilder:
        self._opts.min_fa_rep_scale = min_fa_rep_scale
        return self

    def repack_fa_rep_scale(self, repack_fa_rep_scale) -> OptionsBuilder:
        self._opts.repack_fa_rep_scale = repack_fa_rep_scale
        return self

    def check_improvement(self, check_improvement) -> OptionsBuilder:
        self._opts.check_improvement = check_improvement
        return self

    def log_stats(self, log_stats) -> OptionsBuilder:
        self._opts.log_stats = log_stats
        return self

    def out_stream(self, out_stream) -> OptionsBuilder:
        self._opts.out_stream = out_stream
        return self

    def chi_per_res(self, chi_per_res) -> OptionsBuilder:
        self._opts.chi_per_res = chi_per_res
        return self

    def popsize(self, popsize) -> OptionsBuilder:
        self._opts.popsize = popsize
        return self

    def aas_to_repmin(self, aas_to_repmin) -> OptionsBuilder:
        self._opts.aas_to_repmin = aas_to_repmin
        return self

    def aa_proportion_repmin(self, aa_proportion_repmin) -> OptionsBuilder:
        self._opts.aa_proportion_repmin = aa_proportion_repmin
        return self

    def sort_worst_eterms(self, sort_worst_eterms) -> OptionsBuilder:
        self._opts.sort_worst_eterms = sort_worst_eterms
        return self

    def probabilistic_rep_min(self, probabilistic_rep_min) -> OptionsBuilder:
        self._opts.probabilistic_rep_min = probabilistic_rep_min
        return self
