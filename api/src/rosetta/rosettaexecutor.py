from __future__ import annotations
from .executionoptions import ExecutionOptions
from pyrosetta import create_score_function
from pyrosetta.rosetta.protocols.minimization_packing import MinMover
from pyrosetta.rosetta.core.kinematics import MoveMap
from pyrosetta.rosetta.core.pack.task import TaskFactory
from pyrosetta.rosetta.core.pack.task.operation import TaskOperation, AppendRotamerSet, ExtraRotamersGeneric, InitializeFromCommandline, IncludeCurrent, RestrictToRepacking, NoRepackDisulfides
from pyrosetta.rosetta.core.pack.rotamer_set import UnboundRotamersOperation
from pyrosetta.rosetta.core.pack.dunbrack import load_unboundrot
from pyrosetta.rosetta.protocols.minimization_packing import PackRotamersMover
from pyrosetta.rosetta.core.scoring import ScoreType
from pyrosetta.rosetta.utility import vector1_bool

class RosettaExecutor:
    def __init__(self, next_executor: RosettaExecutor = None):
        self.execute_scfxn = create_score_function("ref2015")
        self.scoring_sfcxn = create_score_function("ref2015")
        self.next_executor = next_executor
        self.is_first_exec = False
        self.executor_name = type(self).__name__
        self.successful_applies = 0
        self.total_applies = 0

    def apply(self, ind, options: ExecutionOptions):
        if self.next_executor:
            self.next_executor.apply(ind, options)

    def declare_first(self):
        self.is_first_exec = True

    def __str__(self):
        _str = "Executors: "
        next_exec = self
        while next_exec:
            _str += type(next_exec).__name__ + ", "
            next_exec = next_exec.next_executor
        return _str

class Repacker(RosettaExecutor):
    def __init__(self, next_executor: RosettaExecutor = None):
        super().__init__(next_executor)
        self.local_tf = TaskFactory()
        self.local_tf.push_back(InitializeFromCommandline())
        self.local_tf.push_back(IncludeCurrent())
        self.local_tf.push_back(RestrictToRepacking())
        self.local_tf.push_back(NoRepackDisulfides())
        # extrarot = ExtraRotamersGeneric()
        # extrarot.ex1(True)
        # extrarot.ex2aro(True)
        # local_tf.push_back(extrarot)
        unboundrot = UnboundRotamersOperation()
        unboundrot.initialize_from_command_line()
        unboundrot_op = AppendRotamerSet(unboundrot)
        self.local_tf.push_back(unboundrot_op)

    def __configure_residues_to_repack(self, aas_to_repack, packer_task, total_residue):
        # excluding_aas = set(range(total_residue + 1)) - aas_to_repack
        print(aas_to_repack)
        pack_res = vector1_bool(total_residue)
        for aa in aas_to_repack:
            pack_res[aa] = True
        packer_task.restrict_to_residues(pack_res)

    # def __reset_packer_task(self, packer_task):
    #     packer_task.

    def apply(self, ind, options: ExecutionOptions):
        self.execute_scfxn.set_weight(ScoreType.fa_rep, .55 * options.repack_fa_rep_scale)
        backup_pose = None
        if self.is_first_exec:
            backup_pose = ind.pose.clone()
        load_unboundrot(ind.pose) # adds scoring bonuses for the "unbound" rotamers, if any
        packer_task = self.local_tf.create_task_and_apply_taskoperations(ind.pose)
        if len(options.aas_to_repmin) < ind.pose.total_residue():
            self.__configure_residues_to_repack(options.aas_to_repmin, packer_task, ind.pose.total_residue())
        # print(packer_task)
        pack = PackRotamersMover( self.execute_scfxn, packer_task )
        pre_score = ind.score
        pack.apply(ind.pose)
        # if len(options.aas_to_repmin) < ind.pose.total_residue():
        #     self.__reset_packer_task(packer_task)
        post_score = self.scoring_sfcxn(ind.pose)
        if post_score < pre_score: self.successful_applies += 1
        self.total_applies+=1
        if self.total_applies == options.popsize and options.log_stats:
            print("% of total improved inds of {0}: {1}\n".format(self.executor_name,(float(self.successful_applies)/float(options.popsize))*100))
            self.successful_applies=0
            self.total_applies=0
        super().apply(ind, options)
        if self.is_first_exec:
            post_score = self.scoring_sfcxn.score(ind.pose)
            if options.check_improvement and post_score > ind.score:
                ind.pose = backup_pose
            else:
                ind.score = post_score
                ind.update_genotype(options.chi_per_res)

class Minimizer(RosettaExecutor):
    def __init__(self, next_executor: RosettaExecutor = None):
        super().__init__(next_executor)
        # min_type = "dfpmin_armijo_nonmonotone"
        min_type = "lbfgs_armijo_nonmonotone"
        # min_type = "linmin_iterated"
        move_map = MoveMap()
        move_map.set_bb(True)
        move_map.set_chi(True)
        move_map.set_jump(True)
        cartesian = False
        tolerance = 0.01
        max_iter = 2000
        self.min_mover = MinMover(move_map, self.execute_scfxn, min_type, tolerance, cartesian)
        self.min_mover.max_iter(max_iter)

    def __configure_residues_to_minimize(self, aas_to_minimize, total_residue):
        mm = MoveMap()
        mm.set_bb(True)
        mm.set_chi(True)
        mm.set_jump(True)
        excluding_aas = set(range(1,total_residue + 1)) - aas_to_minimize
        print(excluding_aas)
        for aa in excluding_aas:
            mm.set_bb(aa, False)
            mm.set_chi(aa, False)
            # mm.set_jump(aa, False)
        self.min_mover.set_movemap(mm)

    def __reset_movemap(self):
        mm = MoveMap()
        mm.set_bb(True)
        mm.set_chi(True)
        mm.set_jump(True)
        self.min_mover.set_movemap(mm)

    def apply(self, ind, options: ExecutionOptions):
        self.execute_scfxn.set_weight(ScoreType.fa_rep, .55 * options.min_fa_rep_scale)
        self.min_mover.tolerance(options.min_tolerance)
        self.min_mover.max_iter(options.min_maxiter)
        backup_pose = None
        if self.is_first_exec:
            backup_pose = ind.pose.clone()
        pre_score = ind.score
        if len(options.aas_to_repmin) < ind.pose.total_residue():
            self.__configure_residues_to_minimize(options.aas_to_repmin, ind.pose.total_residue())
        # print(self.min_mover)
        self.min_mover.apply(ind.pose)
        if len(options.aas_to_repmin) < ind.pose.total_residue():
            self.__reset_movemap()
        post_score = self.scoring_sfcxn(ind.pose)
        if post_score < pre_score: self.successful_applies += 1
        self.total_applies+=1
        if self.total_applies == options.popsize and options.log_stats:
            print("% of total improved inds of {0}: {1}\n".format(self.executor_name,(float(self.successful_applies)/float(options.popsize))*100))
            self.successful_applies=0
            self.total_applies=0
        super().apply(ind, options)
        if self.is_first_exec:
            post_score = self.scoring_sfcxn.score(ind.pose)
            if options.check_improvement and post_score > ind.score:
                ind.pose = backup_pose
            else:
                ind.score = post_score
                ind.update_genotype(options.chi_per_res)
