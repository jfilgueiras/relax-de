from __future__ import annotations
import sys

class DEOptions:
    def __init__(self):
        self.scheme:str = "RANDOM"
        self.popsize:int = 100
        self.mutate:float = 0.05
        self.recombination:float = 0.95
        self.generations:int = 100
        self.selection_opt:str = "Greedy"
        self.disturb_degrees:float = 0
        self.execute_each:int = 1
        self.repack_scale_fa_rep:[float] = []
        self.min_scale_fa_rep:[float] = []
        self.min_maxiter:int = 20
        self.pop_to_repackmin:float = 100
        self.popul_executor_chain:str = ''
        self.trial_executor_chain:str = ''
        self.executions_per_gen:int = 4
        self.check_improvement:bool = True
        self.log_stats:bool = True
        self.extra_tag:str = "extra_tag"
        self.aa_proportion_repmin:float = 1
        self.sort_worst_eterms:bool = False
        self.probabilistic_rep_min:bool = False
        self.evo_file:str = ''
        self.time_file:str = ''
        self.debug_file:str = ''
        self.pdb_dump_path:str = ''
        self.tstamp:str = ''

class DEOptionsBuilder:
    def __init__(self) -> None:
        self._opts = None
        self.reset()

    def reset(self) -> None:
        self._opts = DEOptions()

    def build(self) -> DEOptions:
        opts = self._opts
        self.reset()
        return opts

    def scheme(self, scheme) -> DEOptionsBuilder:
        self._opts.scheme = scheme
        return self

    def mutate(self, mutate) -> DEOptionsBuilder:
        self._opts.mutate = mutate
        return self

    def recombination(self, recombination) -> DEOptionsBuilder:
        self._opts.recombination = recombination
        return self

    def generations(self, generations) -> DEOptionsBuilder:
        self._opts.generations = generations
        return self

    def selection_opt(self, selection_opt) -> DEOptionsBuilder:
        self._opts.selection_opt = selection_opt
        return self

    def disturb_degrees(self, disturb_degrees) -> DEOptionsBuilder:
        self._opts.disturb_degrees = disturb_degrees
        return self

    def execute_each(self, execute_each) -> DEOptionsBuilder:
        self._opts.execute_each = execute_each
        return self

    def popsize(self, popsize) -> DEOptionsBuilder:
        self._opts.popsize = popsize
        return self

    def min_scale_fa_rep(self, min_scale_fa_rep) -> DEOptionsBuilder:
        self._opts.min_scale_fa_rep = min_scale_fa_rep
        return self

    def repack_scale_fa_rep(self, repack_scale_fa_rep) -> DEOptionsBuilder:
        self._opts.repack_scale_fa_rep = repack_scale_fa_rep
        return self

    def min_maxiter(self, min_maxiter) -> DEOptionsBuilder:
        self._opts.min_maxiter = min_maxiter
        return self

    def pop_to_repackmin(self, pop_to_repackmin) -> DEOptionsBuilder:
        self._opts.pop_to_repackmin = pop_to_repackmin
        return self

    def popul_executor_chain(self, popul_executor_chain) -> DEOptionsBuilder:
        self._opts.popul_executor_chain = popul_executor_chain
        return self

    def trial_executor_chain(self, trial_executor_chain) -> DEOptionsBuilder:
        self._opts.trial_executor_chain = trial_executor_chain
        return self

    def executions_per_gen(self, executions_per_gen) -> DEOptionsBuilder:
        self._opts.executions_per_gen = executions_per_gen
        return self

    def check_improvement(self, check_improvement) -> DEOptionsBuilder:
        self._opts.check_improvement = check_improvement
        return self

    def log_stats(self, log_stats) -> DEOptionsBuilder:
        self._opts.log_stats = log_stats
        return self

    def extra_tag(self, extra_tag) -> DEOptionsBuilder:
        self._opts.extra_tag = extra_tag
        return self

    def aa_proportion_repmin(self, aa_proportion_repmin) -> DEOptionsBuilder:
        self._opts.aa_proportion_repmin = aa_proportion_repmin
        return self

    def sort_worst_eterms(self, sort_worst_eterms) -> DEOptionsBuilder:
        self._opts.sort_worst_eterms = sort_worst_eterms
        return self

    def probabilistic_rep_min(self, probabilistic_rep_min) -> DEOptionsBuilder:
        self._opts.probabilistic_rep_min = probabilistic_rep_min
        return self

    def evo_file(self, evo_file) -> DEOptionsBuilder:
        self._opts.evo_file = evo_file
        return self

    def time_file(self, time_file) -> DEOptionsBuilder:
        self._opts.time_file = time_file
        return self

    def debug_file(self, debug_file) -> DEOptionsBuilder:
        self._opts.debug_file = debug_file
        return self

    def pdb_dump_path(self, pdb_dump_path) -> DEOptionsBuilder:
        self._opts.pdb_dump_path = pdb_dump_path
        return self

    def tstamp(self, tstamp) -> DEOptionsBuilder:
        self._opts.tstamp = tstamp
        return self