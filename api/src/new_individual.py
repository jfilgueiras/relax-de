from pyrosetta import Pose
from src.utils import convert_range

class Individual:
    def __init__(self, _id, genotype, pose=None):
        self._id = _id
        self.genotype = genotype
        self.pose = pose
        self.score = 999999
        self.rmsd = -1

    @classmethod
    def from_pose(cls, _id, pose, chis_per_res=None):
        "Initialize Inidividual from pose"
        phis = []
        psis = []
        chis = []
        # coords = Individual.__extract_coords(pose)
        for i in range(1, pose.total_residue() + 1):
            phis.append(convert_range(pose.phi(i), (-180, 180), (-1, 1)))
            psis.append(convert_range(pose.psi(i), (-180, 180), (-1, 1)))
            for chino in range (1, chis_per_res[i-1]+1 if chis_per_res else 6):
                try:
                    chis.append(convert_range(pose.chi(chino, i), (-180, 180), (-1, 1)))
                    # chis_for_res.append(pose.chi(chino, i))
                except:
                    break
        genotype = phis + psis + chis
        return cls(_id, genotype, pose)

    def update_pose_angles(self, chis_per_res=None):
        ind_pose = self.pose
        res_count = ind_pose.total_residue()
        phis = self.genotype[:res_count]
        psis = self.genotype[res_count:res_count*2]
        chis = self.genotype[res_count*2:]
        chi_count = 0
        for i in range(1, len(phis) + 1):
            ind_pose.set_phi(i, convert_range(phis[i - 1], (-1, 1), (-180, 180)))
            ind_pose.set_psi(i, convert_range(psis[i - 1], (-1, 1), (-180, 180)))
            for chino in range (1, chis_per_res[i-1]+1 if chis_per_res else 6):
                try:
                    #chino, seqpos, value
                    ind_pose.set_chi(chino, i, convert_range(chis[chi_count], (-1, 1), (-180, 180)))
                    chi_count += 1
                except:
                    break

    def update_genotype(self, chis_per_res=None):
        phis = []
        psis = []
        chis = []
        # coords = Individual.__extract_coords(pose)
        for i in range(1, self.pose.total_residue() + 1):
            phis.append(convert_range(self.pose.phi(i), (-180, 180), (-1, 1)))
            psis.append(convert_range(self.pose.psi(i), (-180, 180), (-1, 1)))
            for chino in range (1, chis_per_res[i-1]+1 if chis_per_res else 6):
                try:
                    chis.append(convert_range(self.pose.chi(chino, i), (-180, 180), (-1, 1)))
                    # chis_for_res.append(pose.chi(chino, i))
                except:
                    break
        self.genotype = phis + psis + chis

    def get_pose_object(self):
        return self.pose

    @staticmethod
    def get_chis_per_res(pose):
        chis_per_res = []
        for i in range(1, pose.total_residue() + 1):
            chi_count = 0
            for chino in range (1,6):
                try:
                    #chino, seqpos
                    pose.chi(chino, i)
                    chi_count += 1
                except:
                    break
            chis_per_res.append(chi_count)
        return chis_per_res

    @staticmethod
    def print_chis(pose, chis_per_res=None):
        for i in range(1, pose.total_residue() + 1):
            # ind_pose.set_phi(i, convert_range(phis[i - 1], (-1, 1), (-180, 180)))
            # ind_pose.set_psi(i, convert_range(psis[i - 1], (-1, 1), (-180, 180)))
            print("Res no: " + str(i) + "\n")
            for chino in range (1, chis_per_res[i-1]+1 if chis_per_res else 6):
                try:
                    #chino, seqpos, value
                    print("\t" + str(pose.chi(chino, i)))
                except:
                    break
            print("\n")

    @staticmethod
    def print_dofs(pose, chis_per_res=None, tag=""):
        print(tag)
        print("phis: [", end="")
        for i in range(1, pose.total_residue() + 1):
            print(str(pose.phi(i)), end=",")
        print("]")
        print("psis: [", end="")
        for i in range(1, pose.total_residue() + 1):
            print(str(pose.psi(i)), end=",")
        print("]")
        print("chis: [", end="")
        for i in range(1, pose.total_residue() + 1):
            for chino in range (1, chis_per_res[i-1]+1 if chis_per_res else 6):
                try:
                    print(str(pose.chi(chino, i)), end=",")
                except:
                    break
        print("]")