#!/usr/bin/env python
# coding: utf-8


import configparser
import sys
import time

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from pyrosetta import Pose, create_score_function, init, pose_from_file
from pyrosetta.rosetta.protocols.relax import FastRelax
from pyrosetta.rosetta.core.scoring import CA_rmsd

def get_score_function(stage="None"):
    if stage == "stage1":
        scfxn = create_score_function("score0")
    if stage == "stage2":
        scfxn = create_score_function("score1")
    if stage == "stage3":
        scfxn = create_score_function("score2")
    if stage == "stage4":
        scfxn = create_score_function("score3")
    if stage == "None":
        scfxn = create_score_function("score3")
    return scfxn


def init_options():
    opts = [
        "-mute all",
        # "-out:level 500",
        # "-unmute protocols.abinitio",
        "-ignore_unrecognized_res",
        "-score::weights ref2015",
    ]
    return " ".join(opts)


def main():
    init(extra_options=init_options())

    config = configparser.ConfigParser()
    config.read(sys.argv[-1], encoding="utf-8-sig")

    jobid = "./" + config["outputs"].get("output_file")
    native_input = config["inputs"].get("pose_input")
    native_pose = pose_from_file(native_input)

    scfxn_ref2015 = create_score_function("ref2015")

    print("native score {} ".format(scfxn_ref2015.score(native_pose)))

    nstruct = config["DE"].getint("maxiter")
    results = []
    total_time = 0
    for n in range(nstruct):
        pose = Pose()
        pose.assign(native_pose)
        start = time.time()
        fast_relax = FastRelax(scorefxn_in=scfxn_ref2015, standard_repeats=5)
        fast_relax.apply(pose)
        end = time.time()
        energy = scfxn_ref2015.score(pose)
        rmsd = CA_rmsd(native_pose, pose)
        results.append({"energy": energy, "rmsd": rmsd})
        relax_time = end - start
        total_time += relax_time
        print("%f \n" % relax_time)

    print("Total time: %f \n" % total_time)
    df = pd.DataFrame(results)
    ax = sns.scatterplot(x="rmsd", y="energy", data=df, palette=["b"])
    ax.set_title("Relax results")
    fig = ax.get_figure()
    fig.tight_layout()
    output_file = jobid.replace(".log", ".png")
    fig.savefig(output_file)
    plt.close()
    # df.to_csv(jobid.replace(".log", ".csv"))


if __name__ == "__main__":
    main()
