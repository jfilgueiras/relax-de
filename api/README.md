# Protein structure refinement ProtRefDE

# Dependencies
matplotlib==3.3.3
pandas==1.1.4
mpi4py==3.0.3
numpy==1.19.4
seaborn==0.11.0
./pyrosetta/pyrosetta-2022.5+release.b3365a12916-cp39-cp39-linux_x86_64.whl

# Installation

This package is only compatible with Python 3.8 and above. To install this package, please follow the instructions below:

* Install the previous descripted dependencies
* Download and install PyRosetta following the instructions found at http://www.pyrosetta.org/dow
* Install the package itself:

```console
git clone https://github.com/jfilgueiras/ProtRefDE.git
cd ProtRefDE
pip install -r requirements.txt
```

# Basic Usage

1. Create a configuration file following this example <!-- found at https://github.com/jfilgueiras/PSPHybridDE/blob/main/configs/sample.ini -->
```dosini
[inputs]
pose_input=./input_files/CASP14_R1030-D2/R1030.pdb

[outputs]
output_file=/resultados/evolution_sample.log

[initial]
disturb_degrees=0

[DE]
selection=Greedy
scheme=RANDOM
popsize=100
mutate=0.01
recombination=1¡.95
generations=100
local_search=stage_relax
extra_tag=tag

[Relax]
pop_to_repackmin=100

[exec]
time_limit = 23h 55m 30s

```
information about the DE parameters can be found at https://en.wikipedia.org/wiki/Differential_evolution

2. Run with the algorithm with the desired configuration

```console
python protref.py configs/sample_ref.ini
```

<!-- run with mpi4py

```console
mpirun -np 2 python evopsp.py configs/sample_ref.ini
```
 -->


<!-- # Protein Structure Refinement

One of the most important problems in molecular biology is to obtain the native structure of a protein from its primary structure, i.e., the amino acids chain. Ab-initio methods adopt different approaches for the protein structure representation. For example, coarse-grained protein representation models considered the phi, psi and omega angles of the backbone structure while the sidechains are representated by a centroid. In the ab initio protein structure prediction problem (PSP) many authors have been working on the use of search methods, specially evolutionary algorithms, employing the coarse-grained representation model provided by the Rosetta software suit.

# Differential Evolution Algorithm

Differential Evolution [Price97] is a population-based search method. DE creates new candidate solutions by combining existing ones according to a simple formula of vector crossover and mutation, and then keeping whichever candidate solution has the best score or fitness on the optimization problem at hand.


# Bibliography

* Varela, D., Santos, J. Protein structure prediction in an atomic model with differential evolution integrated with the crowding niching method. Nat Comput (2020). https://doi.org/10.1007/s11047-020-09801-7

* Storn, R., Price, K. Differential Evolution – A Simple and Efficient Heuristic for global Optimization over Continuous Spaces. Journal of Global Optimization 11, 341–359 (1997). https://doi.org/10.1023/A:1008202821328 
 -->