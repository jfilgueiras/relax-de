from flask import jsonify, Flask, request, redirect, url_for, send_file
from celery import Celery
from protref import de_relax, get_de_completion_status
import os, json, pickle, time
from shutil import rmtree, make_archive
from pathlib import Path
from jinja2 import Template, Environment, FileSystemLoader
import pandas as pd

app = Flask(__name__)
app.config["DEBUG"] = True

ALLOWED_PROTEIN_EXTENSIONS = {'pdb'}
ALLOWED_CONFIG_EXTENSIONS = {'ini'}
DATA_DIR = os.path.dirname(os.path.abspath(__file__)) + "/projects/"
TEMPLATE_PATH = os.path.dirname(os.path.abspath(__file__)) + "/template-config.ini"
# CONFIG_DIR = os.path.dirname(os.path.abspath(__file__)) + "/configs/"

app.config['CELERY_BROKER_URL'] = 'redis://redis:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://redis:6379/0'
app.config['CELERY_REDIS_MAX_CONNECTIONS'] = 5

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

def allowed_file(filename, type):
    if (type == 'PROTEIN'):
        return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_PROTEIN_EXTENSIONS
    elif (type == 'CONFIG'):
        return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_CONFIG_EXTENSIONS
    return False

def parse_evo_file(file):
    gens, scores, rmsds = [], [], []

    with open(file, 'r') as fin:
        for line in fin:
            print(line)
            if '[[GEN]]' in line:
                continue
            if 'GENERATION' in line:
                gen = {}
                results = line.replace('GENERATION:', '').replace('\n', '').split()
                gen['gen'] = int(results[0])
                gen['best'] = float(results[1])
                gen['avg'] = float(results[2])
                gens.append(gen)
            if '[POP]' in line:
                results = line.replace('[POP]', '').replace(' ', '').replace('\n', '').replace('[',' ').replace(']',' ').split(',')
                scores = [float(i) for i in results if i]
            if '[RMSD_NATIVE]' in line:
                results = line.replace('[RMSD_NATIVE]', '').replace(' ', '').replace('\n', '').replace('[',' ').replace(']',' ').split(',')
                rmsds = [float(i) for i in results if i]

        return pd.DataFrame({'score': scores, 'rmsd': rmsds}).assign(dataset='Relax-DE'), pd.DataFrame(gens)

@celery.task(bind=True)
def async_relax(self, config_filename, tstamp):
    try:
        de_relax(asyncTask=self, config_filename=config_filename, tstamp=tstamp)
    except RuntimeError as runtime:
        if not runtime.args[0] == 'generator didn\'t stop':
            raise RuntimeError(runtime.args[0])
    except Exception as e:
        print(e)
        self.update_state(state='FAILURE',
            meta={'current': -1, 'total': -1,
            'exception': e.args[0]}
        )
    # return {'current': get_de_completion_status(),
    #         'completed': 1 if get_de_completion_status() == 100  else 0}
    return {'current': 1, 'total': 1,
            'completed': 1}

@app.route('/status/<task_id>')
def taskstatus(task_id):
    task = async_relax.AsyncResult(task_id)
    print(task)
    if task.state == 'PENDING':
        # job did not start yet
        response = {
            'state': task.state,
            'current': 0,
            'total': 1,
            'status': 'Pending...',
            'a': str(task)
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'current': task.info.get('current', 0),
            'total': task.info.get('total', 1),
            'status': task.info.get('status', '')
        }
        if 'result' in task.info:
            response['result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'current': -1,
            'total': -1,
            'status': str(task.info),  # this is the exception raised
        }
    response['taskId'] = task_id
    print(response)
    return jsonify(response)

@app.route('/upload-protein', methods=['POST'])
def upload_protein():
    projectName = request.args.get('projectName')
    if not projectName or len(projectName) == 0:
        resp = jsonify('No projectName provided.')
        print("projectname")
        resp.status_code = 401
        return resp
    # check if the post request has the file part
    if 'protein' not in request.files:
        resp = jsonify('Missing protein file.')
        print("proteinfile")
        resp.status_code = 401
        return resp
    file = request.files['protein']
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        print("filename")
        resp = jsonify('No file selected.')
        resp.status_code = 401
        return resp
    if file and allowed_file(file.filename, 'PROTEIN'):
        save_path = DATA_DIR + "{}/input_file/".format(projectName)
        # config_path = DATA_DIR + "{}/configs/".format(projectName)
        protein_save_path = save_path + 'input.pdb'

        Path(save_path).mkdir(parents=True, exist_ok=True)
        file.save(protein_save_path)

        # Path(config_path).mkdir(parents=True, exist_ok=True)
        # file.save(config_path + '_' + projectName + '.ini')
        resp = jsonify('Protein uploaded.')
        resp.status_code = 200
        return resp
    else:
        resp = jsonify('File extension not allowed.')
        resp.status_code = 401
        return resp

@app.route('/execute-refinement', methods=['POST'])
def execute_refinement():
    projectName = request.args.get('projectName')
    options = request.get_json()

    if not projectName or not 'configName' in options.keys():
        resp = jsonify('No projectName/configName provided.')
        resp.status_code = 401
        return resp
    print(options)
    configName = options['configName']

    configPath = DATA_DIR + projectName + '/configs/'
    configFile = configPath + configName + '.ini'
    poseInput = DATA_DIR + projectName + '/input_file/input.pdb'
    resultsPath = DATA_DIR + projectName + '/results/'

    #Check reusing file config?
    file_loader = FileSystemLoader('.')
    env = Environment(loader=file_loader)
    template = env.get_template(TEMPLATE_PATH)
    Path(configPath).mkdir(parents=True, exist_ok=True)
    template.stream(poseInput=poseInput, resultsPath=resultsPath,
                    disturbDegrees=options['disturbDegrees'], de=options['de'],
                    relax=options['relax'], exec=options['exec']
                    ).dump(configFile)

    tstamp = time.strftime("%Y%m%d-%H%M%S")
    task = async_relax.apply_async(args=[configFile, tstamp])

    return jsonify(taskId=task.id, executionName=tstamp), 202

@app.route('/get-refinement', methods=['GET'])
def get_refinement():
    projectName = request.args.get('projectName')
    execName = request.args.get('execName')

    if not projectName or not execName:
        resp = jsonify('No projectName/execName provided.')
        resp.status_code = 404
        return resp

    resultsPath = DATA_DIR + projectName + '/results/' + execName
    logsPath = resultsPath + '/log/'
    scatter_df, evo_df = [], []
    for filename in os.listdir(logsPath):
        f = os.path.join(logsPath, filename)
        # checking if it is a file
        if os.path.isfile(f):
            if 'evo' in filename:
                print(f)
                [scatter_df, evo_df] = parse_evo_file(f)
            #if 'time' in filename:

    print(scatter_df.dtypes)
    print(evo_df.dtypes)
    poseRef = DATA_DIR + projectName + '/input_file/input.pdb'
    poseBestNo = scatter_df['score'].idxmin()
    poseBest = resultsPath + '/samples/' + 'cart_fa_' + str(poseBestNo) + '.pdb'
    with open(poseBest) as f, open(poseRef) as f_ref:
       posePDBString = f.read()
       refPDBString = f_ref.read()
    print(scatter_df.to_dict(orient='records'),
            evo_df.to_dict(orient='records'),
            refPDBString,
            posePDBString)
    return jsonify(populInfo=scatter_df.to_dict(orient='records'),
                   gensInfo=evo_df.to_dict(orient='records'),
                   nativePDB=refPDBString,
                   resultPDB=posePDBString), 200

@app.route('/download-results', methods=['GET'])
def download_results():
    projectName = request.args.get('projectName')
    execName = request.args.get('execName')

    if not projectName or not execName:
        resp = jsonify('No projectName/execName provided.')
        resp.status_code = 404
        return resp

    resultsPath = DATA_DIR + projectName + '/results/' + execName
    zipresults = make_archive(execName, 'zip', resultsPath)
    return send_file(zipresults)

if __name__ == '__main__':
    app.run()
