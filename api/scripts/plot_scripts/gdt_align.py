#!/usr/bin/env python
# -*- coding: UTF8 -*-

# Author: Guillaume Bouvier -- guillaume.bouvier@pasteur.fr
# https://research.pasteur.fr/en/member/guillaume-bouvier/
# 2018-01-24 15:23:33 (UTC+0100)

from pymol import cmd
import numpy

@cmd.extend
def gdt(sel1, sel2):
    """
    sel1: protein to compare
    sel2: reference protein
    """
    # Select only C alphas
    sel1 += ' and name CA'
    sel2 += ' and name CA'
    gdts = [] # Store GDT at the different thresholds
    cutoffs = [1., 2., 4., 8.]
    cmd.align(sel1, sel2, cycles=0, transform=0, object='aln')
    mapping = cmd.get_raw_alignment('aln') # e.g. [[('prot1', 2530), ('prot2', 2540)], ...]
    distances = []
    for mapping_ in mapping:
        atom1 = '%s and id %d'%(mapping_[0][0], mapping_[0][1])
        atom2 = '%s and id %d'%(mapping_[1][0], mapping_[1][1])
        dist = cmd.get_distance(atom1, atom2)
        cmd.alter(atom1, 'b = %.4f'%dist)
        distances.append(dist)
    distances = numpy.asarray(distances)
    gdts = []
    for cutoff in cutoffs:
        gdts.append((distances <= cutoff).sum()/float(len(distances)))
    out = numpy.asarray(zip(cutoffs, gdts)).flatten()
    print("GDT_%d: %.4f; GDT_%d: %.4f; GDT_%d: %.4f; GDT_%d: %.4f;"%tuple(out))
    GDT = numpy.mean(gdts)
    print("GDT: %.4f"%GDT)
    cmd.spectrum('b', 'green_yellow_red', sel1)
    return GDT

if __name__ == '__main__':
    model1, reference = cmd.get_object_list('all')
    gdt(model1, reference)