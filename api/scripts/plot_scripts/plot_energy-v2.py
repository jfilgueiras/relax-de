#!/usr/bin/env python
# coding: utf-8


# rosetta_score_rmsd_plot.py
# script to plot *.fsc files from rosetta as scatter plots
# Use "python rosetta_score_rmsd_plot.py <filename> <prot name>"
# to parse several *.fsc files:
# Use "python rosetta_score_rmsd_plot.py /path_results/*/*.fsc <prot name>"

# Dependencies:
# matplotlib, pandas, seaborn and rstoolbox

import os
import sys

import matplotlib.pyplot as plt
import matplotlib.patches as patches

import pandas as pd
import seaborn as sns
import numpy as np
from rstoolbox.io import parse_rosetta_file
from seaborn.utils import ci_to_errsize

def parse_file(file):
    gens = []
    current_stage = 2
    current_x = 0
    with open(file, 'r') as fin:
        for line in fin:
            if 'init MPI super fast Crowding Mover Score Function for stage' in line:
                current_stage = int(line.replace('\n', '')[-1])
            if '[[GEN]]' in line:
                continue
            if '[GEN]' in line:
                gen = {}
                results = line.replace('[GEN]', '').replace('\n', '').split(' ')
                gen['stage'] = current_stage
                gen['total_gen'] = current_x
                gen['gen'] = int(results[1])
                gen['best'] = float(results[2])
                gen['avg'] = float(results[3])
                gens.append(gen)
                current_x = current_x + 1
        return pd.DataFrame(gens)


def plot_score_file(file, prot_name):
    # os.makedirs("figures", exist_ok=True)
    # parse files
    
    df = parse_file(file)

    ymin = df["best"].min() - 5
    ymax = max(df["best"].max(),df["avg"].max()) + 10
    stage2_frame = len(df[df.stage == 2])
    stage3_frame = len(df[df.stage == 3]) + stage2_frame  
    ax = sns.lineplot(x="total_gen", y="best", data=df, color='red')
    ax = sns.lineplot(x="total_gen", y="avg", data=df, color='green')
    # center the view of results
    ax.set_xlim((0, df["total_gen"].max()))
    #ax.set_xlim(0, 17.5)
    ax.set_ylim((ymin, ymax))
    #ax.set_ylim(-110, 60)

    x = ax.lines[0].get_xdata()
    y = ax.lines[0].get_ydata()

    plt.axvline(x[stage2_frame], color='black', linestyle='--')
    plt.axvline(x[stage3_frame], color='black', linestyle='--')
    ax.set(xlabel="generation", ylabel="")
    # output figure
    fig = ax.get_figure()

    fig.suptitle("Protein: " + prot_name, x=0.17, y=0.2, weight="bold", size="12")
    ax.annotate("Stage2", xy=(0.14, 0.92), xycoords='axes fraction', weight="bold", size="10")
    ax.annotate("score1", xy=(0.14, 0.87), xycoords='axes fraction', size="10")
    ax.annotate("Stage3", xy=(0.47, 0.92), xycoords='axes fraction', weight="bold", size="10")
    ax.annotate("score2/\nscore5", xy=(0.47, 0.83), xycoords='axes fraction', size="10")
    ax.annotate("Stage4", xy=(0.81, 0.92), xycoords='axes fraction', weight="bold", size="10")
    ax.annotate("score3", xy=(0.81, 0.87), xycoords='axes fraction', size="10")
    fig.set_size_inches(8.7, 4.5)
    fig.tight_layout()
    name = os.path.splitext(file)[0]+'.png'
    fig.savefig(name)
    plt.close()


def main():
    filenames = sys.argv[2:]
    prot_name = sys.argv[1]
    for filename in filenames:
        print(filename)
        plot_score_file(filename, prot_name)

if __name__ == "__main__":
    main()
