#!/usr/bin/env python
# coding: utf-8


# rosetta_score_rmsd_plot.py
# script to plot *.fsc files from rosetta as scatter plots
# Use "python rosetta_score_rmsd_plot.py <filename> <prot name>"
# to parse several *.fsc files:
# Use "python rosetta_score_rmsd_plot.py /path_results/*/*.fsc <prot name>"

# Dependencies:
# matplotlib, pandas, seaborn and rstoolbox

import os, argparse, shutil
import sys

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from rstoolbox.io import parse_rosetta_file

definition = {"scores": ["score", "rms", "I_sc", "Irms", "Fnat", "time", "description"]}

def split_filenames(filenames):
	evo_files, rosetta_files = [], []
	for fname in filenames:
		(evo_files if "nmpi" in fname else rosetta_files).append(fname)
	return evo_files, rosetta_files
	
def parse_evolution_file(file):
    scores, rmsds = [], []
    gen = 0
    dataset = "HybridDE no relax" if "HDE" in file else "CrowdingDE no relax" 
    with open(file, 'r') as fin:
        for line in fin:
            if '[GEN]' in line:
                gen = int(line.split(' ')[1])
            if '[POP]' in line and gen == 99999:
                results = line.replace('[POP]', '').replace(' ', '').replace('\n', '').split(',')
                scores = [float(i) for i in results if i]
            elif '[RMSD_NATIVE]' in line and gen == 99999:
                results = line.replace('[RMSD_NATIVE]', '').replace(' ', '').replace('\n', '').split(',')
                rmsds = [float(i) for i in results if i]
        return pd.DataFrame({'score': scores, 'rms': rmsds}).assign(dataset=dataset)

def plot_score_file(rmsdfile, scorefile, outfile):
	# parse files
	#[evo_files, rosetta_files] = split_filenames(files)
	shutil.copyfile(scorefile, outfile)
	with open(outfile, 'rw') as fout, open(rmsdfile, 'r') as fin:
		in_rms_pos = 0
		out_param_pos = 0
		for out_line in fout:
			in_line = fin.readline()
			out_param_list = out_line.split(' ')
			in_rms_list = in_line.split(' ')
			if 'rms' in out_line:
				out_param_pos = out_param_list.index('rms')
			elif 'rms' in in_line:
				in_rms_pos = in_rms_list.index('rms')
			else:
				out_param_list[out_param_pos] = in_rms_list[in_rms_pos] 





def main():
	parser = argparse.ArgumentParser(prog='replace_rmsd.py', description='Replaces "input" files scores and RMSD values to "output" file.')

    parser.add_argument('-rmsd', type=str, default="rmsd.fsc", help='Input scorefile with desired RMSD values.')
    parser.add_argument('-score', type=str, default="score.fsc", help='Input scorefile with desired scores.')
    parser.add_argument('-output', type=str, default="output.fsc", help='Output scorefile with desired scores and RMSD values combined.')
    args = parser.parse_args()

	mix_scorefiles(args.rmsd, args.score, args.output)

if __name__ == "__main__":
	main()
