#!/usr/bin/env python
# coding: utf-8


# rosetta_score_rmsd_plot.py
# script to plot *.fsc files from rosetta as scatter plots
# Use "python rosetta_score_rmsd_plot.py <filename> <prot name>"
# to parse several *.fsc files:
# Use "python rosetta_score_rmsd_plot.py /path_results/*/*.fsc <prot name>"

# Dependencies:
# matplotlib, pandas, seaborn and rstoolbox

import os
import sys

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
from rstoolbox.io import parse_rosetta_file
import seaborn.categorical

seaborn.categorical._Old_Violin = seaborn.categorical._ViolinPlotter

class NewViolinPlotter(seaborn.categorical._Old_Violin):

    def __init__(self, *args, **kwargs):
        super(NewViolinPlotter, self).__init__(*args, **kwargs)
        self.gray='black'
    
def adjacent_values(vals, q1, q3):
    upper_adjacent_value = q3 + (q3 - q1) * 1.5
    upper_adjacent_value = np.clip(upper_adjacent_value, q3, vals[-1])

    lower_adjacent_value = q1 - (q3 - q1) * 1.5
    lower_adjacent_value = np.clip(lower_adjacent_value, vals[0], q1)
    return lower_adjacent_value, upper_adjacent_value
        
seaborn.categorical._ViolinPlotter = NewViolinPlotter

definition = {"scores": ["score", "rms", "I_sc", "Irms", "Fnat", "time", "description"]}

colors = {'Rosetta relax': "#606060", 'Rosetta no relax': "#606060", \
    'HybridDE no relax': "#000080", 'HybridDE relax': "#000080", \
    'CrowdingDE no relax': "#990000", 'CrowdingDE relax': "#990000", \
    'trRosetta, no templates, score3': "#FF0000", \
    'trRosetta, no templates, ref2015': "#FF0000", \
    'trRosetta, with templates, score3': "#FA8072", \
    'trRosetta, with templates, ref2015': "#FA8072", \
    'AlphaFold, no templates, score3': "#008000", \
    'AlphaFold, no templates, ref2015': "#008000", \
    'AlphaFold, with templates, score3': "#00FF00", \
    'AlphaFold, with templates, ref2015': "#00FF00",\
    'RoseTTAFold, no templates, score3': "#530B6C", \
    'RoseTTAFold, no templates, ref2015': "#530B6C", \
    'ProtRefDE, score3': "#ADD8E6", \
    'ProtRefDE, ref2015': "#ADD8E6", \
    'native': "#000000"}

colors = {'Rosetta ab initio': "#606060", \
    'HybridDE': "#000080", \
    'CrowdingDE': "#990000", \
    'trRosetta': "#FF0000", \
    'AlphaFold2': "#008000", \
    'RoseTTAFold': "#530B6C", \
    'ProtRefDE': "#ADD8E6", \
    'native': "#000000"}

markers = {'Rosetta relax': "o", 'Rosetta no relax': "o", \
    'HybridDE no relax': "o", 'HybridDE relax': "o", \
    'CrowdingDE no relax': "o", 'CrowdingDE relax': "o", \
    'trRosetta, no templates, score3': "P", \
    'trRosetta, no templates, ref2015': "P", \
    'trRosetta, with templates, score3': "P", \
    'trRosetta, with templates, ref2015': "P", \
    'AlphaFold, no templates, score3': "X", \
    'AlphaFold, no templates, ref2015': "X", \
    'AlphaFold, with templates, score3': "X", \
    'AlphaFold, with templates, ref2015': "X", \
    'RoseTTAFold, no templates, score3': "^", \
    'RoseTTAFold, no templates, ref2015': "^", \
    'ProtRefDE, score3': "D", \
    'ProtRefDE, ref2015': "D", \
    'native': "o"}

markers = {'Rosetta ab initio': "o", \
    'HybridDE': "o", \
    'CrowdingDE': "o", \
    'trRosetta': "P", \
    'AlphaFold2': "X", \
    'RoseTTAFold': "^", \
    'ProtRefDE': "D", \
    'native': "o"}

colors_violin = {'Rosetta relax': "#606060", 'Rosetta no relax': "#606060", \
    'HybridDE no relax': "#000080", 'HybridDE relax': "#000080", \
    'CrowdingDE no relax': "#990000", 'CrowdingDE relax': "#990000", \
    'trRosetta, no templates, score3': "#ffffff", \
    'trRosetta, no templates, ref2015': "#ffffff", \
    'trRosetta, with templates, score3': "#ffffff", \
    'trRosetta, with templates, ref2015': "#ffffff", \
    'AlphaFold, no templates, score3': "#ffffff", \
    'AlphaFold, no templates, ref2015': "#ffffff", \
    'AlphaFold, no templates, score3, r1': "#ffffff", \
    'AlphaFold, no templates, ref2015, r1': "#ffffff", \
    # 'AlphaFold, no templates, score3, r2': "#009600b4", \
    'AlphaFold, no templates, score3, r2': "#ffffff", \
    # 'AlphaFold, no templates, ref2015, r2': "#009600b4", \
    'AlphaFold, no templates, ref2015, r2': "#ffffff", \
    # 'AlphaFold, no templates, score3, r3': "#00b40096", \
    'AlphaFold, no templates, score3, r3': "#ffffff", \
    # 'AlphaFold, no templates, ref2015, r3': "#00b40096", \
    'AlphaFold, no templates, ref2015, r3': "#ffffff", \
    # 'AlphaFold, no templates, score3, r4': "#00d20060", \
    'AlphaFold, no templates, score3, r4': "#ffffff", \
    # 'AlphaFold, no templates, ref2015, r4': "#00d20060", \
    'AlphaFold, no templates, ref2015, r4': "#ffffff", \
    # 'AlphaFold, no templates, score3, r5': "#00ff0020", \
    'AlphaFold, no templates, score3, r5': "#ffffff", \
    # 'AlphaFold, no templates, ref2015, r5': "#00ff0020", \
    'AlphaFold, no templates, ref2015, r5': "#ffffff", \
    'AlphaFold, with templates, score3': "#ffffff", \
    'AlphaFold, with templates, ref2015': "#ffffff",\
    'RoseTTAFold, no templates, score3': "#ffffff", \
    'RoseTTAFold, no templates, ref2015': "#ffffff", \
    'ProtRefDE, score3': "#ffffff", \
    'ProtRefDE, ref2015': "#ffffff", \
    'native': "#000000"}

colors_violin = {'Rosetta ab initio': "#606060", \
    'HybridDE': "#000080", \
    'CrowdingDE': "#990000", \
    'trRosetta': "#FF0000", \
    'AlphaFold2': "#008000", \
    'RoseTTAFold': "#530B6C", \
    'ProtRefDE': "#ADD8E6", \
    'native': "#000000"}

axes_rmsd = {
    'M_norelax': (0,30),
    'M_relax': (0,30),
    'N_norelax': (0,22),
    'N_relax': (0,22),
    'nsp2_norelax': (0,70),
    'nsp2_relax': (0,60),
    'nsp4_norelax': (0,50),
    'nsp4_relax': (0,60),
    'nsp6_norelax': (0,35),
    'nsp6_relax': (0,60),
    'nsp11_norelax': (0,6),
    'nsp11_relax': (0,10),
    'nsp14_norelax': (0,50),
    'nsp14_relax': (0,50),
    'nsp3_norelax': (0,22),
    'nsp3_relax': (0,22),
    'nsp9_norelax': (0,20),
    'nsp9_relax': (0,20),
    'nsp10_norelax': (0,22),
    'nsp10_relax': (0,22),
    'orf3b_norelax': (0,10),
    'orf3b_relax': (0,22),
    'orf6_norelax': (0,30),
    'orf6_relax': (0,22),
    'orf7b_norelax': (0,20),
    'orf7b_relax': (0,22),
    'orf9c_norelax': (0,25),
    'orf9c_relax': (0,22),
    'orf10_norelax': (0,20),
    'orf10_relax': (0,22),
    'orf8_norelax': (0,22),
    'orf8_relax': (0,22)
}

axes_score = {
    'M_norelax': (-200,200),
    'M_relax': (-200,200),
    'N_norelax': (-125, 125),
    'N_relax': (-300, -100),
    'nsp2_norelax': (-200,500),
    'nsp2_relax': (-300,300),
    'nsp4_norelax': (0,400),
    'nsp4_relax': (-300,300),
    'nsp6_norelax': (-100,500),
    'nsp6_relax': (-300,300),
    'nsp11_norelax': (0,60),
    'nsp11_relax': (-300,300),
    'nsp14_norelax': (-100,400),
    'nsp14_relax': (-300,300),
    'nsp3_norelax': (-140,75),
    'nsp3_relax': (-500,-180),
    'nsp9_norelax': (-200,10),
    'nsp9_relax': (-300,-100),
    'nsp10_norelax': (-120,150),
    'nsp10_relax': (-240, 100),
    'orf3b_norelax': (0,100),
    'orf3b_relax': (-250,0),
    'orf6_norelax': (-50,200),
    'orf6_relax': (-250,0),
    'orf7b_norelax': (-25,175),
    'orf7b_relax': (-250,0),
    'orf9c_norelax': (-50,250),
    'orf9c_relax': (-250,0),
    'orf10_norelax': (-50,150),
    'orf10_relax': (-250,0),    
    'orf8_norelax': (-150,100),
    'orf8_relax': (-250,0)
}

title_position_x = {
    'M_norelax': 0.24,
    'M_relax': 0.3,
    'N_norelax': 0.88,
    'N_relax': 0.25,
    'nsp2_norelax': 0.27,
    'nsp2_relax': 0.86,
    'nsp4_norelax': 0.25,
    'nsp4_relax': 0.86,
    'nsp6_norelax': 0.26,
    'nsp6_relax': 0.86,
    'nsp11_norelax': 0.24,
    'nsp11_relax': 0.86,
    'nsp14_norelax': 0.28,
    'nsp14_relax': 0.86,
    'nsp3_norelax': 0.3,
    'nsp3_relax': 0.86,
    'nsp9_norelax': 0.3,
    'nsp9_relax': 0.86,
    'nsp10_norelax': 0.3,
    'nsp10_relax': 0.86,
    'orf3b_norelax': 0.85,
    'orf3b_relax': 0.86,
    'orf6_norelax': 0.85,
    'orf6_relax': 0.86,
    'orf7b_norelax': 0.835,
    'orf7b_relax': 0.86,
    'orf9c_norelax': 0.25,
    'orf9c_relax': 0.86,
    'orf10_norelax': 0.835,
    'orf10_relax': 0.86,    
    'orf8_norelax': 0.3,
    'orf8_relax': 0.86
}

title_position_y = {
    'M_norelax': 0.79,
    'M_relax': 0.79,
    'N_norelax': 0.79,
    'N_relax': 0.79,
    'nsp2_norelax': 0.79,
    'nsp2_relax': 0.79,
    'nsp4_norelax': 0.79,
    'nsp4_relax': 0.79,
    'nsp6_norelax': 0.79,
    'nsp6_relax': 0.79,
    'nsp11_norelax': 0.79,
    'nsp11_relax': 0.79,
    'nsp14_norelax': 0.79,
    'nsp14_relax': 0.79,
    'nsp3_norelax': 0.79,
    'nsp3_relax': 0.79,
    'nsp9_norelax':  0.79,
    'nsp9_relax':  0.79,
    'nsp10_norelax': 0.79,
    'nsp10_relax': 0.79,
    'orf3b_norelax': 0.79,
    'orf3b_relax': 0.79,
    'orf6_norelax': 0.79,
    'orf6_relax': 0.79,
    'orf7b_norelax': 0.79,
    'orf7b_relax': 0.79,
    'orf9c_norelax': 0.79,
    'orf9c_relax': 0.79,
    'orf10_norelax': 0.79,
    'orf10_relax': 0.79,    
    'orf8_norelax': 0.79,
    'orf8_relax': 0.79
}
palette = {}

def split_filenames(filenames):
    evo_files, rosetta_files = [], []
    for fname in filenames:
        (evo_files if "nmpi" in fname or "evolution" in fname else rosetta_files).append(fname)
    return evo_files, rosetta_files
    
def assignDataset(df, filename):
    datasetName = "Rosetta"
    hasTemplates = ""
    hasRelax = ""
    score_type = ""
    dataset = ""
    if "initial" in filename:
        return df.assign(dataset="native")
    if "alphafold" in filename:
        datasetName = "AlphaFold, "
        hasTemplates = "with templates, "
    elif "trRosetta" in filename:
        datasetName = "trRosetta, "
        hasTemplates = "with templates, "
    elif "RoseTTAFold" in filename:
        datasetName = "RoseTTAFold, "
        hasTemplates = "no templates, "
    elif "CDE" in filename:
        datasetName = "CrowdingDE"
    elif "HDE" in filename:
        datasetName = "HybridDE"
    elif "evolution_example" in filename:
        datasetName = "ProtRefDE"
    if "not" in filename:
        hasTemplates = "no templates, "
    if "ref2015" in filename:
        score_type = "ref2015"
    elif "score3" in filename:
        score_type = "score3"
    if "norelax" in filename:
        hasRelax = " no relax"
    elif "relax" in filename:
        hasRelax = " relax"
    dataset = datasetName + hasRelax + hasTemplates + score_type
    return df.assign(dataset=dataset)

def collapse_dataset_column(df):
    labels = {'Rosetta relax': "Rosetta ab initio", 'Rosetta no relax': "Rosetta ab initio", \
        'HybridDE no relax': "HybridDE", 'HybridDE relax': "HybridDE", \
        'CrowdingDE no relax': "CrowdingDE", 'CrowdingDE relax': "CrowdingDE", \
        'trRosetta, no templates, score3': "trRosetta", \
        'trRosetta, no templates, ref2015': "trRosetta", \
        'AlphaFold, no templates, score3': "AlphaFold2", \
        'AlphaFold, no templates, ref2015': "AlphaFold2", \
        'RoseTTAFold, no templates, score3': "RoseTTAFold", \
        'RoseTTAFold, no templates, ref2015': "RoseTTAFold", \
        'ProtRefDE, score3': "ProtRefDE", \
        'ProtRefDE, ref2015': "ProtRefDE", \
        'native': "native"
    }
    df["dataset"].replace(labels, inplace=True)
    print(df['dataset'])
    return df

def parse_evolution_file(file):
    scores, rmsds = [], []
    evo_gen, evo_avg_score, evo_avg_rmsd, evo_score_best = [], [], [], []
    gen = 0
    dataset = "HybridDE no relax" if "HDE" in file else "CrowdingDE no relax" if "CDE" in file else "ProtRefDE" 
    with open(file, 'r') as fin:
        for line in fin:
            if '[GEN]' in line:
                gen = int(line.split(' ')[1])
            if '[POP]' in line and gen == 99999:
                results = line.replace('[POP]', '').replace(' ', '').replace('\n', '').replace('[',' ').replace(']',' ').split(',')
                scores = [float(i) for i in results if i]
            elif '[RMSD_NATIVE]' in line and gen == 99999:
                results = line.replace('[RMSD_NATIVE]', '').replace(' ', '').replace('\n', '').replace('[',' ').replace(']',' ').split(',')
                rmsds = [float(i) for i in results if i]
        return pd.DataFrame({'score': scores, 'rms': rmsds}).assign(dataset=dataset)

def print_whiskers(df, ax, column):
    in_data = [list(d) for _, d in df.groupby(['dataset'], sort=False)[column]]
    quartile1 = []
    medians = []
    quartile3 = []
    max_min = []
    ax_min = ax.get_xlim()[0] if (column == 'rms') else ax.get_ylim()[0]
    ax_max = ax.get_xlim()[1] if (column == 'rms') else ax.get_ylim()[1]
    for my_data in in_data:
        quartile1_, medians_, quartile3_ = np.percentile(my_data, [25, 50, 75])
        quartile1.append(quartile1_)
        # medians.append(np.average(my_data))
        medians.append(medians_)
        quartile3.append(quartile3_)
        max_min.append(((np.min(my_data)-ax_min)/(ax_max-ax_min),(np.max(in_data[0])-ax_min)/(ax_max-ax_min)))
    whiskers = np.array([adjacent_values(sorted_array, q1, q3) for sorted_array, q1, q3 in zip(in_data, quartile1, quartile3)])
    whiskersMin, whiskersMax = whiskers[:, 0], whiskers[:, 1]
    # inds = np.arange(1 if (column == 'rms') else 0, len(medians) +1 if (column == 'rms') else len(medians))
    inds = np.arange(0, len(medians))
    # for (median, q1, q3, m_m, i) in zip(medians_, quartile1, quartile3, max_min, inds):
    if (column == 'rms'):
        medians = medians[::-1]
        quartile1 = quartile1[::-1]
        quartile3 = quartile3[::-1]
        ax.scatter(medians, inds, marker='*', color='#00FFFF', s=30, zorder=400)
        ax.scatter(quartile1, inds, marker='|', color='black',s=50, zorder=300)
        ax.scatter(quartile3, inds, marker='|', color='black',s=50, zorder=300)
    else:
        ax.scatter(inds, medians, marker='*', color='#00FFFF', s=30, zorder=400)
        ax.scatter(inds, quartile1, marker='_', color='black',s=50, zorder=300)
        ax.scatter(inds, quartile3, marker='_', color='black',s=50, zorder=300)

def plot_score_file(files, prot_name="", filename=""):
    os.makedirs("figures", exist_ok=True)
    # parse files
    [evo_files, rosetta_files] = split_filenames(files)
    df_rosetta, df_evo, df = pd.DataFrame(), pd.DataFrame(), pd.DataFrame()
    if rosetta_files:
        df_rosetta = pd.concat([assignDataset(parse_rosetta_file(ifile, definition), ifile) for ifile in rosetta_files])
        df = df_rosetta
    if evo_files:
        df_evo = pd.concat([parse_evolution_file(ifile) for ifile in evo_files])
        df = df_evo
    if not df_evo.empty and not df_rosetta.empty:
        df = pd.concat([df_rosetta,df_evo])
    # order = {'Rosetta no relax': 0, 'Rosetta relax': 0, 'HybridDE no relax': 1, 'HybridDE relax': 1, 'CrowdingDE no relax': 2, 'CrowdingDE relax': 2, 'trRosetta, no templates, score3': 3, 'trRosetta, no templates, ref2015': 3, 'AlphaFold, no templates, score3': 4, 'AlphaFold, no templates, ref2015': 4, 'RoseTTAFold, no templates, score3': 5, 'RoseTTAFold, no templates, ref2015':5 } 
    order = {'Rosetta ab initio': 0, 'HybridDE': 2, 'CrowdingDE': 1, 'ProtRefDE': 3, 'trRosetta': 6, 'AlphaFold2': 4, 'RoseTTAFold': 5 } 
    
    # df = df.assign(dataset='results')
    # if "None" not in trRosettaFile:
    #   dfrefTrRosetta = parse_rosetta_file(trRosettaFile, definition)
    #   dfrefTrRosetta = dfrefTrRosetta.assign(dataset='trRosetta')
    #   df = pd.concat([df,dfrefTrRosetta])
    # if "None" not in alphaFoldFile:
    #   dfrefAlphaFold = parse_rosetta_file(alphaFoldFile, definition)
    #   dfrefAlphaFold = dfrefAlphaFold.assign(dataset='AlphaFold')
    #   #ax.set_ylim((min(df["score"].min(), dfref["score"].min()) - 30, max(df["score"].max(), dfref["score"].max()) + 30))
    #   df = pd.concat([df,dfrefAlphaFold])
    # sns.set(font_scale=1)
    # sns.set_context("paper", rc={"font.size":10,"axes.titlesize":11,"axes.labelsize":11})
    # , rc={"font.size":5,"axes.titlesize":7,"axes.labelsize":15}
    isRef2015 = any('ref2015' in s for s in files) or any('_relax' in s for s in files)
    df = collapse_dataset_column(df)
    #PURGING
    # prepurge_df = df
    df = df[df['score'] <= 0]
    sns.set_context("notebook", font_scale = 1.5)
    g = sns.JointGrid(height=6, ratio=2, space=0)

    df = df.sort_values(by=['dataset'], key=lambda x: x.map(order))
    ax = sns.scatterplot(ax=g.ax_joint, x="rms", y="score", data=df, hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=True)  
    # df = prepurge_df
    # order = {'Rosetta ab initio': 0, 'HybridDE': 1, 'CrowdingDE': 2, 'trRosetta': 3, 'AlphaFold2': 4, 'RoseTTAFold': 5 } 
    order = {'Rosetta ab initio': 0, 'HybridDE': 1, 'CrowdingDE': 2, 'ProtRefDE': 3, 'trRosetta': 4, 'AlphaFold2': 5, 'RoseTTAFold': 6 } 
    df = df.sort_values(by=['dataset'], key=lambda x: x.map(order))
    violin_order = ["Rosetta ab initio", "HybridDE", "CrowdingDE", "ProtRefDE", "trRosetta", "AlphaFold2", "RoseTTAFold"]
    # print([x for x in violin_order if x in df.dataset.unique()])
    ax1 = sns.violinplot(ax=g.ax_marg_y, data=df, scale='width', x="dataset", y="score", linewidth=0.7, inner=None, order=[x for x in violin_order if x in df.dataset.unique()], palette=colors_violin, saturation=1)
    violin_order.reverse()
    # print([x for x in violin_order if x in df.dataset.unique()])
    ax0 = sns.violinplot(ax=g.ax_marg_x, data=df, scale='width', x="rms", y="dataset", linewidth=0.7, inner=None, order=[x for x in violin_order if x in df.dataset.unique()], palette=colors_violin, saturation=1)
    handles, labels = ax.get_legend_handles_labels()
    print(handles)
    print(labels)
    # sort both labels and handles by labels
    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: order[t[0]]))
    for handle in handles:
        handle.set_sizes([120.0])
    # labels.sort(key=lambda val: order[val[1]])
    ax.legend(handles, labels)
    sns.move_legend(ax, "upper right", bbox_to_anchor=(1.52, 1.46), fontsize=18, title="")

    # if not isRef2015:
    #     ax0 = sns.violinplot(ax=g.ax_marg_x, data=df, scale='width', x="rms", y="dataset", linewidth=1, order=["Rosetta no relax", "HybridDE no relax", "CrowdingDE no relax"], palette=colors_violin, saturation=.3)
    #     # ax0 = sns.violinplot(ax=g.ax_marg_x, data=df, scale='width', x="rms", y="dataset", linewidth=1, order=["Rosetta no relax", "HybridDE no relax", "CrowdingDE no relax", "trRosetta, no templates, score3", "AlphaFold, no templates, score3", "RoseTTAFold, no templates, score3"], color="white", saturation=.3)
    # else:
    #     ax0 = sns.violinplot(ax=g.ax_marg_x, data=df, scale='width', x="rms", y="dataset", linewidth=1, order=["Rosetta relax", "HybridDE relax", "CrowdingDE relax"], palette=colors_violin, saturation=.3)
    #     # ax0 = sns.violinplot(ax=g.ax_marg_x, data=df, scale='width', x="rms", y="dataset", linewidth=1, order=["Rosetta relax", "HybridDE relax", "CrowdingDE relax", "trRosetta, no templates, ref2015", "AlphaFold, no templates, ref2015", "RoseTTAFold, no templates, ref2015"], color="white", saturation=.3)
    # if not isRef2015:
    #     ax1 = sns.violinplot(ax=g.ax_marg_y, data=df, scale='width', x="dataset", y="score", linewidth=1, order=["Rosetta no relax", "HybridDE no relax", "CrowdingDE no relax"], palette=colors_violin, saturation=.3)
    # else:
    #     ax1 = sns.violinplot(ax=g.ax_marg_y, data=df, scale='width', x="dataset", y="score", linewidth=1, order=["Rosetta relax", "HybridDE relax", "CrowdingDE relax"], palette=colors_violin, saturation=.3)
    
    print_whiskers(df, ax0, 'rms')
    print_whiskers(df, ax1, 'score')
    ax1.set_ylabel('')
    # axes[1].set_ylabel('')
    # replace_y_labels(axes[0])
    # axes[0].set_yticklabels(axes[0].get_yticklabels(), rotation = 30)
    # ax1.set_yticklabels('')
    # if not isRef2015:
    #     ax1 = sns.violinplot(ax=axes[1], data=df, scale='width', x="score", y="dataset", linewidth=1, order=["Rosetta no relax", "HybridDE no relax", "CrowdingDE no relax", "trRosetta, no templates, score3", "AlphaFold, no templates, score3", "RoseTTAFold, no templates, score3"], color="white", saturation=.3)
    # else:
    #     ax1 = sns.violinplot(ax=axes[1], data=df, scale='width', x="score", y="dataset", linewidth=1, order=["Rosetta relax", "HybridDE relax", "CrowdingDE relax", "trRosetta, no templates, ref2015", "AlphaFold, no templates, ref2015", "RoseTTAFold, no templates, ref2015"], color="white", saturation=.3)

    prot_properties = prot_name + "_" + ("relax" if isRef2015 else "norelax")
    # center the view of results
    ax.set_xlim((0, df["rms"].max() + 4))
    # ax.set_xlim(axes_rmsd[prot_properties])
    ax.set_ylim((df["score"].min() - 30, df["score"].max() + 30))
    # ax.set_ylim(axes_score[prot_properties])
    ax.set(xlabel="RMSD", ylabel="ref2015" if isRef2015 else "score3")

    # output figure
    fig = ax.get_figure()
    # fig.suptitle(prot_name, x=title_position_x[prot_properties], y=title_position_y[prot_properties], weight="bold", size="28")
    x = 0.20
    y = 0.63
    if prot_name in ['M', 'N']:
        x = 0.18
        y = 0.63
    if prot_name in ['nsp11', 'nsp14', 'orf3b', 'orf7b', 'orf9c', 'orf10']:
        x = 0.21
        y = 0.63
    fig.suptitle(prot_name, x=x, y=y, weight="bold", size="22")
    fig.set_size_inches(40, 30)
    #fig.tight_layout()
    name = "figures/" + prot_name + "_" + filename + ".png"
    # g.fig.set_figwidth(15)
    # g.fig.set_figheight(8)
    g.fig.set_figwidth(11)
    g.fig.set_figheight(10)
    fig.savefig(name, bbox_inches = 'tight', pad_inches = 0.1, dpi=200)
    plt.close()


def main():
    files = sys.argv[3:]
    prot_name = sys.argv[1]
    filename = sys.argv[2]

    plot_score_file(files, prot_name, filename)


if __name__ == "__main__":
    main()
