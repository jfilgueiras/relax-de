#!/usr/bin/env python
# coding: utf-8


# rosetta_score_rmsd_plot.py
# script to plot *.fsc files from rosetta as scatter plots
# Use "python rosetta_score_rmsd_plot.py <filename> <prot name>"
# to parse several *.fsc files:
# Use "python rosetta_score_rmsd_plot.py /path_results/*/*.fsc <prot name>"

# Dependencies:
# matplotlib, pandas, seaborn and rstoolbox

import os
import sys

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from rstoolbox.io import parse_rosetta_file

definition = {"scores": ["score", "rms", "I_sc", "Irms", "Fnat", "time", "description"]}

colors = {'Rosetta relax': "#606060", 'Rosetta no relax': "#606060", \
    'HybridDE no relax': "#000080", 'HybridDE relax': "#000080", \
    'CrowdingDE no relax': "#990000", 'CrowdingDE relax': "#990000", \
    'trRosetta, no templates, score3': "#FF0000", \
    'trRosetta, no templates, ref2015': "#FF0000", \
    'trRosetta, with templates, score3': "#FA8072", \
    'trRosetta, with templates, ref2015': "#FA8072", \
    'AlphaFold, no templates, score3': "#008000", \
    'AlphaFold, no templates, ref2015': "#008000", \
    'AlphaFold, no templates, score3, r1': "#008000ff", \
    'AlphaFold, no templates, ref2015, r1': "#008000ff", \
    # 'AlphaFold, no templates, score3, r2': "#009600b4", \
    'AlphaFold, no templates, score3, r2': "#008000ff", \
    # 'AlphaFold, no templates, ref2015, r2': "#009600b4", \
    'AlphaFold, no templates, ref2015, r2': "#008000ff", \
    # 'AlphaFold, no templates, score3, r3': "#00b40096", \
    'AlphaFold, no templates, score3, r3': "#008000ff", \
    # 'AlphaFold, no templates, ref2015, r3': "#00b40096", \
    'AlphaFold, no templates, ref2015, r3': "#008000ff", \
    # 'AlphaFold, no templates, score3, r4': "#00d20060", \
    'AlphaFold, no templates, score3, r4': "#008000ff", \
    # 'AlphaFold, no templates, ref2015, r4': "#00d20060", \
    'AlphaFold, no templates, ref2015, r4': "#008000ff", \
    # 'AlphaFold, no templates, score3, r5': "#00ff0020", \
    'AlphaFold, no templates, score3, r5': "#008000ff", \
    # 'AlphaFold, no templates, ref2015, r5': "#00ff0020", \
    'AlphaFold, no templates, ref2015, r5': "#008000ff", \
    'AlphaFold, with templates, score3': "#00FF00", \
    'AlphaFold, with templates, ref2015': "#00FF00",\
    'RoseTTAFold, no templates, score3': "#530B6C", \
    'RoseTTAFold, no templates, ref2015': "#530B6C", \
    'native': "#000000"}

markers = {'Rosetta relax': "o", 'Rosetta no relax': "o", \
    'HybridDE no relax': "o", 'HybridDE relax': "o", \
    'CrowdingDE no relax': "o", 'CrowdingDE relax': "o", \
    'trRosetta, no templates, score3': "P", \
    'trRosetta, no templates, ref2015': "P", \
    'trRosetta, with templates, score3': "P", \
    'trRosetta, with templates, ref2015': "P", \
    'AlphaFold, no templates, score3': "X", \
    'AlphaFold, no templates, ref2015': "X", \
    'AlphaFold, no templates, score3, r1': "X", \
    'AlphaFold, no templates, ref2015, r1': "X", \
    'AlphaFold, no templates, score3, r2': "X", \
    'AlphaFold, no templates, ref2015, r2': "X", \
    'AlphaFold, no templates, score3, r3': "X", \
    'AlphaFold, no templates, ref2015, r3': "X", \
    'AlphaFold, no templates, score3, r4': "X", \
    'AlphaFold, no templates, ref2015, r4': "X", \
    'AlphaFold, no templates, score3, r5': "X", \
    'AlphaFold, no templates, ref2015, r5': "X", \
    'AlphaFold, with templates, score3': "X", \
    'AlphaFold, with templates, ref2015': "X", \
    'RoseTTAFold, no templates, score3': "^", \
    'RoseTTAFold, no templates, ref2015': "^", \
    'native': "o"}

axes_rmsd = {
    'M_norelax': (0,30),
    'M_relax': (0,30),
    'N_norelax': (0,22),
    'N_relax': (0,22),
    'nsp2_norelax': (0,80),
    'nsp2_relax': (0,60),
    'nsp4_norelax': (0,50),
    'nsp4_relax': (0,60),
    'nsp6_norelax': (0,33),
    'nsp6_relax': (0,60),
    'nsp11_norelax': (0,6),
    'nsp11_relax': (0,10),
    'nsp14_norelax': (0,50),
    'nsp14_relax': (0,50),
    'nsp3_norelax': (0,22),
    'nsp3_relax': (0,22),
    'nsp9_norelax': (0,20),
    'nsp9_relax': (0,4),
    'nsp10_norelax': (0,22),
    'nsp10_relax': (0,22),
    'orf3b_norelax': (0,10),
    'orf3b_relax': (0,22),
    'orf6_norelax': (0,30),
    'orf6_relax': (0,22),
    'orf7b_norelax': (0,20),
    'orf7b_relax': (0,22),
    'orf9c_norelax': (0,25),
    'orf9c_relax': (0,22),
    'orf10_norelax': (0,20),
    'orf10_relax': (0,22),
    'orf8_norelax': (0,22),
    'orf8_relax': (0,22),
    '1r69_norelax': (0,15),
    '1r69_relax': (0,22),
    '1j7b_norelax': (0,22),
    '1j7b_relax': (0,22),
    '1n8n_norelax': (0,30),
    '1n8n_relax': (0,30),
    '2aca_norelax': (0,22),
    '2aca_relax': (0,22),
    '2xqo_norelax': (0,25),
    '2xqo_relax': (0,22),
    '1ha8_norelax': (0,13),
    '1ha8_relax': (0,13),
    '2ksw_norelax': (0,15),
    '2ksw_relax': (0,22),
    '1mw5_norelax': (0,26),
    '1mw5_relax': (0,22),
       
}

axes_score = {
    'M_norelax': (-200,200),
    'M_relax': (-200,200),
    'N_norelax': (-125, 125),
    'N_relax': (-300, 350),
    'nsp2_norelax': (-150,600),
    'nsp2_relax': (-300,300),
    'nsp4_norelax': (-50,400),
    'nsp4_relax': (-300,300),
    'nsp6_norelax': (-75,420),
    'nsp6_relax': (-300,300),
    'nsp11_norelax': (10,50),
    'nsp11_relax': (-300,300),
    'nsp14_norelax': (-75,350),
    'nsp14_relax': (-300,300),
    'nsp3_norelax': (-140,75),
    'nsp3_relax': (-500,-180),
    'nsp9_norelax': (-200,10),
    'nsp9_relax': (-350,-150),
    'nsp10_norelax': (-120,150),
    'nsp10_relax': (-240, 100),
    'orf3b_norelax': (0,100),
    'orf3b_relax': (-250,0),
    'orf6_norelax': (-50,75),
    'orf6_relax': (-250,0),
    'orf7b_norelax': (-10,40),
    'orf7b_relax': (-250,0),
    'orf9c_norelax': (-50,250),
    'orf9c_relax': (-250,0),
    'orf10_norelax': (-50,150),
    'orf10_relax': (-250,0),    
    'orf8_norelax': (-150,200),
    'orf8_relax': (-250,0),
    '1r69_norelax': (-40,40),
    '1r69_relax': (-250,0),
    '1j7b_norelax': (-250,400),
    '1j7b_relax': (-250,400),
    '1n8n_norelax': (-100,220),
    '1n8n_relax': (-150,220),
    '2aca_norelax': (-250,400),
    '2aca_relax': (-250,400),
    '2xqo_norelax': (-100,150),
    '2xqo_relax': (-150,150),
    '1ha8_norelax': (-75,150),
    '1ha8_relax': (-150,150),
    '2ksw_norelax': (-50,75),
    '2ksw_relax': (-200,200),
    '1mw5_norelax': (-75,200),
    '1mw5_relax': (-200,200),
}

title_position_x = {
    'M_norelax': 0.25,
    'M_relax': 0.3,
    'N_norelax': 0.88,
    'N_relax': 0.25,
    'nsp2_norelax': 0.3,
    'nsp2_relax': 0.86,
    'nsp4_norelax': 0.26,
    'nsp4_relax': 0.86,
    'nsp6_norelax': 0.28,
    'nsp6_relax': 0.86,
    'nsp11_norelax': 0.84,
    'nsp11_relax': 0.86,
    'nsp14_norelax': 0.28,
    'nsp14_relax': 0.86,
    'nsp3_norelax': 0.3,
    'nsp3_relax': 0.86,
    'nsp9_norelax': 0.3,
    'nsp9_relax': 0.86,
    'nsp10_norelax': 0.3,
    'nsp10_relax': 0.86,
    'orf3b_norelax': 0.84,
    'orf3b_relax': 0.86,
    'orf6_norelax': 0.84,
    'orf6_relax': 0.86,
    'orf7b_norelax': 0.28,
    'orf7b_relax': 0.86,
    'orf9c_norelax': 0.28,
    'orf9c_relax': 0.86,
    'orf10_norelax': 0.28,
    'orf10_relax': 0.86,    
    'orf8_norelax': 0.3,
    'orf8_relax': 0.86,
    '1r69_norelax': 0.3,
    '1r69_relax': 0.86,
    '1j7b_norelax': 0.3,
    '1j7b_relax': 0.86,
    '1n8n_norelax': 0.3,
    '1n8n_relax': 0.86,
    '2aca_norelax': 0.3,
    '2aca_relax': 0.86,
    '2xqo_norelax': 0.3,
    '2xqo_relax': 0.86,
    '1ha8_norelax': 0.26,
    '1ha8_relax': 0.86,
    '2ksw_norelax': 0.3,
    '2ksw_relax': 0.86,
    '1mw5_norelax': 0.3,
    '1mw5_relax': 0.86,
}

title_position_y = {
    'M_norelax': 0.78,
    'M_relax': 0.78,
    'N_norelax': 0.78,
    'N_relax': 0.78,
    'nsp2_norelax': 0.78,
    'nsp2_relax': 0.78,
    'nsp4_norelax': 0.78,
    'nsp4_relax': 0.78,
    'nsp6_norelax': 0.78,
    'nsp6_relax': 0.78,
    'nsp11_norelax': 0.78,
    'nsp11_relax': 0.78,
    'nsp14_norelax': 0.78,
    'nsp14_relax': 0.78,
    'nsp3_norelax': 0.78,
    'nsp3_relax': 0.78,
    'nsp9_norelax':  0.78,
    'nsp9_relax':  0.78,
    'nsp10_norelax': 0.78,
    'nsp10_relax': 0.78,
    'orf3b_norelax': 0.78,
    'orf3b_relax': 0.78,
    'orf6_norelax': 0.78,
    'orf6_relax': 0.78,
    'orf7b_norelax': 0.76,
    'orf7b_relax': 0.78,
    'orf9c_norelax': 0.76,
    'orf9c_relax': 0.78,
    'orf10_norelax': 0.76,
    'orf10_relax': 0.78,
    'orf8_norelax': 0.78,
    'orf8_relax': 0.78,
    '1r69_norelax': 0.78,
    '1r69_relax': 0.78,
    '1j7b_norelax': 0.78,
    '1j7b_relax': 0.78,
    '1n8n_norelax': 0.78,
    '1n8n_relax': 0.78,
    '2aca_norelax': 0.78,
    '2aca_relax': 0.78,
    '2xqo_norelax': 0.78,
    '2xqo_relax': 0.78,
    '1ha8_norelax': 0.78,
    '1ha8_relax': 0.78,
    '2ksw_norelax': 0.78,
    '2ksw_relax': 0.78,
    '1mw5_norelax': 0.78,
    '1mw5_relax': 0.78,
}
palette = {}

def set_alpha(row):
    if 'rank_1' in row['description']:
      return row['dataset'] + ', r1'
    if 'rank_2' in row['description']:
      return row['dataset'] + ', r2'
    if 'rank_3' in row['description']:
      return row['dataset'] + ', r3'
    if 'rank_4' in row['description']:
      return row['dataset'] + ', r4'
    if 'rank_5' in row['description']:
      return row['dataset'] + ', r5'
    return row['dataset']

def split_filenames(filenames):
    evo_files, rosetta_files = [], []
    for fname in filenames:
        (evo_files if "nmpi" in fname else rosetta_files).append(fname)
    return evo_files, rosetta_files
    
def assignDataset(df, filename):
    datasetName = "Rosetta"
    hasTemplates = ""
    hasRelax = ""
    score_type = ""
    dataset = ""
    if "initial" in filename:
        return df.assign(dataset="native")
    if "alphafold" in filename:
        datasetName = "AlphaFold, "
        hasTemplates = "with templates, "
    elif "trRosetta" in filename:
        datasetName = "trRosetta, "
        hasTemplates = "with templates, "
    elif "RoseTTAFold" in filename:
        datasetName = "RoseTTAFold, "
        hasTemplates = "no templates, "
    elif "CDE" in filename:
        datasetName = "CrowdingDE"
    elif "HDE" in filename:
        datasetName = "HybridDE"
    if "not" in filename:
        hasTemplates = "no templates, "
    if "ref2015" in filename:
        score_type = "ref2015"
    elif "score3" in filename:
        score_type = "score3"
    if "norelax" in filename:
        hasRelax = " no relax"
    elif "relax" in filename:
        hasRelax = " relax"
    dataset = datasetName + hasRelax + hasTemplates + score_type
    df = df.assign(dataset=dataset)
    if "alphafold" in filename:
        df['dataset']= df.apply(lambda row: set_alpha(row), axis=1)
    return df

def parse_evolution_file(file):
    scores, rmsds = [], []
    gen = 0
    dataset = "HybridDE no relax" if "HDE" in file else "CrowdingDE no relax" 
    with open(file, 'r') as fin:
        for line in fin:
            if '[GEN]' in line:
                gen = int(line.split(' ')[1])
            if '[POP]' in line and gen == 99999:
                results = line.replace('[POP]', '').replace(' ', '').replace('\n', '').split(',')
                scores = [float(i) for i in results if i]
            elif '[RMSD_NATIVE]' in line and gen == 99999:
                results = line.replace('[RMSD_NATIVE]', '').replace(' ', '').replace('\n', '').split(',')
                rmsds = [float(i) for i in results if i]
        return pd.DataFrame({'score': scores, 'rms': rmsds}).assign(dataset=dataset)

def plot_score_file(files, prot_name="", filename=""):
    os.makedirs("figures", exist_ok=True)
    # parse files
    [evo_files, rosetta_files] = split_filenames(files)
    df_rosetta, df_evo, df = pd.DataFrame(), pd.DataFrame(), pd.DataFrame()
    if rosetta_files:
        df_rosetta = pd.concat([assignDataset(parse_rosetta_file(ifile, definition), ifile) for ifile in rosetta_files])
        df = df_rosetta
    if evo_files:
        df_evo = pd.concat([parse_evolution_file(ifile) for ifile in evo_files])
        df = df_evo
    if not df_evo.empty and not df_rosetta.empty:
        df = pd.concat([df_rosetta,df_evo])
    # df = df.assign(dataset='results')
    # if "None" not in trRosettaFile:
    #   dfrefTrRosetta = parse_rosetta_file(trRosettaFile, definition)
    #   dfrefTrRosetta = dfrefTrRosetta.assign(dataset='trRosetta')
    #   df = pd.concat([df,dfrefTrRosetta])
    # if "None" not in alphaFoldFile:
    #   dfrefAlphaFold = parse_rosetta_file(alphaFoldFile, definition)
    #   dfrefAlphaFold = dfrefAlphaFold.assign(dataset='AlphaFold')
    #   #ax.set_ylim((min(df["score"].min(), dfref["score"].min()) - 30, max(df["score"].max(), dfref["score"].max()) + 30))
    #   df = pd.concat([df,dfrefAlphaFold])
    # sns.set(font_scale=1)
    # sns.set_context("paper", rc={"font.size":10,"axes.titlesize":11,"axes.labelsize":11})
    # , rc={"font.size":5,"axes.titlesize":7,"axes.labelsize":15}
    sns.set_context("notebook", font_scale = 2)
    # df["alpha"] = 0.3 if (df["description"].contains('rank5')) else 1
    # df['alpha']= df.apply(lambda row: set_alpha(row), axis=1)

    print(df)
    ax = sns.jointplot(x="rms", y="score", data=df, hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=False)
    # ax = sns.violinplot(x="dataset", y="score", data=df[df['dataset'] =='Rosetta no relax'], legend=False)

    # ax = sns.scatterplot(x="rms", y="score", data=(df[df['dataset'] =='Rosetta no relax']), hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=False) 
    # ax = sns.scatterplot(x="rms", y="score", data=(df[df['dataset'] == 'HybridDE no relax']), hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=False) 
    # ax = sns.scatterplot(x="rms", y="score", data=(df[df['dataset'] == 'CrowdingDE no relax']), hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=False) 
    # ax = sns.scatterplot(x="rms", y="score", data=(df[df['dataset'] == 'AlphaFold, no templates, score3, r5']), hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=False) 
    # ax = sns.scatterplot(x="rms", y="score", data=(df[df['dataset'] == 'AlphaFold, no templates, score3, r4']), hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=False) 
    # ax = sns.scatterplot(x="rms", y="score", data=(df[df['dataset'] == 'AlphaFold, no templates, score3, r3']), hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=False) 
    # ax = sns.scatterplot(x="rms", y="score", data=(df[df['dataset'] == 'AlphaFold, no templates, score3, r2']), hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=False) 
    # ax = sns.scatterplot(x="rms", y="score", data=(df[df['dataset'] == 'AlphaFold, no templates, score3, r1']), hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=False) 
    isRef2015 = any('ref2015' in s for s in df.dataset) or any('_relax' in s for s in df.dataset)

    prot_properties = prot_name + "_" + ("relax" if isRef2015 else "norelax")
    # center the view of results
    ax.set_xlim((0, df["rms"].max() + 2))
    ax.set_xlim(axes_rmsd[prot_properties])
    ax.set_ylim((df["score"].min() - 30, df["score"].max() + 30))
    ax.set_ylim(axes_score[prot_properties])
    ax.set(xlabel="RMSD", ylabel="ref2015" if isRef2015 else "score3")

    # output figure
    fig = ax.get_figure()
    fig.suptitle(prot_name, x=title_position_x[prot_properties], y=title_position_y[prot_properties], weight="bold", size="28")
    fig.set_size_inches(8.7, 6.5)
    fig.tight_layout()
    name = "figures/" + prot_name + "_" + filename + ".png"
    fig.savefig(name, bbox_inches = 'tight',
    pad_inches = 0.1)
    plt.close()


def main():
    files = sys.argv[3:]
    prot_name = sys.argv[1]
    filename = sys.argv[2]

    plot_score_file(files, prot_name, filename)


if __name__ == "__main__":
    main()
