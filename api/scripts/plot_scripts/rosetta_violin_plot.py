#!/usr/bin/env python
# coding: utf-8


# rosetta_score_rmsd_plot.py
# script to plot *.fsc files from rosetta as scatter plots
# Use "python rosetta_score_rmsd_plot.py <filename> <prot name>"
# to parse several *.fsc files:
# Use "python rosetta_score_rmsd_plot.py /path_results/*/*.fsc <prot name>"

# Dependencies:
# matplotlib, pandas, seaborn and rstoolbox
import seaborn.categorical
seaborn.categorical._Old_Violin = seaborn.categorical._ViolinPlotter

class NewViolinPlotter(seaborn.categorical._Old_Violin):

    def __init__(self, *args, **kwargs):
        super(NewViolinPlotter, self).__init__(*args, **kwargs)
        self.gray='#BFF6FA'

class TransparentViolinPlotter(seaborn.categorical._Old_Violin):

    def __init__(self, *args, **kwargs):
        super(TransparentViolinPlotter, self).__init__(*args, **kwargs)
        self.gray='#ffffff00'

seaborn.categorical._ViolinPlotter = NewViolinPlotter

import os
import sys

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
from rstoolbox.io import parse_rosetta_file

definition = {"scores": ["score", "rms", "I_sc", "Irms", "Fnat", "time", "description"]}

colors_violin = {'Rosetta relax': "#606060", 'Rosetta no relax': "#606060", \
    'HybridDE no relax': "#000080", 'HybridDE relax': "#000080", \
    'CrowdingDE no relax': "#990000", 'CrowdingDE relax': "#990000", \
    'trRosetta, no templates, score3': "#ffffff", \
    'trRosetta, no templates, ref2015': "#ffffff", \
    'trRosetta, with templates, score3': "#ffffff", \
    'trRosetta, with templates, ref2015': "#ffffff", \
    'AlphaFold, no templates, score3': "#ffffff", \
    'AlphaFold, no templates, ref2015': "#ffffff", \
    'AlphaFold, no templates, score3, r1': "#ffffff", \
    'AlphaFold, no templates, ref2015, r1': "#ffffff", \
    # 'AlphaFold, no templates, score3, r2': "#009600b4", \
    'AlphaFold, no templates, score3, r2': "#ffffff", \
    # 'AlphaFold, no templates, ref2015, r2': "#009600b4", \
    'AlphaFold, no templates, ref2015, r2': "#ffffff", \
    # 'AlphaFold, no templates, score3, r3': "#00b40096", \
    'AlphaFold, no templates, score3, r3': "#ffffff", \
    # 'AlphaFold, no templates, ref2015, r3': "#00b40096", \
    'AlphaFold, no templates, ref2015, r3': "#ffffff", \
    # 'AlphaFold, no templates, score3, r4': "#00d20060", \
    'AlphaFold, no templates, score3, r4': "#ffffff", \
    # 'AlphaFold, no templates, ref2015, r4': "#00d20060", \
    'AlphaFold, no templates, ref2015, r4': "#ffffff", \
    # 'AlphaFold, no templates, score3, r5': "#00ff0020", \
    'AlphaFold, no templates, score3, r5': "#ffffff", \
    # 'AlphaFold, no templates, ref2015, r5': "#00ff0020", \
    'AlphaFold, no templates, ref2015, r5': "#ffffff", \
    'AlphaFold, with templates, score3': "#ffffff", \
    'AlphaFold, with templates, ref2015': "#ffffff",\
    'RoseTTAFold, no templates, score3': "#ffffff", \
    'RoseTTAFold, no templates, ref2015': "#ffffff", \
    'native': "#000000"}

colors = {'Rosetta relax': "#606060", 'Rosetta no relax': "#606060", \
    'HybridDE no relax': "#000080", 'HybridDE relax': "#000080", \
    'CrowdingDE no relax': "#990000", 'CrowdingDE relax': "#990000", \
    'trRosetta, no templates, score3': "#FF0000", \
    'trRosetta, no templates, ref2015': "#FF0000", \
    'trRosetta, with templates, score3': "#FA8072", \
    'trRosetta, with templates, ref2015': "#FA8072", \
    'AlphaFold, no templates, score3': "#008000", \
    'AlphaFold, no templates, ref2015': "#008000", \
    'AlphaFold, no templates, score3, r1': "#008000ff", \
    'AlphaFold, no templates, ref2015, r1': "#008000ff", \
    # 'AlphaFold, no templates, score3, r2': "#009600b4", \
    'AlphaFold, no templates, score3, r2': "#008000ff", \
    # 'AlphaFold, no templates, ref2015, r2': "#009600b4", \
    'AlphaFold, no templates, ref2015, r2': "#008000ff", \
    # 'AlphaFold, no templates, score3, r3': "#00b40096", \
    'AlphaFold, no templates, score3, r3': "#008000ff", \
    # 'AlphaFold, no templates, ref2015, r3': "#00b40096", \
    'AlphaFold, no templates, ref2015, r3': "#008000ff", \
    # 'AlphaFold, no templates, score3, r4': "#00d20060", \
    'AlphaFold, no templates, score3, r4': "#008000ff", \
    # 'AlphaFold, no templates, ref2015, r4': "#00d20060", \
    'AlphaFold, no templates, ref2015, r4': "#008000ff", \
    # 'AlphaFold, no templates, score3, r5': "#00ff0020", \
    'AlphaFold, no templates, score3, r5': "#008000ff", \
    # 'AlphaFold, no templates, ref2015, r5': "#00ff0020", \
    'AlphaFold, no templates, ref2015, r5': "#008000ff", \
    'AlphaFold, with templates, score3': "#00FF00", \
    'AlphaFold, with templates, ref2015': "#00FF00",\
    'RoseTTAFold, no templates, score3': "#530B6C", \
    'RoseTTAFold, no templates, ref2015': "#530B6C", \
    'native': "#000000"}

markers = {'Rosetta relax': "o", 'Rosetta no relax': "o", \
    'HybridDE no relax': "o", 'HybridDE relax': "o", \
    'CrowdingDE no relax': "o", 'CrowdingDE relax': "o", \
    'trRosetta, no templates, score3': "P", \
    'trRosetta, no templates, ref2015': "P", \
    'trRosetta, with templates, score3': "P", \
    'trRosetta, with templates, ref2015': "P", \
    'AlphaFold, no templates, score3': "X", \
    'AlphaFold, no templates, ref2015': "X", \
    'AlphaFold, no templates, score3, r1': "X", \
    'AlphaFold, no templates, ref2015, r1': "X", \
    'AlphaFold, no templates, score3, r2': "X", \
    'AlphaFold, no templates, ref2015, r2': "X", \
    'AlphaFold, no templates, score3, r3': "X", \
    'AlphaFold, no templates, ref2015, r3': "X", \
    'AlphaFold, no templates, score3, r4': "X", \
    'AlphaFold, no templates, ref2015, r4': "X", \
    'AlphaFold, no templates, score3, r5': "X", \
    'AlphaFold, no templates, ref2015, r5': "X", \
    'AlphaFold, with templates, score3': "X", \
    'AlphaFold, with templates, ref2015': "X", \
    'RoseTTAFold, no templates, score3': "^", \
    'RoseTTAFold, no templates, ref2015': "^", \
    'native': "o"}

axes_rmsd = {
    'M_norelax': (0,30),
    'M_relax': (0,30),
    'N_norelax': (0,22),
    'N_relax': (0,22),
    'nsp2_norelax': (0,80),
    'nsp2_relax': (0,60),
    'nsp4_norelax': (0,50),
    'nsp4_relax': (0,60),
    'nsp6_norelax': (0,33),
    'nsp6_relax': (0,60),
    'nsp11_norelax': (0,6),
    'nsp11_relax': (0,10),
    'nsp14_norelax': (0,50),
    'nsp14_relax': (0,50),
    'nsp3_norelax': (0,22),
    'nsp3_relax': (0,22),
    'nsp9_norelax': (0,20),
    'nsp9_relax': (0,20),
    'nsp10_norelax': (0,22),
    'nsp10_relax': (0,22),
    'orf3b_norelax': (0,10),
    'orf3b_relax': (0,22),
    'orf6_norelax': (0,30),
    'orf6_relax': (0,22),
    'orf7b_norelax': (0,20),
    'orf7b_relax': (0,22),
    'orf9c_norelax': (0,25),
    'orf9c_relax': (0,22),
    'orf10_norelax': (0,20),
    'orf10_relax': (0,22),
    'orf8_norelax': (0,23),
    'orf8_relax': (0,22),
    '1r69_norelax': (0,15),
    '1r69_relax': (0,22),
    '1j7b_norelax': (0,22),
    '1j7b_relax': (0,22),
    '1n8n_norelax': (0,30),
    '1n8n_relax': (0,30),
    '2aca_norelax': (0,22),
    '2aca_relax': (0,22),
    '2xqo_norelax': (0,25),
    '2xqo_relax': (0,22),
    '1ha8_norelax': (0,13),
    '1ha8_relax': (0,13),
    '2ksw_norelax': (0,15),
    '2ksw_relax': (0,22),
    '1mw5_norelax': (0,26),
    '1mw5_relax': (0,22),
       
}

axes_score = {
    'M_norelax': (-200,200),
    'M_relax': (-200,200),
    'N_norelax': (-110, 90),
    'N_relax': (-275, -80),
    'nsp2_norelax': (-120,500),
    'nsp2_relax': (-300,300),
    'nsp4_norelax': (-50,400),
    'nsp4_relax': (-300,300),
    'nsp6_norelax': (-75,420),
    'nsp6_relax': (-300,300),
    'nsp11_norelax': (10,50),
    'nsp11_relax': (-300,300),
    'nsp14_norelax': (-75,350),
    'nsp14_relax': (-300,300),
    'nsp3_norelax': (-140,75),
    'nsp3_relax': (-500,-180),
    'nsp9_norelax': (-200,30),
    'nsp9_relax': (-290,-95),
    'nsp10_norelax': (-120,150),
    'nsp10_relax': (-240, 100),
    'orf3b_norelax': (0,100),
    'orf3b_relax': (-250,0),
    'orf6_norelax': (-50,75),
    'orf6_relax': (-250,0),
    'orf7b_norelax': (-10,40),
    'orf7b_relax': (-250,0),
    'orf9c_norelax': (-50,250),
    'orf9c_relax': (-250,0),
    'orf10_norelax': (-50,150),
    'orf10_relax': (-250,0),    
    'orf8_norelax': (-150,160),
    'orf8_relax': (-250,-30),
    '1r69_norelax': (-40,40),
    '1r69_relax': (-250,0),
    '1j7b_norelax': (-250,400),
    '1j7b_relax': (-250,400),
    '1n8n_norelax': (-100,220),
    '1n8n_relax': (-150,220),
    '2aca_norelax': (-250,400),
    '2aca_relax': (-250,400),
    '2xqo_norelax': (-100,150),
    '2xqo_relax': (-150,150),
    '1ha8_norelax': (-75,150),
    '1ha8_relax': (-150,150),
    '2ksw_norelax': (-50,75),
    '2ksw_relax': (-200,200),
    '1mw5_norelax': (-75,200),
    '1mw5_relax': (-200,200),
}

title_position_x = {
    'M_norelax': 0.25,
    'M_relax': 0.95,
    'N_norelax': 0.88,
    'N_relax': 0.25,
    'nsp2_norelax': 0.95,
    'nsp2_relax': 0.86,
    'nsp4_norelax': 0.26,
    'nsp4_relax': 0.86,
    'nsp6_norelax': 0.28,
    'nsp6_relax': 0.86,
    'nsp11_norelax': 0.84,
    'nsp11_relax': 0.86,
    'nsp14_norelax': 0.28,
    'nsp14_relax': 0.86,
    'nsp3_norelax': 0.95,
    'nsp3_relax': 0.86,
    'nsp9_norelax': 0.95,
    'nsp9_relax': 0.86,
    'nsp10_norelax': 0.95,
    'nsp10_relax': 0.86,
    'orf3b_norelax': 0.84,
    'orf3b_relax': 0.86,
    'orf6_norelax': 0.84,
    'orf6_relax': 0.86,
    'orf7b_norelax': 0.28,
    'orf7b_relax': 0.86,
    'orf9c_norelax': 0.28,
    'orf9c_relax': 0.86,
    'orf10_norelax': 0.28,
    'orf10_relax': 0.86,    
    'orf8_norelax': 0.95,
    'orf8_relax': 0.86,
    '1r69_norelax': 0.95,
    '1r69_relax': 0.86,
    '1j7b_norelax': 0.95,
    '1j7b_relax': 0.86,
    '1n8n_norelax': 0.95,
    '1n8n_relax': 0.86,
    '2aca_norelax': 0.95,
    '2aca_relax': 0.86,
    '2xqo_norelax': 0.95,
    '2xqo_relax': 0.86,
    '1ha8_norelax': 0.26,
    '1ha8_relax': 0.86,
    '2ksw_norelax': 0.95,
    '2ksw_relax': 0.86,
    '1mw5_norelax': 0.95,
    '1mw5_relax': 0.86,
}

title_position_y = {
    'M_norelax': 0.82,
    'M_relax': 0.82,
    'N_norelax': 0.82,
    'N_relax': 0.82,
    'nsp2_norelax': 0.82,
    'nsp2_relax': 0.82,
    'nsp4_norelax': 0.82,
    'nsp4_relax': 0.82,
    'nsp6_norelax': 0.82,
    'nsp6_relax': 0.82,
    'nsp11_norelax': 0.82,
    'nsp11_relax': 0.82,
    'nsp14_norelax': 0.82,
    'nsp14_relax': 0.82,
    'nsp3_norelax': 0.82,
    'nsp3_relax': 0.82,
    'nsp9_norelax':  0.82,
    'nsp9_relax':  0.82,
    'nsp10_norelax': 0.82,
    'nsp10_relax': 0.82,
    'orf3b_norelax': 0.82,
    'orf3b_relax': 0.82,
    'orf6_norelax': 0.82,
    'orf6_relax': 0.82,
    'orf7b_norelax': 0.82,
    'orf7b_relax': 0.82,
    'orf9c_norelax': 0.82,
    'orf9c_relax': 0.82,
    'orf10_norelax': 0.82,
    'orf10_relax': 0.82,
    'orf8_norelax': 0.82,
    'orf8_relax': 0.82,
    '1r69_norelax': 0.82,
    '1r69_relax': 0.82,
    '1j7b_norelax': 0.82,
    '1j7b_relax': 0.82,
    '1n8n_norelax': 0.82,
    '1n8n_relax': 0.82,
    '2aca_norelax': 0.82,
    '2aca_relax': 0.82,
    '2xqo_norelax': 0.82,
    '2xqo_relax': 0.82,
    '1ha8_norelax': 0.82,
    '1ha8_relax': 0.82,
    '2ksw_norelax': 0.85,
    '2ksw_relax': 0.82,
    '1mw5_norelax': 0.82,
    '1mw5_relax': 0.82,
}
palette = {}

def adjacent_values(vals, q1, q3):
    upper_adjacent_value = q3 + (q3 - q1) * 1.5
    upper_adjacent_value = np.clip(upper_adjacent_value, q3, vals[-1])

    lower_adjacent_value = q1 - (q3 - q1) * 1.5
    lower_adjacent_value = np.clip(lower_adjacent_value, vals[0], q1)
    return lower_adjacent_value, upper_adjacent_value
    
def set_alpha(row):
    if 'rank_1' in row['description']:
      return row['dataset'] + ', r1'
    if 'rank_2' in row['description']:
      return row['dataset'] + ', r2'
    if 'rank_3' in row['description']:
      return row['dataset'] + ', r3'
    if 'rank_4' in row['description']:
      return row['dataset'] + ', r4'
    if 'rank_5' in row['description']:
      return row['dataset'] + ', r5'
    return row['dataset']

def split_filenames(filenames):
    evo_files, rosetta_files = [], []
    for fname in filenames:
        (evo_files if "nmpi" in fname else rosetta_files).append(fname)
    return evo_files, rosetta_files
    
def assignDataset(df, filename):
    datasetName = "Rosetta"
    hasTemplates = ""
    hasRelax = ""
    score_type = ""
    dataset = ""
    if "initial" in filename:
        return df.assign(dataset="native")
    if "alphafold" in filename:
        datasetName = "AlphaFold, "
        hasTemplates = "with templates, "
    elif "trRosetta" in filename:
        datasetName = "trRosetta, "
        hasTemplates = "with templates, "
    elif "RoseTTAFold" in filename:
        datasetName = "RoseTTAFold, "
        hasTemplates = "no templates, "
    elif "CDE" in filename:
        datasetName = "CrowdingDE"
    elif "HDE" in filename:
        datasetName = "HybridDE"
    if "not" in filename:
        hasTemplates = "no templates, "
    if "ref2015" in filename:
        score_type = "ref2015"
    elif "score3" in filename:
        score_type = "score3"
    if "norelax" in filename:
        hasRelax = " no relax"
    elif "relax" in filename:
        hasRelax = " relax"
    dataset = datasetName + hasRelax + hasTemplates + score_type
    df = df.assign(dataset=dataset)
    # if "alphafold" in filename:
        # df['dataset']= df.apply(lambda row: set_alpha(row), axis=1)
    return df

def parse_evolution_file(file):
    scores, rmsds = [], []
    gen = 0
    dataset = "HybridDE no relax" if "HDE" in file else "CrowdingDE no relax" 
    with open(file, 'r') as fin:
        for line in fin:
            if '[GEN]' in line:
                gen = int(line.split(' ')[1])
            if '[POP]' in line and gen == 99999:
                results = line.replace('[POP]', '').replace(' ', '').replace('\n', '').split(',')
                scores = [float(i) for i in results if i]
            elif '[RMSD_NATIVE]' in line and gen == 99999:
                results = line.replace('[RMSD_NATIVE]', '').replace(' ', '').replace('\n', '').split(',')
                rmsds = [float(i) for i in results if i]
        return pd.DataFrame({'score': scores, 'rms': rmsds}).assign(dataset=dataset)

def print_whiskers(df, axes, column, axes_no):
    in_data = [list(d) for _, d in df.groupby(['dataset'], sort=False)[column]]
    quartile1 = []
    medians = []
    quartile3 = []
    max_min = []
    ax_min = axes[axes_no].get_xlim()[0]
    ax_max = axes[axes_no].get_xlim()[1]
    for my_data in in_data:
        quartile1_, medians_, quartile3_ = np.percentile(my_data, [25, 50, 75])
        quartile1.append(quartile1_)
        # medians.append(np.average(my_data))
        medians.append(medians_)
        quartile3.append(quartile3_)
        max_min.append(((np.min(my_data)-ax_min)/(ax_max-ax_min),(np.max(in_data[0])-ax_min)/(ax_max-ax_min)))
    whiskers = np.array([adjacent_values(sorted_array, q1, q3) for sorted_array, q1, q3 in zip(in_data, quartile1, quartile3)])
    whiskersMin, whiskersMax = whiskers[:, 0], whiskers[:, 1]
    inds = np.arange(0, len(medians))
    # for (median, q1, q3, m_m, i) in zip(medians_, quartile1, quartile3, max_min, inds):

    axes[axes_no].scatter(medians[:3], inds[:3], marker='*', color='#00FFFF', s=100, zorder=300)
    axes[axes_no].scatter(quartile1[:3], inds[:3], marker='|', color='black',s=250, zorder=300)
    axes[axes_no].scatter(quartile3[:3], inds[:3], marker='|', color='black',s=250, zorder=300)

    # if (column =="score"):
    #     # axes[axes_no].axhline(0, 0, violin_vals_r[0], color="lightgrey", lw=.8)
    #     # axes[axes_no].axhline(0, violin_vals_r[1], 1, color="lightgrey", lw=.8)
    #     axes[axes_no].axhline(0, violin_vals_r[0], violin_vals_r[1], color="white", lw=0.6)
    #     axes[axes_no].axhline(1, 0, violin_vals_h[0], color="black", lw=.6)
    #     axes[axes_no].axhline(1, violin_vals_h[1], 1, color="black", lw=.6)
    #     axes[axes_no].axhline(1, violin_vals_h[0], violin_vals_h[1], color="white", lw=0.6)
    #     axes[axes_no].axhline(2, 0, violin_vals_c[0], color="black", lw=.6)
    #     axes[axes_no].axhline(2, violin_vals_c[1], 1, color="black", lw=.6)
    #     axes[axes_no].axhline(2, violin_vals_c[0], violin_vals_c[1], color="white", lw=0.6)
    #     axes[axes_no].scatter(medians[:2], inds[:2], marker='o', color='black', s=30, zorder=3)
    #     axes[axes_no].scatter(quartile1[:2], inds[:2], marker='|', color='black',s=250, zorder=3)
    #     axes[axes_no].scatter(quartile3[:2], inds[:2], marker='|', color='black',s=250, zorder=3)
    #     axes[axes_no].scatter(medians[2], inds[2], marker='o', color='white', s=30, zorder=3)
    #     axes[axes_no].scatter(quartile1[2], inds[2], marker='|', color='white',s=250, zorder=3)
    #     axes[axes_no].scatter(quartile3[2], inds[2], marker='|', color='white',s=250, zorder=3)
    # else:
    #     axes[axes_no].scatter(medians, inds, marker='o', color='lightgrey', s=30, zorder=3)
    #     axes[axes_no].scatter(quartile1, inds, marker='|', color='lightgrey',s=250, zorder=3)
    #     axes[axes_no].scatter(quartile3, inds, marker='|', color='lightgrey',s=250, zorder=3)

def replace_y_labels(ax):
    labels = {'Rosetta relax': "Rosetta ab i.", 'Rosetta no relax': "Rosetta ab i.", \
    'HybridDE no relax': "HybridDE", 'HybridDE relax': "HybridDE", \
    'CrowdingDE no relax': "CrowdingDE", 'CrowdingDE relax': "CrowdingDE", \
    'trRosetta, no templates, score3': "trRosetta", \
    'trRosetta, no templates, ref2015': "trRosetta", \
    'AlphaFold, no templates, score3': "AlphaFold2", \
    'AlphaFold, no templates, ref2015': "AlphaFold2", \
    'RoseTTAFold, no templates, score3': "RoseTTAFold", \
    'RoseTTAFold, no templates, ref2015': "RoseTTAFold", \
    'native': "native"}

    print(ax.get_yticklabels())
    for label in ax.get_yticklabels():
        label.set_text(labels[label.get_text()])

def plot_score_file(files, prot_name="", filename=""):
    os.makedirs("figures", exist_ok=True)
    # parse files
    [evo_files, rosetta_files] = split_filenames(files)
    df_rosetta, df_evo, df = pd.DataFrame(), pd.DataFrame(), pd.DataFrame()
    if rosetta_files:
        df_rosetta = pd.concat([assignDataset(parse_rosetta_file(ifile, definition), ifile) for ifile in rosetta_files])
        df = df_rosetta
    if evo_files:
        df_evo = pd.concat([parse_evolution_file(ifile) for ifile in evo_files])
        df = df_evo
    if not df_evo.empty and not df_rosetta.empty:
        df = pd.concat([df_rosetta,df_evo])
    # df = df.assign(dataset='results')
    # if "None" not in trRosettaFile:
    #   dfrefTrRosetta = parse_rosetta_file(trRosettaFile, definition)
    #   dfrefTrRosetta = dfrefTrRosetta.assign(dataset='trRosetta')
    #   df = pd.concat([df,dfrefTrRosetta])
    # if "None" not in alphaFoldFile:
    #   dfrefAlphaFold = parse_rosetta_file(alphaFoldFile, definition)
    #   dfrefAlphaFold = dfrefAlphaFold.assign(dataset='AlphaFold')
    #   #ax.set_ylim((min(df["score"].min(), dfref["score"].min()) - 30, max(df["score"].max(), dfref["score"].max()) + 30))
    #   df = pd.concat([df,dfrefAlphaFold])
    # sns.set(font_scale=1)
    # sns.set_context("paper", rc={"font.size":10,"axes.titlesize":11,"axes.labelsize":11})
    # , rc={"font.size":5,"axes.titlesize":7,"axes.labelsize":15}
    sns.set_context("notebook", font_scale = 1.6, rc={'figure.figsize':(11.7,8.27)})
    # df["alpha"] = 0.3 if (df["description"].contains('rank5')) else 1
    # df['alpha']= df.apply(lambda row: set_alpha(row), axis=1)
    #ax = sns.scatterplot(x="rms", y="score", data=df, hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=False)
    fig, axes = plt.subplots(1, 2)

    order = {'Rosetta no relax': 0, 'Rosetta relax': 0, 'HybridDE no relax': 1, 'HybridDE relax': 1, 'CrowdingDE no relax': 2, 'CrowdingDE relax': 2, 'trRosetta, no templates, score3': 3, 'trRosetta, no templates, ref2015': 3, 'AlphaFold, no templates, score3': 4, 'AlphaFold, no templates, ref2015': 4, 'RoseTTAFold, no templates, score3': 5, 'RoseTTAFold, no templates, ref2015':5 } 
    #order = {'Rosetta no relax': 0, 'HybridDE no relax': 1, 'CrowdingDE no relax': 2} 
    
    df = df.sort_values(by=['dataset'], key=lambda x: x.map(order))

    scatter_data = ['trRosetta, no templates, score3', 'trRosetta, no templates, ref2015', 'AlphaFold, no templates, score3', 'AlphaFold, no templates, ref2015', 'RoseTTAFold, no templates, score3', 'RoseTTAFold, no templates, ref2015']
    violin_data = ['Rosetta no relax', 'HybridDE no relax', 'CrowdingDE no relax', 'Rosetta relax', 'HybridDE relax', 'CrowdingDE relax']
    df_scatter = df.loc[df['dataset'].isin(scatter_data)]
    df_violin = df.loc[df['dataset'].isin(violin_data)]
    print(df_scatter)
    y_scatter = np.concatenate((np.full((len(df_scatter[df_scatter['dataset'].str.contains('trRosetta')]), 1), 3),np.full((len(df_scatter[df_scatter['dataset'].str.contains('AlphaFold')]),1),4),np.full((len(df_scatter[df_scatter['dataset'].str.contains('RoseTTAFold')]),1),5))).flatten()
    # plt.setp(ax0.lines, zorder=100)
    # plt.setp(ax0.collections, zorder=100, label="")
    # plt.setp(ax1.lines, zorder=100)
    # plt.setp(ax1.collections, zorder=100, label="")
    isRef2015 = any('ref2015' in s for s in df.dataset) or any('_relax' in s for s in df.dataset)

    if not isRef2015:
        ax0 = sns.violinplot(ax=axes[0], data=df, scale='width', x="rms", y="dataset", linewidth=1, order=["Rosetta no relax", "HybridDE no relax", "CrowdingDE no relax"], palette=colors_violin, saturation=.3)
    else:
        ax0 = sns.violinplot(ax=axes[0], data=df, scale='width', x="rms", y="dataset", linewidth=1, order=["Rosetta relax", "HybridDE relax", "CrowdingDE relax"], palette=colors_violin, saturation=.3)
    if not isRef2015:
        ax1 = sns.violinplot(ax=axes[1], data=df, scale='width', x="score", y="dataset", linewidth=1, order=["Rosetta no relax", "HybridDE no relax", "CrowdingDE no relax"], palette=colors_violin, saturation=.3)
    else:
        ax1 = sns.violinplot(ax=axes[1], data=df, scale='width', x="score", y="dataset", linewidth=1, order=["Rosetta relax", "HybridDE relax", "CrowdingDE relax"], palette=colors_violin, saturation=.3)
    ax0 = sns.scatterplot(ax=axes[0], x="rms", y=y_scatter, data=df_scatter, hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=False)
    ax1 = sns.scatterplot(ax=axes[1], x="score", y=y_scatter, data=df_scatter, hue='dataset', style='dataset', palette=colors, markers=markers, s=150, legend=False)
    plt.setp(ax0.lines, zorder=100)
    plt.setp(ax0.collections, zorder=100, label="")
    plt.setp(ax1.lines, zorder=100)
    plt.setp(ax1.collections, zorder=100, label="")
    seaborn.categorical._ViolinPlotter = TransparentViolinPlotter
    if not isRef2015:
        ax0 = sns.violinplot(ax=axes[0], data=df, scale='width', x="rms", y="dataset", linewidth=1, order=["Rosetta no relax", "HybridDE no relax", "CrowdingDE no relax", "trRosetta, no templates, score3", "AlphaFold, no templates, score3", "RoseTTAFold, no templates, score3"], color="white", saturation=.3)
    else:
        ax0 = sns.violinplot(ax=axes[0], data=df, scale='width', x="rms", y="dataset", linewidth=1, order=["Rosetta relax", "HybridDE relax", "CrowdingDE relax", "trRosetta, no templates, ref2015", "AlphaFold, no templates, ref2015", "RoseTTAFold, no templates, ref2015"], color="white", saturation=.3)
    if not isRef2015:
        ax1 = sns.violinplot(ax=axes[1], data=df, scale='width', x="score", y="dataset", linewidth=1, order=["Rosetta no relax", "HybridDE no relax", "CrowdingDE no relax", "trRosetta, no templates, score3", "AlphaFold, no templates, score3", "RoseTTAFold, no templates, score3"], color="white", saturation=.3)
    else:
        ax1 = sns.violinplot(ax=axes[1], data=df, scale='width', x="score", y="dataset", linewidth=1, order=["Rosetta relax", "HybridDE relax", "CrowdingDE relax", "trRosetta, no templates, ref2015", "AlphaFold, no templates, ref2015", "RoseTTAFold, no templates, ref2015"], color="white", saturation=.3)

    axes[0].set_ylabel('')
    axes[1].set_ylabel('')
    replace_y_labels(axes[0])
    axes[0].set_yticklabels(axes[0].get_yticklabels(), rotation = 30)
    axes[1].set_yticklabels('')
    print_whiskers(df, axes, 'rms', 0)
    print_whiskers(df, axes, 'score', 1)
   
    # ax = sns.violinplot(x="dataset", y="score", data=df[df['dataset'] =='Rosetta no relax'], legend=False)
    axes[0].set_xlabel('RMSD', fontsize=24)
    axes[1].set_xlabel('score3' if not isRef2015 else 'ref2015', fontsize=24)
    prot_properties = prot_name + "_" + ("relax" if isRef2015 else "norelax")
    # center the view of results
    # ax0.set_xlim((0, df["rms"].max()))
    ax0.set_xlim(axes_rmsd[prot_properties])
    # ax1.set_xlim((df["score"].min() - 30, df["score"].max() + 30))
    ax1.set_xlim(axes_score[prot_properties])
    # ax.set(xlabel="RMSD", ylabel="ref2015" if isRef2015 else "score3")

    # output figure
    fig = axes[0].get_figure()
    fig.suptitle(prot_name, x=0.13, y=0.84, weight="bold", size="26")
    fig.set_size_inches(20, 7.5)
    # fig.set_size_inches(18, 5)
    fig.tight_layout()
    name = "figures/" + prot_name + "_" + filename + ".png"
    fig.savefig(name, bbox_inches = 'tight', pad_inches = 0.1)
    plt.close()


def main():
    files = sys.argv[3:]
    prot_name = sys.argv[1]
    filename = sys.argv[2]

    plot_score_file(files, prot_name, filename)


if __name__ == "__main__":
    main()
