#!/usr/bin/env python
# coding: utf-8


# rosetta_score_rmsd_plot.py
# script to plot *.fsc files from rosetta as scatter plots
# Use "python rosetta_score_rmsd_plot.py <filename> <prot name>"
# to parse several *.fsc files:
# Use "python rosetta_score_rmsd_plot.py /path_results/*/*.fsc <prot name>"

# Dependencies:
# matplotlib, pandas, seaborn and rstoolbox

import os
import sys

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from rstoolbox.io import parse_rosetta_file
definition = {"scores": ["score", "rms", "I_sc", "Irms", "Fnat", "time", "description"]}

def parse_file(file):
    scores, rmsds = [], []
    gen = 0
    with open(file, 'r') as fin:
        for line in fin:
            if '[GEN]' in line:
                gen = int(line.split(' ')[1])
            if '[POP]' in line and gen == 99999:
                results = line.replace('[POP]', '').replace(' ', '').replace('\n', '').split(',')
                print(results)
                scores = [float(i) for i in results if i]
            elif '[RMSD_NATIVE]' in line and gen == 99999:
                results = line.replace('[RMSD_NATIVE]', '').replace(' ', '').replace('\n', '').split(',')
                print(results)
                rmsds = [float(i) for i in results if i]
        return pd.DataFrame({'score': scores, 'rms':rmsds})


def get_mean(files):
    filepath = os.path.dirname(files[0])
    # os.makedirs("figures", exist_ok=True)
    # parse files
    df = pd.concat([parse_rosetta_file(ifile, definition) for ifile in files])
    # df = pd.concat([parse_file(file) for file in files])
    print("RMSD: " , str(df["rms"].mean()))
    print("Score: ", str(df["score"].mean()))

def main():
    filenames = sys.argv[1:]
    # prot_name = sys.argv[1]
    get_mean(filenames)

if __name__ == "__main__":
    main()
