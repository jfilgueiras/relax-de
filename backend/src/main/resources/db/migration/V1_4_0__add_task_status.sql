ALTER TABLE `RefinementCall` ADD COLUMN `taskState` VARCHAR(20);
ALTER TABLE `RefinementCall` ALTER COLUMN `taskState` SET NOT NULL;