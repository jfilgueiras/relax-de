package es.udc.relaxed.backend.model.pojo;

public class ExecutionConfirmation {

	private String taskId;
	private String executionName;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getExecutionName() {
		return executionName;
	}

	public void setExecutionName(String executionName) {
		this.executionName = executionName;
	}
}
