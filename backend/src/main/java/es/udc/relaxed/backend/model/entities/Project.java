package es.udc.relaxed.backend.model.entities;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.BatchSize;

import es.udc.relaxed.backend.model.enums.Accesibility;

@Entity
@BatchSize(size=10)
public class Project {

	private Long id;
	private String name;
	private String description;
	private Accesibility accesibility;
	private LocalDateTime date;
	private User user;
	private ApiDirectory apiDirectory;
	
	public Project() {}

	public Project(String name, String description, Accesibility accesibility, User user) {
		super();
		this.name = name;
		this.description = description;
		this.accesibility = accesibility;
		this.date = LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS);
		this.user = user;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Enumerated(EnumType.STRING)	
	public Accesibility getAccesibility() {
		return accesibility;
	}

	public void setAccesibility(Accesibility accesibility) {
		this.accesibility = accesibility;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(name="userId")
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	@OneToOne(optional=true, cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="apiDirectoryId")
	public ApiDirectory getApiDirectory() {
		return apiDirectory;
	}

	public void setApiDirectory(ApiDirectory apiDirectory) {
		this.apiDirectory = apiDirectory;
	}

}
