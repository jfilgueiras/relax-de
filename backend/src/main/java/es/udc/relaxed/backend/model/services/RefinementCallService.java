package es.udc.relaxed.backend.model.services;

import java.util.List;

import es.udc.relaxed.backend.model.entities.RefinementCall;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;

public interface RefinementCallService {

	Block<RefinementCall> getUserRefinementCalls(Long callerId, Long projectId, int page, int size)
			throws InstanceNotFoundException;

	List<String> getUserConfigNames(Long callerId) throws InstanceNotFoundException;

	RefinementCall getRefinementByConfigName(Long callerId, String configName) throws InstanceNotFoundException;

}
