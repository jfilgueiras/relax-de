package es.udc.relaxed.backend.rest.dtos;

import java.util.List;

public class RefinementResultDto {

	private List<GenInfoDto> gensInfo;
	private List<IndInfoDto> populInfo;
	private String resultPDB;
	private String nativePDB;
	private RefinementCallDto refinementCall;
	
	public RefinementResultDto() {
	}

	public RefinementResultDto(List<GenInfoDto> gensInfo, List<IndInfoDto> populInfo, String resultPDB, String nativePDB, RefinementCallDto refinementCall) {
		super();
		this.gensInfo = gensInfo;
		this.populInfo = populInfo;
		this.resultPDB = resultPDB;
		this.nativePDB = nativePDB;
		this.refinementCall = refinementCall;
	}

	public List<GenInfoDto> getGensInfo() {
		return gensInfo;
	}

	public void setGensInfo(List<GenInfoDto> gensInfo) {
		this.gensInfo = gensInfo;
	}

	public List<IndInfoDto> getPopulInfo() {
		return populInfo;
	}

	public void setPopulInfo(List<IndInfoDto> populInfo) {
		this.populInfo = populInfo;
	}

	public String getResultPDB() {
		return resultPDB;
	}

	public void setResultPDB(String resultPDB) {
		this.resultPDB = resultPDB;
	}

	public String getNativePDB() {
		return nativePDB;
	}

	public void setNativePDB(String nativePDB) {
		this.nativePDB = nativePDB;
	}

	public RefinementCallDto getRefinementCall() {
		return refinementCall;
	}

	public void setRefinementCall(RefinementCallDto refinementCall) {
		this.refinementCall = refinementCall;
	}

}
