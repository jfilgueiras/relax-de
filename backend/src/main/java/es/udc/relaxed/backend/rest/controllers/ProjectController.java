package es.udc.relaxed.backend.rest.controllers;

import static es.udc.relaxed.backend.rest.dtos.ProjectConversor.toProjectDto;
import static es.udc.relaxed.backend.rest.dtos.ProjectConversor.toProjectSummaryDtos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.udc.relaxed.backend.model.entities.Project;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.exceptions.UnauthorizedProjectAccessException;
import es.udc.relaxed.backend.model.exceptions.UnhandledAPIException;
import es.udc.relaxed.backend.model.services.Block;
import es.udc.relaxed.backend.model.services.ProjectService;
import es.udc.relaxed.backend.rest.dtos.AddProjectParamsDto;
import es.udc.relaxed.backend.rest.dtos.BlockDto;
import es.udc.relaxed.backend.rest.dtos.IdDto;
import es.udc.relaxed.backend.rest.dtos.ProjectDto;
import es.udc.relaxed.backend.rest.dtos.ProjectSummaryDto;

@RestController
@RequestMapping("/projects")
public class ProjectController {

	@Autowired
	private ProjectService projectService;

	@GetMapping("/{projectId}")
	public ProjectDto getProjectDetail(@RequestAttribute Long userId, @PathVariable Long projectId)
			throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		return toProjectDto(projectService.getProjectDetail(userId, projectId), userId);
	}

	@PostMapping("/{projectId}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteProject(@RequestAttribute Long userId, @PathVariable Long projectId)
			throws InstanceNotFoundException, UnauthorizedProjectAccessException, UnhandledAPIException {
		projectService.deleteProject(userId, projectId);
	}
	
	@PutMapping("/{projectId}")
	public ProjectDto updateProject(@RequestAttribute Long userId, @PathVariable Long projectId, @RequestBody AddProjectParamsDto params)
			throws InstanceNotFoundException, UnauthorizedProjectAccessException, UnhandledAPIException {
		return toProjectDto(projectService.updateProject(userId, projectId, params.getName(), params.getDescription(), params.getAccesibility()), userId);
	}
	
	@GetMapping
	public BlockDto<ProjectSummaryDto> getUserProjects(@RequestAttribute Long userId,
			@RequestParam(defaultValue = "0") int page) throws InstanceNotFoundException {

		Block<Project> projectBlock = projectService.getUserProjects(userId, page, 10);

		return new BlockDto<>(toProjectSummaryDtos(projectBlock.getItems()), projectBlock.getExistMoreItems());
	}

	@PostMapping
	public IdDto addProject(@RequestAttribute Long userId, @Validated @RequestBody AddProjectParamsDto params)
			throws InstanceNotFoundException {
		Long projectId = projectService.addProject(userId, params.getName(), params.getDescription(), params.getAccesibility());
		return new IdDto(projectId);
	}

}
