package es.udc.relaxed.backend.rest.dtos;

import javax.validation.constraints.NotNull;

import es.udc.relaxed.backend.model.enums.Accesibility;

public class AddProjectParamsDto {

	private String name;
	private String description;
	private Accesibility accesibility;
	
	public AddProjectParamsDto() {}

	public AddProjectParamsDto(String name, String description, Accesibility accesibility) {
		super();
		this.name = name;
		this.description = description;
		this.accesibility = accesibility;
	}

	@NotNull
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@NotNull
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@NotNull
	public Accesibility getAccesibility() {
		return accesibility;
	}

	public void setAccesibility(Accesibility accesibility) {
		this.accesibility = accesibility;
	}

}
