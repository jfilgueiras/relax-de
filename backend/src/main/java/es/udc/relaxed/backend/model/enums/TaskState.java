package es.udc.relaxed.backend.model.enums;

public enum TaskState {
	PENDING, EXECUTING, SUCCESS, FAILURE
}
