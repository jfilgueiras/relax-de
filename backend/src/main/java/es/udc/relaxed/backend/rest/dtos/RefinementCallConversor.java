package es.udc.relaxed.backend.rest.dtos;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.relaxed.backend.model.entities.RefinementCall;

public class RefinementCallConversor {

	private RefinementCallConversor() {
	}

	public static final List<RefinementCallDto> toRefinementCallDtos(List<RefinementCall> refinementCalls) {
		return refinementCalls.stream().map(p -> toRefinementCallDto(p)).collect(Collectors.toList());
	}

	public static final RefinementCallDto toRefinementCallDto(RefinementCall refinementCall) {

		return new RefinementCallDto(refinementCall.getId(), refinementCall.getTaskState(), 
				refinementCall.getExecutionName(), refinementCall.getConfigName(), refinementCall.getDisturbDegrees(),
				refinementCall.getScheme(), refinementCall.getPopsize(), refinementCall.getF(), refinementCall.getCr(),
				refinementCall.getGens(), refinementCall.getTag(), refinementCall.getLogStats(), refinementCall.getPopulExecutorChain(),
				refinementCall.getTrialExecutorChain(), refinementCall.getExecuteEach(), refinementCall.getExecutionsPerGen(),
				refinementCall.getPopToRepackmin(), refinementCall.getRepackFaRepScale(), refinementCall.getMinFaRepScale(),
				refinementCall.getCheckExecutionImprovement(), refinementCall.getAaProportionRepmin(), refinementCall.getSortWorstEterms(),
				refinementCall.getTimeLimit(), refinementCall.getDate());
	}

}
