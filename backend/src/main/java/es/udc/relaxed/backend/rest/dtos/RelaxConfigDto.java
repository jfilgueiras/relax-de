package es.udc.relaxed.backend.rest.dtos;

public class RelaxConfigDto {

	private String populExecutorChain;
	private String trialExecutorChain;
	private Integer executeEach;
	private Integer executionsPerGen;
	private Float popToRepackmin;
	private String repackFaRepScale;
	private String minFaRepScale;
	private String checkExecutionImprovement;
	private Float aaProportionRepmin;
	private String sortWorstEterms;

	public RelaxConfigDto() {}

	public String getPopulExecutorChain() {
		return populExecutorChain;
	}

	public void setPopulExecutorChain(String populExecutorChain) {
		this.populExecutorChain = populExecutorChain;
	}

	public String getTrialExecutorChain() {
		return trialExecutorChain;
	}

	public void setTrialExecutorChain(String trialExecutorChain) {
		this.trialExecutorChain = trialExecutorChain;
	}

	public Integer getExecuteEach() {
		return executeEach;
	}

	public void setExecuteEach(Integer executeEach) {
		this.executeEach = executeEach;
	}

	public Integer getExecutionsPerGen() {
		return executionsPerGen;
	}

	public void setExecutionsPerGen(Integer executionsPerGen) {
		this.executionsPerGen = executionsPerGen;
	}

	public Float getPopToRepackmin() {
		return popToRepackmin;
	}

	public void setPopToRepackmin(Float popToRepackmin) {
		this.popToRepackmin = popToRepackmin;
	}

	public String getRepackFaRepScale() {
		return repackFaRepScale;
	}

	public void setRepackFaRepScale(String repackFaRepScale) {
		this.repackFaRepScale = repackFaRepScale;
	}

	public String getMinFaRepScale() {
		return minFaRepScale;
	}

	public void setMinFaRepScale(String minFaRepScale) {
		this.minFaRepScale = minFaRepScale;
	}

	public String getCheckExecutionImprovement() {
		return checkExecutionImprovement;
	}

	public void setCheckExecutionImprovement(String checkExecutionImprovement) {
		this.checkExecutionImprovement = checkExecutionImprovement;
	}

	public Float getAaProportionRepmin() {
		return aaProportionRepmin;
	}

	public void setAaProportionRepmin(Float aaProportionRepmin) {
		this.aaProportionRepmin = aaProportionRepmin;
	}

	public String getSortWorstEterms() {
		return sortWorstEterms;
	}

	public void setSortWorstEterms(String sortWorstEterms) {
		this.sortWorstEterms = sortWorstEterms;
	}
}
