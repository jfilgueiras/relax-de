package es.udc.relaxed.backend.model.services;

import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang3.tuple.Pair;

import es.udc.relaxed.backend.model.entities.ApiDirectory;
import es.udc.relaxed.backend.model.exceptions.DirectoryNotExistingException;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.exceptions.InvalidFileExtensionException;
import es.udc.relaxed.backend.model.exceptions.MissingProteinFileException;
import es.udc.relaxed.backend.model.exceptions.UnauthorizedProjectAccessException;
import es.udc.relaxed.backend.model.exceptions.UnhandledAPIException;
import es.udc.relaxed.backend.rest.dtos.ExecuteRefinementParamsDto;
import es.udc.relaxed.backend.rest.dtos.RefinementResultDto;
import es.udc.relaxed.backend.rest.dtos.TaskProgressDto;
import okhttp3.ResponseBody;

public interface RelaxDEAPIService {

	ApiDirectory uploadProtein(Long userId, Long projectId, FileItem file)
			throws InvalidFileExtensionException, InstanceNotFoundException, UnauthorizedProjectAccessException,
			UnhandledAPIException, MissingProteinFileException;

	List<TaskProgressDto> getUserPendingTasks(Long userId) throws UnhandledAPIException, InstanceNotFoundException;

	void executeRefinement(Long userId, Long projectId, ExecuteRefinementParamsDto executionParams)
			throws InstanceNotFoundException, UnauthorizedProjectAccessException, DirectoryNotExistingException,
			UnhandledAPIException;

	List<TaskProgressDto> getProjectPendingTasks(Long userId, Long projectId)
			throws InstanceNotFoundException, UnhandledAPIException;

	RefinementResultDto getRefinement(Long userId, Long callId) throws UnhandledAPIException, InstanceNotFoundException, UnauthorizedProjectAccessException, DirectoryNotExistingException;

	Pair<String, ResponseBody> downloadResults(Long userId, Long callId) throws UnhandledAPIException, InstanceNotFoundException,
			UnauthorizedProjectAccessException, DirectoryNotExistingException;

}
