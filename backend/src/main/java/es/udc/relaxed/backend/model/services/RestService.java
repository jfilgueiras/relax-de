package es.udc.relaxed.backend.model.services;

import es.udc.relaxed.backend.model.pojo.ExecutionConfirmation;
import es.udc.relaxed.backend.rest.dtos.ExecuteRefinementParamsDto;
import es.udc.relaxed.backend.rest.dtos.RefinementResultDto;
import es.udc.relaxed.backend.rest.dtos.TaskProgressDto;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

public interface RestService {

	@Multipart
	@POST("upload-protein")
	Call<String> uploadProtein(@Query("projectName") String projectName, @Part MultipartBody.Part protein);

	@POST("execute-refinement")
	Call<ExecutionConfirmation> executeRefinement(@Query("projectName") String projectName,
			@Body ExecuteRefinementParamsDto executionParams);
	
	@GET("/status/{task_id}")
	Call<TaskProgressDto> updateProgress(@Path("task_id") String taskId);
	
	@GET("/get-refinement")
	Call<RefinementResultDto> getRefinement(@Query("projectName") String projectName, @Query("execName") String execName);

    @Streaming
	@GET("/download-results")
	Call<ResponseBody> downloadResults(@Query("projectName") String projectName, @Query("execName") String execName);
}
