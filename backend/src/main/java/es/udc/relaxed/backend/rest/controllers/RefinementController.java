package es.udc.relaxed.backend.rest.controllers;

import static es.udc.relaxed.backend.rest.dtos.RefinementCallConversor.toRefinementCallDtos;
import static es.udc.relaxed.backend.rest.dtos.RefinementCallConversor.toRefinementCallDto;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.udc.relaxed.backend.model.entities.RefinementCall;
import es.udc.relaxed.backend.model.exceptions.DirectoryNotExistingException;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.exceptions.UnauthorizedProjectAccessException;
import es.udc.relaxed.backend.model.exceptions.UnhandledAPIException;
import es.udc.relaxed.backend.model.services.Block;
import es.udc.relaxed.backend.model.services.RefinementCallService;
import es.udc.relaxed.backend.model.services.RelaxDEAPIService;
import es.udc.relaxed.backend.rest.common.ErrorsDto;
import es.udc.relaxed.backend.rest.dtos.BlockDto;
import es.udc.relaxed.backend.rest.dtos.ExecuteRefinementParamsDto;
import es.udc.relaxed.backend.rest.dtos.RefinementCallDto;
import es.udc.relaxed.backend.rest.dtos.RefinementResultDto;
import es.udc.relaxed.backend.rest.dtos.TaskProgressDto;

@RestController
@RequestMapping("/refinements")
public class RefinementController {

	private static final String DIRECTORY_NOT_EXISTING_EXCEPTION_CODE = "project.exceptions.DirectoryNotExistingException";

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private RelaxDEAPIService relaxEDApiService;

	@Autowired
	private RefinementCallService refinementCallService;
	
	@ExceptionHandler(DirectoryNotExistingException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorsDto handleDirectoryNotExistingException(DirectoryNotExistingException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(DIRECTORY_NOT_EXISTING_EXCEPTION_CODE, null,
				DIRECTORY_NOT_EXISTING_EXCEPTION_CODE, locale);

		return new ErrorsDto(errorMessage);

	}

	@PostMapping
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void createRefinementExecution(@RequestAttribute Long userId, @RequestParam Long projectId,
			@Validated @RequestBody ExecuteRefinementParamsDto params) throws InstanceNotFoundException,
			UnauthorizedProjectAccessException, UnhandledAPIException, DirectoryNotExistingException {
		relaxEDApiService.executeRefinement(userId, projectId, params);
	}
	
	@GetMapping
	public BlockDto<RefinementCallDto> getRefinements(@RequestAttribute Long userId, @RequestParam Long projectId,
			@RequestParam(defaultValue = "0") int page) throws InstanceNotFoundException,
			UnauthorizedProjectAccessException, UnhandledAPIException, DirectoryNotExistingException {
		
		Block<RefinementCall> refinementsBlock = refinementCallService.getUserRefinementCalls(userId, projectId, page, 10);
		
		return new BlockDto<>(toRefinementCallDtos(refinementsBlock.getItems()), refinementsBlock.getExistMoreItems());
	}
	
	@GetMapping("/{callId}")
	public RefinementResultDto getRefinementResult(@RequestAttribute Long userId, @PathVariable Long callId) throws InstanceNotFoundException,
			UnauthorizedProjectAccessException, UnhandledAPIException, DirectoryNotExistingException {
		
		return relaxEDApiService.getRefinement(userId, callId);		
	}
	
	@GetMapping("/download/{callId}")
	public ResponseEntity<InputStreamResource> downloadRefinementResult(@RequestAttribute Long userId, @PathVariable Long callId, Model model) throws InstanceNotFoundException,
			UnauthorizedProjectAccessException, UnhandledAPIException, DirectoryNotExistingException, IOException {
        MediaType mediaType = MediaType.MULTIPART_FORM_DATA;
        Pair<String,okhttp3.ResponseBody> responseBodyPair = relaxEDApiService.downloadResults(userId, callId);
        InputStreamResource resource = new InputStreamResource(responseBodyPair.getRight().byteStream());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, responseBodyPair.getLeft()) // "attachment;filename=result.zip" +
                .contentType(mediaType)
                .contentLength(responseBodyPair.getRight().contentLength())
                .body(resource);
	}
	
	
	@GetMapping("/userPending")
	public List<TaskProgressDto> getUserPendingTasks(@RequestAttribute Long userId)
			throws InstanceNotFoundException, UnhandledAPIException {

		List<TaskProgressDto> pendingTasks = relaxEDApiService.getUserPendingTasks(userId);

		return pendingTasks;
	}

	@GetMapping("/projectPending")
	public List<TaskProgressDto> getProjectPendingTasks(@RequestAttribute Long userId, @RequestParam Long projectId)
			throws InstanceNotFoundException, UnhandledAPIException {

		List<TaskProgressDto> pendingTasks = relaxEDApiService.getProjectPendingTasks(userId, projectId);

		return pendingTasks;
	}
	
	@GetMapping("/configNames")
	public List<String> getUserConfigNames(@RequestAttribute Long userId)
			throws InstanceNotFoundException, UnhandledAPIException {

		return refinementCallService.getUserConfigNames(userId);
	}
	
	@GetMapping("/refinementConfig")
	public RefinementCallDto getRefinementConfiguration(@RequestAttribute Long userId, @RequestParam String configName)
			throws InstanceNotFoundException, UnhandledAPIException {

		return toRefinementCallDto(refinementCallService.getRefinementByConfigName(userId, configName));
	}
}
