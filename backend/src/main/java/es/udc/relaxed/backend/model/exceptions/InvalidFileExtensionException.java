package es.udc.relaxed.backend.model.exceptions;

@SuppressWarnings("serial")
public class InvalidFileExtensionException extends Exception {

	private String fileType;
	private String expectedExtension;
	private String givenExtension;

	public InvalidFileExtensionException(String fileType, String expectedExtension, String givenExtension) {
		this.fileType = fileType;
		this.expectedExtension = expectedExtension;
		this.givenExtension = givenExtension;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getExpectedExtension() {
		return expectedExtension;
	}

	public void setExpectedExtension(String expectedExtension) {
		this.expectedExtension = expectedExtension;
	}

	public String getGivenExtension() {
		return givenExtension;
	}

	public void setGivenExtension(String givenExtension) {
		this.givenExtension = givenExtension;
	}

}
