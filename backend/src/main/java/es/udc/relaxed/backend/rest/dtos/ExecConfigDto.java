package es.udc.relaxed.backend.rest.dtos;

public class ExecConfigDto {

	private String timeLimit;

	public String getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(String timeLimit) {
		this.timeLimit = timeLimit;
	}
}
