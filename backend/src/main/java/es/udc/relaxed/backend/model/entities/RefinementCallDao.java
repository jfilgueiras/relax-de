package es.udc.relaxed.backend.model.entities;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RefinementCallDao extends PagingAndSortingRepository<RefinementCall, Long> {

	@Query("SELECT r "
			+ "FROM RefinementCall r "
			+ "WHERE r.caller.id = ?1 AND r.project.id = ?2 AND (r.taskState = 'SUCCESS' OR r.taskState = 'FAILURE') "
			+ "ORDER BY r.date DESC")
	Slice<RefinementCall> findUserRefinements(Long callerId, Long projectId, Pageable pageable);

	@Query("SELECT r "
			+ "FROM RefinementCall r "
			+ "WHERE r.project.id = ?1 AND r.taskState != 'FAILURE' AND r.taskState != 'SUCCESS'")
	List<RefinementCall> findProjectPendingTasks(Long projectId);
	
	@Query("SELECT r "
			+ "FROM RefinementCall r "
			+ "WHERE r.caller.id = ?1 AND r.taskState != 'FAILURE' AND r.taskState != 'SUCCESS'")
	List<RefinementCall> findUserPendingTasks(Long callerId);
	
	@Query("SELECT DISTINCT(r.configName) "
			+ "FROM RefinementCall r "
			+ "WHERE r.caller.id = ?1 AND configName <> ''")	
	List<String> findDistinctUserConfigNames(Long callerId);
	
	Slice<RefinementCall> findRefinementsByCallerIdAndConfigNameOrderByDateDesc(
			Long callerId, String configName, Pageable pageable);
}
