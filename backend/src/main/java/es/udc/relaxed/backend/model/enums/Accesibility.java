package es.udc.relaxed.backend.model.enums;

public enum Accesibility {
	PRIVATE, READ_ONLY, PUBLIC
}