package es.udc.relaxed.backend.rest.dtos;

import java.time.LocalDateTime;

import es.udc.relaxed.backend.model.enums.Accesibility;

public class ProjectSummaryDto {

	private Long id;
	private String name;
	private Accesibility accesibility;
	private LocalDateTime creationDate;

	public ProjectSummaryDto() {
	}

	public ProjectSummaryDto(Long id, String name, Accesibility accesibility, LocalDateTime creationDate) {
		super();
		this.id = id;
		this.name = name;
		this.accesibility = accesibility;
		this.creationDate = creationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Accesibility getAccesibility() {
		return accesibility;
	}

	public void setAccesibility(Accesibility accesibility) {
		this.accesibility = accesibility;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

}
