package es.udc.relaxed.backend.rest.dtos;

import java.time.LocalDateTime;

import es.udc.relaxed.backend.model.enums.TaskState;

public class TaskProgressDto {

	private String taskId;
	private String projectName;
	private String configName;
	private LocalDateTime constructionDate;
	private TaskState state;
	private Integer current;
	private Integer total;

	public TaskProgressDto() {
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}

	public LocalDateTime getConstructionDate() {
		return constructionDate;
	}

	public void setConstructionDate(LocalDateTime constructionDate) {
		this.constructionDate = constructionDate;
	}

	public TaskState getState() {
		return state;
	}

	public void setState(TaskState state) {
		this.state = state;
	}

	public Integer getCurrent() {
		return current;
	}

	public void setCurrent(Integer current) {
		this.current = current;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

}
