package es.udc.relaxed.backend.model.services;

import java.time.LocalDateTime;
import org.apache.commons.lang3.RandomStringUtils;

public class Utils {

	public static String generateUniqueFileName() {
		String filename = "";
		long millis = System.currentTimeMillis();
		String datetime = LocalDateTime.now().toString().replaceAll("[ .:-]", "");
		String rndchars = RandomStringUtils.randomAlphanumeric(8);
		filename = rndchars + datetime + millis;
		return filename.toLowerCase();
	}

}
