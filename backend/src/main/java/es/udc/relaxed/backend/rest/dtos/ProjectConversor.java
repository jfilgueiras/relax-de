package es.udc.relaxed.backend.rest.dtos;

import static es.udc.relaxed.backend.rest.dtos.ApiDirectoryConversor.toApiDirectoryDto;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.relaxed.backend.model.entities.ApiDirectory;
import es.udc.relaxed.backend.model.entities.Project;

public class ProjectConversor {

	private ProjectConversor() {
	}

	public static final List<ProjectSummaryDto> toProjectSummaryDtos(List<Project> projects) {
		return projects.stream().map(p -> toProjectSummaryDto(p)).collect(Collectors.toList());
	}

	private static final ProjectSummaryDto toProjectSummaryDto(Project project) {

		return new ProjectSummaryDto(project.getId(), project.getName(), project.getAccesibility(), project.getDate());
	}

	public static final ProjectDto toProjectDto(Project project, Long userId) {

		ApiDirectory apiDirectory = project.getApiDirectory();
		ApiDirectoryDto apiDirectoryDto = null;

		if (apiDirectory != null) {
			apiDirectoryDto = toApiDirectoryDto(apiDirectory);
		}

		return new ProjectDto(project.getId(), project.getName(), project.getDescription(), project.getAccesibility(),
				project.getDate(), project.getUser().getUserName(), apiDirectoryDto, project.getUser().getId().equals(userId));
	}

}
