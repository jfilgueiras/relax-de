package es.udc.relaxed.backend.rest.dtos;

public class IndInfoDto {

	private Float score;
	private Float rmsd;

	public IndInfoDto() {
	}

	public IndInfoDto(Float score, Float rmsd) {
		super();
		this.score = score;
		this.rmsd = rmsd;
	}
	
	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	public Float getRmsd() {
		return rmsd;
	}

	public void setRmsd(Float rmsd) {
		this.rmsd = rmsd;
	}
}
