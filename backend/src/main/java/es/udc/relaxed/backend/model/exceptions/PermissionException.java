package es.udc.relaxed.backend.model.exceptions;

@SuppressWarnings("serial")
public class PermissionException extends Exception {}
