package es.udc.relaxed.backend.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.relaxed.backend.model.entities.RefinementCall;
import es.udc.relaxed.backend.model.entities.RefinementCallDao;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;

@Service
@Transactional
public class RefinementCallServiceImpl implements RefinementCallService {

	@Autowired
	private RefinementCallDao refinementCallDao;

	@Autowired
	private PermissionChecker permissionChecker;

	@Override
	@Transactional(readOnly = true)
	public Block<RefinementCall> getUserRefinementCalls(Long callerId, Long projectId, int page, int size)
			throws InstanceNotFoundException {

		permissionChecker.checkUserExists(callerId);

		Slice<RefinementCall> slice = refinementCallDao.findUserRefinements(callerId,
				projectId, PageRequest.of(page, size));

		return new Block<>(slice.getContent(), slice.hasNext());
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<String> getUserConfigNames(Long callerId)
			throws InstanceNotFoundException {

		permissionChecker.checkUserExists(callerId);

		return refinementCallDao.findDistinctUserConfigNames(callerId);
	}


	@Override
	@Transactional(readOnly = true)
	public RefinementCall getRefinementByConfigName(Long callerId, String configName)
			throws InstanceNotFoundException {

		permissionChecker.checkUserExists(callerId);

		Slice<RefinementCall> refinementsByConfig = refinementCallDao.findRefinementsByCallerIdAndConfigNameOrderByDateDesc
				(callerId, configName, PageRequest.of(0, 1));

		if (refinementsByConfig.getContent().isEmpty()) {
			throw new InstanceNotFoundException("project.entity.configName", configName);
		}
		
		return refinementsByConfig.getContent().get(0);
	}

}
