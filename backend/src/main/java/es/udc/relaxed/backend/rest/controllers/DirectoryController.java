package es.udc.relaxed.backend.rest.controllers;

import static es.udc.relaxed.backend.rest.dtos.ApiDirectoryConversor.toApiDirectoryDto;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.exceptions.InvalidFileExtensionException;
import es.udc.relaxed.backend.model.exceptions.MissingProteinFileException;
import es.udc.relaxed.backend.model.exceptions.UnauthorizedProjectAccessException;
import es.udc.relaxed.backend.model.exceptions.UnhandledAPIException;
import es.udc.relaxed.backend.model.services.RelaxDEAPIService;
import es.udc.relaxed.backend.rest.common.ErrorsDto;
import es.udc.relaxed.backend.rest.dtos.ApiDirectoryDto;

@RestController
@RequestMapping("/directory")
public class DirectoryController {

	private static final String INVALID_FILE_EXTENSION_EXCEPTION_CODE = "project.exceptions.InvalidFileExtensionException";
	private static final String MISSING_PROTEIN_FILE_EXCEPTION_CODE = "project.exceptions.MissingProteinFileException";

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private RelaxDEAPIService relaxEDApiService;

	@ExceptionHandler(InvalidFileExtensionException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorsDto handleInvalidFileExtensionException(InvalidFileExtensionException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(
				INVALID_FILE_EXTENSION_EXCEPTION_CODE, new Object[] { exception.getFileType(),
						exception.getExpectedExtension(), exception.getGivenExtension() },
				INVALID_FILE_EXTENSION_EXCEPTION_CODE, locale);

		return new ErrorsDto(errorMessage);

	}

	@ExceptionHandler(MissingProteinFileException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorsDto handleMissingProteinFileException(MissingProteinFileException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(MISSING_PROTEIN_FILE_EXCEPTION_CODE, null,
				MISSING_PROTEIN_FILE_EXCEPTION_CODE, locale);

		return new ErrorsDto(errorMessage);

	}

	private Iterator<FileItem> processUpload(HttpServletRequest request) throws FileUploadException {
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		factory.setSizeThreshold(DiskFileItemFactory.DEFAULT_SIZE_THRESHOLD);
		factory.setFileCleaningTracker(null);

		ServletFileUpload upload = new ServletFileUpload(factory);
		List<FileItem> items = upload.parseRequest(request);
		return items.iterator();
	}
	
	@PostMapping("/upload-protein")
	public ApiDirectoryDto addDataByProteinFile(@RequestAttribute Long userId, @RequestParam Long projectId,
			HttpServletRequest request) throws InstanceNotFoundException, InvalidFileExtensionException,
			UnauthorizedProjectAccessException, UnhandledAPIException, FileUploadException, MissingProteinFileException {

		FileItem protein = null;

		Iterator<FileItem> iter = processUpload(request);
		
		while (iter.hasNext()) {
			FileItem item = iter.next();

			if (!item.isFormField() && item.getFieldName().equals("protein")) {
				protein = item;
			}
		}
		return toApiDirectoryDto(relaxEDApiService.uploadProtein(userId, projectId, protein));
	}

}
