package es.udc.relaxed.backend.rest.dtos;

import es.udc.relaxed.backend.model.entities.ApiDirectory;

public class ApiDirectoryConversor {

	private ApiDirectoryConversor() {
	}

	public static final ApiDirectoryDto toApiDirectoryDto(ApiDirectory apiDirectory) {

		return new ApiDirectoryDto(apiDirectory.getDate(), apiDirectory.getUploader().getUserName());
	}

}
