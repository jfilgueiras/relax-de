package es.udc.relaxed.backend.model.entities;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProjectDao extends PagingAndSortingRepository<Project, Long> {

	@Query("SELECT p "
		 + "FROM Project p "
		 + "WHERE p.accesibility = 'PUBLIC' OR p.accesibility = 'READ_ONLY' OR p.user.id = ?1 "
		 + "ORDER BY date DESC")
	public Slice<Project> findUserProject(Long userId, Pageable pageable);
}
