package es.udc.relaxed.backend.model.entities;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface ApiDirectoryDao extends PagingAndSortingRepository<ApiDirectory, Long> {

}
