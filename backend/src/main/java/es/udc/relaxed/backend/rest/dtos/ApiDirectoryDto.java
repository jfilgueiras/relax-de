package es.udc.relaxed.backend.rest.dtos;

import java.time.LocalDateTime;

public class ApiDirectoryDto {

	private LocalDateTime date;
	private String uploader;
	
	public ApiDirectoryDto() {}

	public ApiDirectoryDto(LocalDateTime date, String uploader) {
		super();
		this.date = date;
		this.uploader = uploader;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getUploader() {
		return uploader;
	}

	public void setUploader(String uploader) {
		this.uploader = uploader;
	}

}
