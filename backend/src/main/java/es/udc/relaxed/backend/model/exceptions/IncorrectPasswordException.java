package es.udc.relaxed.backend.model.exceptions;

@SuppressWarnings("serial")
public class IncorrectPasswordException extends Exception {}
