package es.udc.relaxed.backend.rest.dtos;

public class DeConfigDto {

	private String scheme;
	private Integer popsize;
	private Float f;
	private Float cr;
	private Integer gens;
	private String tag;
	private String logStats;

	public DeConfigDto() {}
	
	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public Integer getPopsize() {
		return popsize;
	}

	public void setPopsize(Integer popsize) {
		this.popsize = popsize;
	}

	public Float getF() {
		return f;
	}

	public void setF(Float f) {
		this.f = f;
	}

	public Float getCr() {
		return cr;
	}

	public void setCr(Float cr) {
		this.cr = cr;
	}

	public Integer getGens() {
		return gens;
	}

	public void setGens(Integer gens) {
		this.gens = gens;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getLogStats() {
		return logStats;
	}

	public void setLogStats(String logStats) {
		this.logStats = logStats;
	}

}
