package es.udc.relaxed.backend.rest.dtos;

public class GenInfoDto {

	private Integer gen;
	private Float best;
	private Float avg;

	public GenInfoDto() {
	}

	public GenInfoDto(Integer gen, Float best, Float avg) {
		super();
		this.gen = gen;
		this.best = best;
		this.avg = avg;
	}

	public Integer getGen() {
		return gen;
	}

	public void setGen(Integer gen) {
		this.gen = gen;
	}

	public Float getBest() {
		return best;
	}

	public void setBest(Float best) {
		this.best = best;
	}

	public Float getAvg() {
		return avg;
	}

	public void setAvg(Float avg) {
		this.avg = avg;
	}

}
