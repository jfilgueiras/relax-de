package es.udc.relaxed.backend.rest.dtos;

import java.time.LocalDateTime;

import es.udc.relaxed.backend.model.enums.Accesibility;

public class ProjectDto {

	private Long id;
	private String name;
	private String description;
	private Accesibility accesibility;
	private LocalDateTime creationDate;
	private String userName;
	private ApiDirectoryDto directory;
	private Boolean canDelete;

	public ProjectDto() {}

	public ProjectDto(Long id, String name, String description, Accesibility accesibility, LocalDateTime creationDate, String userName, ApiDirectoryDto directory, Boolean canDelete) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.accesibility = accesibility;
		this.creationDate = creationDate;
		this.userName = userName;
		this.directory = directory;
		this.canDelete = canDelete;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Accesibility getAccesibility() {
		return accesibility;
	}

	public void setAccesibility(Accesibility accesibility) {
		this.accesibility = accesibility;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public ApiDirectoryDto getDirectory() {
		return directory;
	}

	public void setDirectory(ApiDirectoryDto directory) {
		this.directory = directory;
	}

	public Boolean getCanDelete() {
		return canDelete;
	}

	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}

}
