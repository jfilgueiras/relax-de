package es.udc.relaxed.backend.model.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ApiDirectory {
	
	private Long id;
	private String name;
	private LocalDateTime date;
	private User uploader;
	
	public ApiDirectory() {}

	public ApiDirectory(String name, User uploader) {
		super();
		this.name = name;
		this.date = LocalDateTime.now();
		this.uploader = uploader;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	@ManyToOne(optional=true, fetch=FetchType.LAZY)
	@JoinColumn(name="uploaderId")
	public User getUploader() {
		return uploader;
	}

	public void setUploader(User uploader) {
		this.uploader = uploader;
	}

}
