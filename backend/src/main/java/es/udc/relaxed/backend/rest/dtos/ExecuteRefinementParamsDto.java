package es.udc.relaxed.backend.rest.dtos;

public class ExecuteRefinementParamsDto {

	private String configName;
	private Float disturbDegrees;
	private DeConfigDto de;
	private RelaxConfigDto relax;
	private ExecConfigDto exec;

	public ExecuteRefinementParamsDto() {}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}

	public Float getDisturbDegrees() {
		return disturbDegrees;
	}

	public void setDisturbDegrees(Float disturbDegrees) {
		this.disturbDegrees = disturbDegrees;
	}

	public DeConfigDto getDe() {
		return de;
	}

	public void setDe(DeConfigDto de) {
		this.de = de;
	}

	public RelaxConfigDto getRelax() {
		return relax;
	}

	public void setRelax(RelaxConfigDto relax) {
		this.relax = relax;
	}

	public ExecConfigDto getExec() {
		return exec;
	}

	public void setExec(ExecConfigDto exec) {
		this.exec = exec;
	}

}
