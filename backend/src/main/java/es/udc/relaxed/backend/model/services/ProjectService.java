package es.udc.relaxed.backend.model.services;

import es.udc.relaxed.backend.model.entities.Project;
import es.udc.relaxed.backend.model.enums.Accesibility;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.exceptions.UnauthorizedProjectAccessException;

public interface ProjectService {

	Long addProject(Long userId, String projectName, String description, Accesibility accesibility) throws InstanceNotFoundException;

	Block<Project> getUserProjects(Long userId, int page, int size) throws InstanceNotFoundException;
	
	Project getProjectDetail(Long userId, Long projectId) throws InstanceNotFoundException, UnauthorizedProjectAccessException;

	void deleteProject(Long userId, Long projectId) throws InstanceNotFoundException, UnauthorizedProjectAccessException;

	Project updateProject(Long userId, Long projectId, String projectName, String description, Accesibility accesibility) throws InstanceNotFoundException, UnauthorizedProjectAccessException;
}
