package es.udc.relaxed.backend.model.services;

import static es.udc.relaxed.backend.rest.dtos.RefinementCallConversor.toRefinementCallDto;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import es.udc.relaxed.backend.model.entities.ApiDirectory;
import es.udc.relaxed.backend.model.entities.ApiDirectoryDao;
import es.udc.relaxed.backend.model.entities.Project;
import es.udc.relaxed.backend.model.entities.ProjectDao;
import es.udc.relaxed.backend.model.entities.RefinementCall;
import es.udc.relaxed.backend.model.entities.RefinementCallDao;
import es.udc.relaxed.backend.model.entities.User;
import es.udc.relaxed.backend.model.enums.Accesibility;
import es.udc.relaxed.backend.model.enums.TaskState;
import es.udc.relaxed.backend.model.exceptions.DirectoryNotExistingException;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.exceptions.InvalidFileExtensionException;
import es.udc.relaxed.backend.model.exceptions.MissingProteinFileException;
import es.udc.relaxed.backend.model.exceptions.UnauthorizedProjectAccessException;
import es.udc.relaxed.backend.model.exceptions.UnhandledAPIException;
import es.udc.relaxed.backend.model.pojo.ExecutionConfirmation;
import es.udc.relaxed.backend.rest.dtos.ExecuteRefinementParamsDto;
import es.udc.relaxed.backend.rest.dtos.RefinementResultDto;
import es.udc.relaxed.backend.rest.dtos.TaskProgressDto;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

@Service
@Transactional
public class RelaxDEAPIServiceImpl implements RelaxDEAPIService {

	@Autowired
	private ProjectDao projectDao;

	@Autowired
	private ApiDirectoryDao apiDirectoryDao;

	@Autowired
	private RefinementCallDao refinementCallDao;

	@Autowired
	private PermissionChecker permissionChecker;

	private ApiDirectory getApiDirectory(Long projectId, User user)
			throws InstanceNotFoundException, UnauthorizedProjectAccessException, DirectoryNotExistingException {

		Optional<Project> optProject = projectDao.findById(projectId);
		Project project;
		ApiDirectory apiDirectory;
		// Check for existing project
		if (!optProject.isPresent()) {
			throw new InstanceNotFoundException("project.entities.project", projectId);
		} else {
			project = optProject.get();
		}

		// Make sure the user is either the owner of the project entity or it is PUBLIC
		if (!(project.getUser().equals(user) || project.getAccesibility().equals(Accesibility.PUBLIC))) {
			throw new UnauthorizedProjectAccessException();
		}
		apiDirectory = project.getApiDirectory();

		// If there's directory, we get it
		// There must be a directory
		String apiProjectname = apiDirectory != null ? apiDirectory.getName() : null;
		if (apiProjectname == null) {
			throw new DirectoryNotExistingException();
		}
		return apiDirectory;
	}

	private ArrayList<TaskProgressDto> getPendingTasks(List<RefinementCall> pendingCalls, boolean updateSuccess)
			throws UnhandledAPIException {
		ArrayList<TaskProgressDto> pendingTasks = new ArrayList<>();

		RestService restService = RestServiceImpl.getInstance();
		for (RefinementCall call : pendingCalls) {
			final Call<TaskProgressDto> getProgressCall = restService.updateProgress(call.getTaskId());
			final Response<TaskProgressDto> getProgressResponse;
			TaskProgressDto taskProgress = new TaskProgressDto();
			try {
				getProgressResponse = getProgressCall.execute();
				if (getProgressResponse.isSuccessful()) {
					taskProgress = getProgressResponse.body();
					if (updateSuccess) {
						if (taskProgress.getState().equals(TaskState.PENDING)) {
							// Se ha perdido el estado de la tarea debido a una tarea o mucha espera
							if (call.getTaskState().equals(TaskState.EXECUTING)
									|| Duration.between(call.getDate(), LocalDateTime.now()).toDays() >= 1) {
								call.setTaskState(TaskState.FAILURE);
							}
						} else {
							call.setTaskState(taskProgress.getState());
						}

					}
				}
			} catch (IOException e) {
				taskProgress.setState(TaskState.PENDING);
			}
			taskProgress.setProjectName(call.getProject().getName());
			taskProgress.setConfigName(call.getConfigName());
			taskProgress.setConstructionDate(call.getDate());
			pendingTasks.add(taskProgress);
		}
		return pendingTasks;
	}

	@Override
	public ApiDirectory uploadProtein(Long userId, Long projectId, FileItem protein)
			throws InstanceNotFoundException, UnauthorizedProjectAccessException, MissingProteinFileException,
			InvalidFileExtensionException, UnhandledAPIException {

		User user = permissionChecker.checkUser(userId);

		String apiProjectName;
		Optional<Project> optProject = projectDao.findById(projectId);
		Project project;
		ApiDirectory apiDirectory;

		// Check for existing project
		if (optProject.isPresent()) {
			project = optProject.get();
		} else {
			throw new InstanceNotFoundException("project.entities.project", projectId);
		}

		// Make sure the user is either the owner of the project entity or it is PUBLIC
		if (!(project.getUser().equals(user) || project.getAccesibility().equals(Accesibility.PUBLIC))) {
			throw new UnauthorizedProjectAccessException();
		}

		if (protein.getSize() == 0) {
			throw new MissingProteinFileException();
		}
		// Extension service checks (belongs here due to API changes, not in controller)
		String proteinExtension = FilenameUtils.getExtension(protein.getName());
		if (!proteinExtension.equalsIgnoreCase("pdb")) {
			throw new InvalidFileExtensionException("protein", "pdb", proteinExtension);
		}

		apiDirectory = project.getApiDirectory();

		// If there's directory, we update it
		// If there's no directory, we create it
		if (apiDirectory != null) {
			apiProjectName = apiDirectory.getName();
			apiDirectory.setDate(LocalDateTime.now());
			apiDirectory.setUploader(user);
		} else {
			apiProjectName = Utils.generateUniqueFileName();
			apiDirectory = new ApiDirectory(apiProjectName, user);
			project.setApiDirectory(apiDirectory);
		}

		try {
			RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), protein.get());

			MultipartBody.Part body = MultipartBody.Part.createFormData("protein", protein.getName(), requestFile);

			RestService restService = RestServiceImpl.getInstance();
			final Call<String> uploadProteinCall = restService.uploadProtein(apiProjectName, body);

			final Response<String> uploadProteinResponse = uploadProteinCall.execute();
			if (uploadProteinResponse.isSuccessful()) {
				apiDirectory = apiDirectoryDao.save(apiDirectory);
				return apiDirectory;
			}
			throw new UnhandledAPIException();
		} catch (Exception e) {
			throw new UnhandledAPIException();
		}
	}

	@Override
	public void executeRefinement(Long userId, Long projectId, ExecuteRefinementParamsDto executionParams)
			throws InstanceNotFoundException, UnauthorizedProjectAccessException, DirectoryNotExistingException,
			UnhandledAPIException {

		User user = permissionChecker.checkUser(userId);

		Project project;
		ApiDirectory apiDirectory;

		apiDirectory = getApiDirectory(projectId, user);
		project = projectDao.findById(projectId).get();

		try {
			RestService restService = RestServiceImpl.getInstance();

			final Call<ExecutionConfirmation> createRefinementCall = restService
					.executeRefinement(apiDirectory.getName(), executionParams);
			RefinementCall refinementCall = new RefinementCall("", "", executionParams.getConfigName(),
					executionParams.getDisturbDegrees(), executionParams.getDe().getScheme(),
					executionParams.getDe().getPopsize(), executionParams.getDe().getF(),
					executionParams.getDe().getCr(), executionParams.getDe().getGens(),
					executionParams.getDe().getTag(), executionParams.getDe().getLogStats(),
					executionParams.getRelax().getPopulExecutorChain(),
					executionParams.getRelax().getTrialExecutorChain(), executionParams.getRelax().getExecuteEach(),
					executionParams.getRelax().getExecutionsPerGen(), executionParams.getRelax().getPopToRepackmin(),
					executionParams.getRelax().getRepackFaRepScale(), executionParams.getRelax().getMinFaRepScale(),
					executionParams.getRelax().getCheckExecutionImprovement(),
					executionParams.getRelax().getAaProportionRepmin(), executionParams.getRelax().getSortWorstEterms(),
					executionParams.getExec().getTimeLimit(), project, user);
			final Response<ExecutionConfirmation> createRefinementResponse = createRefinementCall.execute();
			if (createRefinementResponse.isSuccessful()) {
				ExecutionConfirmation exec = createRefinementResponse.body();
				String taskId = exec.getTaskId();
				String executionName = exec.getExecutionName();
				refinementCall.setTaskId(taskId);
				refinementCall.setExecutionName(executionName);
				refinementCallDao.save(refinementCall);
				return;
			}
			throw new UnhandledAPIException();
		} catch (IOException e) {
			throw new UnhandledAPIException();
		}
	}

	@Override
	public List<TaskProgressDto> getUserPendingTasks(Long userId)
			throws UnhandledAPIException, InstanceNotFoundException {

		permissionChecker.checkUserExists(userId);
		List<RefinementCall> pendingCalls = refinementCallDao.findUserPendingTasks(userId);

		ArrayList<TaskProgressDto> pendingTasks = getPendingTasks(pendingCalls, true);

		return pendingTasks;
	}

	@Override
	public List<TaskProgressDto> getProjectPendingTasks(Long userId, Long projectId)
			throws InstanceNotFoundException, UnhandledAPIException {

		permissionChecker.checkUserExists(userId);
		List<RefinementCall> pendingCalls = refinementCallDao.findProjectPendingTasks(projectId);

		ArrayList<TaskProgressDto> pendingTasks = getPendingTasks(pendingCalls, false);

		return pendingTasks;
	}
	
	@Override
	public RefinementResultDto getRefinement(Long userId, Long callId) throws UnhandledAPIException, InstanceNotFoundException, UnauthorizedProjectAccessException, DirectoryNotExistingException {

		User user = permissionChecker.checkUser(userId);

		Optional<RefinementCall> optRefinementCall = refinementCallDao.findById(callId);
		RefinementCall refinementCall;

		if (optRefinementCall.isPresent()) {
			refinementCall = optRefinementCall.get();
		} else {
			throw new InstanceNotFoundException("project.entities.refinementCall", callId);
		}
		
		if (!refinementCall.getCaller().getId().equals(userId)) {
			throw new UnauthorizedProjectAccessException();
		}
		
		ApiDirectory apiDir = getApiDirectory(refinementCall.getProject().getId(), user);
		
		RestService restService = RestServiceImpl.getInstance();
		final Call<RefinementResultDto> getRefinementCall = restService.getRefinement(apiDir.getName(), refinementCall.getExecutionName());
		try {
			final Response<RefinementResultDto> getRefinementResponse = getRefinementCall.execute();
			if (getRefinementResponse.isSuccessful()) {
				RefinementResultDto result = getRefinementResponse.body();
				result.setRefinementCall(toRefinementCallDto(refinementCall));
			}
			throw new UnhandledAPIException();
		} catch (IOException e) {
			throw new UnhandledAPIException();
		}
	}
	
	@Override
	public Pair<String,ResponseBody> downloadResults(Long userId, Long callId) throws UnhandledAPIException, InstanceNotFoundException, UnauthorizedProjectAccessException, DirectoryNotExistingException {

		User user = permissionChecker.checkUser(userId);

		Optional<RefinementCall> refinementCall = refinementCallDao.findById(callId);
		RefinementCall ref;

		if (refinementCall.isPresent()) {
			ref = refinementCall.get();
		} else {
			throw new InstanceNotFoundException("project.entities.refinementCall", callId);
		}
		
		if (!ref.getCaller().getId().equals(userId)) {
			throw new UnauthorizedProjectAccessException();
		}
		
		ApiDirectory dir = getApiDirectory(ref.getProject().getId(), user);
		
		RestService restService = RestServiceImpl.getInstance();
		final Call<ResponseBody> downloadResultsCall = restService.downloadResults(dir.getName(), ref.getExecutionName());
		try {
			final Response<ResponseBody> downloadResultsResponse = downloadResultsCall.execute();
			if (downloadResultsResponse.isSuccessful()) {
				Headers headers = downloadResultsResponse.headers();
				String filename = headers.get("Content-Disposition");
				return new ImmutablePair<String, ResponseBody>(filename, downloadResultsResponse.body());

			}
			throw new UnhandledAPIException();
		} catch (IOException e) {
			throw new UnhandledAPIException();
		}
	}
}
