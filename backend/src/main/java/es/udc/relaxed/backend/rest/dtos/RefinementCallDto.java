package es.udc.relaxed.backend.rest.dtos;

import java.time.LocalDateTime;

import es.udc.relaxed.backend.model.enums.TaskState;

public class RefinementCallDto {

	private Long id;
	private TaskState taskState;
	private String taskId;
	private String executionName;
	private String configName;
	private Float disturbDegrees;
	private String scheme;
	private Integer popsize;
	private Float f;
	private Float cr;
	private Integer gens;
	private String tag;
	private String logStats;
	private String populExecutorChain;
	private String trialExecutorChain;
	private Integer executeEach;
	private Integer executionsPerGen;
	private Float popToRepackmin;
	private String repackFaRepScale;
	private String minFaRepScale;
	private String checkExecutionImprovement;
	private Float aaProportionRepmin;
	private String sortWorstEterms;
	private String timeLimit;
	private LocalDateTime date;

	public RefinementCallDto() {
	}

	public RefinementCallDto(Long id, TaskState taskState, String executionName, String configName,
			Float disturbDegrees, String scheme, Integer popsize, Float f, Float cr, Integer gens, String tag,
			String logStats, String populExecutorChain, String trialExecutorChain, Integer executeEach,
			Integer executionsPerGen, Float popToRepackmin, String repackFaRepScale, String minFaRepScale,
			String checkExecutionImprovement, Float aaProportionRepmin, String sortWorstEterms, String timeLimit,
			LocalDateTime date) {
		super();
		this.id = id;
		this.taskState = taskState;
		this.executionName = executionName;
		this.configName = configName;
		this.disturbDegrees = disturbDegrees;
		this.scheme = scheme;
		this.popsize = popsize;
		this.f = f;
		this.cr = cr;
		this.gens = gens;
		this.tag = tag;
		this.logStats = logStats;
		this.populExecutorChain = populExecutorChain;
		this.trialExecutorChain = trialExecutorChain;
		this.executeEach = executeEach;
		this.executionsPerGen = executionsPerGen;
		this.popToRepackmin = popToRepackmin;
		this.repackFaRepScale = repackFaRepScale;
		this.minFaRepScale = minFaRepScale;
		this.checkExecutionImprovement = checkExecutionImprovement;
		this.aaProportionRepmin = aaProportionRepmin;
		this.sortWorstEterms = sortWorstEterms;
		this.timeLimit = timeLimit;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TaskState getTaskState() {
		return taskState;
	}

	public void setTaskState(TaskState taskState) {
		this.taskState = taskState;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getExecutionName() {
		return executionName;
	}

	public void setExecutionName(String executionName) {
		this.executionName = executionName;
	}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}

	public Float getDisturbDegrees() {
		return disturbDegrees;
	}

	public void setDisturbDegrees(Float disturbDegrees) {
		this.disturbDegrees = disturbDegrees;
	}

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public Integer getPopsize() {
		return popsize;
	}

	public void setPopsize(Integer popsize) {
		this.popsize = popsize;
	}

	public Float getF() {
		return f;
	}

	public void setF(Float f) {
		this.f = f;
	}

	public Float getCr() {
		return cr;
	}

	public void setCr(Float cr) {
		this.cr = cr;
	}

	public Integer getGens() {
		return gens;
	}

	public void setGens(Integer gens) {
		this.gens = gens;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getLogStats() {
		return logStats;
	}

	public void setLogStats(String logStats) {
		this.logStats = logStats;
	}

	public String getPopulExecutorChain() {
		return populExecutorChain;
	}

	public void setPopulExecutorChain(String populExecutorChain) {
		this.populExecutorChain = populExecutorChain;
	}

	public String getTrialExecutorChain() {
		return trialExecutorChain;
	}

	public void setTrialExecutorChain(String trialExecutorChain) {
		this.trialExecutorChain = trialExecutorChain;
	}

	public Integer getExecuteEach() {
		return executeEach;
	}

	public void setExecuteEach(Integer executeEach) {
		this.executeEach = executeEach;
	}

	public Integer getExecutionsPerGen() {
		return executionsPerGen;
	}

	public void setExecutionsPerGen(Integer executionsPerGen) {
		this.executionsPerGen = executionsPerGen;
	}

	public Float getPopToRepackmin() {
		return popToRepackmin;
	}

	public void setPopToRepackmin(Float popToRepackmin) {
		this.popToRepackmin = popToRepackmin;
	}

	public String getRepackFaRepScale() {
		return repackFaRepScale;
	}

	public void setRepackFaRepScale(String repackFaRepScale) {
		this.repackFaRepScale = repackFaRepScale;
	}

	public String getMinFaRepScale() {
		return minFaRepScale;
	}

	public void setMinFaRepScale(String minFaRepScale) {
		this.minFaRepScale = minFaRepScale;
	}

	public String getCheckExecutionImprovement() {
		return checkExecutionImprovement;
	}

	public void setCheckExecutionImprovement(String checkExecutionImprovement) {
		this.checkExecutionImprovement = checkExecutionImprovement;
	}

	public Float getAaProportionRepmin() {
		return aaProportionRepmin;
	}

	public void setAaProportionRepmin(Float aaProportionRepmin) {
		this.aaProportionRepmin = aaProportionRepmin;
	}

	public String getSortWorstEterms() {
		return sortWorstEterms;
	}

	public void setSortWorstEterms(String sortWorstEterms) {
		this.sortWorstEterms = sortWorstEterms;
	}

	public String getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(String timeLimit) {
		this.timeLimit = timeLimit;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

}
