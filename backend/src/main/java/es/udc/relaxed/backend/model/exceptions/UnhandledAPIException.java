package es.udc.relaxed.backend.model.exceptions;

@SuppressWarnings("serial")
public class UnhandledAPIException extends Exception {

	public UnhandledAPIException() {
		super("Something went wrong, try again later or contact support service :(");
	}

}
