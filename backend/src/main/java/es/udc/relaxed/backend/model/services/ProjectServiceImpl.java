package es.udc.relaxed.backend.model.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.relaxed.backend.model.entities.Project;
import es.udc.relaxed.backend.model.entities.ProjectDao;
import es.udc.relaxed.backend.model.entities.User;
import es.udc.relaxed.backend.model.enums.Accesibility;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.exceptions.UnauthorizedProjectAccessException;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectDao projectDao;

	@Autowired
	private PermissionChecker permissionChecker;

	private Project getProjectOrThrowInstanceNotFoundException(Long projectId) throws InstanceNotFoundException {
		Optional<Project> optProject = projectDao.findById(projectId);

		if (optProject.isPresent()) {
			return optProject.get();
		} else {
			throw new InstanceNotFoundException("project.entities.project", projectId);
		}
	}
	
	@Override
	public Long addProject(Long userId, String projectName, String description, Accesibility accesibility)
			throws InstanceNotFoundException {

		User user = permissionChecker.checkUser(userId);

		Project project = new Project(projectName, description, accesibility, user);
		project = projectDao.save(project);
		return project.getId();
	}

	@Override
	public void deleteProject(Long userId, Long projectId) throws InstanceNotFoundException, UnauthorizedProjectAccessException {
		
		User user = permissionChecker.checkUser(userId);
		Project project = getProjectOrThrowInstanceNotFoundException(projectId);
		if (!(project.getUser().equals(user))) {
			throw new UnauthorizedProjectAccessException();
		}
		projectDao.delete(project);
	}
	
	@Override
	public Project updateProject(Long userId, Long projectId, String projectName, String description, Accesibility accesibility) throws InstanceNotFoundException, UnauthorizedProjectAccessException {
		
		User user = permissionChecker.checkUser(userId);

		Project project = getProjectOrThrowInstanceNotFoundException(projectId);

		if (!(project.getUser().equals(user))) {
			throw new UnauthorizedProjectAccessException();
		}
		project.setName(projectName);
		project.setDescription(description);
		project.setAccesibility(accesibility);
		
		return projectDao.save(project);
	}

	@Override
	@Transactional(readOnly = true)
	public Block<Project> getUserProjects(Long userId, int page, int size) throws InstanceNotFoundException {

		permissionChecker.checkUserExists(userId);

		Slice<Project> slice = projectDao.findUserProject(userId, PageRequest.of(page, size));

		return new Block<>(slice.getContent(), slice.hasNext());
	}

	@Override
	public Project getProjectDetail(Long userId, Long projectId)
			throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		User user = permissionChecker.checkUser(userId);
		Optional<Project> optProject = projectDao.findById(projectId);
		Project project;

		if (optProject.isPresent()) {
			project = optProject.get();
		} else {
			throw new InstanceNotFoundException("project.entities.project", projectId);
		}
		// Check if we have privacy access to this!!
		// We might see a READ_ONLY or PUBLIC detail
		if (!(project.getUser().equals(user)) && 
				project.getAccesibility().equals(Accesibility.PRIVATE)) {
			throw new UnauthorizedProjectAccessException();
		}
		return project;
	}
}
