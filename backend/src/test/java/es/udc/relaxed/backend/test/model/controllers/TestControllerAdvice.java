package es.udc.relaxed.backend.test.model.controllers;

import java.util.ArrayList;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.relaxed.backend.model.exceptions.IncorrectLoginException;
import es.udc.relaxed.backend.rest.common.ErrorsDto;
import es.udc.relaxed.backend.rest.common.FieldErrorDto;
import es.udc.relaxed.backend.rest.common.JwtInfo;


@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class TestControllerAdvice {

	@Test
	public void testIncorrectLoginException() throws Exception {

		IncorrectLoginException exception = new IncorrectLoginException("un", "pw");
		exception.getPassword();
		exception.getUserName();
	}

	@Test
	public void testJwtInfo() throws Exception {

		JwtInfo filter = new JwtInfo(1L, "un", "USER");
		filter.setRole(filter.getRole());
		filter.setUserId(filter.getUserId());
		filter.setUserName(filter.getUserName());
	}

	@Test
	public void testFieldErrorDto() throws Exception {

		FieldErrorDto error = new FieldErrorDto("name", "message");
		error.setFieldName(error.getFieldName());
		error.setMessage(error.getMessage());
	}

	@Test
	public void testErrorsDto() throws Exception {

		FieldErrorDto error = new FieldErrorDto("name", "message");
		error.setFieldName(error.getFieldName());
		error.setMessage(error.getMessage());
		ArrayList<FieldErrorDto> fieldErrors = new ArrayList<FieldErrorDto>();
		fieldErrors.add(error);
		ErrorsDto errors = new ErrorsDto(new ArrayList<FieldErrorDto>());
		errors.setGlobalError("global");
		errors.setFieldErrors(fieldErrors);
	}

}