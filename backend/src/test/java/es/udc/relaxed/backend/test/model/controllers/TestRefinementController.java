package es.udc.relaxed.backend.test.model.controllers;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import es.udc.relaxed.backend.model.entities.RefinementCall;
import es.udc.relaxed.backend.model.exceptions.DirectoryNotExistingException;
import es.udc.relaxed.backend.model.exceptions.UnhandledAPIException;
import es.udc.relaxed.backend.model.services.Block;
import es.udc.relaxed.backend.model.services.RefinementCallService;
import es.udc.relaxed.backend.model.services.RelaxDEAPIService;
import es.udc.relaxed.backend.rest.common.JwtGenerator;
import es.udc.relaxed.backend.rest.controllers.RefinementController;
import es.udc.relaxed.backend.rest.dtos.DeConfigDto;
import es.udc.relaxed.backend.rest.dtos.ExecConfigDto;
import es.udc.relaxed.backend.rest.dtos.ExecuteRefinementParamsDto;
import es.udc.relaxed.backend.rest.dtos.RefinementResultDto;
import es.udc.relaxed.backend.rest.dtos.RelaxConfigDto;
import es.udc.relaxed.backend.test.model.util.JsonUtil;
import okhttp3.ResponseBody;

@RunWith(SpringRunner.class)
@WebMvcTest(RefinementController.class)
public class TestRefinementController {

	@MockBean
	private RelaxDEAPIService relaxEDApiService;
	@MockBean
	private RefinementCallService refinementCallService;
	@MockBean
	private JwtGenerator generator;

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	@Test
	@WithMockUser
	public void whenPostRefinement_thenCreateRefinement() throws Exception {
		ExecuteRefinementParamsDto params = new ExecuteRefinementParamsDto();
		DeConfigDto de = new DeConfigDto();
		RelaxConfigDto relax = new RelaxConfigDto();
		ExecConfigDto exec = new ExecConfigDto();
		de.setCr(de.getCr());
		de.setF(de.getF());
		de.setGens(de.getGens());
		de.setLogStats(de.getLogStats());
		de.setPopsize(de.getPopsize());
		de.setScheme(de.getScheme());
		de.setTag(de.getTag());
		relax.setAaProportionRepmin(relax.getAaProportionRepmin());
		relax.setCheckExecutionImprovement(relax.getCheckExecutionImprovement());
		relax.setExecuteEach(relax.getExecuteEach());
		relax.setExecutionsPerGen(relax.getExecutionsPerGen());
		relax.setMinFaRepScale(relax.getMinFaRepScale());
		relax.setPopToRepackmin(relax.getPopToRepackmin());
		relax.setPopulExecutorChain(relax.getPopulExecutorChain());
		relax.setRepackFaRepScale(relax.getRepackFaRepScale());
		relax.setSortWorstEterms(relax.getSortWorstEterms());
		relax.setTrialExecutorChain(relax.getTrialExecutorChain());
		exec.setTimeLimit(exec.getTimeLimit());
		params.setConfigName(params.getConfigName());
		params.setDisturbDegrees(params.getDisturbDegrees());
		params.setDe(params.getDe());
		params.setDe(de);
		params.setRelax(params.getRelax());
		params.setRelax(relax);
		params.setExec(params.getExec());
		params.setExec(exec);
		mvc.perform(post("/refinements").contentType(MediaType.APPLICATION_JSON).requestAttr("userId", 1L)
				.param("projectId", "1").content(JsonUtil.toJson(params)).with(csrf()))
				.andExpect(status().is2xxSuccessful());

		verify(relaxEDApiService, VerificationModeFactory.times(1)).executeRefinement(Mockito.anyLong(), Mockito.anyLong(),
				Mockito.any());
		reset(relaxEDApiService);
	}

	@Test
	@WithMockUser
	public void whenPostRefinement_thenThrowException() throws Exception {

		doThrow(DirectoryNotExistingException.class).when(relaxEDApiService).executeRefinement(Mockito.anyLong(),
				Mockito.anyLong(), Mockito.any());

		ExecuteRefinementParamsDto params = new ExecuteRefinementParamsDto();
		mvc.perform(post("/refinements").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L)
				.param("projectId", "1")
				.content(JsonUtil.toJson(params)).with(csrf()))
				.andExpect(status().isNotFound());

		verify(relaxEDApiService, VerificationModeFactory.times(1)).executeRefinement(Mockito.anyLong(), 
				Mockito.anyLong(), Mockito.any());
		reset(relaxEDApiService);
	}

	@Test
	@WithMockUser
	public void whenPostRefinement_thenThrowUnhandledAPIExceptionException() throws Exception {

		doThrow(UnhandledAPIException.class).when(relaxEDApiService).executeRefinement(Mockito.anyLong(),
				Mockito.anyLong(), Mockito.any());

		ExecuteRefinementParamsDto params = new ExecuteRefinementParamsDto();

		mvc.perform(post("/refinements").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L)
				.param("projectId", "1")
				.content(JsonUtil.toJson(params)).with(csrf()))
				.andExpect(status().isBadRequest());

		verify(relaxEDApiService, VerificationModeFactory.times(1)).executeRefinement(Mockito.anyLong(),
				Mockito.anyLong(), Mockito.any());
		reset(relaxEDApiService);
	}
	
	@Test
	@WithMockUser
	public void whenGetUserPendingTasks_thenShowUserPendingTasks() throws Exception {
		mvc.perform(get("/refinements/userPending").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L).with(csrf()))
				.andExpect(status().is2xxSuccessful());

		verify(relaxEDApiService, VerificationModeFactory.times(1))
		.getUserPendingTasks(Mockito.anyLong());
		reset(relaxEDApiService);
	}
	
	@Test
	@WithMockUser
	public void whenGetProjectPendingTasks_thenShowProjectPendingTasks() throws Exception {
		mvc.perform(get("/refinements/projectPending").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L).queryParam("projectId", "1").with(csrf()))
				.andExpect(status().is2xxSuccessful());

		verify(relaxEDApiService, VerificationModeFactory.times(1))
		.getProjectPendingTasks(Mockito.anyLong(), Mockito.anyLong());
		reset(relaxEDApiService);
	}
	
	@Test
	@WithMockUser
	public void whenGetRefinements_thenShowProjectPendingTasks() throws Exception {
		given(refinementCallService.getUserRefinementCalls(1L,1L, 0, 10)).willReturn(new Block<>(Arrays.asList(new RefinementCall()), false));
		mvc.perform(get("/refinements").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L).queryParam("projectId", "1").queryParam("page", "0").with(csrf()))
				.andExpect(status().is2xxSuccessful());

		verify(refinementCallService, VerificationModeFactory.times(1))
		.getUserRefinementCalls(Mockito.anyLong(), Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt());
		reset(relaxEDApiService);
	}
	
	@Test
	@WithMockUser
	public void whenGetRefinementResult_thenShowRefinementResult() throws Exception {
		given(relaxEDApiService.getRefinement(1L,1L)).willReturn(new RefinementResultDto());
		mvc.perform(get("/refinements/1").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L).with(csrf()))
				.andExpect(status().is2xxSuccessful());

		verify(relaxEDApiService, VerificationModeFactory.times(1))
		.getRefinement(Mockito.anyLong(), Mockito.anyLong());
		reset(relaxEDApiService);
	}
	
	@Test
	@WithMockUser
	public void whenDownloadRefinementResult_thenDownloadRefinementResult() throws Exception {
		given(relaxEDApiService.downloadResults(1L,1L)).willReturn(new 
				ImmutablePair<String, ResponseBody>("", ResponseBody.create(okhttp3.MediaType.parse("application/text"), "content")));
		mvc.perform(get("/refinements/download/1").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L).with(csrf()))
				.andExpect(status().is2xxSuccessful());

		verify(relaxEDApiService, VerificationModeFactory.times(1))
		.downloadResults(Mockito.anyLong(), Mockito.anyLong());
		reset(relaxEDApiService);
	}
	
	@Test
	@WithMockUser
	public void whenGetUserConfigNames_thenGetUserConfigNames() throws Exception {
		given(refinementCallService.getUserConfigNames(1L)).willReturn(List.of("name"));
		mvc.perform(get("/refinements/configNames").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L).with(csrf()))
				.andExpect(status().is2xxSuccessful());

		verify(refinementCallService, VerificationModeFactory.times(1))
		.getUserConfigNames(Mockito.anyLong());
		reset(refinementCallService);
	}
	
	@Test
	@WithMockUser
	public void whenGetRefinementConfig_thenGetRefinementConfig() throws Exception {
		given(refinementCallService.getRefinementByConfigName(1L, "config")).willReturn(new RefinementCall());
		mvc.perform(get("/refinements/refinementConfig").contentType(MediaType.APPLICATION_JSON)
				.queryParam("configName", "config")
				.requestAttr("userId", 1L).with(csrf()))
				.andExpect(status().is2xxSuccessful());

		verify(refinementCallService, VerificationModeFactory.times(1))
		.getRefinementByConfigName(Mockito.anyLong(), Mockito.anyString());
		reset(refinementCallService);
	}
	
}