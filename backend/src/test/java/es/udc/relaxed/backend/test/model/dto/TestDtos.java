package es.udc.relaxed.backend.test.model.dto;

import static es.udc.relaxed.backend.rest.dtos.ApiDirectoryConversor.toApiDirectoryDto;
import static es.udc.relaxed.backend.rest.dtos.UserConversor.toAuthenticatedUserDto;
import static es.udc.relaxed.backend.rest.dtos.ProjectConversor.toProjectDto;
import static es.udc.relaxed.backend.rest.dtos.UserConversor.toUser;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.relaxed.backend.model.entities.ApiDirectory;
import es.udc.relaxed.backend.model.entities.Project;
import es.udc.relaxed.backend.model.entities.User;
import es.udc.relaxed.backend.model.entities.User.RoleType;
import es.udc.relaxed.backend.model.enums.Accesibility;
import es.udc.relaxed.backend.model.enums.TaskState;
import es.udc.relaxed.backend.model.pojo.ExecutionConfirmation;
import es.udc.relaxed.backend.rest.dtos.AddProjectParamsDto;
import es.udc.relaxed.backend.rest.dtos.ApiDirectoryDto;
import es.udc.relaxed.backend.rest.dtos.AuthenticatedUserDto;
import es.udc.relaxed.backend.rest.dtos.BlockDto;
import es.udc.relaxed.backend.rest.dtos.GenInfoDto;
import es.udc.relaxed.backend.rest.dtos.IdDto;
import es.udc.relaxed.backend.rest.dtos.IndInfoDto;
import es.udc.relaxed.backend.rest.dtos.LoginParamsDto;
import es.udc.relaxed.backend.rest.dtos.ProjectDto;
import es.udc.relaxed.backend.rest.dtos.ProjectSummaryDto;
import es.udc.relaxed.backend.rest.dtos.RefinementCallDto;
import es.udc.relaxed.backend.rest.dtos.RefinementResultDto;
import es.udc.relaxed.backend.rest.dtos.TaskProgressDto;
import es.udc.relaxed.backend.rest.dtos.UserDto;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class TestDtos {

	@Test
	public void testProjectDto() {

		ProjectDto projectDto = new ProjectDto();
		User user = new User();
		user.setUserName("uploader");
		user.setId(1L);
		Project project = new Project("name", "desc", Accesibility.PRIVATE, user);
		toProjectDto(project, 2L);
		toProjectDto(project, 1L);
		ApiDirectoryDto origDir = new ApiDirectoryDto(LocalDateTime.now(), "uploader");
		ApiDirectory apiDir = new ApiDirectory();
		apiDir.setName("name");
		apiDir.setDate(LocalDateTime.now());
		apiDir.setUploader(user);
		
		ApiDirectoryDto apiDirDto = new ApiDirectoryDto();
		apiDirDto = toApiDirectoryDto(apiDir);
		apiDirDto.setDate(origDir.getDate());
		apiDirDto.setUploader(origDir.getUploader());
		assertEquals(apiDirDto.getUploader(), origDir.getUploader());
		assertEquals(apiDirDto.getDate(), origDir.getDate());
		projectDto.setId(1L);
		projectDto.setName("name");
		projectDto.setDescription("desc");
		projectDto.setAccesibility(Accesibility.PRIVATE);
		projectDto.setCreationDate(LocalDateTime.now());
		projectDto.setUserName("username");
		projectDto.setDirectory(origDir);
		projectDto.setCanDelete(projectDto.getCanDelete());
	}

	@Test
	public void testAuthenticatedUserDto() {

		AuthenticatedUserDto authUserDto = new AuthenticatedUserDto();
		UserDto userDto = new UserDto(1L, null, "first", "last", "mail@mail.mail", "role");
		User user = toUser(userDto);
		user.setRole(RoleType.USER);
		authUserDto = toAuthenticatedUserDto("token", user);
		authUserDto.setUserDto(authUserDto.getUserDto());
		authUserDto.setServiceToken(authUserDto.getServiceToken());
	}

	@Test
	public void testProjectSummaryDto() {

		ProjectSummaryDto projectSummaryDto = new ProjectSummaryDto();
		projectSummaryDto.setAccesibility(projectSummaryDto.getAccesibility());
		projectSummaryDto.setCreationDate(projectSummaryDto.getCreationDate());
		projectSummaryDto.setId(projectSummaryDto.getId());
		projectSummaryDto.setName(projectSummaryDto.getName());
	}

	@Test
	public void testLoginParamsDto() {

		LoginParamsDto loginParamsDto = new LoginParamsDto();
		loginParamsDto.setPassword(loginParamsDto.getPassword());
		loginParamsDto.setUserName("   userName     ");
		assertEquals("userName", loginParamsDto.getUserName());
	}

	@Test
	public void testAddProjectParamsDto() {

		AddProjectParamsDto addProjectParamsDto = new AddProjectParamsDto();
		addProjectParamsDto = new AddProjectParamsDto("name", "desc", Accesibility.PRIVATE);		
		addProjectParamsDto.setAccesibility(addProjectParamsDto.getAccesibility());
		addProjectParamsDto.setDescription(addProjectParamsDto.getDescription());
		addProjectParamsDto.setName(addProjectParamsDto.getName());
	}

	@Test
	public void testIdDto() {

		IdDto idDto = new IdDto();
		idDto.setId(idDto.getId());
	}
	
	@Test
	public void testBlockDto() {

		BlockDto<Object> blockDto = new BlockDto<>();
		blockDto.setExistMoreItems(false);
		blockDto.setItems(Collections.emptyList());
	}
	
	@Test
	public void testExecutionConfirmation() {
		ExecutionConfirmation execution = new ExecutionConfirmation();
		execution.setTaskId(execution.getTaskId());
		execution.setExecutionName(execution.getExecutionName());
	}
	
	@Test
	public void testTaskProgressDto() {
		TaskProgressDto task = new TaskProgressDto();
		task.setTaskId(task.getTaskId());
		task.setState(task.getState());
		task.setConfigName(task.getConfigName());
		task.setConstructionDate(task.getConstructionDate());
		task.setCurrent(task.getCurrent());
		task.setProjectName(task.getProjectName());
		task.setTotal(task.getTotal());
	}

	@Test
	public void testRefinementCallDto() {
		RefinementCallDto ref = new RefinementCallDto();
		ref.setId(1L);
		ref.setTaskId(ref.getTaskId());
		ref.setTaskState(TaskState.SUCCESS);
		ref.setExecutionName("name");
		ref.setConfigName("config");
		ref.setDisturbDegrees(1F);
		ref.setScheme("BEST");
		ref.setPopsize(100);
		ref.setF(1F);
		ref.setCr(1F);
		ref.setGens(100);
		ref.setTag("tag");
		ref.setLogStats("No");
		ref.setPopulExecutorChain("");
		ref.setTrialExecutorChain("");
		ref.setExecuteEach(1);
		ref.setExecutionsPerGen(1);
		ref.setPopToRepackmin(1F);
		ref.setRepackFaRepScale("");
		ref.setMinFaRepScale("");
		ref.setCheckExecutionImprovement("True");
		ref.setAaProportionRepmin(1F);
		ref.setSortWorstEterms("True");
		ref.setTimeLimit("");
		ref.setDate(LocalDateTime.now());
	}
	
	@Test
	public void testRefinementResultDto() {
		GenInfoDto genInfo = new GenInfoDto();
		GenInfoDto genInfo2 = new GenInfoDto(1, 1F, 1F);
		genInfo.setAvg(1F);
		genInfo.setBest(1F);
		genInfo.setGen(1);
		ArrayList<GenInfoDto> genList = new ArrayList<>();
		genList.add(genInfo);
		genList.add(genInfo2);
		IndInfoDto indInfo = new IndInfoDto();
		IndInfoDto indInfo2 = new IndInfoDto(1F, 1F);
		indInfo.setRmsd(1F);
		indInfo.setScore(1F);
		ArrayList<IndInfoDto> popul = new ArrayList<>();
		popul.add(indInfo);
		popul.add(indInfo2);
		RefinementResultDto ref = new RefinementResultDto();
		ref.setGensInfo(genList);
		ref.setPopulInfo(popul);
		ref.setNativePDB("");
		ref.setResultPDB("");
		ref = new RefinementResultDto(genList, popul, "", "", new RefinementCallDto());
		ref.setRefinementCall(ref.getRefinementCall());
		genInfo.setAvg(genInfo.getAvg());
		genInfo.setBest(genInfo.getBest());
		genInfo.setGen(genInfo.getGen());
		indInfo.setRmsd(indInfo.getRmsd());
		indInfo.setScore(indInfo.getScore());
	}

}