package es.udc.relaxed.backend.test.model.controllers;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.MapBindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.WebApplicationContext;

import es.udc.relaxed.backend.model.entities.User;
import es.udc.relaxed.backend.model.entities.User.RoleType;
import es.udc.relaxed.backend.model.exceptions.DuplicateInstanceException;
import es.udc.relaxed.backend.model.exceptions.IncorrectLoginException;
import es.udc.relaxed.backend.model.exceptions.IncorrectPasswordException;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.services.UserService;
import es.udc.relaxed.backend.rest.common.CommonControllerAdvice;
import es.udc.relaxed.backend.rest.common.JwtGenerator;
import es.udc.relaxed.backend.rest.common.JwtInfo;
import es.udc.relaxed.backend.rest.controllers.UserController;
import es.udc.relaxed.backend.rest.dtos.ChangePasswordParamsDto;
import es.udc.relaxed.backend.rest.dtos.LoginParamsDto;
import es.udc.relaxed.backend.rest.dtos.UserDto;
import es.udc.relaxed.backend.test.model.util.JsonUtil;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class TestUserController {

	@Autowired
	private JwtGenerator jwt;

	@MockBean
	private JwtGenerator generator;

	@MockBean
	private UserService userService;

	@Autowired
	private UserController controller;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private CommonControllerAdvice advice;

	private MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
	}

	private User getUser(String userName) {
		User user = new User(userName, "password", "firstName", "lastName", userName + "@" + userName + ".com");
		user.setId(1L);
		user.setRole(RoleType.USER);
		return user;
	}
//
//	@Test
//	@WithMockUser
//	public void whenPostSignUp_thenThrowDuplicateInstanceException() throws Exception {
//		given(generator.generate(Mockito.any())).willReturn("key");
//		User user = getUser("user");
//		Mockito.doThrow(new DuplicateInstanceException("name", "key")).when(userService).signUp(user);
////		Mockito.doAnswer(new Answer<Void>() {
////
////			@Override
////			public Void answer(final InvocationOnMock invocation) throws Throwable {
////
////				User user = (User) (invocation.getArguments())[0];
////				user.setId(1L);
////				user.setRole(RoleType.USER);
////				return null;
////			}
////		}).when(userService).signUp(user);
//		UserDto params = new UserDto(1L, "name", "firstn", "lastn", "user@user", "USER");
//		params.setPassword("aa");
//		mvc.perform(post("/users/signUp").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(params))
//				.with(csrf())).andExpect(status().isBadRequest());
//
//		verify(userService, VerificationModeFactory.times(1)).signUp(Mockito.any());
//		reset(userService);
//	}

	@Test
	@WithMockUser
	public void whenChangePassword_thenChangePassword() throws Exception {

		ChangePasswordParamsDto params = new ChangePasswordParamsDto();
		params.setNewPassword("new");
		params.setOldPassword("old");
		mvc.perform(post("/users/1/changePassword").requestAttr("userId", 1L).contentType(MediaType.APPLICATION_JSON)
				.content(JsonUtil.toJson(params)).with(csrf())).andExpect(status().isNoContent());

		verify(userService, VerificationModeFactory.times(1)).changePassword(1L, "old", "new");
		reset(userService);
	}

	@Test
	@WithMockUser
	public void whenChangePassword_thenThrowPermissionException() throws Exception {

		ChangePasswordParamsDto params = new ChangePasswordParamsDto();
		params.setNewPassword("new");
		params.setOldPassword("old");
		mvc.perform(post("/users/1/changePassword").requestAttr("userId", 2L).contentType(MediaType.APPLICATION_JSON)
				.content(JsonUtil.toJson(params)).with(csrf())).andExpect(status().isForbidden());

		verify(userService, VerificationModeFactory.times(0)).signUp(Mockito.any());
		reset(userService);
	}

	@Test
	@WithMockUser
	public void whenUpdateProfile_thenThrowMethodNotAllowed() throws Exception {

		UserDto params = new UserDto(1L, "name", "firstn", "lastn", "user@user", "USER");
		params.setPassword("new");
		mvc.perform(post("/users/1").requestAttr("userId", 1L).contentType(MediaType.APPLICATION_JSON)
				.content(JsonUtil.toJson(params)).with(csrf())).andExpect(status().isMethodNotAllowed());

		verify(userService, VerificationModeFactory.times(0)).updateProfile(1L, "firstn", "lastn", "user@user");
		reset(userService);
	}

	@Test
	@WithMockUser
	public void whenUpdateProfile_thenUpdateProfile() throws Exception {

		UserDto params = new UserDto(1L, "name", "firstn", "lastn", "user@user", "USER");
		given(userService.updateProfile(1L, "firstn", "lastn", "user@user")).willReturn(getUser("name"));

		params.setPassword("new");
		mvc.perform(put("/users/1").requestAttr("userId", 1L).contentType(MediaType.APPLICATION_JSON)
				.content(JsonUtil.toJson(params)).with(csrf())).andExpect(status().isOk());

		verify(userService, VerificationModeFactory.times(1)).updateProfile(1L, "firstn", "lastn", "user@user");
		reset(userService);
	}

	@Test
	@WithMockUser
	public void whenUpdateProfile_thenThrowPermissionException() throws Exception {

		UserDto params = new UserDto(1L, "name", "firstn", "lastn", "user@user", "USER");
		params.setPassword("new");
		mvc.perform(put("/users/2").requestAttr("userId", 1L).contentType(MediaType.APPLICATION_JSON)
				.content(JsonUtil.toJson(params)).with(csrf())).andExpect(status().isForbidden());

		verify(userService, VerificationModeFactory.times(0)).updateProfile(Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any());
		reset(userService);
	}

	@Test
	@WithMockUser
	public void testHandleIncorrectLoginException() throws Exception {
		controller.handleIncorrectLoginException(new IncorrectLoginException("username", "pw"), Locale.ENGLISH);

	}

	@Test
	@WithMockUser
	public void testHandleIncorrectPasswordException() throws Exception {
		controller.handleIncorrectPasswordException(new IncorrectPasswordException(), Locale.ENGLISH);

	}

	@Test
	@WithMockUser
	public void testSignUpException() throws Exception {
		UserDto params = new UserDto(1L, "name", "firstn", "lastn", "user@user", "USER");

		try {
			controller.signUp(params);
		} catch (Exception e) {

		}
	}

	@Test
	@WithMockUser
	public void testLoginException() throws Exception {
		LoginParamsDto params = new LoginParamsDto();
		params.setUserName("us");
		params.setPassword("pw");

		try {
			controller.login(params);
		} catch (Exception e) {

		}
	}

	@Test
	@WithMockUser
	public void testLoginServiceTokenException() throws Exception {

		try {
			controller.loginFromServiceToken(1L, "tokens");
		} catch (Exception e) {

		}
	}

	@Test
	public void testJwtInfo() throws Exception {

		JwtInfo filter = new JwtInfo(1L, "un", "USER");
		filter.setRole(filter.getRole());
		filter.setUserId(filter.getUserId());
		filter.setUserName(filter.getUserName());
		jwt.generate(filter);
		jwt.getInfo("sss");
	}

	@Test
	public void testCommonControllerAdviceDto() throws Exception {

		advice.handleDuplicateInstanceException(new DuplicateInstanceException("name", "key"), Locale.ENGLISH);
	}
	
	@Test
	public void testHandleInstanceNotFoundException() throws Exception {

		User user = new User("user", "pass", "", "", "");
		user.setId(1L);
		advice.handleInstanceNotFoundException(new InstanceNotFoundException("name", user), Locale.ENGLISH);
	}
	
	@Test
	public void testHandleMethodArgumentNotValidException() throws Exception {

		User user = new User("user", "pass", "", "", "");
		user.setId(1L);
		HashMap<String, User> map = new HashMap<String, User>();
		map.put("user", user);
		BindingResult result = new MapBindingResult(map, "user");
		result.addError(new FieldError("user", "user", "error"));
		advice.handleMethodArgumentNotValidException(
				new MethodArgumentNotValidException(
						new MethodParameter(CommonControllerAdvice.class.
								getMethod("handleMethodArgumentNotValidException",
							MethodArgumentNotValidException.class),
						0),
					result
				)
			);
	}

}