package es.udc.relaxed.backend.test.model.services;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.relaxed.backend.model.entities.ApiDirectory;
import es.udc.relaxed.backend.model.entities.ApiDirectoryDao;
import es.udc.relaxed.backend.model.entities.Project;
import es.udc.relaxed.backend.model.entities.ProjectDao;
import es.udc.relaxed.backend.model.entities.User;
import es.udc.relaxed.backend.model.enums.Accesibility;
import es.udc.relaxed.backend.model.exceptions.DuplicateInstanceException;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.exceptions.UnauthorizedProjectAccessException;
import es.udc.relaxed.backend.model.services.Block;
import es.udc.relaxed.backend.model.services.ProjectService;
import es.udc.relaxed.backend.model.services.UserService;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class TestProjectService {

	@Autowired
	private ProjectService projectService;

	@Autowired
	private UserService userService;

	@Autowired
	private ProjectDao projectDao;

	@Autowired
	private ApiDirectoryDao apiDirectoryDao;

	private final Long NON_EXISTENT_ID = -1L;

	private User signUpUser(String userName) {

		User user = new User(userName, "password", "firstName", "lastName", userName + "@" + userName + ".com");

		try {
			userService.signUp(user);
		} catch (DuplicateInstanceException e) {
			throw new RuntimeException(e);
		}

		return user;

	}

	@Test
	public void testAddProject() throws InstanceNotFoundException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");

		Project expectedProject = new Project(projectname, description, accesibility, user);
		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);

		Optional<Project> optResultProject = projectDao.findById(id);
		Project resultProject = optResultProject.get();

		assertEquals(expectedProject.getAccesibility(), resultProject.getAccesibility());
		assertEquals(expectedProject.getName(), resultProject.getName());
		assertEquals(expectedProject.getUser(), resultProject.getUser());

	}

	@Test
	public void testAddProjectNoSignedUpUser() throws InstanceNotFoundException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;

		Assertions.assertThrows(InstanceNotFoundException.class, () -> {
			projectService.addProject(NON_EXISTENT_ID, projectname, description, accesibility);
		});
	}

	@Test
	public void testDeleteProject() throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);
		Optional<Project> optResultProject = projectDao.findById(id);
		Project project = optResultProject.get();

		projectService.deleteProject(user.getId(), project.getId());

		Assertions.assertThrows(NoSuchElementException.class, () -> {
			final Optional<Project> optExceptionProject = projectDao.findById(id);
			optExceptionProject.get();
		});
	}

	@Test
	public void testDeleteProjectDeletesOnCascade() throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);
		ApiDirectory apiDirectory = apiDirectoryDao.save(new ApiDirectory("dir", user));

		Optional<Project> optResultProject = projectDao.findById(id);
		Project project = optResultProject.get();
		project.setApiDirectory(apiDirectory);

		projectService.deleteProject(user.getId(), project.getId());

		Assertions.assertThrows(NoSuchElementException.class, () -> {
			final Optional<Project> optExceptionProject = projectDao.findById(id);
			optExceptionProject.get();
		});

		Assertions.assertThrows(NoSuchElementException.class, () -> {
			final Optional<ApiDirectory> optExceptionDirectory = apiDirectoryDao.findById(apiDirectory.getId());
			optExceptionDirectory.get();
		});
	}

	@Test
	public void testDeleteProjectThrowsUnauthorizedProjectAccessException()
			throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PUBLIC;
		User user1 = signUpUser("user1");
		User user2 = signUpUser("user2");

		Long id = projectService.addProject(user1.getId(), projectname, description, accesibility);

		Optional<Project> optResultProject = projectDao.findById(id);
		Project project = optResultProject.get();

		Assertions.assertThrows(UnauthorizedProjectAccessException.class, () -> {
			projectService.deleteProject(user2.getId(), project.getId());
		});
	}

	@Test
	public void testDeleteProjectThrowsInstanceNotFoundException()
			throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		User user = signUpUser("user1");
		Assertions.assertThrows(InstanceNotFoundException.class, () -> {
			projectService.deleteProject(user.getId(), NON_EXISTENT_ID);
		});
	}
	
	@Test
	public void testGetProjectDetail() throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");
		ApiDirectory apiDir = new ApiDirectory("name", user);
		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);

		Project expectedProject = projectDao.findById(id).get();
		expectedProject.setId(id);

		Project projectDetail = projectService.getProjectDetail(user.getId(), id);
		assertEquals(expectedProject, projectDetail);
		apiDirectoryDao.save(apiDir);
		projectDetail.setApiDirectory(apiDir);
		projectDao.save(projectDetail);
		Project projectDetailWithApiDir = projectService.getProjectDetail(user.getId(), id);
		expectedProject = projectDao.findById(id).get();
		assertEquals(expectedProject, projectDetailWithApiDir);

	}

	@Test
	public void testGetNonExistingProjectDetail() throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		User user1 = signUpUser("user1");

		Assertions.assertThrows(InstanceNotFoundException.class, () -> {
			projectService.getProjectDetail(user1.getId(), NON_EXISTENT_ID);
		});
	}

	@Test
	public void testGetProjectDetailNonAuthorized() throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user1 = signUpUser("user1");
		User user2 = signUpUser("user2");

		Long id = projectService.addProject(user1.getId(), projectname, description, accesibility);

		Assertions.assertThrows(UnauthorizedProjectAccessException.class, () -> {
			projectService.getProjectDetail(user2.getId(), id);
		});
	}

	@Test
	public void testGetProjectDetailPublicProject() throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibilityPublic = Accesibility.PUBLIC;
		Accesibility accesibilityReadOnly = Accesibility.READ_ONLY;
		User user1 = signUpUser("user1");
		User user2 = signUpUser("user2");

		Long projectIdP = projectService.addProject(user1.getId(), projectname, description, accesibilityPublic);
		Long projectIdRO = projectService.addProject(user1.getId(), projectname, description, accesibilityReadOnly);

		Assertions.assertDoesNotThrow(() -> {
			projectService.getProjectDetail(user2.getId(), projectIdP);
			projectService.getProjectDetail(user2.getId(), projectIdRO);
		});
	}

	@Test
	public void testGetUserProjects() throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibilityPublic = Accesibility.PUBLIC;
		Accesibility accesibilityReadOnly = Accesibility.READ_ONLY;
		Accesibility accesibilityPrivate = Accesibility.PRIVATE;
		User user1 = signUpUser("user1");

		Long projectIdP = projectService.addProject(user1.getId(), projectname, description, accesibilityPublic);
		Project projectDetailP = projectService.getProjectDetail(user1.getId(), projectIdP);
		projectDetailP.setDate(LocalDateTime.now().minusHours(2).truncatedTo(ChronoUnit.MILLIS));
		projectDetailP = projectDao.save(projectDetailP);

		Long projectIdRO = projectService.addProject(user1.getId(), projectname, description, accesibilityReadOnly);
		Project projectDetailRO = projectService.getProjectDetail(user1.getId(), projectIdRO);
		projectDetailRO.setDate(LocalDateTime.now().minusHours(1).truncatedTo(ChronoUnit.MILLIS));
		projectDetailRO = projectDao.save(projectDetailRO);

		Long projectIdPr = projectService.addProject(user1.getId(), projectname, description, accesibilityPrivate);
		Project projectDetailPr = projectService.getProjectDetail(user1.getId(), projectIdPr);

		Block<Project> blockExpectedUser1 = new Block<>(Arrays.asList(projectDetailPr, projectDetailRO, projectDetailP), false);

		assertEquals(blockExpectedUser1, projectService.getUserProjects(user1.getId(), 0, 3));
	}

	@Test
	public void testGetUserProjectsPagination() throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibilityPublic = Accesibility.PUBLIC;
		Accesibility accesibilityReadOnly = Accesibility.READ_ONLY;
		Accesibility accesibilityPrivate = Accesibility.PRIVATE;
		User user1 = signUpUser("user1");

		Long projectIdP = projectService.addProject(user1.getId(), projectname, description, accesibilityPublic);
		Project projectDetailP = projectService.getProjectDetail(user1.getId(), projectIdP);
		projectDetailP.setDate(LocalDateTime.now().minusHours(2).truncatedTo(ChronoUnit.MILLIS));
		projectDetailP = projectDao.save(projectDetailP);

		Long projectIdRO = projectService.addProject(user1.getId(), projectname, description, accesibilityReadOnly);
		Project projectDetailRO = projectService.getProjectDetail(user1.getId(), projectIdRO);
		projectDetailRO.setDate(LocalDateTime.now().minusHours(1).truncatedTo(ChronoUnit.MILLIS));
		projectDetailRO = projectDao.save(projectDetailRO);

		Long projectIdPr = projectService.addProject(user1.getId(), projectname, description, accesibilityPrivate);
		Project projectDetailPr = projectService.getProjectDetail(user1.getId(), projectIdPr);

		Block<Project> blockExpectedFirstPage = new Block<>(Arrays.asList(projectDetailPr, projectDetailRO), true);
		Block<Project> blockExpectedSecondPage = new Block<>(Arrays.asList(projectDetailP), false);

		assertEquals(blockExpectedFirstPage, projectService.getUserProjects(user1.getId(), 0, 2));
		assertEquals(blockExpectedSecondPage, projectService.getUserProjects(user1.getId(), 1, 2));
	}

	@Test
	public void testGetUserProjectsMixedProjectAccess() throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibilityPublic = Accesibility.PUBLIC;
		Accesibility accesibilityReadOnly = Accesibility.READ_ONLY;
		Accesibility accesibilityPrivate = Accesibility.PRIVATE;
		User user1 = signUpUser("user1");
		User user2 = signUpUser("user2");

		Long projectIdP = projectService.addProject(user1.getId(), projectname, description, accesibilityPublic);
		Project projectDetailP = projectService.getProjectDetail(user1.getId(), projectIdP);
		projectDetailP.setDate(LocalDateTime.now().minusHours(2).truncatedTo(ChronoUnit.MILLIS));
		projectDetailP = projectDao.save(projectDetailP);

		Long projectIdRO = projectService.addProject(user1.getId(), projectname, description, accesibilityReadOnly);
		Project projectDetailRO = projectService.getProjectDetail(user1.getId(), projectIdRO);
		projectDetailRO.setDate(LocalDateTime.now().minusHours(1).truncatedTo(ChronoUnit.MILLIS));
		projectDetailRO = projectDao.save(projectDetailRO);

		Long projectIdPr = projectService.addProject(user1.getId(), projectname, description, accesibilityPrivate);
		Project projectDetailPr = projectService.getProjectDetail(user1.getId(), projectIdPr);

		Block<Project> blockExpectedUser1 = new Block<>(Arrays.asList(projectDetailPr, projectDetailRO, projectDetailP), false);
		Block<Project> blockExpectedUser2 = new Block<>(Arrays.asList(projectDetailRO, projectDetailP), false);

		assertEquals(blockExpectedUser1, projectService.getUserProjects(user1.getId(), 0, 3));
		assertEquals(blockExpectedUser2, projectService.getUserProjects(user2.getId(), 0, 2));
	}

	@Test
	public void testUpdateProject() throws InstanceNotFoundException, UnauthorizedProjectAccessException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);
		
		String newProjectName = "newName";
		String newDescription = "newDesc";
		Accesibility newAccesibility = Accesibility.PUBLIC;
		
		Project expectedProject = new Project(newProjectName, newDescription, newAccesibility, user);
		projectService.updateProject(user.getId(), id, newProjectName, newDescription, newAccesibility);

		Optional<Project> optResultProject = projectDao.findById(id);
		Project resultProject = optResultProject.get();
		assertEquals(expectedProject.getAccesibility(), resultProject.getAccesibility());
		assertEquals(expectedProject.getName(), resultProject.getName());
		assertEquals(expectedProject.getUser(), resultProject.getUser());
	}

	@Test
	public void testUpdateNonExistingProject() throws InstanceNotFoundException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");

		Assertions.assertThrows(InstanceNotFoundException.class, () -> {
			projectService.updateProject(user.getId(), NON_EXISTENT_ID, projectname, description, accesibility);
		});
	}
	
	@Test
	public void testUpdateProjectNotAllowed() throws InstanceNotFoundException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");
		User user2 = signUpUser("user2");
		
		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);

		Assertions.assertThrows(UnauthorizedProjectAccessException.class, () -> {
			projectService.updateProject(user2.getId(), id, projectname, description, accesibility);
		});
	}

}
