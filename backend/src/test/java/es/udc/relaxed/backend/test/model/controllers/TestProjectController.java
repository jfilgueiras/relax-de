package es.udc.relaxed.backend.test.model.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import es.udc.relaxed.backend.model.entities.ApiDirectory;
import es.udc.relaxed.backend.model.entities.Project;
import es.udc.relaxed.backend.model.entities.User;
import es.udc.relaxed.backend.model.enums.Accesibility;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.exceptions.UnauthorizedProjectAccessException;
import es.udc.relaxed.backend.model.services.Block;
import es.udc.relaxed.backend.model.services.ProjectService;
import es.udc.relaxed.backend.rest.common.JwtGenerator;
import es.udc.relaxed.backend.rest.controllers.ProjectController;
import es.udc.relaxed.backend.test.model.util.JsonUtil;

@RunWith(SpringRunner.class)
@WebMvcTest(ProjectController.class)
public class TestProjectController {

	@MockBean
	private ProjectService service;

	@MockBean
	private JwtGenerator generator;

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	private User getUser(String userName) {
		User user = new User(userName, "password", "firstName", "lastName", userName + "@" + userName + ".com");
		user.setId(1L);
		return user;
	}

	@Test
	@WithMockUser
	public void whenPostProject_thenCreateProject() throws Exception {
		
		User user = getUser("user");
		Project project = new Project("project", "des", Accesibility.PRIVATE, user);

		given(service.addProject(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).willReturn(1L);
		project.setId(1L);

		mvc.perform(post("/projects").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L)
				.with(csrf())
				.content(JsonUtil.toJson(project)))
				.andExpect(status().is2xxSuccessful()).andExpect(jsonPath("$.id", is(1)));
		verify(service, VerificationModeFactory.times(1)).addProject(Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any());
		reset(service);
	}
	
	@Test
	@WithMockUser
	public void whenPutProject_thenUpdateProject() throws Exception {
		
		User user = getUser("user");
		Project project = new Project("project", "des", Accesibility.PRIVATE, user);
		project.setId(1L);

		given(service.updateProject(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).willReturn(project);

		mvc.perform(put("/projects/1").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L)
				.with(csrf())
				.content(JsonUtil.toJson(project)))
				.andExpect(status().is2xxSuccessful()).andExpect(jsonPath("$.id", is(1)));
		verify(service, VerificationModeFactory.times(1)).updateProject(Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any());
		reset(service);
	}


	@Test
	@WithMockUser
	public void whenPostDelete_thenDeleteData() throws Exception {
		mvc.perform(post("/projects/1").contentType(MediaType.APPLICATION_JSON).requestAttr("userId", 1L)
				.with(csrf())).andExpect(status().is2xxSuccessful());
		verify(service, VerificationModeFactory.times(1)).deleteProject(Mockito.anyLong(), Mockito.anyLong());
		reset(service);
	}

	@Test
	@WithMockUser
	public void whenGetProjectDetails_thenShowProjectDetails() throws Exception {
		
		User user = getUser("user");
		Project project = new Project("project", "des", Accesibility.PRIVATE, user);
		ApiDirectory apiDir = new ApiDirectory("nombre", user);
		project.setApiDirectory(apiDir);
		
		given(service.getProjectDetail(1L, 1L)).willReturn(project);

		mvc.perform(get("/projects/1").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L).with(csrf()))
				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$.name", is(project.getName())));
		verify(service, VerificationModeFactory.times(1)).getProjectDetail(1L, 1L);
		reset(service);
	}

	@Test
	@WithMockUser
	public void whenGetProjectDetails_thenThrowUnauthorizedProjectAccessException() throws Exception {
		given(service.getProjectDetail(2L, 1L)).willThrow(new UnauthorizedProjectAccessException());

		mvc.perform(
				get("/projects/1").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 2L).with(csrf()))
				.andExpect(status().isForbidden());
		verify(service, VerificationModeFactory.times(1)).getProjectDetail(2L, 1L);
		reset(service);
	}

	@Test
	@WithMockUser
	public void whenGetProjectDetails_thenThrowInstanceNotFoundException() throws Exception {
		given(service.getProjectDetail(1L, -1L)).willThrow(new InstanceNotFoundException("project", "object"));

		mvc.perform(
				get("/projects/-1").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L).with(csrf()))
				.andExpect(status().isNotFound());
		verify(service, VerificationModeFactory.times(1)).getProjectDetail(1L, -1L);
		reset(service);
	}
	
	@Test
	@WithMockUser
	public void whenGetUserProject_ShowUserProject() throws Exception {
		User user = getUser("user");

		Project project = new Project("project", "des", Accesibility.PRIVATE, user);
		given(service.getUserProjects(1L, 0, 10)).willReturn(new Block<>(Arrays.asList(project), false));

		mvc.perform(get("/projects").contentType(MediaType.APPLICATION_JSON)
				.requestAttr("userId", 1L).with(csrf()))
				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$.existMoreItems", is(false)));
		verify(service, VerificationModeFactory.times(1)).getUserProjects(1L, 0, 10);
		reset(service);
	}

}