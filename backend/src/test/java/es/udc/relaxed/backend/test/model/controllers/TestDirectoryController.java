package es.udc.relaxed.backend.test.model.controllers;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import es.udc.relaxed.backend.model.entities.ApiDirectory;
import es.udc.relaxed.backend.model.entities.User;
import es.udc.relaxed.backend.model.exceptions.InvalidFileExtensionException;
import es.udc.relaxed.backend.model.exceptions.MissingProteinFileException;
import es.udc.relaxed.backend.model.services.RelaxDEAPIService;
import es.udc.relaxed.backend.rest.common.JwtGenerator;
import es.udc.relaxed.backend.rest.controllers.DirectoryController;

@RunWith(SpringRunner.class)
@WebMvcTest(DirectoryController.class)
public class TestDirectoryController {

	@MockBean
	private RelaxDEAPIService relaxEDApiService;

	@MockBean
	private JwtGenerator generator;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private DirectoryController directoryController;

	private MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
	}

	private User getUser(String userName) {
		return new User(userName, "password", "firstName", "lastName", userName + "@" + userName + ".com");
	}

	@Test
	@WithMockUser
	public void whenPostAddDataByProteinFile_thenAddData() throws Exception {
		given(relaxEDApiService.uploadProtein(Mockito.anyLong(), Mockito.anyLong(), Mockito.any()))
				.willReturn(new ApiDirectory("apiDir", getUser("user")));

		MockMultipartFile protein = new MockMultipartFile("protein", "protein.pdb", "chemical/x-pdb", "ATOM".getBytes());
		HashMap<String, String> contentTypeParams = new HashMap<String, String>();
		contentTypeParams.put("boundary", "----WebKitFormBoundaryaDEFKSFMY18ehkjt");
		MediaType mediaType = new MediaType("multipart", "form-data", contentTypeParams);

		mvc.perform(MockMvcRequestBuilders.multipart("/directory/upload-protein").file(protein).content(protein.getBytes())
				.contentType(mediaType).requestAttr("userId", 1L).param("projectId", "1").with(csrf()))
				.andExpect(status().is2xxSuccessful());

		verify(relaxEDApiService, VerificationModeFactory.times(1)).uploadProtein(Mockito.any(), Mockito.any(),
				Mockito.any());
		reset(relaxEDApiService);
	}

	@Test
	@WithMockUser
	public void whenPostAddDataByProteinFile_thenThrowException() throws Exception {
		given(relaxEDApiService.uploadProtein(Mockito.anyLong(), Mockito.anyLong(), Mockito.any()))
				.willThrow(InvalidFileExtensionException.class);

		MockMultipartFile protein = new MockMultipartFile("protein", "protein.json", "appliaction/json", "ATOM".getBytes());
		HashMap<String, String> contentTypeParams = new HashMap<String, String>();
		contentTypeParams.put("boundary", "----WebKitFormBoundaryaDEFKSFMY18ehkjt");
		MediaType mediaType = new MediaType("multipart", "form-data", contentTypeParams);

		mvc.perform(MockMvcRequestBuilders.multipart("/directory/upload-protein").file(protein).content(protein.getBytes())
				.contentType(mediaType).requestAttr("userId", 1L).param("projectId", "1").with(csrf()))
				.andExpect(status().isForbidden());

		verify(relaxEDApiService, VerificationModeFactory.times(1)).uploadProtein(Mockito.any(), Mockito.any(),
				Mockito.any());
		reset(relaxEDApiService);
	}

	@Test
	@WithMockUser
	public void whenPostAddDataByProteinFile_thenThrowMissingCorpusException() throws Exception {
		given(relaxEDApiService.uploadProtein(Mockito.anyLong(), Mockito.anyLong(), Mockito.any()))
				.willThrow(MissingProteinFileException.class);

		MockMultipartFile protein = new MockMultipartFile("protein", "protein.pdb", "chemical/x-pdb", "".getBytes());
		HashMap<String, String> contentTypeParams = new HashMap<String, String>();
		contentTypeParams.put("boundary", "----WebKitFormBoundaryaDEFKSFMY18ehkjt");
		MediaType mediaType = new MediaType("multipart", "form-data", contentTypeParams);

		mvc.perform(MockMvcRequestBuilders.multipart("/directory/upload-protein").file(protein).content(protein.getBytes())
				.contentType(mediaType).requestAttr("userId", 1L).param("projectId", "1").with(csrf()))
				.andExpect(status().isNotFound());

		verify(relaxEDApiService, VerificationModeFactory.times(1)).uploadProtein(Mockito.any(), Mockito.any(),
				Mockito.any());
		reset(relaxEDApiService);
	}

	@Test
	public void testInvalidFileExtensionException() throws Exception {
		InvalidFileExtensionException exception = new InvalidFileExtensionException(null, null, null);
		exception.setExpectedExtension(exception.getExpectedExtension());
		exception.setFileType(exception.getFileType());
		exception.setGivenExtension(exception.getGivenExtension());
		directoryController.handleInvalidFileExtensionException(
				new InvalidFileExtensionException(".txt", ".txt", "file"), Locale.ENGLISH);
	}

	@Test
	public void testMissingProteinFileException() throws Exception {
		directoryController.handleMissingProteinFileException(new MissingProteinFileException(), Locale.ENGLISH);
	}

}