package es.udc.relaxed.backend.test.model.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.relaxed.backend.model.entities.ApiDirectory;
import es.udc.relaxed.backend.model.entities.ApiDirectoryDao;
import es.udc.relaxed.backend.model.entities.Project;
import es.udc.relaxed.backend.model.entities.RefinementCall;
import es.udc.relaxed.backend.model.entities.RefinementCallDao;
import es.udc.relaxed.backend.model.entities.User;
import es.udc.relaxed.backend.model.enums.Accesibility;
import es.udc.relaxed.backend.model.enums.TaskState;
import es.udc.relaxed.backend.model.exceptions.DirectoryNotExistingException;
import es.udc.relaxed.backend.model.exceptions.DuplicateInstanceException;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.exceptions.InvalidFileExtensionException;
import es.udc.relaxed.backend.model.exceptions.MissingProteinFileException;
import es.udc.relaxed.backend.model.exceptions.UnauthorizedProjectAccessException;
import es.udc.relaxed.backend.model.exceptions.UnhandledAPIException;
import es.udc.relaxed.backend.model.services.ProjectService;
import es.udc.relaxed.backend.model.services.RelaxDEAPIService;
import es.udc.relaxed.backend.model.services.UserService;
import es.udc.relaxed.backend.rest.dtos.DeConfigDto;
import es.udc.relaxed.backend.rest.dtos.ExecConfigDto;
import es.udc.relaxed.backend.rest.dtos.ExecuteRefinementParamsDto;
import es.udc.relaxed.backend.rest.dtos.RelaxConfigDto;
import es.udc.relaxed.backend.rest.dtos.TaskProgressDto;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class TestRelaxDEAPIService {

	@Value("classpath:test/source/1j4m.pdb")
	Resource PROTEIN_TEST;

	@Value("classpath:test/source/1j4m.json")
	Resource WRONG_EXTENSION_PROTEIN_TEST;

	@Value("classpath:test/source/empty.pdb")
	Resource EMPTY_PROTEIN_TEST;

	@Autowired
	private ApiDirectoryDao apiDirectoryDao;
	
	@Autowired
	private RefinementCallDao refinementCallDao;

	@Autowired
	private RelaxDEAPIService relaxEDApiService;

	@Autowired
	private UserService userService;

	@Autowired
	private ProjectService projectService;

	private final Long NON_EXISTENT_ID = -1L;

	private User signUpUser(String userName) {

		User user = new User(userName, "password", "firstName", "lastName", userName + "@" + userName + ".com");

		try {
			userService.signUp(user);
		} catch (DuplicateInstanceException e) {
			throw new RuntimeException(e);
		}

		return user;

	}

	private DiskFileItem createFileItem(File file) throws IOException {
		DiskFileItem fileItem = (DiskFileItem) new DiskFileItemFactory().createItem("fileProject", "text/plain", true,
				file.getName());
		InputStream input = new FileInputStream(file);
		OutputStream os = fileItem.getOutputStream();
		int ret = input.read();
		while (ret != -1) {
			os.write(ret);
			ret = input.read();
		}
		os.flush();
		input.close();
		return fileItem;
	}

	@Test
	public void testUploadProteinFile() throws InstanceNotFoundException, InvalidFileExtensionException,
			UnauthorizedProjectAccessException, UnhandledAPIException, MissingProteinFileException, IOException {
		String dataname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");
		apiDirectoryDao.save(new ApiDirectory("test", user));
		Long id = projectService.addProject(user.getId(), dataname, description, accesibility);

		DiskFileItem proteinFileItem = createFileItem(PROTEIN_TEST.getFile());
		try {
			relaxEDApiService.uploadProtein(user.getId(), id, proteinFileItem);
			relaxEDApiService.uploadProtein(user.getId(), id, proteinFileItem);
		} catch (UnhandledAPIException e) {

		}

	}

	@Test
	public void testUploadProteinFileThrowsInstanceNotFoundException()
			throws InstanceNotFoundException, InvalidFileExtensionException, UnauthorizedProjectAccessException,
			UnhandledAPIException, MissingProteinFileException, IOException {
		User user = signUpUser("user1");

		DiskFileItem proteinFileItem = createFileItem(PROTEIN_TEST.getFile());
		assertThrows(InstanceNotFoundException.class,
				() -> relaxEDApiService.uploadProtein(user.getId(), NON_EXISTENT_ID, proteinFileItem));
	}

	@Test
	public void testUploadProteinFileThrowsUnauthorizedProjectAccessException()
			throws InstanceNotFoundException, InvalidFileExtensionException, UnauthorizedProjectAccessException,
			UnhandledAPIException, MissingProteinFileException, IOException {
		String dataname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");
		User anotherUser = signUpUser("user2");
		apiDirectoryDao.save(new ApiDirectory("test", user));
		Long id1 = projectService.addProject(user.getId(), dataname, description, accesibility);
		Long id2 = projectService.addProject(user.getId(), dataname, description, Accesibility.PUBLIC);

		DiskFileItem proteinFileItem = createFileItem(PROTEIN_TEST.getFile());
		assertThrows(UnauthorizedProjectAccessException.class,
				() -> relaxEDApiService.uploadProtein(anotherUser.getId(), id1, proteinFileItem));
		try {
			relaxEDApiService.uploadProtein(anotherUser.getId(), id2, proteinFileItem);
			relaxEDApiService.uploadProtein(anotherUser.getId(), id2, proteinFileItem);
		} catch (UnhandledAPIException e) {

		}

	}

	@Test
	public void testUploadProteinFileThrowsMissingProteinFileException()
			throws InstanceNotFoundException, InvalidFileExtensionException, UnauthorizedProjectAccessException,
			UnhandledAPIException, MissingProteinFileException, IOException {
		String dataname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");
		apiDirectoryDao.save(new ApiDirectory("test", user));
		Long id = projectService.addProject(user.getId(), dataname, description, accesibility);

		DiskFileItem proteinFileItem = createFileItem(EMPTY_PROTEIN_TEST.getFile());
		assertThrows(MissingProteinFileException.class,
				() -> relaxEDApiService.uploadProtein(user.getId(), id, proteinFileItem));
	}

	@Test
	public void testUploadProteinFileThrowsInvalidFileExtensionException()
			throws InstanceNotFoundException, InvalidFileExtensionException, UnauthorizedProjectAccessException,
			UnhandledAPIException, MissingProteinFileException, IOException {
		String dataname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");
		apiDirectoryDao.save(new ApiDirectory("test", user));
		Long id = projectService.addProject(user.getId(), dataname, description, accesibility);

		DiskFileItem proteinFileItem = createFileItem(WRONG_EXTENSION_PROTEIN_TEST.getFile());
		assertThrows(InvalidFileExtensionException.class,
				() -> relaxEDApiService.uploadProtein(user.getId(), id, proteinFileItem));
	}

	@Test
	public void testCreateRefinement()
			throws InstanceNotFoundException, InvalidFileExtensionException, UnauthorizedProjectAccessException,
			UnhandledAPIException, MissingProteinFileException, IOException, DirectoryNotExistingException {
		String dataname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");
		ApiDirectory apiDir = apiDirectoryDao.save(new ApiDirectory("test", user));

		ExecuteRefinementParamsDto params = new ExecuteRefinementParamsDto();
		DeConfigDto de = new DeConfigDto();
		RelaxConfigDto relax = new RelaxConfigDto();
		ExecConfigDto exec = new ExecConfigDto();
		de.setCr(0.95F);
		de.setF(0.05F);
		de.setGens(100);
		de.setLogStats("True");
		de.setPopsize(100);
		de.setScheme("BEST");
		de.setTag("tag");
		relax.setAaProportionRepmin(1F);
		relax.setCheckExecutionImprovement("True");
		relax.setExecuteEach(1);
		relax.setExecutionsPerGen(4);
		relax.setMinFaRepScale("");
		relax.setRepackFaRepScale("");
		relax.setPopulExecutorChain("");
		relax.setTrialExecutorChain("");
		relax.setPopToRepackmin(100F);
		relax.setSortWorstEterms("False");
		exec.setTimeLimit("");
		params.setConfigName(null);
		params.setDisturbDegrees(0F);
		params.setDe(de);
		params.setRelax(relax);
		params.setExec(exec);
		Long id = projectService.addProject(user.getId(), dataname, description, accesibility);
		Project project = projectService.getProjectDetail(user.getId(), id);
		project.setApiDirectory(apiDir);
		try {
			relaxEDApiService.executeRefinement(user.getId(), id, params);
		} catch (UnhandledAPIException e) {

		}

	}
	

	@Test
	public void testCreateRefinement_ThrowsUnauthorizedProjectAccessException()
			throws InstanceNotFoundException, InvalidFileExtensionException, UnauthorizedProjectAccessException,
			UnhandledAPIException, MissingProteinFileException, IOException, DirectoryNotExistingException {
		String dataname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");
		User user2 = signUpUser("user2");
		ApiDirectory apiDir = apiDirectoryDao.save(new ApiDirectory("test", user));

		ExecuteRefinementParamsDto params = new ExecuteRefinementParamsDto();
		Long id = projectService.addProject(user.getId(), dataname, description, accesibility);
		Project project = projectService.getProjectDetail(user.getId(), id);
		project.setApiDirectory(apiDir);
		assertThrows(UnauthorizedProjectAccessException.class, () -> relaxEDApiService.executeRefinement(user2.getId(), id, params));

	}
	@Test
	public void testCreateRefinement_ThrowsInstanceNotFoundException()
			throws InstanceNotFoundException, InvalidFileExtensionException, UnauthorizedProjectAccessException,
			UnhandledAPIException, MissingProteinFileException, IOException, DirectoryNotExistingException {
		User user = signUpUser("user1");
		ExecuteRefinementParamsDto params = new ExecuteRefinementParamsDto();
		assertThrows(InstanceNotFoundException.class, () -> relaxEDApiService.executeRefinement(user.getId(), NON_EXISTENT_ID, params));

	}

	@Test
	public void testCreateRefinement_ThrowsDirectoryNotExistingException()
			throws InstanceNotFoundException, InvalidFileExtensionException, UnauthorizedProjectAccessException,
			UnhandledAPIException, MissingProteinFileException, IOException, DirectoryNotExistingException {
		String dataname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");

		ExecuteRefinementParamsDto params = new ExecuteRefinementParamsDto();
		Long id = projectService.addProject(user.getId(), dataname, description, accesibility);
		assertThrows(DirectoryNotExistingException.class, () -> relaxEDApiService.executeRefinement(user.getId(), id, params));

	}
	
	@Test
	public void testRefinementCallDao() throws InstanceNotFoundException, UnauthorizedProjectAccessException {
		User user = signUpUser("user");
		Long id = projectService.addProject(user.getId(), "name", "desc", Accesibility.PRIVATE);
		Project project = projectService.getProjectDetail(user.getId(), id);
		RefinementCall refinementCall = refinementCallDao.save(new RefinementCall("taskId", "exec", "config", 0F, "BEST", 100, 1F, 1F, 100, "tag", "True", "", "", 1, 4, 100F, "", "", "True", 1F, "False", "", project, user));
		RefinementCall call = refinementCallDao.findById(refinementCall.getId()).get();
		assertEquals(refinementCall, call);
		call.setTaskId(call.getTaskId());
		call.setConfigName(call.getConfigName());
		call.setExecutionName(call.getExecutionName());
		call.setDisturbDegrees(call.getDisturbDegrees());
		call.setScheme(call.getScheme());
		call.setPopsize(call.getPopsize());
		call.setF(call.getF());
		call.setCr(call.getCr());
		call.setGens(call.getGens());
		call.setTag(call.getTag());
		call.setLogStats(call.getLogStats());
		call.setPopulExecutorChain(call.getPopulExecutorChain());
		call.setTrialExecutorChain(call.getTrialExecutorChain());
		call.setExecuteEach(call.getExecuteEach());
		call.setExecutionsPerGen(call.getExecutionsPerGen());
		call.setPopToRepackmin(call.getPopToRepackmin());
		call.setRepackFaRepScale(call.getRepackFaRepScale());
		call.setMinFaRepScale(call.getMinFaRepScale());
		call.setCheckExecutionImprovement(call.getCheckExecutionImprovement());
		call.setAaProportionRepmin(call.getAaProportionRepmin());
		call.setSortWorstEterms(call.getSortWorstEterms());
		call.setTimeLimit(call.getTimeLimit());
		call.setDate(call.getDate());
		call.setProject(call.getProject());
		call.setCaller(call.getCaller());
		call.setTaskState(TaskState.SUCCESS);
	}
	
	@Test
	public void testGetUserPendingTasks() throws InstanceNotFoundException, UnhandledAPIException, UnauthorizedProjectAccessException {
		User user = signUpUser("user");
		ApiDirectory apiDir = apiDirectoryDao.save(new ApiDirectory("test", user));
		Long id = projectService.addProject(user.getId(), "name", "desc", Accesibility.PRIVATE);
		Project project = projectService.getProjectDetail(user.getId(), id);
		project.setApiDirectory(apiDir);
		refinementCallDao.save(new RefinementCall("taskId", "exec", "config", 0F, "BEST", 100, 1F, 1F, 100, "tag", "True", "", "", 1, 4, 100F, "", "", "True", 1F, "False", "", project, user));
		List<TaskProgressDto> tasks = relaxEDApiService.getUserPendingTasks(user.getId());
		assertNotEquals(tasks.get(0).getState(), TaskState.SUCCESS);
	}
	
	@Test
	public void testGetProjectPendingTasks() throws InstanceNotFoundException, UnhandledAPIException, UnauthorizedProjectAccessException {
		User user = signUpUser("user");
		ApiDirectory apiDir = apiDirectoryDao.save(new ApiDirectory("test", user));
		Long id = projectService.addProject(user.getId(), "name", "desc", Accesibility.PRIVATE);
		Project project = projectService.getProjectDetail(user.getId(), id);
		project.setApiDirectory(apiDir);
		refinementCallDao.save(new RefinementCall("taskId", "exec", "config", 0F, "BEST", 100, 1F, 1F, 100, "tag", "True", "", "", 1, 4, 100F, "", "", "True", 1F, "False", "", project, user));
		List<TaskProgressDto> tasks = relaxEDApiService.getProjectPendingTasks(user.getId(), project.getId());
		assertNotEquals(tasks.get(0).getState(), TaskState.SUCCESS);
	}
	
	@Test
	public void testGetRefinementResult() throws InstanceNotFoundException, UnauthorizedProjectAccessException, DirectoryNotExistingException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user");
		ApiDirectory apiDir = apiDirectoryDao.save(new ApiDirectory("test", user));

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);
		Project project = projectService.getProjectDetail(user.getId(), id);
		project.setApiDirectory(apiDir);
		RefinementCall refinementCall = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", project, user);
		refinementCall.setTaskState(TaskState.SUCCESS);
		refinementCallDao.save(refinementCall);
		
		try {
			relaxEDApiService.getRefinement(user.getId(), refinementCall.getId());
		} catch (UnhandledAPIException e) {

		}
	}
	
	@Test
	public void testGetRefinementResultThrowsInstanceNotFoundException() throws InstanceNotFoundException, UnauthorizedProjectAccessException, DirectoryNotExistingException {

		User user = signUpUser("user");
		assertThrows(InstanceNotFoundException.class, () -> relaxEDApiService.getRefinement(user.getId(), NON_EXISTENT_ID));
	}
	
	@Test
	public void testGetRefinementResultThrowsUnauthorizedProjectAccessException() throws InstanceNotFoundException, UnauthorizedProjectAccessException, DirectoryNotExistingException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user");
		User user2 = signUpUser("user2");
		ApiDirectory apiDir = apiDirectoryDao.save(new ApiDirectory("test", user));

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);
		Project project = projectService.getProjectDetail(user.getId(), id);
		project.setApiDirectory(apiDir);
		RefinementCall refinementCall = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", project, user);
		refinementCall.setTaskState(TaskState.SUCCESS);
		refinementCallDao.save(refinementCall);
		
		assertThrows(UnauthorizedProjectAccessException.class, () -> relaxEDApiService.getRefinement(user2.getId(), refinementCall.getId()));
	}
	
	@Test
	public void testDownloadRefinementResult() throws InstanceNotFoundException, UnauthorizedProjectAccessException, DirectoryNotExistingException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user");
		ApiDirectory apiDir = apiDirectoryDao.save(new ApiDirectory("test", user));

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);
		Project project = projectService.getProjectDetail(user.getId(), id);
		project.setApiDirectory(apiDir);
		RefinementCall refinementCall = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", project, user);
		refinementCall.setTaskState(TaskState.SUCCESS);
		refinementCallDao.save(refinementCall);
		
		try {
			relaxEDApiService.downloadResults(user.getId(), refinementCall.getId());
		} catch (UnhandledAPIException e) {

		}
	}
	
	@Test
	public void testDownloadRefinementResultThrowsInstanceNotFoundException() throws InstanceNotFoundException, UnauthorizedProjectAccessException, DirectoryNotExistingException {

		User user = signUpUser("user");
		assertThrows(InstanceNotFoundException.class, () -> relaxEDApiService.downloadResults(user.getId(), NON_EXISTENT_ID));
	}
	
	@Test
	public void testDownloadRefinementResultThrowsUnauthorizedProjectAccessException() throws InstanceNotFoundException, UnauthorizedProjectAccessException, DirectoryNotExistingException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user");
		User user2 = signUpUser("user2");
		ApiDirectory apiDir = apiDirectoryDao.save(new ApiDirectory("test", user));

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);
		Project project = projectService.getProjectDetail(user.getId(), id);
		project.setApiDirectory(apiDir);
		RefinementCall refinementCall = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", project, user);
		refinementCall.setTaskState(TaskState.SUCCESS);
		refinementCallDao.save(refinementCall);
		
		assertThrows(UnauthorizedProjectAccessException.class, () -> relaxEDApiService.downloadResults(user2.getId(), refinementCall.getId()));
	}
}