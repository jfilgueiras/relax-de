package es.udc.relaxed.backend.test.model.services;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.relaxed.backend.model.entities.Project;
import es.udc.relaxed.backend.model.entities.ProjectDao;
import es.udc.relaxed.backend.model.entities.RefinementCall;
import es.udc.relaxed.backend.model.entities.RefinementCallDao;
import es.udc.relaxed.backend.model.entities.User;
import es.udc.relaxed.backend.model.enums.Accesibility;
import es.udc.relaxed.backend.model.enums.TaskState;
import es.udc.relaxed.backend.model.exceptions.DuplicateInstanceException;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.services.Block;
import es.udc.relaxed.backend.model.services.ProjectService;
import es.udc.relaxed.backend.model.services.RefinementCallService;
import es.udc.relaxed.backend.model.services.UserService;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class TestRefinementCallService {

	@Autowired
	private RefinementCallDao refinementCallDao;
	@Autowired
	private ProjectService projectService;
	@Autowired
	private RefinementCallService refinementCallService;
	@Autowired
	private ProjectDao projectDao;
	@Autowired
	private UserService userService;

	private User signUpUser(String userName) {

		User user = new User(userName, "password", "firstName", "lastName", userName + "@" + userName + ".com");

		try {
			userService.signUp(user);
		} catch (DuplicateInstanceException e) {
			throw new RuntimeException(e);
		}

		return user;

	}

	@Test
	public void testGetUserRefinementCalls() throws InstanceNotFoundException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);

		Project resultProject = projectDao.findById(id).get();

		RefinementCall refinementCall1 = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", resultProject, user);
		refinementCall1 = refinementCallDao.save(refinementCall1);
		refinementCall1.setDate(LocalDateTime.now().minusHours(1).truncatedTo(ChronoUnit.MILLIS));
		refinementCall1.setTaskState(TaskState.SUCCESS);
		refinementCall1 = refinementCallDao.save(refinementCall1);

		RefinementCall refinementCall2 = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", resultProject, user);
		refinementCall2.setTaskState(TaskState.SUCCESS);
		refinementCall2 = refinementCallDao.save(refinementCall2);
		
		Block<RefinementCall> blockExpectedFirstPage = new Block<>(
				Arrays.asList(refinementCall2, refinementCall1), false);

		assertEquals(blockExpectedFirstPage,
				refinementCallService.getUserRefinementCalls(user.getId(), resultProject.getId(), 0, 2));
	}
	
	@Test
	public void testGetUserRefinementCallsPagination() throws InstanceNotFoundException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);

		Project resultProject = projectDao.findById(id).get();

		RefinementCall refinementCall1 = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", resultProject, user);
		refinementCall1.setTaskState(TaskState.SUCCESS);
		refinementCall1 = refinementCallDao.save(refinementCall1);
		refinementCall1.setDate(LocalDateTime.now().minusHours(2).truncatedTo(ChronoUnit.MILLIS));
		refinementCall1 = refinementCallDao.save(refinementCall1);

		RefinementCall refinementCall2 = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", resultProject, user);
		refinementCall2.setTaskState(TaskState.SUCCESS);
		refinementCall2 = refinementCallDao.save(refinementCall2);
		refinementCall2.setDate(LocalDateTime.now().minusHours(1).truncatedTo(ChronoUnit.MILLIS));
		refinementCall2 = refinementCallDao.save(refinementCall2);

		RefinementCall refinementCall3 = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", resultProject, user);
		refinementCall3.setTaskState(TaskState.SUCCESS);
		refinementCall3 = refinementCallDao.save(refinementCall3);

		Block<RefinementCall> blockExpectedFirstPage = new Block<>(
				Arrays.asList(refinementCall3, refinementCall2), true);
		Block<RefinementCall> blockExpectedSecondPage = new Block<>(Arrays.asList(refinementCall1), false);

		assertEquals(blockExpectedFirstPage,
				refinementCallService.getUserRefinementCalls(user.getId(), resultProject.getId(), 0, 2));
		assertEquals(blockExpectedSecondPage,
				refinementCallService.getUserRefinementCalls(user.getId(), resultProject.getId(), 1, 2));
	}

	@Test
	public void testGetUserRefinementCallsEmptyBlock() throws InstanceNotFoundException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user1");

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);

		Project resultProject = projectDao.findById(id).get();

		assertEquals(new Block<RefinementCall>(Collections.emptyList(), false),
				refinementCallService.getUserRefinementCalls(user.getId(), resultProject.getId(), 0, 1));
	}
	
	@Test
	public void testGetUserConfigNames() throws InstanceNotFoundException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user");

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);
		Project resultProject = projectDao.findById(id).get();

		RefinementCall refinementCall = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", resultProject, user);

		refinementCallDao.save(refinementCall);
		
		assertEquals(List.of("config"),
				refinementCallService.getUserConfigNames(user.getId()));
	}
	
	@Test
	public void testGetUserConfigNamesEmptyList() throws InstanceNotFoundException {

		User user = signUpUser("user");

		assertEquals(Collections.emptyList(),
				refinementCallService.getUserConfigNames(user.getId()));
	}
	
	@Test
	public void testGetRefinementByConfigName() throws InstanceNotFoundException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user");

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);
		Project resultProject = projectDao.findById(id).get();

		RefinementCall refinementCall = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", resultProject, user);

		refinementCallDao.save(refinementCall);
		
		assertEquals(refinementCall,
				refinementCallService.getRefinementByConfigName(user.getId(), "config"));
	}
	
	@Test
	public void testGetRefinementByConfigNameLastRefinement() throws InstanceNotFoundException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PRIVATE;
		User user = signUpUser("user");

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);
		Project resultProject = projectDao.findById(id).get();
		
		Long id2 = projectService.addProject(user.getId(), projectname, description, accesibility);
		Project resultProject2 = projectDao.findById(id2).get();
		
		RefinementCall refinementCall = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", resultProject, user);
		refinementCall.setDate(refinementCall.getDate().minus(2, ChronoUnit.HOURS));
		refinementCallDao.save(refinementCall);

		RefinementCall refinementCall2 = new RefinementCall("taskId2", "exec2", "config", 0.1F, "RANDOM", 10, 1F,
				0.8F, 10, "new_tag", "False", "", "", 1, 2, 1F, "", "", "True", 1F, "False", "", resultProject2, user);
		refinementCallDao.save(refinementCall2);

		assertEquals(refinementCall2,
				refinementCallService.getRefinementByConfigName(user.getId(), "config"));
	}
	
	@Test
	public void testGetRefinementByConfigNameThrowsInstanceNotFoundException() throws InstanceNotFoundException {

		String projectname = "test";
		String description = "des";
		Accesibility accesibility = Accesibility.PUBLIC;
		User user = signUpUser("user");
		User user1 = signUpUser("user1");

		Long id = projectService.addProject(user.getId(), projectname, description, accesibility);
		Project resultProject = projectDao.findById(id).get();

		RefinementCall refinementCall = new RefinementCall("taskId", "exec", "config", 0.5F, "BEST", 100, 0.5F,
				0.3F, 100, "tag", "True", "", "", 1, 4, 1F, "", "", "True", 1F, "False", "", resultProject, user1);

		refinementCallDao.save(refinementCall);
		
		assertThrows(InstanceNotFoundException.class, () ->
				refinementCallService.getRefinementByConfigName(user.getId(), "config"));
	}
}