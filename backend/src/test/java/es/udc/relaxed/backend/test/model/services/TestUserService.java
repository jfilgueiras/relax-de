package es.udc.relaxed.backend.test.model.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.relaxed.backend.model.entities.User;
import es.udc.relaxed.backend.model.exceptions.DuplicateInstanceException;
import es.udc.relaxed.backend.model.exceptions.IncorrectLoginException;
import es.udc.relaxed.backend.model.exceptions.IncorrectPasswordException;
import es.udc.relaxed.backend.model.exceptions.InstanceNotFoundException;
import es.udc.relaxed.backend.model.services.Block;
import es.udc.relaxed.backend.model.services.PermissionChecker;
import es.udc.relaxed.backend.model.services.UserService;
import es.udc.relaxed.backend.rest.common.JwtGenerator;
import es.udc.relaxed.backend.rest.common.JwtInfo;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class TestUserService {
	
	private final Long NON_EXISTENT_ID = -1L;

	@Autowired
	private JwtGenerator jwt;

	@Autowired
	private UserService userService;
	
	@Autowired
	private PermissionChecker permissionChecker;
	
	private User createUser(String userName) {
		return new User(userName, "password", "firstName", "lastName", userName + "@" + userName + ".com");
	}
	
	@Test
	public void testSignUpAndLoginFromId() throws DuplicateInstanceException, InstanceNotFoundException {
		
		User user = createUser("user");
		
		userService.signUp(user);
		
		User loggedInUser = userService.loginFromId(user.getId());
		
		assertEquals(user, loggedInUser);
		assertEquals(User.RoleType.USER, user.getRole());
		
	}
	
	@Test
	public void testSignUpDuplicatedUserName() throws DuplicateInstanceException {
		
		User user = createUser("user");
		
		userService.signUp(user);
		assertThrows(DuplicateInstanceException.class, () -> userService.signUp(user));
		
	}
	
	@Test
	public void testLoginFromNonExistentId() {
		assertThrows(InstanceNotFoundException.class, () -> userService.loginFromId(NON_EXISTENT_ID));
	}
	
	@Test
	public void testLogin() throws DuplicateInstanceException, IncorrectLoginException {
		
		User user = createUser("user");
		String clearPassword = user.getPassword();
				
		userService.signUp(user);
		
		User loggedInUser = userService.login(user.getUserName(), clearPassword);
		
		assertEquals(user, loggedInUser);
		
	}
	
	@Test
	public void testLoginWithIncorrectPassword() throws DuplicateInstanceException {
		
		User user = createUser("user");
		String clearPassword = user.getPassword();
		
		userService.signUp(user);
		assertThrows(IncorrectLoginException.class, () ->
			userService.login(user.getUserName(), 'X' + clearPassword));
		
	}
	
	@Test
	public void testLoginWithNonExistentUserName() {
		assertThrows(IncorrectLoginException.class, () -> userService.login("X", "Y"));
	}
	
	@Test
	public void testUpdateProfile() throws InstanceNotFoundException, DuplicateInstanceException {
		
		User user = createUser("user");
		
		userService.signUp(user);
		
		user.setFirstName('X' + user.getFirstName());
		user.setLastName('X' + user.getLastName());
		user.setEmail('X' + user.getEmail());
		
		userService.updateProfile(user.getId(), 'X' + user.getFirstName(), 'X' + user.getLastName(),
			'X' + user.getEmail());
		
		User updatedUser = userService.loginFromId(user.getId());
		
		assertEquals(user, updatedUser);
		
	}
	
	@Test
	public void testUpdateProfileWithNonExistentId() {
		assertThrows(InstanceNotFoundException.class, () ->
			userService.updateProfile(NON_EXISTENT_ID, "X", "X", "X"));
	}
	
	@Test
	public void testChangePassword() throws DuplicateInstanceException, InstanceNotFoundException,
		IncorrectPasswordException, IncorrectLoginException {
		
		User user = createUser("user");
		String oldPassword = user.getPassword();
		String newPassword = 'X' + oldPassword;
		
		userService.signUp(user);
		userService.changePassword(user.getId(), oldPassword, newPassword);
		userService.login(user.getUserName(), newPassword);
		
	}
	
	@Test
	public void testChangePasswordWithNonExistentId() {
		assertThrows(InstanceNotFoundException.class, () ->
			userService.changePassword(NON_EXISTENT_ID, "X", "Y"));
	}
	
	@Test
	public void testChangePasswordWithIncorrectPassword() throws DuplicateInstanceException {
		
		User user = createUser("user");
		String oldPassword = user.getPassword();
		String newPassword = 'X' + oldPassword;
		
		userService.signUp(user);
		assertThrows(IncorrectPasswordException.class, () ->
			userService.changePassword(user.getId(), 'Y' + oldPassword, newPassword));
		
	}

	@Test
	public void testPermissionChecker() throws DuplicateInstanceException, InstanceNotFoundException, IncorrectLoginException {
	
		User user = createUser("user");
		String clearPassword = user.getPassword();
		
		Assertions.assertThrows(InstanceNotFoundException.class, () -> {
			permissionChecker.checkUserExists(1L);
		});
		userService.signUp(user);
		User user1 = userService.login(user.getUserName(), clearPassword);
		Assertions.assertDoesNotThrow(() -> {
			permissionChecker.checkUserExists(user1.getId());
		});
	}
	
	@Test
	public void testBlock() {
		
		User user1 = new User();
		User user2 = new User();
		User user3 = new User();
		
		user1.setUserName("user1");
		user2.setUserName("user2");
		user3.setUserName("user3");
		
		Block<User> blockExpectedUser1 = new Block<>(Arrays.asList(user1, user2, user3), false);
		Block<User> blockExpectedUser2 = new Block<>(Arrays.asList(user2, user3), false);
		Block<User> blockExpectedUser3 = new Block<>(Arrays.asList(user2, user3), true);
		Block<User> blockExpectedUser4 = new Block<>(new ArrayList<User>(), true);
		Block<User> blockExpectedUser5 = new Block<>(null, true);
		Block<User> blockExpectedUser6 = new Block<>(null, true);
		
		assertEquals(blockExpectedUser1, new Block<>(Arrays.asList(user1, user2, user3), false));
		assertNotEquals(blockExpectedUser1.hashCode(), new Block<>(Arrays.asList(user1, user2, user3), true).hashCode());		
		assertNotEquals(blockExpectedUser2, blockExpectedUser1);
		assertNotEquals(blockExpectedUser2, blockExpectedUser3);
		assertEquals(blockExpectedUser1, blockExpectedUser1);
		assertEquals(blockExpectedUser4.getItems(), new ArrayList<User>());
		assertFalse(blockExpectedUser5.equals(blockExpectedUser4));
		assertNotEquals(blockExpectedUser5.hashCode(), blockExpectedUser4.hashCode());
		assertTrue(blockExpectedUser6.equals(blockExpectedUser5));
		assertNotEquals(blockExpectedUser1, null);
		assertNotEquals(blockExpectedUser1, user1);
		assertTrue(blockExpectedUser3.getExistMoreItems());
	}

	@Test
	public void testJwtGenerator() throws Exception {

		JwtInfo filter = new JwtInfo(1L, "un", "USER");
		filter.setUserName(filter.getUserName());
		filter.setUserId(filter.getUserId());
		filter.setRole(filter.getRole());
		String token = jwt.generate(filter);
		jwt.getInfo(token);
	}

}
